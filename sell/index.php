<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Продажа авто");
?>

<h1 class="main__title">
    <?$APPLICATION->showTitle(false)?>
</h1>
<?$APPLICATION->IncludeComponent(
    "sniper:tag.filter",
    ".default",
    array('SECTION_CODE' => $_REQUEST['ELEMENT_CODE'])
);?>

<?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "sell",
    Array(
        "IBLOCK_ID" => "5",
        "IBLOCK_TYPE" => "ads",
        "NEWS_COUNT" => "20",
        "SORT_BY1" => "SORT",
        "SORT_ORDER1" => "ASC",
        "FIELD_CODE" => array(
            0 => "NAME",
            1 => "PREVIEW_PICTURE",
            2 => "DETAIL_TEXT",
            3 => "DETAIL_PICTURE",
            4 => "DATE_ACTIVE_FROM",
            5 => "ACTIVE_FROM",
            6 => "DATE_ACTIVE_TO",
            7 => "ACTIVE_TO",
            8 => "SHOW_COUNTER",
            9 => "SHOW_COUNTER_START",
            10 => "DATE_CREATE",
            11 => "",
        ),
        "PROPERTY_CODE" => array(
            0 => "IS_HYBRID",
            1 => "MODEL_YEAR",
            2 => "CITY",
            3 => "HAS_RF_MILEAGE",
            4 => "IS_FOREIGN",
            5 => "CONTACT_NAME",
            6 => "MANUFACTURER",
            7 => "MODEL",
            8 => "POWER",
            9 => "IS_NEW",
            10 => "ENGINE",
            11 => "DRIVE",
            12 => "MILEAGE",
            13 => "STEERING_WHEEL",
            14 => "PRICE",
            15 => "PHONE",
            16 => "TRANSMISSION",
            17 => "CAR_BODY",
            18 => "FUEL",
            19 => "COLOR",
            20 => "YOUTUBE",
            21 => "STATE",
        ),
        "SET_TITLE" => "N",
        "SET_LAST_MODIFIED" =>"Y",
        "MESSAGE_404" => "",
        "SET_STATUS_404" => "Y",
        "SHOW_404" => "Y",
        "FILE_404" => "",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "DETAIL_DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TEMPLATE" => "main",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "DISPLAY_DATE" => "N",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "N",
        "DISPLAY_PREVIEW_TEXT" => "N",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "j F Y",
        "USE_PERMISSIONS" => "N",
        "FILTER_NAME" => "arrSellFilter",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "CHECK_DATES" => "Y",
    )
);?>

<?$APPLICATION->ShowViewContent('tag-text')?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>