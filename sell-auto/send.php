<?    define("NO_KEEP_STATISTIC", true);
    define("NO_AGENT_CHECK", true);
    define('PUBLIC_AJAX_MODE', true);
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
    $_SESSION["SESS_SHOW_INCLUDE_TIME_EXEC"]="N";
    $APPLICATION->ShowIncludeStat = false;
 
    
    if(isset($_REQUEST["data"])){
        
        $REQUEST = array();
        $data = $_REQUEST["data"];
        $arCalc = array("calc-1" => "Калькулятор выкупа","calc-2" => "Калькулятор обмена","calc-3" => "Комиссионный калькулятор");
        
        foreach($data as $field){
            
            if(!empty($field["name"]) && strstr($field["name"], 'zayavka_')){
                
                $REQUEST[$field["name"]] = htmlspecialchars($field["value"],ENT_QUOTES);
                
            }
            
        }
        
        $type = !empty($_REQUEST["type"])?htmlspecialchars($_REQUEST["type"],ENT_QUOTES):"calc-1";
        
        //$REQUEST[$field["name"]]
 
		CModule::IncludeModule('iblock');
		CModule::IncludeModule("form");
    
    
        $rsFieldList = CFormField::GetList(5, "ALL", $by="s_sort", $order="asc", array(), $is_filtered);
        $arFieldList = array();
        $arEventFields = array();
        $arFields = array();
        $arErrors = array();
       
        while ($arField = $rsFieldList->GetNext())
        {
            
            //$arFieldList[] = $arField;
            
            if(!empty($REQUEST[$arField["SID"]])){
                $val = htmlspecialchars($REQUEST[$arField["SID"]], ENT_QUOTES);
                $arFieldList[$arField["SID"]] = $val;
                $arEventFields["form_".$arField["TITLE_TYPE"]."_".$arField["ID"]] = $val;
        
            }else{
                $arErrors[$arField["SID"]] = $arField["SID"];
                
            }
            
        }
        
        $arFieldList["zayavka_type"] = $arCalc[$type];
        $arEventFields["form_text_35"] = $arCalc[$type];
        
        CEvent::Send("SIMPLE_FORM_4", 's1', $arFieldList,"N",52);

        if ($RESULT_ID = CFormResult::Add(5, $arEventFields))
        {
           $arResult["send"] = "Y";
        }else{
           $arResult["send"] = "N";
        }
                    
    $arResult = array("req"=>$_REQUEST,"_req"=>$REQUEST,"arFieldList"=>$arFieldList,"arEventFields"=>$arEventFields,"status"=>true);
    
    
    }else{
        
        $arResult = array("status"=>false);
        
    }
    
    
    
    
    echo json_encode($arResult);
?>