<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

//include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/scripts/HLClass.php');

$markaId = trim($_REQUEST["marka_id"]);
$markaId = htmlspecialchars($markaId,ENT_QUOTES);
$hLBlockMark = new HLBlock('CarMark');

//$arRes = $hLBlock->getFields(array('UF_CAR_MARK' => $markaId), array('UF_NAME' => 'ASC'));
$arResMark = $hLBlockMark->getFields(array('ID'=>$markaId), array('UF_NAME' => 'ASC'));

if(count($arResMark)>0){
$arMark = array_shift($arResMark);	


if(!empty($arMark['UF_XML_ID'])){
$hLBlock = new HLBlock('CarModel');
$arRes = $hLBlock->getFields(array('UF_CAR_MARK' => $arMark['UF_XML_ID']), array('UF_NAME' => 'ASC'));

?><option value="">--</option><?
foreach($arRes as $res){

    if($res['UF_NAME'])
        $name = $res['UF_NAME'];
    elseif($res['UF_NAME_RUS'])
        $name = $res['UF_NAME_RUS'];
    else
        continue;?>
    
    <option class="auto-form__item-select-var" value="<?=$name?>">
        <?=$name?>
    </option> 
<?}}
}

