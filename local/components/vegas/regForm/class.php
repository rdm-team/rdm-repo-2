<? 
    use Bitrix\Main\Loader;
    use Bitrix\Main\Application; 
    use Bitrix\Main\Web\Uri;
     
    if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

    class regForm extends CBitrixComponent
    {
        
        public function executeComponent()
        {
           

       

			// $this->arResult["date"] = date("H:i:s");

			$this->includeComponentTemplate();
                    


        }    

        public function onPrepareComponentParams($arParams)
        {
            
            
            if(!isset($arParams["CACHE_TIME"]))
                $arParams["CACHE_TIME"] = 36000000;

            $arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
            if(strlen($arParams["IBLOCK_TYPE"])<=0)
                $arParams["IBLOCK_TYPE"] = "news";
            
            $arParams["IBLOCK_ID"] = trim($arParams["IBLOCK_ID"]);
            $arParams["SET_LAST_MODIFIED"] = $arParams["SET_LAST_MODIFIED"]==="Y";
            $arParams["AJAX_MODE"] = "N";
/*
            $arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
            if(strlen($arParams["SORT_BY1"])<=0)
                $arParams["SORT_BY1"] = "ACTIVE_FROM";
            if(!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["SORT_ORDER1"]))
                $arParams["SORT_ORDER1"]="DESC";

                $arrFilter = array();

            $arParams["CHECK_DATES"] = $arParams["CHECK_DATES"]!="N";

            if(!is_array($arParams["FIELD_CODE"]))
                $arParams["FIELD_CODE"] = array();
            foreach($arParams["FIELD_CODE"] as $key=>$val)
                if(!$val)
                    unset($arParams["FIELD_CODE"][$key]);

            if(!is_array($arParams["PROPERTY_CODE"]))
                $arParams["PROPERTY_CODE"] = array();
            foreach($arParams["PROPERTY_CODE"] as $key=>$val)
                if($val==="")
                    unset($arParams["PROPERTY_CODE"][$key]);

            $arParams["DETAIL_URL"]=trim($arParams["DETAIL_URL"]);

            $arParams["NEWS_COUNT"] = intval($arParams["NEWS_COUNT"]);
            if($arParams["NEWS_COUNT"]<=0)
                $arParams["NEWS_COUNT"] = 20;

            $arParams["DISPLAY_BOTTOM_PAGER"] = $arParams["DISPLAY_BOTTOM_PAGER"]!="N";
            $arParams["PAGER_TITLE"] = trim($arParams["PAGER_TITLE"]);
            $arParams["PAGER_SHOW_ALWAYS"] = $arParams["PAGER_SHOW_ALWAYS"]=="Y";
            $arParams["PAGER_TEMPLATE"] = trim($arParams["PAGER_TEMPLATE"]);
            $arParams["PAGER_DESC_NUMBERING"] = $arParams["PAGER_DESC_NUMBERING"]=="Y";
            $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] = intval($arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]);
            $arParams["PAGER_SHOW_ALL"] = $arParams["PAGER_SHOW_ALL"]=="Y";
            $arParams["CHECK_PERMISSIONS"] = $arParams["CHECK_PERMISSIONS"]!="N";
*/
            return $arParams;
        }
        
    }
?>