<?include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/scripts/HLClass.php');
    use Bitrix\Main\Loader;
    use Bitrix\Main\Application; 
    use Bitrix\Main\Web\Uri;
     
    if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

    class calcForm extends CBitrixComponent
    {
        
        public function executeComponent()
        {

            $hLBlock = new HLBlock('CarMark');
            $arResultMark = $hLBlock->getFields(array(), array('UF_NAME' => 'ASC'));   


            $this->arResult["MARK"] = $arResultMark;

			$arSelect = Array(
			"ID", 
			"NAME",
			"CODE", 
			"DATE_ACTIVE_FROM", 
			"DETAIL_PAGE_URL", 
			"PREVIEW_TEXT", 
			"PREVIEW_PICTURE", 
			"PROPERTY_MEDIA_SHOW",
			"PROPERTY_MEDIA_YOUTUBE",
			"PROPERTY_MEDIA_NAME",
			"PROPERTY_MEDIA_BLOCK",
			"PROPERTY_MEDIA_URL",
			);
			$arFilter = Array("IBLOCK_ID"=>24, "CODE"=>$this->arParams['LEAD_CODE'], "ACTIVE"=>"Y");
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			while($ob = $res->GetNext())
			{	
				//$item = $ob->GetFields();
				$this->arResult["lead"][] = $ob;
				
				$arMedia = false;
				$strText = false;
				$blockStyle='two';
				//$mediaStyle='media-half';
				if($ob['PROPERTY_MEDIA_SHOW_ENUM_ID']==114){
					//�������� �����
					
					if(!empty($ob['PROPERTY_MEDIA_YOUTUBE_VALUE'])){
						
						$arMedia = array(
						'value'=>$ob['PROPERTY_MEDIA_YOUTUBE_VALUE'],
						'type'=>'video',
						'thumb'=>'https://img.youtube.com/vi/'.$ob['PROPERTY_MEDIA_YOUTUBE_VALUE'].'/0.jpg'
						);
						
					}
					
				}elseif($ob['PROPERTY_MEDIA_SHOW_ENUM_ID']==115){
					//�������� ����
					if(!empty($ob['PREVIEW_PICTURE'])){
						$arMedia = array(
						'value'=>CFile::GetFileArray($ob['PREVIEW_PICTURE']),
						'type'=>'photo'
						);	
					}
				}else{
					//������ �� ����������
					
				$blockStyle='one';
			
				}
				
				if(empty($ob['~PREVIEW_TEXT'])){
				$textStyle='';
				$mediaStyle='';	
				}else{
					
					$strText = $ob['~PREVIEW_TEXT'];
				}
				
				$this->arResult["leads"][$ob['CODE']] = array(
				'TITLE'=>!empty($ob['~PROPERTY_MEDIA_NAME_VALUE']['TEXT'])?$ob['~PROPERTY_MEDIA_NAME_VALUE']['TEXT']:false,
				'TEXTBLOCK'=>!empty($ob['PROPERTY_MEDIA_BLOCK_VALUE'])?$ob['PROPERTY_MEDIA_BLOCK_VALUE']:false,				
				'NAME'=>$ob['NAME'],
				'CODE'=>$ob['CODE'],		
				'SHOW'=>$ob['PROPERTY_MEDIA_SHOW_ENUM_ID'],			
				'MEDIA' => $arMedia,
				'TEXT' => $strText,
				'STYLE' => $blockStyle,
				'URL' => 'http://'.$_SERVER['HTTP_HOST'].$ob['PROPERTY_MEDIA_URL_VALUE'],
				);
	
			}
			
            $this->includeComponentTemplate();

        }    

        public function onPrepareComponentParams($arParams)
        { 
            return $arParams;
        }
        
    }
?>