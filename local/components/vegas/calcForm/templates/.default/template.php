<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?CJSCore::Init(array("jquery"));?>
<?$this->addExternalJS($templateFolder."/jquery.validate.js");?>

<div class="modalForm modalForm__heightOne" id="calcForm">
    <div class="modalForm__header">
    <img class="modalForm__headerLogo" src="/local/assets/img/assets/header/logo.svg" alt="">    
    </div>
	<?foreach($arResult['leads'] as $arItem){?>
	<div class="tbox" id="lead-<?=$arItem['CODE']?>">
	<?if($arItem['TITLE']!=false){?><div class="tbox__title"><?=$arItem['TITLE']?></div><?}?>
		<?if($arItem['TEXT']!=false){?><div class="tbox-col-<?=$arItem['STYLE']?> tbox__text"><?=$arItem['TEXT']?></div><?}?>
		<?if($arItem['MEDIA']!=false){?>
		<div class="tbox-col-<?=$arItem['STYLE']?> tbox__media">
		<?if($arItem['MEDIA']['type'] == 'video'){?>
		<div class="tbox__video onLeadMedia" data-src="https://www.youtube.com/embed/<?=$arItem['MEDIA']['value']?>" data-media="video">
		<img class="tbox__thumb" src="<?=$arItem['MEDIA']['thumb']?>" alt="" />
		</div>
		<?}elseif($arItem['MEDIA']['type'] == 'photo'){?>
		<div class="tbox__img onLeadMedia" data-src="<?=$arItem['MEDIA']['value']['SRC']?>" data-media="img">
			<img class="tbox__thumb" src="<?=$arItem['MEDIA']['value']['SRC']?>" alt="" />
			</div>
		<?}?>
		</div>
		<?}?>
		<?if($arItem['TEXTBLOCK']!=false){?><span class="tbox__addtext"><?=$arItem['TEXTBLOCK']?></span><?}?>
	</div>
	<?}?>
    <!--span class="titleFormType" id="titleForm"></span-->
	<form class="zayavka-form" id="zayavka-form" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
		<div class="zayavka-input-row" id="marka_form">
			<div class="zayavka-input">
				Марка
				<div class="zayavka-box">
					<select id="marka" name="zayavka_mark" class="auto-form__item-select _js-select _js-mark-select _js-form-field">
						<option class="auto-form__item-select-var" value="">--</option>
						<?
						$hLBlock = new HLBlock('CarMark');
						$arRes = $hLBlock->getFields(array(), array('UF_NAME' => 'ASC'));   

						foreach($arRes as $res){
							
							if($res['UF_NAME'])
								$name = $res['UF_NAME'];
							elseif($res['UF_NAME_RUS'])
								$name = $res['UF_NAME_RUS'];
							else
								continue;?>
							
							<option class="auto-form__item-select-var" value="<?=$name?>" data-id="<?=$res['ID']?>">
								<?=$name?>
							</option> 
						<?}?>
					</select>
				</div>
			</div>
		</div>
		<div class="zayavka-input-row" id="model_form">
			<div class="zayavka-input">
				 Модель
				<div class="zayavka-box">			 
					 <select id="model"  name="zayavka_model" class="">
						<option value="">--</option>
					</select>
				</div>
			</div>
		</div>
		<div class="zayavka-input-row">
			<div class="zayavka-input" id="zayavka_year">
				Год
				<select id="god" name="zayavka_year">
					<?for($year=date('Y'); $year > 1989; $year--){?>
						<option value="><?=$year?>"><?=$year?></option>
					<?}?>
				</select>
			</div>
		</div>
		
		<div class="zayavka-input-row">
			<div class="zayavka-input">
				 Пробег (км) <input type="text" name="zayavka_probeg" class="zayavka-in-text" id="probeg">
			</div>
		</div>    

		<div class="zayavka-sub-title">Как сообщить Вам результат</div>
		
		<div class="zayavka-input-fullrow">

			 Имя <input type="text"  name="zayavka_name" class="zayavka-in-text" id="name">

		</div>
		
		<div class="zayavka-input-row">
			<div class="zayavka-input">
				 Телефон <input type="text"  name="zayavka_phone" class="zayavka-in-text" id="phone">
			</div>
		</div>
		<div class="zayavka-input-row">
			<div class="zayavka-input">
				 Эл. почта <input type="text"  name="zayavka_email" class="zayavka-in-text" id="email">
			</div>
		</div>				
		<div class="zayavka-input-fullrow">					
			 Доп. информация:
			<textarea class="zayavka-input-tarea"  name="zayavka_comment" id="info" style="height: 50px;"></textarea>
		</div>		
		<div class="zayavka-input-fullrow">					
			<input type="checkbox" class="in-text-data"  name="zayavka_edata"  id="edata" checked="checked">Подтверждаю согласие на обработку <a href="/personal-data/" target="_blank">персональных данных</a>
		</div>
		<div class="zayavka-input-fullrow zayavka-input-row-center">    
			<button id="go_mail">Узнать цену вашего авто</button>
		</div>
	</form>
</div>

<div class="winCallback">

    <div class="modalForm__header"><img class="modalForm__logo" src="/local/assets/img/assets/header/logo.svg" alt="" /></div>
    <div class="modalForm__body">
        <strong>Спасибо!</strong> 
    
   <span>Мы рассмотрим сообщение и обязательно свяжемся с вами.<br />
   Пожалуйста, дождитесь ответа.</span>
   
   </div>
</div>