var typeForm = "osago";
var edataChecked = false;

$(document).ready(function(){
	
    $("body").on("click",".eventCalc", function(){
				
        onEventOrder = $('#calcForm');
        dataForm = $(this).attr("data-form");
		target = $(this).data('target');
		var mtype = $(this).data('mtype');
		
		//console.log(target);
		
		$("#zayavka-form").trigger("reset");
		
		$('.zayavka-input select').each(function(e){
			$(this).selectOrDie("update");
			$('._model .sod_select').addClass('disabled');
		});
		
		$('.focus').removeClass('.focus');
		$('.zayavka-input .sod_label').text('--');
		$('.full-form__item-checkbox, .auto-form__item').each(function(e){
			$(this).find('input').prop('checked', '');
		});
		

			if(dataForm=="kasko"){
			
                $(".go_mail").text("Заказать расчет КАСКО");
            }else if(dataForm=="osago"){
			
				$(".go_mail").text("Заказать расчет ОСАГО");
				
			}else{
				$(".go_mail").text("Заказать услугу");
			}
			
			
			$('#lead-osago').css({display:'none'});
			$('#lead-kasko').css({display:'none'});
			$('#lead-'+dataForm).css({display:'inline-block'});
			
			$('#calcForm')
			.removeClass('box-1')
			.removeClass('box-2')
			.addClass(mtype);
			
		
			if(isApple()){$('body, html').scrollTop(0);}
			$.fancybox.open(onEventOrder,{touch:false});	
        return false;
    });

	 
/* ------------------------- */    

    $('#marka').change(function(){
         marka = $(this).val();
         $.ajax({
            type:"POST",
            url:"/local/ajax/_getModels.php",
            data:{
                marka_id: $(this).find('option:selected').data('id'),
            },
            success:function(data){
				

				
                $('#model').html(data);
                $('#model').selectOrDie("update");
                $('#model_form .sod_select').removeClass('disabled').find('.sod_list').css({'max-height': '165px'});
            }
        });
    })
$('.onData').change(function() {
 if ($(this).prop("checked")) {
        
		edataChecked = true;
		$(".go_mail").removeAttr("disabled");
		$(".go_mail").removeClass("disabled");
       
    }	else{
		edataChecked = false;
		$(".go_mail").attr("disabled","disabled");
		$(".go_mail").addClass("disabled");
	
	}
	
});

// Проверка формы
var v = $("#zayavka-form");

//Show modal form

//jQuery validate
	$.validator.setDefaults({});

	$("#zayavka-form").validate({
		rules:
			{
                zayavka_phone: {required: true},
			},
		messages:
			{
                zayavka_phone: { required: 'Заполните поле'}

			},
		submitHandler: function(form)
			{
          
				$.post("/sendinsurence/",{"data":$("#zayavka-form").serializeArray(),dataform:dataForm},function(data){

					eventTarget(target);
					
                    $.fancybox.close();
					$.fancybox.open( {
						src : ".winCallback-form",
						type: 'inline'
					} );
                								
				},"json");
                

			}
	});

	v.validate({
		focusCleanup: true,
		focusInvalid: false
	});

    
});