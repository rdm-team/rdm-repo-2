var typeForm = "obmen";
$(document).ready(function(){
    
   $('#carName').html(carName);
    
    $("body").on("click",".eventCalc", function(){
	
		var modal = $('#orderToBarter');
        typeForm = $(this).attr("data-type");
        dataForm = $(this).attr("data-form");
        target = $(this).data('target');
		var mtype = $(this).data('mtype');

		//console.log(target);
		
        $("#carName").text(carName);
		
		$('#orderToBarter')
		.removeClass('box-1')
		.removeClass('box-2')
		.addClass(mtype);
		
		$('body, html').scrollTop(0);
		$.fancybox.open([modal], {
				wrapCSS:'styleWrap',
				padding : 0,
				touch:false
			});

        return false;
    });
/* ------------------------- */    
    
    $('#marka').change(function(){
         marka = $(this).val();
         $.ajax({
            type:"POST",
            url:"/local/ajax/_getModels.php",
            data:{
                marka_id: $(this).find('option:selected').data('id'),
            },
            success:function(data){
				

					
                $('#model').html(data);
                $('#model').selectOrDie("update");
                $('#model_form .sod_select').removeClass('disabled').find('.sod_list').css({'max-height': '165px'});
            }
        });
    })

// Проверка формы
var v = $("#zayavka-form");

$('body').on('change','#zayavka-form .onData',function() {	
 if ($(this).prop("checked")) {
        
		edataChecked = true;
		$("#go_mail").removeAttr("disabled");
		$("#go_mail").removeClass("disabled");
       
    }	else{
		edataChecked = false;
		$("#go_mail").attr("disabled","disabled");
		$("#go_mail").addClass("disabled");

	}
	
});

//Show modal form

//jQuery validate
	$.validator.setDefaults({

	});

	$("#zayavka-form").validate({
		rules:
			{
                zayavka_phone: {required: true},
			},
		messages:
			{
                zayavka_phone: { required: 'Заполните поле'}

			},
		submitHandler: function(form)
			{
        
                
				$.post("/local/ajax/send.php",{"data":$("#zayavka-form").serializeArray(),"type":typeForm,href:window.location.href,'dataform':dataForm},function(data){
						
					eventTarget(target);
					eventTarget('S-OBMEN');
					
                    $.fancybox.close();
					$.fancybox.open( {
						src : ".winCallback",
						type: 'inline',
						touch:false
					} ); 
				
				},"json");
                

			}
	});

	v.validate({
		focusCleanup: true,
		focusInvalid: false
	});

    
});