<? 
    use Bitrix\Main\Loader;
    use Bitrix\Main\Application; 
    use Bitrix\Main\Web\Uri;
	
    CModule::IncludeModule('subscribe');
    CModule::IncludeModule("form"); 
	 
    if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

    class regForm extends CBitrixComponent
    {
        
        public function executeComponent()
        {
			define('KUZOV_IBLOCK_ID', 6);
// ######################################
					
	$this->arResult["form_id"] = md5('id_'.$this->arParams["FORM_ID"]);
	$rsFieldList = CFormField::GetList($this->arParams["FORM_ID"], "ALL", $by="s_sort", $order="asc", array(), $is_filtered);
	$this->arResult["fields"] = array();
	$field_id = array();
	while ($arFields = $rsFieldList->GetNext())
	{
		
		$rsAnswer = CFormAnswer::GetByID($arFields["ID"]);
		$arAnswer = $rsAnswer->Fetch();
		$arFields["ANSWER"] = $arAnswer;
		$this->arResult["fields"][] = $arFields;
	}

// ######################################
	$arParams = array("replace_space"=>"-","replace_other"=>"-");
	 
	$arSelect = Array("ID", "NAME","CODE", "DATE_ACTIVE_FROM");
	$arFilter = Array("IBLOCK_ID"=>KUZOV_IBLOCK_ID, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false,true, $arSelect);
	$this->arResult["kuzov"] = array();
	while($ob = $res->GetNextElement())
	{
		$arFields = $ob->GetFields();
		$this->arResult["kuzov"][] = $arFields;
	}

// ######################################
		$this->includeComponentTemplate();
                    


        }    

        public function onPrepareComponentParams($arParams)
        {            
            
            if(!isset($arParams["CACHE_TIME"]))
                $arParams["CACHE_TIME"] = 36000000;

            $arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
            if(strlen($arParams["IBLOCK_TYPE"])<=0)
                $arParams["IBLOCK_TYPE"] = "news";
            
            $arParams["IBLOCK_ID"] = trim($arParams["IBLOCK_ID"]);
            $arParams["SET_LAST_MODIFIED"] = $arParams["SET_LAST_MODIFIED"]==="Y";
            $arParams["AJAX_MODE"] = "N";

            return $arParams;
        }
        
    }
?>