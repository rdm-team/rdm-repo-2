<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?CJSCore::Init(array("jquery"));?>
<?$this->addExternalJS("/local/assets/js/jquery.mask.min.js");?>
<div class="subForm" id="block1" style="display:none">
	<div class="modalForm__header">
		<img class="modalForm__headerLogo" src="/local/assets/img/assets/header/logo.svg" alt="">
	</div>
	<form class="subForm-form" id="sub-form" method="post">

		<div class="subForm-group">

		<?if(count($arResult["fields"])>0){
			foreach($arResult["fields"] as $arFields){
				if(!strstr($arFields["ANSWER"]["FIELD_PARAM"],"hidden")){
			?>
		<div class="subForm-input-fullrow field <?if($arFields["REQUIRED"]=="Y"){?>required<?}?>" id="<?=$arFields["SID"]?>">
			
			  <input type="text" name="<?=$arFields["SID"]?>" <?=$arFields["ANSWER"]["FIELD_PARAM"]?> class="subForm-in-text" value="" />
			<?if($arFields["REQUIRED"]=="Y"){?>
				<div class="error-block">Заполните это поле</div>
			<?}?>
		</div>
			<?}}}?>
		</div>	
			<input type="hidden" name="data" value="<?=$arResult["form_id"]?>" />

		<div class="form__colum-descr">Выберите кузов</div>
		
		<div class="form__colum-button">
		<?if(count($arResult["kuzov"])>0){
			foreach($arResult["kuzov"] as $arKuzov){
			?>
			<div class="check__kuzov">
				<input type="checkbox" name="kuzov_<?=$arKuzov["CODE"]?>" value="<?=$arKuzov["ID"]?>" class="check__kuzov-input" />
				<label for="<?=$arKuzov["CODE"]?>" class="check__kuzov-label"><?=$arKuzov["NAME"]?></labe>
			</div>
			<?}}?>
			
		</div>	
			
		<div class="subForm-input-fullrow edata">					
			<input type="checkbox" class="in-text-data data-style"  name="order_edata"  id="edata" checked="checked">Подтверждаю согласие на обработку <a href="/personal-data/" target="_blank">персональных данных</a>
		</div>
		
		<div class="subForm-input-fullrow subForm-input-row-center">    
			<button class="btn-submit onSubmit">Подписаться</button>
		</div>
		
	</form>
</div>
