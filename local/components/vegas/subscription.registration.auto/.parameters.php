<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arCurrentValues */

if(!CModule::IncludeModule("iblock"))
	return;

CModule::IncludeModule("subscribe");
CModule::IncludeModule("form"); 

$arTypesEx = CIBlockParameters::GetIBlockTypes(array("-"=>" "));
//################################
$arOrder = Array("SORT"=>"ASC", "NAME"=>"ASC"); 
$arFilter = Array("ACTIVE"=>"Y", "LID"=>"s1"); 
$rsRubric = CRubric::GetList($arOrder, $arFilter); 
$arRubricList = array(); 
while($arRubric = $rsRubric->GetNext()) 
{ 
 $arRubricList[$arRubric["ID"]] = $arRubric["NAME"]; 
} 
//################################
$filter = Array
(
    "ID"             => "7 | 8 | 9",
    "ACTIVE"         => "Y"
);
$rsGroups = CGroup::GetList(($by="c_sort"), ($order="desc"), $filter); // выбираем группы

   while($arGroups = $rsGroups->Fetch())
   {
      $arUsersGroups[$arGroups["ID"]] = $arGroups["NAME"];
   }
   
//################################

$rsForms = CForm::GetList($by="s_id", $order="desc", array(), $is_filtered);
$arListForm = array();
while ($arForm = $rsForms->Fetch())
{
	$arListForm[$arForm["ID"]] = $arForm["NAME"];
}
//################################
$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(

		"UGROUP_ID" => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => "Выбрать группу пользователей",
			"TYPE" => "LIST",
			"VALUES" => $arUsersGroups,
			"DEFAULT" => 'Не выбрано',	
			"REFRESH" => "N",
			"MULTIPLE" => "Y",
		),
		"FORM_ID" => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => "Веб форма",
			"TYPE" => "LIST",
			"VALUES" => $arListForm,
			"DEFAULT" => 'Не выбрано',
			"REFRESH" => "N",
			"MULTIPLE" => "N",
		),

		"CACHE_TIME"  =>  array("DEFAULT"=>36000000),
		"CACHE_FILTER" => array(
			"PARENT" => "CACHE_SETTINGS",
			"NAME" => GetMessage("IBLOCK_CACHE_FILTER"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
		),
		"CACHE_GROUPS" => array(
			"PARENT" => "CACHE_SETTINGS",
			"NAME" => GetMessage("CP_BNL_CACHE_GROUPS"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
		),
	),
);


CIBlockParameters::Add404Settings($arComponentParameters, $arCurrentValues);
