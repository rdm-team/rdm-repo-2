<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?CJSCore::Init(array("jquery"));?>
<?$this->addExternalJS($templateFolder."/jquery.validate.js");?>
<?
	$url = $_SERVER['HTTP_HOST'];
	// Ключи для ReCaptcha API
	if($url == 'rdm-import.ru'){
		$sitekey = '6LfI7TwUAAAAAGn0uGdlu0PMYWQG3ZomnYFN0IM-';
	}else if($url == 'marketica-dev.ru'){
		$sitekey = '6LeT1z8UAAAAAF9jN_e-hemvdCoRIArgz6R4d4kY';
	}else{
		$sitekey = '6LeT1z8UAAAAAF9jN_e-hemvdCoRIArgz6R4d4kY';
	}
//$siteKey = "6Lfy-SgUAAAAAHUFP8mCx8lsf5GowSPmeOKaS7b9";
//$secret = "6Lfy-SgUAAAAAM-effqlqdfMsGHzRxXlZ6-ba9fC";
$lang = "ru";
?>
<div class="tradeForm" id="_js-orderForm" style="display:none">
		<div class="complex__header">
			<img class="complex__headerLogo" src="/local/assets/img/assets/header/logo.svg" alt="">
		</div>
		<div class="complex__title">Торгуйтесь с нами, предложив свою цену</div>
	<form class="order-form" id="trade-form" method="post">
		
        <div class="complexForm-group">

		<?foreach($arResult["FIELDS"] as $arFields){

		    $placeholder=!empty($arFields["ANSWER"]["FIELD_PARAM"])?$arFields["ANSWER"]["FIELD_PARAM"]:$arFields["TITLE"];

			if($arFields["ANSWER"]["FIELD_TYPE"]=="text"){
		?>
		<div class="complexForm-input-fullrow formfield <?if($arFields["REQUIRED"]=="Y"){?>requir<?}?>" id="<?=$arFields["SID"]?>">
		
			<input type="<?=$arFields["ANSWER"]["FIELD_TYPE"]?>" name="<?=$arFields["SID"]?>" class="complexForm-in-<?=$arFields["ANSWER"]["FIELD_TYPE"]?>" <?=$placeholder?> value="" />
		<?//if($arFields["REQUIRED"]=="Y"){?>
			<div class="error-block">Заполните это поле</div>
		<?//}?>
		</div>
		
			<?}elseif($arFields["ANSWER"]["FIELD_TYPE"]=="textarea"){?>
				<div class="complexForm-input-fullrow formfield <?if($arFields["REQUIRED"]=="Y"){?>requir<?}?>" id="<?=$arFields["SID"]?>">

				<textarea name="<?=$arFields["SID"]?>" class="complexForm-in-<?=$arFields["ANSWER"]["FIELD_TYPE"]?>" <?=$placeholder?>></textarea>
			<?if($arFields["REQUIRED"]=="Y"){?>
				<div class="error-block">Заполните это поле</div>
			<?}?>
		</div>
	
		<?}else{?>			
			<input type="<?=$arFields["ANSWER"]["FIELD_TYPE"]?>" name="<?=$arFields["SID"]?>" value="<?=$arFields["ANSWER"]["VALUE"]?>" />
		  <?}
		}?>

        </div>
		
		<div class="complexForm-input-fullrow edata">					
			<input type="checkbox" class="in-text-data data-style onData" name="order_edata" checked="checked">
			<span class="data-text">Подтверждаю согласие на обработку <a href="/personal-data/" target="_blank">персональных&nbsp;данных</a></span>
		</div>

		<div class="complexForm-input-fullrow order-input-row-center">    
			<button id="onTradeSubmit" data-type="trade_whit_us">Отправить</button>
		</div>
	</form>
		
</div>
<script src='https://www.google.com/recaptcha/api.js?onload=onloadCallback" async defer'></script>