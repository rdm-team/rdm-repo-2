<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arCurrentValues */

if(!CModule::IncludeModule("iblock"))
	return;

CModule::IncludeModule("subscribe");
CModule::IncludeModule("form"); 

$arTypesEx = CIBlockParameters::GetIBlockTypes(array("-"=>" "));
//################################

$rsForms = CForm::GetList($by="s_id", $order="desc", array(), $is_filtered);
$arListForm = array();
while ($arForm = $rsForms->Fetch())
{
	$arListForm[$arForm["ID"]] = $arForm["NAME"];
}
//################################
$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(

	),
);


CIBlockParameters::Add404Settings($arComponentParameters, $arCurrentValues);
