<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!CModule::IncludeModule("form")) return;

$arStartPage = array("list" => GetMessage("COMP_FORM_VALUES_LIST"), "new" => GetMessage("COMP_FORM_VALUES_NEW"));

$arrForms = array();
$rsForm = CForm::GetList($by='s_sort', $order='asc', !empty($_REQUEST["site"]) ? array("SITE" => $_REQUEST["site"]) : array(), $v3);
while ($arForm = $rsForm->Fetch())
{
	$arrForms[$arForm["ID"]] = "[".$arForm["ID"]."] ".$arForm["NAME"];
}

if (intval($arCurrentValues["WEB_FORM_ID"]) > 0)
{
	$show_list = true;
	$rsFieldList = CFormField::GetList(intval($arCurrentValues["WEB_FORM_ID"]), "ALL", $by="s_sort", $order="asc", array(), $is_filtered);
	$arFieldList = array();
	while ($arField = $rsFieldList->GetNext())
	{
		$arFieldList[$arField["SID"]] = "[".$arField["SID"]."] ".$arField["TITLE"];
	}
}
else
{
	$show_list = false;
}

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"AJAX_MODE" => array(),
        "WEB_FORM_ID" => array(
        "PARENT" => "BASE",
        "NAME" => "Веб форма (Символьный код)",
        "TYPE" => "TEXT",
        "MULTIPLE" => "N",
        "DEFAULT" => "0",
        "REFRESH" => "N",
        ),
        "EMAIL_TEMPLATE" => array(
        "PARENT" => "BASE",
        "NAME" => "Почтовый шаблон(ID)",
        "TYPE" => "TEXT",
        "MULTIPLE" => "N",
        "DEFAULT" => "0",
        "REFRESH" => "N",
        ),
        "REQUIRED_FIELDS" => array(
        "PARENT" => "BASE",
        "NAME" => "Обязательные поля(Символьный код)",
        "TYPE" => "TEXT",
        "MULTIPLE" => "N",
        "DEFAULT" => "0",
        "REFRESH" => "N",
        ),				
	),
);


?>