<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?CJSCore::Init(array("jquery"));?>
<div class="fav__wrap">
<div class="fav__title">Мой список сравнения</div>
<?if(count($arResult["ITEMS"])){?>
<div class="favorite__auto" id="favorite-auto__main">
<div class="favorite-auto__header">
	<div class="favorite-auto__header-item _date ">
		<span class="favorite-auto__header-item-text">дата публикации</span>
	</div>
	<div class="favorite-auto__header-item _picture "></div>
	<div class="favorite-auto__header-item _model ">
		<span class="favorite-auto__header-item-text">модель</span>
	</div>
	<div class="favorite-auto__header-item _year">
		<span class="favorite-auto__header-item-text">
			<span href="" class="favorite-auto__header-item-link">год</span>
		</span>
	</div>
	<div class="favorite-auto__header-item _engine">
		<span class="favorite-auto__header-item-text">двигатель</span>
	</div>
	<div class="favorite-auto__header-item _mileage">
		<span class="favorite-auto__header-item-text">
			<span href="" class="favorite-auto__header-item-link">пробег , тыс. км</span>
		</span>
	</div>
	<div class="favorite-auto__header-item _price">
		<span class="favorite-auto__header-item-text">цена</span>			
	</div>
</div>

		

<?
	$arItems = $arResult["ITEMS"];
	$inxImage = 1;
	
	foreach($arItems as $arItem){
		$arProp = $arItem["PROPERTIES"];
	?>
	<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="favorite-auto__item _main elem<?=$arItem["ID"]?>">
		<div class="favorite-auto__item-cell _date"><?=FormatDate("d/m", MakeTimeStamp($arItem["DATE_CREATE"]))?></div>
		<div class="favorite-auto__item-cell _picture">
			<?if(!empty($arItem["PREVIEW_PICTURE"]["SRC"])):?>
				<!--img src="<?//=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="" class="favorite-auto__item-picture-img" / -->
				<img id="<?=$inxImage?>" src="/local/assets/img/noimg.png" data-src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="" class="favorite-auto__item-picture-img img-load" />
			<?endif?>
		</div>
		<div class="favorite-auto__item-cell _model">
			<span class="favorite-auto__item-cell-important">
				<?=$arProp['MANUFACTURER']['VALUE']." ".$arProp['MODEL']['VALUE'];?>
			</span>
		</div>
		<div class="favorite-auto__item-cell _year">
			<span class="favorite-auto__item-cell-important">
				<?=$arProp['MODEL_YEAR']['VALUE'];?>
			</span>
		</div>
		<div class="favorite-auto__item-cell _engine">
			<span class="favorite-auto__item-cell-important">
				<?=$arProp['ENGINE']['VALUE']." ".GetMessage("AD_LIST_ENGINE_VOLUME_MEASURE");?>
			</span>
			<span class="favorite-auto__item-cell-about">
				<?=strip_tags($arProp["FUEL"]["MODIFY"]);?>
			</span>
			</span>
			<span class="favorite-auto__item-cell-about">
				<?=strip_tags($arProp["TRANSMISSION"]["MODIFY"]);?>
			</span>
			<span class="favorite-auto__item-cell-about">
				<?=strip_tags($arProp["DRIVE"]["MODIFY"]);?>
				
			</span>
		</div>
		<div class="favorite-auto__item-cell _mileage">
			<span class="favorite-auto__item-cell-important <?=(!empty($arProp['IS_NEW']['VALUE'])) ? "_new" : ""?>">
				<?
					if(!empty($arProp['IS_NEW']['VALUE']))
					{
						echo GetMessage("AD_LIST_MILEAGE_NEW_CAR");
					}
					elseif(!empty($arProp['MILEAGE']['VALUE']))
					{
						echo $arProp['MILEAGE']['VALUE'];
					}
					else
					{
						echo GetMessage("AD_LIST_MILEAGE_NULL");
					}
				?>
			</span>
		</div>
		<div class="favorite-auto__item-cell _price">
			<span class="favorite__item-cell-important _price _show _rub">
				<?=$arProp["FORMATTED_PRICE_RUB"];?>
			</span>
		</div>
		<div class="favorite-auto-layout"></div>
	</a>
	<div class="favorite-auto__item-nav elem<?=$arItem["ID"]?>">
		<div class="favorite-auto__nav-cell">
		<button class="favorite-auto__item-btn onCompareRemove" data-id="<?=$arItem["ID"]?>">Убрать из списка</button>
		</div>
	</div>
	<?$inxImage++;?>
<?}?>

</div>

<!-- ####### -->
<div class="page__main-wrapper _auto mobile_autolist">
	<div class="auto">
<div class="auto__item-wrapper">
	<?	
	$inxImage = 100;
	foreach($arItems as $arItem){
		$arProp = $arItem["PROPERTIES"];	
	?>
		<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="auto__item _mobile elem<?=$arItem["ID"]?>">
			<div class="auto__item-left">
				<div class="auto__item-left-wrapper">
					<div class="auto__item-left-model">
						<?=$arProp['MANUFACTURER']['VALUE']." ".$arProp['MODEL']['VALUE'];?>
					</div>			
				</div>
				<div class="auto__item-left-picture">
					<?if(is_array($arItem["PREVIEW_PICTURE"])):?>
						<!--img src="<?//=$arItem["PREVIEW_PICTURE"]['SRC']?>" alt="" class="auto__item-left-picture-img" / -->
						<img id="<?=$inxImage?>" src="/local/assets/img/noimg.png" data-src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="" class="favorite-auto__item-picture-img img-load" />
					<?endif;?>
				</div>
			</div>
			<div class="auto__item-rigth">
				<div class="auto__item-rigth-favorites"></div>
				<div class="auto__item-rigth-price">
					<?=$arProp["FORMATTED_PRICE_RUB"];?>
				</div>
				<div class="auto__item-rigth-about">
					<?if(!empty($arProp['MODEL_YEAR']['VALUE'])):?>
						<?=$arProp['MODEL_YEAR']['NAME'].": ".$arProp['MODEL_YEAR']['VALUE'];?>,<br>
					<?endif;?>
					
					<?if(!empty($arProp['ENGINE']['VALUE'])):?>
						<?=$arProp['ENGINE']['NAME'].": ".$arProp['ENGINE']['VALUE']." ".GetMessage("AD_LIST_ENGINE_VOLUME_MEASURE");?>,<br>
					<?endif;?>
					<?if(!empty($arItem["FUEL"]["MODIFY"])):?>
						<?=$arProp['FUEL']['NAME'].": ".strip_tags($arProp["FUEL"]["MODIFY"]);?>,<br>
					<?endif;?>
					<?if(!empty($arProp["TRANSMISSION"]["MODIFY"])):?>
						<?=$arProp['TRANSMISSION']['NAME'].": ".strip_tags($arProp["TRANSMISSION"]["MODIFY"]);?>,<br>
					<?endif;?>
					<?if(!empty($arProp["DRIVE"]["MODIFY"])):?>
						<?=$arProp['DRIVE']['NAME'].": ".strip_tags($arProp["DRIVE"]["MODIFY"])." ".GetMessage("AD_LIST_DRIVE");?>,<br>
					<?endif;?>
					
					<?=$arProp['MILEAGE']['NAME'].": ".$arProp['MILEAGE']['VALUE']." ".GetMessage("AD_LIST_MILEAGE_MEASURE");?>
				</div>
				<div class="auto__item-rigth-about">
					<div class="favorite-auto__item-nav elem<?=$arItem["ID"]?>">
						<div class="favorite-auto__nav-cell">
							<button class="favorite-auto__item-btn onCompareRemove" data-id="<?=$arItem["ID"]?>">Убрать из списка</button>
						</div>
					</div>
				</div>
			</div>
		</a>
		<?$inxImage++;?>
	<?};?>
</div>
</div>
</div>
<!-- ####### -->

<?}else{?>
<div class="favorite__text"><p>Чтобы добавить автомобиль в список сравнения, нажмите <span class="fav__star">Список сравнения</span> на карточке понравившегося автомобиля.</p></div>
<?}?>
</div>
