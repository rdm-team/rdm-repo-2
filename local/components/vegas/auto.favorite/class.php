<? 
    use Bitrix\Main\Loader;
    use Bitrix\Main\Application; 
    use Bitrix\Main\Web\Uri;
     
    if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

    class regForm extends CBitrixComponent
    {
        
        public function executeComponent()
        {
            global $APPLICATION;      
			$this->arResult = array();
			$arCurrencies = YcawebHelper::getCurrencyPrice();
			
			if(isset($_COOKIE["BITRIX_SM_compare"])){
				
				if(count($_COOKIE["BITRIX_SM_compare"])>0){	


					$strFuelId = "";
					$strTransmId = "";
					$strDriveId = "";
					
					
					$arCompares = $_COOKIE["BITRIX_SM_compare"];				
						
					$arSelect = Array("ID", "IBLOCK_ID", "NAME","DETAIL_PAGE_URL", "DATE_CREATE", "PREVIEW_PICTURE","DATE_ACTIVE_FROM","PROPERTY_*");
					$arFilter = Array("IBLOCK_ID"=>5,"ID"=>$arCompares, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
					$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
					while($ob = $res->GetNextElement()){ 
					
						$arFields = $ob->GetFields();  						
						$arProps = $ob->GetProperties();
						
						if(!empty($arFields["PREVIEW_PICTURE"])){
							
							$arFields["PREVIEW_PICTURE"] = CFile::GetFileArray($arFields['PREVIEW_PICTURE']);
						}
						/* �������� �� ������� */
						$strFuelId .= ','.$arProps["FUEL"]["VALUE"];
						/* �������� �� ����������� */
						$strTransmId .= ','.$arProps["TRANSMISSION"]["VALUE"];
						/* �������� �� ������� */
						$strDriveId .= ','.$arProps["DRIVE"]["VALUE"];
						
		if(!empty($arProps['PRICE']['VALUE']))
		{
			$arProps["FORMATTED_PRICE_RUB"] = YcawebHelper::priceFormat($arProps['PRICE']['VALUE']);
			foreach($arCurrencies as $code=>$arCurrency)
			{
				$price = round( (int)$arProps['PRICE']['VALUE'] / (int)$arCurrency['RATE'], 0 );
				$arProps['PRICE'][$code]['VALUE'] = YcawebHelper::priceFormat($price);
				$arProps['PRICE'][$code]['SYMBOL'] = $arCurrency['SYMBOL'];
			}
		}
						
						
						$arFields["PROPERTIES"] = $arProps;
						$this->arResult["ITEMS"][] = $arFields;
					}
								
				}
				
			}
			
			/* ����������� �� ������� */
			$strFuelId = trim($strFuelId,',');
			$arFuelId = array_unique(explode(',',$strFuelId));
			/* ����������� �� ����������� */
			$strTransmId = trim($strTransmId,',');
			$arTransmId = array_unique(explode(',',$strTransmId));			
			/* ����������� �� ������� */
			$strDriveId = trim($strDriveId,',');
			$arDriveId = array_unique(explode(',',$strDriveId));
			
			$arSelect = Array("ID", "IBLOCK_ID", "NAME");					
			
			/* �������*/
			$arFilter = Array("IBLOCK_ID"=>10,"ID"=>$arFuelId, "ACTIVE"=>"Y");
			$res = CIBlockElement::GetList(Array(), $arFilter, false, true, $arSelect);
			$arFuelId = array();
			while($arRes = $res->GetNext()){ $arFuelId[$arRes["ID"]] = $arRes["NAME"]; }					
			/* �����������*/
			$arFilter = Array("IBLOCK_ID"=>8,"ID"=>$arTransmId, "ACTIVE"=>"Y");
			$res = CIBlockElement::GetList(Array(), $arFilter, false, true, $arSelect);
			$arTransmId = array();
			while($arRes = $res->GetNext()){ $arTransmId[$arRes["ID"]] = $arRes["NAME"]; }
			/* ������*/
			$arFilter = Array("IBLOCK_ID"=>11,"ID"=>$arDriveId, "ACTIVE"=>"Y");
			$res = CIBlockElement::GetList(Array(), $arFilter, false, true, $arSelect);
			$arDriveId = array();
			while($arRes = $res->GetNext()){ $arDriveId[$arRes["ID"]] = $arRes["NAME"]; }	
	
			foreach($this->arResult["ITEMS"] as $k=>$v){
				
				$prop = $v["PROPERTIES"];
				
				$this->arResult["ITEMS"][$k]["PROPERTIES"]["FUEL"]["MODIFY"] = $arFuelId[$prop["FUEL"]["VALUE"]];
				$this->arResult["ITEMS"][$k]["PROPERTIES"]["TRANSMISSION"]["MODIFY"] = $arTransmId[$prop["TRANSMISSION"]["VALUE"]];
				$this->arResult["ITEMS"][$k]["PROPERTIES"]["DRIVE"]["MODIFY"] = $arDriveId[$prop["DRIVE"]["VALUE"]];
				
			}
			
	
			
			if(false){
				$arCookie = $APPLICATION->get_cookie('compare');
				foreach($arCookie as $k=>$arCook){
					$APPLICATION->set_cookie('compare['.$k.']', '',time()+(86400* 5));
				}
				
			}

			$this->includeComponentTemplate();
                    


        }    

        public function onPrepareComponentParams($arParams)
        {
            
            
            if(!isset($arParams["CACHE_TIME"]))
                $arParams["CACHE_TIME"] = 36000000;

            $arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
            if(strlen($arParams["IBLOCK_TYPE"])<=0)
                $arParams["IBLOCK_TYPE"] = "news";
            
            $arParams["IBLOCK_ID"] = trim($arParams["IBLOCK_ID"]);
            $arParams["SET_LAST_MODIFIED"] = $arParams["SET_LAST_MODIFIED"]==="Y";
            $arParams["AJAX_MODE"] = "N";

            return $arParams;
        }
        
    }
?>