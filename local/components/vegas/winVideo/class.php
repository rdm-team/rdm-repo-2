<? 
    use Bitrix\Main\Loader;
    use Bitrix\Main\Application; 
    use Bitrix\Main\Web\Uri;
     
    if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

    class winVideo extends CBitrixComponent
    {
        
        public function executeComponent()
        {

            $this->includeComponentTemplate();
        
        }    

        public function onPrepareComponentParams($arParams)
        {
            

            return $arParams;
        }
        
    }
?>