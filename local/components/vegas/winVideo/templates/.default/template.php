<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="youVideo">
    <button class="youVideo__eventCancel"></button>
    <div id="youtubeContent" class="youVideo__body"></div>
</div>
<div class="youVideo__shadow"></div>