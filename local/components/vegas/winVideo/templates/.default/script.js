$(document).ready(function(){

    $("body").on("click",".eventVideo", function(){
        var th = $(this);
        $(".youVideo__shadow").css({opacity:0,display:"block"}).animate({opacity:.2},400,function(){
            
            $(".youVideo").css({opacity:0,display:"block"}).animate({opacity:1},400);
		
            $.post("/local/ajax/video.php",{video:th.attr("data-src"),time:th.attr("data-time")},function(data){             
                if(data){$("#youtubeContent").html(data);}
            },"html");
            
        });
        
        return false;
    });
	
	
    $("body").on("click",".youVideo__eventCancel", function(){
        
        $(".youVideo").animate({opacity:0},400,function(){
            
            $(this).css({display:"none"});
            $("#youtubeContent").html('');
            
            $(".youVideo__shadow").animate({opacity:0},400,function(){
                
                $(this).css({display:"none"});
                
            });
            
        });        
        
    });

    $("body").on("click",".youVideo__shadow", function(){
        
        $(".youVideo").animate({opacity:0},400,function(){
            
            $(this).css({display:"none"});
            $("#youtubeContent").html('');
            
            $(".youVideo__shadow").animate({opacity:0},400,function(){
                
                $(this).css({display:"none"});
                
            });
            
        });
        
        
    });

});