<? 
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

class TagFilter extends CBitrixComponent
{
	private $IBLOCK_ID = 25;
	private $arTag;

	private function set404()
	{
		global $APPLICATION;
		$APPLICATION->RestartBuffer();
	    require $_SERVER["DOCUMENT_ROOT"] . '/bitrix/header.php';
	    include $_SERVER["DOCUMENT_ROOT"] . '/404.php';
	    require $_SERVER["DOCUMENT_ROOT"] . '/bitrix/footer.php';
	    die;
	}

	private function getTag()
	{
		$arTag = array();
		if ($this->arParams['SECTION_CODE'])
		{
			$arTmp = CIBlockElement::getList(
				array(),
				array(
					'IBLOCK_ID' => $this->IBLOCK_ID,
					'ACTIVE' => 'Y',
					'CODE' => $this->arParams['SECTION_CODE'],
				),
				false, array('nTopCount' => '1'),
				array('ID', 'IBLOCK_ID', 'NAME', 'DETAIL_TEXT', 'PROPERTY_SECTIONS')
			)->fetch();

			if (!$arTmp)
			{
				$this->set404();
			}
			else
			{
				$arTag = array(
					'ID' => $arTmp['ID'],
					'NAME' => $arTmp['NAME'],
					'TEXT' => $arTmp['DETAIL_TEXT'],
				);

				$dbSeo = new \Bitrix\Iblock\InheritedProperty\ElementValues(
					$this->IBLOCK_ID,
					$arTmp['ID']
				);
				$arSeo = $dbSeo->getValues();
				$arTag['SEO'] = $arSeo;

				if ($arTmp['PROPERTY_SECTIONS_VALUE'])
				{
					$arTag['ELEMENTS'] = array();
					$dbAuto = CIBlockElement::getList(
						array(),
						array(
							'IBLOCK_ID' => 5,
							'ACTIVE' => 'Y',
							'SECTION_ID' => $arTmp['PROPERTY_SECTIONS_VALUE'],
							'INCLUDE_SUBSECTIONS' => 'Y'
						),
						false, false,
						array('ID')
					);
					while ($arAuto = $dbAuto->fetch())
					{
						$arTag['ELEMENTS'][] = $arAuto['ID'];
					}
				}
			}
		}
		else
		{
			$this->set404();
		}
		return $arTag;
	}

	private function setSeo()
	{
		global $APPLICATION;
		if ($this->arTag['SEO']['ELEMENT_PAGE_TITLE'])
			$APPLICATION->setTitle($this->arTag['SEO']['ELEMENT_PAGE_TITLE']);
		if ($this->arTag['SEO']['ELEMENT_META_TITLE'])
			$APPLICATION->setPageProperty('title', $this->arTag['SEO']['ELEMENT_META_TITLE']);
		if ($this->arTag['SEO']['ELEMENT_META_DESCRIPTION'])
			$APPLICATION->setPageProperty('description', $this->arTag['SEO']['ELEMENT_META_DESCRIPTION']);
		if ($this->arTag['SEO']['ELEMENT_META_KEYWORDS'])
			$APPLICATION->setPageProperty('keywords', $this->arTag['SEO']['ELEMENT_META_KEYWORDS']);
		if ($this->arTag['NAME'])
			$APPLICATION->AddChainItem($this->arTag['NAME']);
	}
	public function executeComponent()
	{
		$this->arTag = $this->getTag();
		$this->setSeo();

		global ${arrSellFilter};
		${arrSellFilter} = array(
			'ID' => $this->arTag['ELEMENTS']
		);

		$this->arResult['TEXT'] = $this->arTag['TEXT'];
		$this->includeComponentTemplate();
	}

	public function onPrepareComponentParams($arParams)
	{
		$arParams['SECTION_CODE'] = !empty($arParams['SECTION_CODE']) ? $arParams['SECTION_CODE'] : false;
		return $arParams;
	}
}