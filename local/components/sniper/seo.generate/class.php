<? 
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

class SeoGenerate extends CBitrixComponent
{
	private $arTemplate;
	private $arManufacture;
	private $arModel;
	private $year;

	private function setManufacture()
	{
		$arManufacture = array();
		if ($_REQUEST['filter']['MANUFACTURER'])
		{
			$arManufacture = CIBlockSection::getList(
				array(),
				array(
					'IBLOCK_ID' => $this->arParams['IBLOCK_CATALOG_ID'],
					'ACTIVE' => 'Y',
					'CODE' => $_REQUEST['filter']['MANUFACTURER'],
				),
				false,
				array('ID', 'IBLOCK_ID', 'NAME', 'UF_RUS_NAME'),
				array('nTopCount' => '1')
			)->fetch();
		}
		$this->arManufacture = $arManufacture;
	}
	private function setModel()
	{
		$arModel = array();
		if ($_REQUEST['filter']['MODEL'])
		{
			$arModel = CIBlockSection::getList(
				array(),
				array(
					'IBLOCK_ID' => $this->arParams['IBLOCK_CATALOG_ID'],
					'ACTIVE' => 'Y',
					'CODE' => str_replace(' ', '_', $_REQUEST['filter']['MODEL']),
				),
				false,
				array('IBLOCK_ID', 'NAME', 'UF_RUS_NAME'),
				array('nTopCount' => '1')
			)->fetch();
		}
		$this->arModel = $arModel;
	}
	private function setYear()
	{
		$year = false;
		if ($_REQUEST['filter']['YEAR']['FROM'])
			$year = $_REQUEST['filter']['YEAR']['FROM'];
		elseif ($_REQUEST['filter']['YEAR']['TO'])
			$year = $_REQUEST['filter']['YEAR']['TO'];
		$this->year = $year;
	}
	private function setSeoTemplate()
	{
		global $APPLICATION;
		if ($this->arTemplate['H1'])
			$APPLICATION->setTitle($this->arTemplate['H1']);
		if ($this->arTemplate['TITLE'])
			$APPLICATION->setPageProperty('title', $this->arTemplate['TITLE']);
		if ($this->arTemplate['DESCRIPTION'])
			$APPLICATION->setPageProperty('description', $this->arTemplate['DESCRIPTION']);
	}
	public function executeComponent()
	{
		$this->setManufacture();

		if ($this->arManufacture)
		{
			$this->setModel();
			$this->setYear();

			//Title - Продажа #Производитель Eng# #Марка Eng# (#Производитель Rus# #Марка Rus#) #цифра год# года по низкой цене в Новосибирске - автосалон РДМ-Импорт
			$title = 'Продажа ' . $this->arManufacture['NAME'];
			if ($this->arModel)
				$title .= ' ' . $this->arModel['NAME'];
			if ($this->arManufacture['UF_RUS_NAME'] || $this->arModel['UF_RUS_NAME'])
			{
				$title .= ' (';
				if ($this->arManufacture['UF_RUS_NAME'])
					$title .= $this->arManufacture['UF_RUS_NAME'];
				if ($this->arModel['UF_RUS_NAME'])
					$title .= ' ' . $this->arModel['UF_RUS_NAME'];
				$title .= ')';
			}
			if ($this->year)
				$title .= ' ' . $this->year . ' года';
			$title .= ' по низкой цене в Новосибирске - автосалон РДМ-Импорт';
			$this->arTemplate['TITLE'] = $title;

			//H1 - Продажа #Производитель Eng# #Марка Eng# #цифра год# года в Новосибирске
			$h1 = 'Продажа ' . $this->arManufacture['NAME'];
			if ($this->arModel)
				$h1 .= ' ' . $this->arModel['NAME'];
			if ($this->year)
				$h1 .= ' ' . $this->year . ' года';
			$h1 .= '  в Новосибирске';
			$this->arTemplate['H1'] = $h1;

			//Descripition - Предлагаем купить #Производитель Eng# #Марка Eng# #цифра год# года выпуска по выгодной цене в Новосибирске. Воспользуйтесь услугами Trade-in (Обмена), автокредитования, страхования при покупке БУ автомобиля #Производитель Rus# #Марка Rus# в автомобильном салоне РДМ-Импорт
			$description = 'Предлагаем купить ' . $this->arManufacture['NAME'];
			if ($this->arModel)
				$description .= ' ' . $this->arModel['NAME'];
			if ($this->year)
				$description .= ' ' . $this->year . ' года выпуска';
			$description .= ' по выгодной цене в Новосибирске. Воспользуйтесь услугами Trade-in (Обмена), автокредитования, страхования при покупке БУ автомобиля';

			if ($this->arManufacture['UF_RUS_NAME'] || $this->arModel['UF_RUS_NAME'])
			{
				if ($this->arManufacture['UF_RUS_NAME'])
					$description .= ' ' . $this->arManufacture['UF_RUS_NAME'];
				if ($this->arModel['UF_RUS_NAME'])
					$description .= ' ' . $this->arModel['UF_RUS_NAME'];
			}
			$description .= ' в автомобильном салоне РДМ-Импорт';
			$this->arTemplate['DESCRIPTION'] = $description;	
		}
		else
		{
			$this->arTemplate['TITLE'] = "";
			$this->arTemplate['H1'] = "Продажа автомобилей в Новосибирске";
		}
		$this->setSeoTemplate();
	}

	public function onPrepareComponentParams($arParams)
	{
		$arParams['IBLOCK_CATALOG_ID'] = !empty($arParams['IBLOCK_CATALOG_ID']) ? $arParams['IBLOCK_CATALOG_ID'] : false;

		return $arParams;
	}
}