<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader;
use Bitrix\Currency\CurrencyTable;

/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

/*
 * class CLPComponent
 */

class CLPComponent extends CBitrixComponent{

	public $arParams = array();
	public $arrFilter = array();

	/*
	 * Prepare parameters
	 *
	 * @param array $arParams
	 * @return array
	 */
	public function onPrepareComponentParams($arParams){

		$arParams['BLOCK_TYPE'] = trim($arParams['BLOCK_TYPE']);
		$arParams['BLOCK_TITLE'] = trim($arParams['BLOCK_TITLE']);
		$arParams['BLOCK_ID'] = trim($arParams['BLOCK_ID']);
		$arParams['BLOCK_CLASSNAME'] = trim($arParams['BLOCK_CLASSNAME']);
		$arParams['BLOCK_WIDE'] = ($arParams['BLOCK_WIDE'] == 'Y');

		if(!isset($arParams['CACHE_TIME']))
			$arParams['CACHE_TIME'] = 36000000;

		if(strlen($arParams['FILTER_NAME'])<=0 || !preg_match('/^[A-Za-z_][A-Za-z01-9_]*$/', $arParams['FILTER_NAME']))				{
			$this->arrFilter = array();
		}else{
			$arrFilter = $GLOBALS[$arParams['FILTER_NAME']];
			if(!is_array($arrFilter))
				$this->arrFilter = array();
		}

		switch($arParams['BLOCK_TYPE']){
			case 'TEXT':
				$arParams['BLOCK_TEXT_FILE'] = trim($arParams['BLOCK_TEXT_FILE']);
			break;
			case 'ELEMENT_LIST':
				$arParams['IBLOCK_TYPE'] = trim($arParams['IBLOCK_TYPE']);
				$arParams['IBLOCK_ID'] = (int)$arParams['IBLOCK_ID'];
				$arParams['ELEMENT_COUNT'] = (int)$arParams['ELEMENT_COUNT'];


				$arParams['ELEMENT_SORT_FIELD'] = trim($arParams['ELEMENT_SORT_FIELD']);
				if(strlen($arParams['ELEMENT_SORT_FIELD']) <= 0)
					$arParams['ELEMENT_SORT_FIELD'] = 'ACTIVE_FROM';
				if(!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams['ELEMENT_SORT_ORDER']))
					$arParams['ELEMENT_SORT_ORDER']='DESC';

				if(strlen($arParams['ELEMENT_SORT_FIELD2'])<=0)
					$arParams['ELEMENT_SORT_FIELD2'] = 'SORT';
				if(!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams['ELEMENT_SORT_ORDER2']))
					$arParams['ELEMENT_SORT_ORDER2']='ASC';
			break;
			case 'SECTION_LIST':

			break;
			case 'CATALOG':
				$arParams['IBLOCK_TYPE'] = trim($arParams['IBLOCK_TYPE']);
				$arParams['IBLOCK_ID'] = (int)$arParams['IBLOCK_ID'];

				if(strlen($arParams['FILTER_NAME'])<=0 || !preg_match('/^[A-Za-z_][A-Za-z01-9_]*$/', $arParams['FILTER_NAME']))				{
					$this->arrFilter = array();
				}else{
					$arrFilter = $GLOBALS[$arParams['FILTER_NAME']];
					if(!is_array($arrFilter))
						$this->arrFilter = array();
				}

				$arParams['ELEMENT_COUNT'] = (int)$arParams['ELEMENT_COUNT'];


				$arParams['ELEMENT_SORT_FIELD'] = trim($arParams['ELEMENT_SORT_FIELD']);
				if(strlen($arParams['ELEMENT_SORT_FIELD']) <= 0)
					$arParams['ELEMENT_SORT_FIELD'] = 'ACTIVE_FROM';
				if(!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams['ELEMENT_SORT_ORDER']))
					$arParams['ELEMENT_SORT_ORDER']='DESC';

				if(strlen($arParams['ELEMENT_SORT_FIELD2'])<=0)
					$arParams['ELEMENT_SORT_FIELD2'] = 'SORT';
				if(!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams['ELEMENT_SORT_ORDER2']))
					$arParams['ELEMENT_SORT_ORDER2']='ASC';

				if(!is_array($arParams["PRICE_CODE"]))
					$arParams["PRICE_CODE"] = array();

				$arParams['CONVERT_CURRENCY'] = (isset($arParams['CONVERT_CURRENCY']) && 'Y' == $arParams['CONVERT_CURRENCY'] ? 'Y' : 'N');
				$arParams['CURRENCY_ID'] = trim(strval($arParams['CURRENCY_ID']));
				if ('' == $arParams['CURRENCY_ID'])
				{
					$arParams['CONVERT_CURRENCY'] = 'N';
				}
				elseif ('N' == $arParams['CONVERT_CURRENCY'])
				{
					$arParams['CURRENCY_ID'] = '';
				}
			break;
			case 'FORM':

			break;
			case 'MAP':

			break;
		}


		return $arParams;
	}

	/*
	 *
	 */
	public function getSections(){

		if(!CModule::IncludeModule('iblock')){
			return;
		}

		//ORDER BY
		$arSort = array(
			"left_margin"=>"asc",
		);
		//EXECUTE
		$rsSections = CIBlockSection::GetList($arSort, $arFilter, $arParams["COUNT_ELEMENTS"], $arSelect);
		$rsSections->SetUrlTemplates("", $arParams["SECTION_URL"]);
		while($arSection = $rsSections->GetNext())
		{
			$ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues($arSection["IBLOCK_ID"], $arSection["ID"]);
			$arSection["IPROPERTY_VALUES"] = $ipropValues->getValues();

			if($boolPicture)
			{
				$mxPicture = false;
				$arSection["PICTURE"] = intval($arSection["PICTURE"]);
				if (0 < $arSection["PICTURE"])
					$mxPicture = CFile::GetFileArray($arSection["PICTURE"]);
				$arSection["PICTURE"] = $mxPicture;
				if ($arSection["PICTURE"])
				{
					$arSection["PICTURE"]["ALT"] = $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"];
					if ($arSection["PICTURE"]["ALT"] == "")
						$arSection["PICTURE"]["ALT"] = $arSection["NAME"];
					$arSection["PICTURE"]["TITLE"] = $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"];
					if ($arSection["PICTURE"]["TITLE"] == "")
						$arSection["PICTURE"]["TITLE"] = $arSection["NAME"];
				}
			}
			$arSection['RELATIVE_DEPTH_LEVEL'] = $arSection['DEPTH_LEVEL'] - $intSectionDepth;

			$arButtons = CIBlock::GetPanelButtons(
				$arSection["IBLOCK_ID"],
				0,
				$arSection["ID"],
				array("SESSID"=>false, "CATALOG"=>true)
			);
			$arSection["EDIT_LINK"] = $arButtons["edit"]["edit_section"]["ACTION_URL"];
			$arSection["DELETE_LINK"] = $arButtons["edit"]["delete_section"]["ACTION_URL"];

			$arResult["SECTIONS"][]=$arSection;
		}

	}

	/*
	 *
	 */
	public function getElements(){

		if(!CModule::IncludeModule('iblock')){
			return;
		}

		if(is_numeric($this->arParams['IBLOCK_ID']))
		{
			$rsIBlock = CIBlock::GetList(array(), array(
			   'ACTIVE' => 'Y',
			   'ID' => $this->arParams['IBLOCK_ID'],
		  ));
		}
		else
		{
			$rsIBlock = CIBlock::GetList(array(), array(
			   'ACTIVE' => 'Y',
			   'CODE' => $this->arParams['IBLOCK_ID'],
			   'SITE_ID' => SITE_ID,
		  ));
		}
		if($arResult = $rsIBlock->GetNext())
		{
			//SELECT
			$arSelect = array(
				  'ID',
				  'IBLOCK_ID',
				  'IBLOCK_SECTION_ID',
				  'NAME',
				  'ACTIVE_FROM',
				  'DETAIL_PAGE_URL',
				  'DETAIL_TEXT',
				  'DETAIL_TEXT_TYPE',
				  'PREVIEW_TEXT',
				  'PREVIEW_TEXT_TYPE',
				  'PREVIEW_PICTURE',
					'PROPERTY_*'
			 );
			//WHERE
			$arFilter = array (
				'IBLOCK_ID'			=> $arResult['ID'],
				'IBLOCK_LID'		=> SITE_ID,
				'ACTIVE'			=> 'Y',
				'CHECK_PERMISSIONS' => 'Y',
			);
			//ORDER BY
			$arSort = array(
				$this->arParams['ELEMENT_SORT_FIELD'] => $this->arParams['ELEMENT_SORT_ORDER'],
				$this->arParams['ELEMENT_SORT_FIELD2'] => $this->arParams['ELEMENT_SORT_ORDER2'],
			);
			if(!array_key_exists('ID', $arSort))
				$arSort['ID'] = 'DESC';

			$arNavParams = false;
			if($this->arParams['ELEMENT_COUNT']){
				$arNavParams = array(
					'nTopCount' => $this->arParams['ELEMENT_COUNT']
				);
			}

			$arResult['ITEMS'] = array();
			$rsElement = CIBlockElement::GetList($arSort, array_merge($arFilter, $this->arrFilter), false, $arNavParams, $arSelect);
			while($obElement = $rsElement->GetNextElement())
			{
				$arItem = $obElement->GetFields();

				$arButtons = CIBlock::GetPanelButtons(
					$arItem['IBLOCK_ID'],
					$arItem['ID'],
					0,
					array('SECTION_BUTTONS' => false, 'SESSID' => false)
				);
				$arItem['EDIT_LINK'] = $arButtons['edit']['edit_element']['ACTION_URL'];
				$arItem['DELETE_LINK'] = $arButtons['edit']['delete_element']['ACTION_URL'];

				$ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($arItem['IBLOCK_ID'], $arItem['ID']);
				$arItem['IPROPERTY_VALUES'] = $ipropValues->getValues();
				if(isset($arItem['PREVIEW_PICTURE']))
				{
					$arItem['PREVIEW_PICTURE'] = (0 < $arItem['PREVIEW_PICTURE'] ? CFile::GetFileArray($arItem['PREVIEW_PICTURE']) : false);
					if ($arItem['PREVIEW_PICTURE'])
					{
						$arItem['PREVIEW_PICTURE']['ALT'] = $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_ALT'];
						if ($arItem['PREVIEW_PICTURE']['ALT'] == '')
							$arItem['PREVIEW_PICTURE']['ALT'] = $arItem['NAME'];
						$arItem['PREVIEW_PICTURE']['TITLE'] = $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'];
						if ($arItem['PREVIEW_PICTURE']['TITLE'] == '')
							$arItem['PREVIEW_PICTURE']['TITLE'] = $arItem['NAME'];
					}
				}
				if(isset($arItem['DETAIL_PICTURE']))
				{
					$arItem['DETAIL_PICTURE'] = (0 < $arItem['DETAIL_PICTURE'] ? CFile::GetFileArray($arItem['DETAIL_PICTURE']) : false);
					if ($arItem['DETAIL_PICTURE'])
					{
						$arItem['DETAIL_PICTURE']['ALT'] = $arItem['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT'];
						if ($arItem['DETAIL_PICTURE']['ALT'] == '')
							$arItem['DETAIL_PICTURE']['ALT'] = $arItem['NAME'];
						$arItem['DETAIL_PICTURE']['TITLE'] = $arItem['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE'];
						if ($arItem['DETAIL_PICTURE']['TITLE'] == '')
							$arItem['DETAIL_PICTURE']['TITLE'] = $arItem['NAME'];
					}
				}
				$arResult['ITEMS'][] = $arItem;
			}

			$this->arResult = $arResult;
		}

		if(isset($arResult['ID']))
		{
			global $USER, $APPLICATION;
			if($USER->IsAuthorized())
			{

				if(CModule::IncludeModule('iblock'))
				{
					$arButtons = CIBlock::GetPanelButtons(
						$arResult['ID'],
						0,
						'',
						array('SECTION_BUTTONS' => false)
					);

					if($APPLICATION->GetShowIncludeAreas())
						$this->AddIncludeAreaIcons(CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arButtons));
				}
			}
		}
	}

	/*
	 *
	 */
	public function getCatalog(){
		global $APPLICATION;

		$arCurrencyList = array();
		$arConvertParams = array();
		if($this->arParams['CONVERT_CURRENCY'] == 'Y')
		{
			if (!Loader::includeModule('currency'))
			{
				$this->arParams['CONVERT_CURRENCY'] = 'N';
				$this->arParams['CURRENCY_ID'] = '';
			}
			else
			{
				$arResultModules['currency'] = true;
				$currencyIterator = CurrencyTable::getList(array(
					'select' => array('CURRENCY'),
					'filter' => array('=CURRENCY' => $this->arParams['CURRENCY_ID'])
				));
				if ($currency = $currencyIterator->fetch())
				{
					$this->arParams['CURRENCY_ID'] = $currency['CURRENCY'];
					$arConvertParams['CURRENCY_ID'] = $currency['CURRENCY'];
				}
				else
				{
					$this->arParams['CONVERT_CURRENCY'] = 'N';
					$this->arParams['CURRENCY_ID'] = '';
				}
				unset($currency, $currencyIterator);
			}
		}

		$arSelect = array(
			"ID",
			"ACTIVE",
			"IBLOCK_ID",
			"IBLOCK_SECTION_ID",
			"NAME",
			"DESCRIPTION",
			"PICTURE",
			"LEFT_MARGIN",
			"RIGHT_MARGIN",
			"DEPTH_LEVEL",
			"LIST_PAGE_URL",
			"SECTION_PAGE_URL",
			"UF_*"
		);
		$arFilter = array(
			"IBLOCK_ID"=>$this->arParams["IBLOCK_ID"],
			"IBLOCK_ACTIVE"=>"Y",
			"ACTIVE"=>"Y",
			"GLOBAL_ACTIVE"=>"Y",
		);

		if($this->arParams["SECTION_ID"] > 0)
		{
			$arFilter["ID"] = $this->arParams["SECTION_ID"];
			$rsSection = CIBlockSection::GetList(array(), $arFilter, false, $arSelect);
			$arResult = $rsSection->GetNext();
			if($arResult)
				$bSectionFound = true;
		}
		elseif(strlen($this->arParams["SECTION_CODE"]) > 0)
		{
			$arFilter["=CODE"] = $this->arParams["SECTION_CODE"];
			$rsSection = CIBlockSection::GetList(array(), $arFilter, false, $arSelect);
			$arResult = $rsSection->GetNext();
			if($arResult)
				$bSectionFound = true;
		}
		else
		{
			//Root section (no section filter)
			$arResult = array(
				"ID" => 0,
				"IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
			);
			$bSectionFound = true;
		}

		if(!$bSectionFound){
			return;
		}else{
			$mxPicture = false;
			$arResult["PICTURE"] = intval($arResult["PICTURE"]);
			if (0 < $arResult["PICTURE"])
				$mxPicture = CFile::GetFileArray($arResult["PICTURE"]);
			$arResult["PICTURE"] = $mxPicture;
			if ($arResult["PICTURE"])
			{
				$arResult["PICTURE"]["ALT"] = $arResult["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"];
				if ($arResult["PICTURE"]["ALT"] == "")
					$arResult["PICTURE"]["ALT"] = $arResult["NAME"];
				$arResult["PICTURE"]["TITLE"] = $arResult["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"];
				if($arResult["PICTURE"]["TITLE"] == "")
					$arResult["PICTURE"]["TITLE"] = $arResult["NAME"];
			}


			//Buttons
			$UrlDeleteSectionButton = "";
			if($arResult["IBLOCK_SECTION_ID"] > 0)
			{
				$rsSection = CIBlockSection::GetList(
					array(),
					array("=ID" => $arResult["IBLOCK_SECTION_ID"]),
					false,
					array("SECTION_PAGE_URL")
				);
				$rsSection->SetUrlTemplates("", $this->arParams["SECTION_URL"]);
				$arSection = $rsSection->GetNext();
				$UrlDeleteSectionButton = $arSection["SECTION_PAGE_URL"];
			}

			if(empty($UrlDeleteSectionButton))
			{
				$url_template = CIBlock::GetArrayByID($this->arParams["IBLOCK_ID"], "LIST_PAGE_URL");
				$arIBlock = CIBlock::GetArrayByID($this->arParams["IBLOCK_ID"]);
				$arIBlock["IBLOCK_CODE"] = $arIBlock["CODE"];
				$UrlDeleteSectionButton = CIBlock::ReplaceDetailURL($url_template, $arIBlock, true, false);
			}

			$arReturnUrl = array(
				"add_section" => (
					strlen($arResult["SECTION_PAGE_URL"]) ?
						$arResult["SECTION_PAGE_URL"] :
						CIBlock::GetArrayByID($this->arParams["IBLOCK_ID"], "SECTION_PAGE_URL")
					),
				"delete_section" => $UrlDeleteSectionButton,
			);
			$arButtons = CIBlock::GetPanelButtons(
				$this->arParams["IBLOCK_ID"],
				0,
				$arResult["ID"],
				array("RETURN_URL" =>  $arReturnUrl, "CATALOG"=>true)
			);

			if($APPLICATION->GetShowIncludeAreas())
				$this->AddIncludeAreaIcons(CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arButtons));

		}

		$bIBlockCatalog = false;
		$bOffersIBlockExist = false;
		$arCatalog = false;
		$boolNeedCatalogCache = false;
		$bCatalog = Loader::includeModule('catalog');
		if ($bCatalog)
		{
			$arResultModules['catalog'] = true;
			$arResultModules['currency'] = true;
			$arCatalog = CCatalogSKU::GetInfoByIBlock($this->arParams["IBLOCK_ID"]);
			if (!empty($arCatalog) && is_array($arCatalog))
			{
				$bIBlockCatalog = $arCatalog['CATALOG_TYPE'] != CCatalogSKU::TYPE_PRODUCT;
				$bOffersIBlockExist = (
					$arCatalog['CATALOG_TYPE'] == CCatalogSKU::TYPE_PRODUCT
					|| $arCatalog['CATALOG_TYPE'] == CCatalogSKU::TYPE_FULL
				);
				$boolNeedCatalogCache = true;
			}
		}
		$arResult['CATALOG'] = $arCatalog;

		$arResult["PRICES"] = CIBlockPriceTools::GetCatalogPrices($this->arParams["IBLOCK_ID"], $this->arParams["PRICE_CODE"]);
		$arResult['PRICES_ALLOW'] = CIBlockPriceTools::GetAllowCatalogPrices($arResult["PRICES"]);

		$arResult['CONVERT_CURRENCY'] = $arConvertParams;

		if ($arResult["ID"] > 0)
		{
			$ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues($arResult["IBLOCK_ID"], $arResult["ID"]);
			$arResult["IPROPERTY_VALUES"] = $ipropValues->getValues();
		}
		else
		{
			$arResult["IPROPERTY_VALUES"] = array();
		}

		$arSelect = array(
			"ID",
			"IBLOCK_ID",
			"CODE",
			"XML_ID",
			"NAME",
			"ACTIVE",
			"DATE_ACTIVE_FROM",
			"DATE_ACTIVE_TO",
			"SORT",
			"PREVIEW_TEXT",
			"PREVIEW_TEXT_TYPE",
			"DETAIL_TEXT",
			"DETAIL_TEXT_TYPE",
			"DATE_CREATE",
			"CREATED_BY",
			"TIMESTAMP_X",
			"MODIFIED_BY",
			"TAGS",
			"IBLOCK_SECTION_ID",
			"DETAIL_PAGE_URL",
			"DETAIL_PICTURE",
			"PREVIEW_PICTURE",
			"CATALOG_QUANTITY"
		);

		foreach($arResult["PRICES"] as &$value)
		{
			if (!$value['CAN_VIEW'] && !$value['CAN_BUY'])
				continue;
			$arSelect[] = $value["SELECT"];
		}

		$arFilter = array(
			"IBLOCK_ID"			=> $this->arParams["IBLOCK_ID"],
			"IBLOCK_LID"		=> SITE_ID,
			"IBLOCK_ACTIVE"		=> "Y",
			"ACTIVE_DATE"		=> "Y",
			"ACTIVE"			=> "Y",
			"CHECK_PERMISSIONS" => "Y",
			"MIN_PERMISSION"	=> "R",
			"INCLUDE_SUBSECTIONS" => ($this->arParams["INCLUDE_SUBSECTIONS"] == 'N' ? 'N' : 'Y'),
			"SECTION_ID"		=> $arResult["ID"],
		);

		//ORDER BY
		$arSort = array(
			$this->arParams['ELEMENT_SORT_FIELD'] => $this->arParams['ELEMENT_SORT_ORDER'],
			$this->arParams['ELEMENT_SORT_FIELD2'] => $this->arParams['ELEMENT_SORT_ORDER2'],
		);
		if(!array_key_exists('ID', $arSort))
			$arSort['ID'] = 'DESC';

		$arNavParams = false;
		if($this->arParams['ELEMENT_COUNT']){
			$arNavParams = array(
				'nTopCount' => $this->arParams['ELEMENT_COUNT']
			);
		}

		//EXECUTE
		$arResult["ITEMS"] = array();
		$rsElements = CIBlockElement::GetList($arSort, array_merge($arFilter, $this->arrFilter), false, $arNavParams, $arSelect);
		while($objElement = $rsElements->GetNextElement())
		{
			$arItem = $objElement->GetFields();
			$arItem['PROPERTIES'] = $objElement->GetProperties();

			$arItem['ACTIVE_FROM'] = $arItem['DATE_ACTIVE_FROM'];
			$arItem['ACTIVE_TO'] = $arItem['DATE_ACTIVE_TO'];

			if($arResult["ID"])
				$arItem["IBLOCK_SECTION_ID"] = $arResult["ID"];

			$arButtons = CIBlock::GetPanelButtons(
				$arItem["IBLOCK_ID"],
				$arItem["ID"],
				$arResult["ID"],
				array("SECTION_BUTTONS"=>false, "SESSID"=>false, "CATALOG"=>true)
			);
			$arItem["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
			$arItem["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];

			$ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($arItem["IBLOCK_ID"], $arItem["ID"]);
			$arItem["IPROPERTY_VALUES"] = $ipropValues->getValues();

			$arItem["PREVIEW_PICTURE"] = (0 < $arItem["PREVIEW_PICTURE"] ? CFile::GetFileArray($arItem["PREVIEW_PICTURE"]) : false);
			if ($arItem["PREVIEW_PICTURE"])
			{
				$arItem["PREVIEW_PICTURE"]["ALT"] = $arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_ALT"];
				if ($arItem["PREVIEW_PICTURE"]["ALT"] == "")
					$arItem["PREVIEW_PICTURE"]["ALT"] = $arItem["NAME"];
				$arItem["PREVIEW_PICTURE"]["TITLE"] = $arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"];
				if ($arItem["PREVIEW_PICTURE"]["TITLE"] == "")
					$arItem["PREVIEW_PICTURE"]["TITLE"] = $arItem["NAME"];
			}
			$arItem["DETAIL_PICTURE"] = (0 < $arItem["DETAIL_PICTURE"] ? CFile::GetFileArray($arItem["DETAIL_PICTURE"]) : false);
			if ($arItem["DETAIL_PICTURE"])
			{
				$arItem["DETAIL_PICTURE"]["ALT"] = $arItem["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"];
				if ($arItem["DETAIL_PICTURE"]["ALT"] == "")
					$arItem["DETAIL_PICTURE"]["ALT"] = $arItem["NAME"];
				$arItem["DETAIL_PICTURE"]["TITLE"] = $arItem["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"];
				if ($arItem["DETAIL_PICTURE"]["TITLE"] == "")
					$arItem["DETAIL_PICTURE"]["TITLE"] = $arItem["NAME"];
			}


			$arItem["PRICES"] = array();
			$arItem['MIN_PRICE'] = false;

			$arItem["PRICES"] = CIBlockPriceTools::GetItemPrices($this->arParams["IBLOCK_ID"], $arResult["PRICES"], $arItem, $this->arParams['PRICE_VAT_INCLUDE'], $arConvertParams);
			if (!empty($arItem["PRICES"]))
			{
				foreach ($arItem['PRICES'] as &$arOnePrice)
				{
					if ('Y' == $arOnePrice['MIN_PRICE'])
					{
						$arItem['MIN_PRICE'] = $arOnePrice;
						break;
					}
				}
				unset($arOnePrice);
			}


			if ('Y' == $this->arParams['CONVERT_CURRENCY'])
			{
				if (!empty($arItem["PRICES"]))
				{
					foreach ($arItem["PRICES"] as &$arOnePrices)
					{
						if (isset($arOnePrices['ORIG_CURRENCY']))
							$arCurrencyList[] = $arOnePrices['ORIG_CURRENCY'];
					}
					if (isset($arOnePrices))
						unset($arOnePrices);
				}
			}

			$arResult["ITEMS"][] = $arItem;
		}

		$this->arResult = $arResult;
	}

	/*
	 *
	 */
	public function IncludeTypeComponentTemplate($type){
		if(empty($type)){
			return false;
		}

		$type = mb_strtolower(trim($type));

		$typeTemplatePath = $this->getTemplate()->GetFolder().'/type/'.$type.'/'.$this->arParams['BLOCK_TEMPLATE'].'/template.php';

		if(file_exists($_SERVER['DOCUMENT_ROOT'].$typeTemplatePath)){
			extract(array('arParams' => $this->arParams, 'arResult' => $this->arResult), EXTR_SKIP);
			include($_SERVER['DOCUMENT_ROOT'].$typeTemplatePath);
		}
	}

	/*
	 *
	 */
	public function executeComponent(){
		global $USER, $APPLICATION;

		$this->initComponentTemplate();

		switch($this->arParams['BLOCK_TYPE']){
			case 'TEXT':
				$sFilePath = $this->getTemplate()->GetFolder().'/type/'.mb_strtolower($this->arParams['BLOCK_TYPE']).'/'.$this->arParams['BLOCK_TEMPLATE'].'/include/';
				$sFilePathTMP = $sFilePath;
				$sFileName = $this->arParams['BLOCK_TEXT_FILE'];

				$this->arResult['FILE_PATH'] = $sFilePath.$sFileName;
				$this->arResult['FILE_NAME'] = $sFileName;

				if(file_exists($sFilePath) && !empty($sFileName)){
					$editor = '&site='.SITE_ID.'&back_url='.urlencode($_SERVER['REQUEST_URI']).'&templateID='.urlencode(SITE_TEMPLATE_ID);
					$arIcons = array(
						array(
							"URL" => 'javascript:'.$APPLICATION->GetPopupLink(
									array(
										 'URL' => "/bitrix/admin/public_file_edit.php?lang=".LANGUAGE_ID."&from=lp.block&path=".urlencode($sFilePathTMP.$sFileName)."&template=''".$editor,
										 "PARAMS" => array(
											 'width' => 770,
											 'height' => 570,
											 'resize' => true,
											 "dialog_type" => 'EDITOR',
											 "min_width" => 700,
											 "min_height" => 400
										 )
									)
								),
							"DEFAULT" => $APPLICATION->GetPublicShowMode() != 'configure',
							"ICON" => "bx-context-toolbar-edit-icon",
							"TITLE" => GetMessage('EDIT_FILE_TITLE'),
							"ALT" => '',
							"MENU" => array(),
						),
					);
				}
			break;
			case 'ELEMENT_LIST':
				$this->getElements();
			break;
			case 'SECTION_LIST':
				$this->getSections();
			break;
			case 'CATALOG':
				$this->getCatalog();
			break;
			case 'FORM':

			break;
			case 'MAP':

			break;
		}

		if (is_array($arIcons) && count($arIcons) > 0)
		{
			$this->AddIncludeAreaIcons($arIcons);
		}

		$this->IncludeComponentTemplate();
		parent::executeComponent();
	}

} 