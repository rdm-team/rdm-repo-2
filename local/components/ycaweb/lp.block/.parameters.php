<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

$site = ($_REQUEST['site'] <> ''? $_REQUEST['site'] : ($_REQUEST['src_site'] <> ''? $_REQUEST['src_site'] : false));

if(!\Bitrix\Main\Loader::includeModule('iblock'))
	return;

$boolCatalog = \Bitrix\Main\Loader::includeModule('catalog');

$arTypesEx = CIBlockParameters::GetIBlockTypes(array('-'=>' '));

$arIBlocks = array();
$rsIBlock = CIBlock::GetList(Array('SORT'=>'ASC'), Array('SITE_ID' => $site, 'TYPE' => ($arCurrentValues['IBLOCK_TYPE']!='-'?$arCurrentValues['IBLOCK_TYPE']:'')));
while($arRes = $rsIBlock->Fetch())
	$arIBlocks[$arRes['ID']] = $arRes['NAME'];


$arAscDesc = array('ASC' => GetMessage('IBLOCK_DESC_ASC'), 'DESC' => GetMessage('IBLOCK_DESC_DESC'));
$arElementSortFields = CIBlockParameters::GetElementSortFields(
	array('ID', 'NAME', 'ACTIVE_FROM', 'SORT', 'TIMESTAMP_X'),
	array('KEY_LOWERCASE' => 'N')
);
$arSectionSortFields = CIBlockParameters::GetSectionSortFields(
	array('SORT', 'TIMESTAMP_X', 'NAME', 'ID', 'DEPTH_LEVEL'),
	array('KEY_LOWERCASE' => 'N')
);

$arPrice = array();
if ($boolCatalog){
	$arSort = array_merge($arSort, CCatalogIBlockParameters::GetCatalogSortFields());
	$rsPrice=CCatalogGroup::GetList($v1="sort", $v2="asc");
	while($arr=$rsPrice->Fetch()){
		$arPrice[$arr["NAME"]] = "[".$arr["NAME"]."] ".$arr["NAME_LANG"];
	}
}

$arBlockTypes = array(
	'TEXT'			=> GetMessage('LP_BLOCK_TYPE_TEXT'),
	'ELEMENT_LIST'	=> GetMessage('LP_BLOCK_TYPE_ELEMENT_LIST'),
	//'SECTION_LIST'	=> GetMessage('LP_BLOCK_TYPE_SECTION_LIST'),
	'CATALOG'		=> GetMessage('LP_BLOCK_TYPE_CATALOG'),
	'FORM'			=> GetMessage('LP_BLOCK_TYPE_FORM'),
	'MAP'			=> GetMessage('LP_BLOCK_TYPE_MAP'),
);

$arComponentParameters = array(
	'GROUPS' => array(
		'BLOCK_TYPE_SETTINGS' => array(
			'NAME' => GetMessage('GROUP_LP_BLOCK_TYPE_SETTINGS'),
			'SORT' => 150,
		)
	),
	'PARAMETERS' => array(
		'BLOCK_TYPE' => array(
			'PARENT' => 'BASE',
			'NAME' => GetMessage('LP_BLOCK_TYPE'),
			'TYPE' =>'LIST',
			'VALUES' => $arBlockTypes,
			'DEFAULT' => 'TEXT',
			'REFRESH' => 'Y',
		),
		'BLOCK_TITLE' => array(
			'PARENT' => 'BASE',
			'NAME' => GetMessage('LP_BLOCK_TITLE'),
			'TYPE' => 'STRING',
			'COLS' => 60,
			'DEFAULT' => '',
		),
		'BLOCK_ID' => array(
			'PARENT' => 'BASE',
			'NAME' => GetMessage('LP_BLOCK_ID'),
			'TYPE' => 'STRING',
			'DEFAULT' => '',
		),
		'BLOCK_CLASSNAME' => array(
			'PARENT' => 'BASE',
			'NAME' => GetMessage('LP_BLOCK_CLASSNAME'),
			'TYPE' => 'STRING',
			'DEFAULT' => '',
		),
		'BLOCK_WIDE' => array(
			'PARENT' => 'BASE',
			'NAME' => GetMessage('LP_BLOCK_WIDE'),
			'TYPE' => 'CHECKBOX',
			'DEFAULT' => '',
		),
		'CACHE_TIME'  =>  array('DEFAULT'=>36000000),
		'CACHE_FILTER' => array(
			'PARENT' => 'CACHE_SETTINGS',
			'NAME' => GetMessage('IBLOCK_CACHE_FILTER'),
			'TYPE' => 'CHECKBOX',
			'DEFAULT' => 'N',
		),
		'CACHE_GROUPS' => array(
			'PARENT' => 'CACHE_SETTINGS',
			'NAME' => GetMessage('IBLOCK_CACHE_GROUPS'),
			'TYPE' => 'CHECKBOX',
			'DEFAULT' => 'Y',
		),
	)
);

if($arCurrentValues['BLOCK_TYPE']){

	$blockType = mb_strtolower($arCurrentValues['BLOCK_TYPE']);

	$arLPBlockTypeTemplates = array();
	$curComponentName = $_GET['component_name'];
	$curComponentTemplateName = (!empty($_GET['component_template'])) ? $_GET['component_template'] : '.default';
	$curSiteTemplate = $_GET['template_id'];

	$arComponentTemplates = CComponentUtil::GetTemplatesList($curComponentName);
	$path2Comp = CComponentEngine::MakeComponentPath($curComponentName);

	if(!empty($arComponentTemplates)){
		$arFolders = array();

		foreach($arComponentTemplates as $arTplItem){
			if($arTplItem['NAME'] = $curComponentTemplateName){
				$arFolders[] = '/local/templates/'.$arTplItem['TEMPLATE'].'/components'.$path2Comp.'/'.$curComponentTemplateName;
				break;
			}
		}

		$arFolders[] = '/local/templates/'.$curSiteTemplate.'/components'.$path2Comp.'/'.$templateName;
		$arFolders[] = '/local/templates/.default/components'.$path2Comp.'/'.$curComponentTemplateName;
		$arFolders[] = '/local/components'.$path2Comp.'/templates/'.$curComponentTemplateName;

		$curTemplateDir = '';
		foreach($arFolders as $folder){
			$path = $_SERVER['DOCUMENT_ROOT'].$folder.'/';
			if(file_exists($path.'template.php')){

				if($handle = opendir($path.'/type/'.$blockType.'/')){
					while(false !== ($file = readdir($handle))){
						if($file !== '.' && $file !== '..'){
							$arLPBlockTypeTemplates[$file] = $file;
						}
						if($file == $arCurrentValues['BLOCK_TEMPLATE']){
							$curTemplateDir = $folder.'/type/'.$blockType.'/'.$file.'/';
						}
					}
					closedir($handle);
				}

				break;
			}
		}
	}

	$arComponentParameters['PARAMETERS']['BLOCK_TEMPLATE'] = array(
		'PARENT' => 'BASE',
		'NAME' => GetMessage('LP_BLOCK_TEMPLATE'),
		'TYPE' => 'LIST',
		'VALUES' => $arLPBlockTypeTemplates,
		'DEFAULT' => '.default'
	);

	if($arCurrentValues['BLOCK_TYPE'] == 'TEXT'){

		$path = $_SERVER['DOCUMENT_ROOT'].$curTemplateDir.'include/';
		if($handle = opendir($path)){
			while(false !== ($file = readdir($handle))){
				if($file !== '.' && $file !== '..'){
					$arLPBlockTextFiles[$file] = $file;
				}
			}
			closedir($handle);
		}

		$arComponentParameters['PARAMETERS']['BLOCK_TEXT_FILE'] = array(
			'PARENT' => 'BASE',
			'NAME' => GetMessage('LP_BLOCK_TEXT_FILE'),
			'TYPE' => 'LIST',
			'VALUES' => $arLPBlockTextFiles,
			'DEFAULT' => '',
			'ADDITIONAL_VALUES' => 'Y',
		);
	}
}

if($arCurrentValues['BLOCK_TYPE'] == 'TEXT'){
	$arAdditionalParameters = array();
}elseif($arCurrentValues['BLOCK_TYPE'] == 'ELEMENT_LIST'){
	$arAdditionalParameters = array(
		'IBLOCK_TYPE' => array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('IBLOCK_TYPE'),
			'TYPE' => 'LIST',
			'VALUES' => $arTypesEx,
			'REFRESH' => 'Y',
		),
		'IBLOCK_ID' => array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('IBLOCK_IBLOCK'),
			'TYPE' => 'LIST',
			'ADDITIONAL_VALUES' => 'Y',
			'VALUES' => $arIBlocks,
			'REFRESH' => 'Y',
		),
		'FILTER_NAME' => array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('IBLOCK_FILTER_NAME'),
			'TYPE' => 'STRING',
			'DEFAULT' => '',
		),
		'ELEMENT_COUNT' => array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('IBLOCK_ELEMENT_COUNT'),
			'TYPE' => 'STRING',
			'COLS' => 5,
			'DEFAULT' => '20',
		),
		'ELEMENT_SORT_FIELD' => array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('IBLOCK_ELEMENT_SORT_FIELD'),
			'TYPE' => 'LIST',
			'VALUES' => $arElementSortFields,
			'ADDITIONAL_VALUES' => 'Y',
			'DEFAULT' => 'SORT',
		),
		'ELEMENT_SORT_ORDER' => array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('IBLOCK_ELEMENT_SORT_ORDER'),
			'TYPE' => 'LIST',
			'VALUES' => $arAscDesc,
			'DEFAULT' => 'ASC',
			'ADDITIONAL_VALUES' => 'Y',
		),
		'ELEMENT_SORT_FIELD2' => array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('IBLOCK_ELEMENT_SORT_FIELD2'),
			'TYPE' => 'LIST',
			'VALUES' => $arElementSortFields,
			'ADDITIONAL_VALUES' => 'Y',
			'DEFAULT' => 'ID',
		),
		'ELEMENT_SORT_ORDER2' => array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('IBLOCK_ELEMENT_SORT_ORDER2'),
			'TYPE' => 'LIST',
			'VALUES' => $arAscDesc,
			'DEFAULT' => 'DESC',
			'ADDITIONAL_VALUES' => 'Y',
		),
	);
}elseif($arCurrentValues['BLOCK_TYPE'] == 'SECTION_LIST'){
	$arAdditionalParameters = array(
		'IBLOCK_TYPE' => array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('IBLOCK_TYPE'),
			'TYPE' => 'LIST',
			'VALUES' => $arTypesEx,
			'REFRESH' => 'Y',
		),
		'IBLOCK_ID' => array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('IBLOCK_IBLOCK'),
			'TYPE' => 'LIST',
			'ADDITIONAL_VALUES' => 'Y',
			'VALUES' => $arIBlocks,
			'REFRESH' => 'Y',
		),
		'FILTER_NAME' => array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('IBLOCK_FILTER_NAME'),
			'TYPE' => 'STRING',
			'DEFAULT' => '',
		),
		'SECTION_COUNT' => array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('IBLOCK_SECTION_COUNT'),
			'TYPE' => 'STRING',
			'COLS' => 5,
			'DEFAULT' => '20',
		),
		'DEPTH_LEVEL' => array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('IBLOCK_SECTION_DEPTH_LEVEL'),
			'TYPE' => 'STRING',
			'COLS' => 5,
			'DEFAULT' => '1',
		),
		'SECTION_SORT_FIELD' => array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('IBLOCK_SECTION_SORT_FIELD'),
			'TYPE' => 'LIST',
			'VALUES' => $arSectionSortFields,
			'ADDITIONAL_VALUES' => 'Y',
			'DEFAULT' => 'sort',
		),
		'SECTION_SORT_ORDER' => array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('IBLOCK_SECTION_SORT_ORDER'),
			'TYPE' => 'LIST',
			'VALUES' => $arAscDesc,
			'ADDITIONAL_VALUES' => 'Y',
			'DEFAULT' => 'asc',
		),
	);
}elseif($arCurrentValues['BLOCK_TYPE'] == 'CATALOG' && $boolCatalog){
	$arAdditionalParameters = array(
		'IBLOCK_TYPE' => array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('IBLOCK_TYPE'),
			'TYPE' => 'LIST',
			'VALUES' => $arTypesEx,
			'REFRESH' => 'Y',
		),
		'IBLOCK_ID' => array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('IBLOCK_IBLOCK'),
			'TYPE' => 'LIST',
			'ADDITIONAL_VALUES' => 'Y',
			'VALUES' => $arIBlocks,
			'REFRESH' => 'Y',
		),
		'SECTION_ID' => array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('IBLOCK_SECTION_ID'),
			'TYPE' => 'STRING',
			'DEFAULT' => '',
		),
		'SECTION_CODE' => array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('IBLOCK_SECTION_CODE'),
			'TYPE' => 'STRING',
			'DEFAULT' => '',
		),
		'FILTER_NAME' => array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('IBLOCK_FILTER_NAME'),
			'TYPE' => 'STRING',
			'DEFAULT' => '',
		),
		'ELEMENT_COUNT' => array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('IBLOCK_ELEMENT_COUNT'),
			'TYPE' => 'STRING',
			'COLS' => 5,
			'DEFAULT' => '20',
		),
		'ELEMENT_SORT_FIELD' => array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('IBLOCK_ELEMENT_SORT_FIELD'),
			'TYPE' => 'LIST',
			'VALUES' => $arElementSortFields,
			'ADDITIONAL_VALUES' => 'Y',
			'DEFAULT' => 'SORT',
		),
		'ELEMENT_SORT_ORDER' => array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('IBLOCK_ELEMENT_SORT_ORDER'),
			'TYPE' => 'LIST',
			'VALUES' => $arAscDesc,
			'DEFAULT' => 'ASC',
			'ADDITIONAL_VALUES' => 'Y',
		),
		'ELEMENT_SORT_FIELD2' => array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('IBLOCK_ELEMENT_SORT_FIELD2'),
			'TYPE' => 'LIST',
			'VALUES' => $arElementSortFields,
			'ADDITIONAL_VALUES' => 'Y',
			'DEFAULT' => 'ID',
		),
		'ELEMENT_SORT_ORDER2' => array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('IBLOCK_ELEMENT_SORT_ORDER2'),
			'TYPE' => 'LIST',
			'VALUES' => $arAscDesc,
			'DEFAULT' => 'DESC',
			'ADDITIONAL_VALUES' => 'Y',
		),
		'PRICE_CODE' => array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('IBLOCK_PRICE_CODE'),
			'TYPE' => 'LIST',
			'MULTIPLE' => 'Y',
			'VALUES' => $arPrice,
		),
		'CONVERT_CURRENCY' => array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('IBLOCK_CONVERT_CURRENCY'),
			'TYPE' => 'CHECKBOX',
			'DEFAULT' => 'N',
			'REFRESH' => 'Y',
		),
	);

	if (isset($arCurrentValues['CONVERT_CURRENCY']) && 'Y' == $arCurrentValues['CONVERT_CURRENCY'])
	{
		$arCurrencyList = array();
		$by = 'SORT';
		$order = 'ASC';
		$rsCurrencies = CCurrency::GetList($by, $order);
		while($arCurrency = $rsCurrencies->Fetch())
		{
			$arCurrencyList[$arCurrency['CURRENCY']] = $arCurrency['CURRENCY'];
		}
		$arAdditionalParameters['CURRENCY_ID'] = array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('IBLOCK_CURRENCY_ID'),
			'TYPE' => 'LIST',
			'VALUES' => $arCurrencyList,
			'DEFAULT' => CCurrency::GetBaseCurrency(),
		);
	}

}elseif($arCurrentValues['BLOCK_TYPE'] == 'FORM'){
	$arAdditionalParameters = array();
}elseif($arCurrentValues['BLOCK_TYPE'] == 'MAP'){
	$arAdditionalParameters = array(
		'LONLAT' => array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('MAP_LONLAT'),
			'TYPE' => 'STRING',
			'DEFAULT' => '',
		),
		'BALLOON_TITLE' => array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('MAP_BALLOON_TITLE'),
			'TYPE' => 'STRING',
			'DEFAULT' => '',
		),
		'BALLOON_CONTENT' => array(
			'PARENT' => 'BLOCK_TYPE_SETTINGS',
			'NAME' => GetMessage('MAP_BALLOON_CONTENT'),
			'TYPE' => 'STRING',
			'DEFAULT' => '',
		),
	);
}
if(!empty($arAdditionalParameters)){
	$arComponentParameters['PARAMETERS'] = array_merge($arComponentParameters['PARAMETERS'], $arAdditionalParameters);
}