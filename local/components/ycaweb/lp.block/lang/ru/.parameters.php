<?
$MESS['GROUP_LP_BLOCK_TYPE_SETTINGS'] = 'Настройки блока';
$MESS['LP_BLOCK_TYPE'] = 'Тип блока';
$MESS['LP_BLOCK_TYPE_CATALOG'] = 'Каталог';
$MESS['LP_BLOCK_TYPE_ELEMENT_LIST'] = 'Список элементов';
$MESS['LP_BLOCK_TYPE_SECTION_LIST'] = 'Список разделов';
$MESS['LP_BLOCK_TYPE_TEXT'] = 'Текст/HTML';
$MESS['LP_BLOCK_TYPE_FORM'] = 'Форма';
$MESS['LP_BLOCK_TYPE_MAP'] = 'Карта';
$MESS['LP_BLOCK_TITLE'] = 'Заголовок блока';
$MESS['LP_BLOCK_ID'] = 'ID блока';
$MESS['LP_BLOCK_CLASSNAME'] = 'Класс блока';
$MESS['LP_BLOCK_TEMPLATE'] = 'Шаблон блока';
$MESS['LP_BLOCK_WIDE'] = 'Широкий шаблон блока';
$MESS['LP_BLOCK_TEXT_FILE'] = 'Название шаблона текстового блока';

$MESS['IBLOCK_TYPE'] = 'Тип инфоблок';
$MESS['IBLOCK_IBLOCK'] = 'Инфоблок';
$MESS['IBLOCK_FILTER_NAME'] = 'Фильтр';
$MESS['IBLOCK_SECTION_ID'] = 'ID раздела';
$MESS['IBLOCK_SECTION_CODE'] = 'Код раздела';
$MESS['IBLOCK_ELEMENT_SORT_FIELD'] = 'Поле для первой сортировки';
$MESS['IBLOCK_ELEMENT_SORT_ORDER'] = 'Направление для первой сортировки';
$MESS['IBLOCK_ELEMENT_SORT_FIELD2'] = 'Поле для второй сортировки';
$MESS['IBLOCK_ELEMENT_SORT_ORDER2'] = 'Направление для второй сортировки';
$MESS['IBLOCK_DESC_ASC'] = 'По возрастанию';
$MESS['IBLOCK_DESC_DESC'] = 'По убыванию';
$MESS['IBLOCK_SECTION_SORT_FIELD'] = 'Поле для сортировки';
$MESS['IBLOCK_SECTION_SORT_ORDER'] = 'Направление для сортировки';
$MESS['IBLOCK_ELEMENT_COUNT'] = 'Количество элементов';
$MESS['IBLOCK_SECTION_COUNT'] = 'Количество разделов';
$MESS['IBLOCK_SECTION_DEPTH_LEVEL'] = 'Максимальная глубина разделов';

$MESS['IBLOCK_PRICE_CODE'] = 'Тип цены';
$MESS['IBLOCK_CONVERT_CURRENCY'] = 'Показывать цены в одной валюте';
$MESS['IBLOCK_CURRENCY_ID'] = 'Валюта, в которую будут сконвертированы цены';

$MESS['MAP_BALLOON_TITLE'] = 'Заголовок балуна';
$MESS['MAP_BALLOON_CONTENT'] = 'Контент балуна';
$MESS['MAP_LONLAT'] = 'Координаты точки';

$MESS['IBLOCK_CACHE_FILTER'] = 'Кешировать при установленном фильтре';
$MESS['IBLOCK_CACHE_GROUPS'] = 'Учитывать права доступа';