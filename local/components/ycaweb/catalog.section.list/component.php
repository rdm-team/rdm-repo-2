<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */

/** @global CMain $APPLICATION */

use Bitrix\Iblock;
use Bitrix\Main\Loader;
/*************************************************************************
 * 
 *************************************************************************/
function sortDesc($a, $b) 
{

    
    if ($a["COUNT"] == $b["COUNT"]) {
        return 0;
    }    
    return ($a["COUNT"] > $b["COUNT"]) ? -1 : 1;
    
}    
/*************************************************************************
 * Processing of received parameters
 *************************************************************************/
if (!isset($arParams["CACHE_TIME"]))
    $arParams["CACHE_TIME"] = 36000000;

$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
$arParams["SECTION_ID"] = intval($arParams["SECTION_ID"]);
$arParams["SECTION_CODE"] = trim($arParams["SECTION_CODE"]);

$arParams["SECTION_URL"] = trim($arParams["SECTION_URL"]);

$arParams["TOP_DEPTH"] = intval($arParams["TOP_DEPTH"]);
if ($arParams["TOP_DEPTH"] <= 0)
    $arParams["TOP_DEPTH"] = 2;
$arParams["COUNT_ELEMENTS"] = $arParams["COUNT_ELEMENTS"] != "N";
$arParams["ADD_SECTIONS_CHAIN"] = $arParams["ADD_SECTIONS_CHAIN"] != "N"; //Turn on by default

$arResult["SECTIONS"] = array();

/*************************************************************************
 * Work with cache
 *************************************************************************/
if ($this->startResultCache(false, ($arParams["CACHE_GROUPS"] === "N" ? false : $USER->GetGroups()))) {
    if (!Loader::includeModule("iblock")) {
        $this->abortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }

    $arFilter = array(
        "ACTIVE" => "Y",
        "GLOBAL_ACTIVE" => "Y",
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "CNT_ACTIVE" => "Y",

    );




    $arSelect = array();
    if (array_key_exists("SECTION_FIELDS", $arParams) && !empty($arParams["SECTION_FIELDS"]) && is_array($arParams["SECTION_FIELDS"])) {
        foreach ($arParams["SECTION_FIELDS"] as &$field) {
            if (!empty($field) && is_string($field))
                $arSelect[] = $field;
        }
        if (isset($field))
            unset($field);
    }

    if (!empty($arSelect)) {
        $arSelect[] = "ID";
        $arSelect[] = "NAME";
        $arSelect[] = "LEFT_MARGIN";
        $arSelect[] = "RIGHT_MARGIN";
        $arSelect[] = "DEPTH_LEVEL";
        $arSelect[] = "IBLOCK_ID";
        $arSelect[] = "IBLOCK_SECTION_ID";
        $arSelect[] = "LIST_PAGE_URL";
        $arSelect[] = "SECTION_PAGE_URL";

    }
    $boolPicture = empty($arSelect) || in_array('PICTURE', $arSelect);

    if (isset($arParams['SECTION_USER_FIELDS']) && !empty($arParams["SECTION_USER_FIELDS"]) && is_array($arParams["SECTION_USER_FIELDS"])) {
        foreach ($arParams["SECTION_USER_FIELDS"] as &$field) {
            if (is_string($field) && preg_match("/^UF_/", $field))
                $arSelect[] = $field;
        }
        if (isset($field))
            unset($field);
    }

    $arResult["SECTION"] = false;
    $intSectionDepth = 0;
    if ($arParams["SECTION_ID"] > 0) {
        $arFilter["ID"] = $arParams["SECTION_ID"];
        $rsSections = CIBlockSection::GetList(array(), $arFilter, $arParams["COUNT_ELEMENTS"], $arSelect);
        $rsSections->SetUrlTemplates("", $arParams["SECTION_URL"]);
        $arResult["SECTION"] = $rsSections->GetNext();
    } elseif ('' != $arParams["SECTION_CODE"]) {

        if ($arParams['PARENT_SECTION_CODE']) {
            $arFilter["CODE"] = $arParams["PARENT_SECTION_CODE"];
            $rsSections = CIBlockSection::GetList(array(), $arFilter, $arParams["COUNT_ELEMENTS"], $arSelect)->GetNext();

            $arFilter["SECTION_ID"] = $rsSections['ID'];
            unset($arFilter["CODE"]);
        }
        $arFilter["=CODE"] = $arParams["SECTION_CODE"];


        $rsSections = CIBlockSection::GetList(array(), $arFilter, $arParams["COUNT_ELEMENTS"], $arSelect);
        $rsSections->SetUrlTemplates("", $arParams["SECTION_URL"]);
        $arResult["SECTION"] = $rsSections->GetNext();

        unset($arFilter["SECTION_ID"]);
    }

    if (is_array($arResult["SECTION"])) {
        unset($arFilter["ID"]);
        unset($arFilter["=CODE"]);
        $arFilter["LEFT_MARGIN"] = $arResult["SECTION"]["LEFT_MARGIN"] + 1;
        $arFilter["RIGHT_MARGIN"] = $arResult["SECTION"]["RIGHT_MARGIN"];
        $arFilter["<=" . "DEPTH_LEVEL"] = $arResult["SECTION"]["DEPTH_LEVEL"] + $arParams["TOP_DEPTH"];

        $ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues($arResult["SECTION"]["IBLOCK_ID"], $arResult["SECTION"]["ID"]);
        $arResult["SECTION"]["IPROPERTY_VALUES"] = $ipropValues->getValues();

        $arResult["SECTION"]["PATH"] = array();
        $rsPath = CIBlockSection::GetNavChain(
            $arResult["SECTION"]["IBLOCK_ID"],
            $arResult["SECTION"]["ID"],
            array(
                "ID", "CODE", "XML_ID", "EXTERNAL_ID", "IBLOCK_ID",
                "IBLOCK_SECTION_ID", "SORT", "NAME", "ACTIVE",
                "DEPTH_LEVEL", "SECTION_PAGE_URL"
            )
        );
        $rsPath->SetUrlTemplates("", $arParams["SECTION_URL"]);
        while ($arPath = $rsPath->GetNext()) {
            if ($arParams["ADD_SECTIONS_CHAIN"]) {
                $ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues($arParams["IBLOCK_ID"], $arPath["ID"]);
                $arPath["IPROPERTY_VALUES"] = $ipropValues->getValues();
            }
            $arResult["SECTION"]["PATH"][] = $arPath;
        }
    } else {
        $arResult["SECTION"] = array("ID" => 0, "DEPTH_LEVEL" => 0);
        $arFilter["<=" . "DEPTH_LEVEL"] = $arParams["TOP_DEPTH"];
    }
    $intSectionDepth = $arResult["SECTION"]['DEPTH_LEVEL'];
    $arFilter['PROPERTY'] = array('STATE' => '110');
    //ORDER BY
    $arSort = array(
        "left_margin" => "asc",
    );
	
	$arTemp = array();
	$arSect = array();
	
	$ind = 0;
	$html = "";
	
	
    //EXECUTE
    $rsSections = CIBlockSection::GetList($arSort, $arFilter, $arParams["COUNT_ELEMENTS"], $arSelect);
    $rsSections->SetUrlTemplates("", $arParams["SECTION_URL"]);
    while ($arSection = $rsSections->GetNext()) {
        $ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues($arSection["IBLOCK_ID"], $arSection["ID"]);
        $arSection["IPROPERTY_VALUES"] = $ipropValues->getValues();

        if ($boolPicture) {
            Iblock\Component\Tools::getFieldImageData(
                $arSection,
                array('PICTURE'),
                Iblock\Component\Tools::IPROPERTY_ENTITY_SECTION,
                'IPROPERTY_VALUES'
            );
        }
        $arSection['RELATIVE_DEPTH_LEVEL'] = $arSection['DEPTH_LEVEL'] - $intSectionDepth;

        $arButtons = CIBlock::GetPanelButtons(
            $arSection["IBLOCK_ID"],
            0,
            $arSection["ID"],
            array("SESSID" => false, "CATALOG" => true)
        );
        $arSection["EDIT_LINK"] = $arButtons["edit"]["edit_section"]["ACTION_URL"];
        $arSection["DELETE_LINK"] = $arButtons["edit"]["delete_section"]["ACTION_URL"];

        $arResult["SECTIONS"][] = $arSection;
		
/*
		
		if($arSection['RELATIVE_DEPTH_LEVEL'] == 1){
			$ind++;
			$arTemp[] = array(
			"NAME"=> $arSection['NAME'], 
			"CODE"=> $arSection['CODE'], 
			"RELATIVE_DEPTH_LEVEL"=> $arSection['RELATIVE_DEPTH_LEVEL'], 
			"SECTION_PAGE_URL"=> $arSection['SECTION_PAGE_URL'], 
			"ENT"=> $arSection['ELEMENT_CNT'], 
			
			);
			
			$arTest[] = array(
			"ID"=> $arSection['ID'], 
			"NAME"=> $arSection['NAME'], 
			"CODE"=> $arSection['CODE'], 
			"RELATIVE_DEPTH_LEVEL"=> $arSection['RELATIVE_DEPTH_LEVEL'], 
			"SECTION_PAGE_URL"=> $arSection['SECTION_PAGE_URL'], 
			"ENT"=> $arSection['ELEMENT_CNT'], 
			"arSection"=> $arSection, 
			);
			
			$html .= "<div style='display:inline-block;width:100%;color:#000'>[".$arSection['RELATIVE_DEPTH_LEVEL']."] " . $arSection['NAME']."</div><br>\r\n";
			
		}else{
			
			$html .= "<div style='display:inline-block;width:100%;color:#ff0000'>[".$arSection['RELATIVE_DEPTH_LEVEL']."] " . $arSection['NAME']."</div><br>\r\n";
		}
		*/
		
    }

    $arResult["SECTIONS_COUNT"] = count($arResult["SECTIONS"]);

    $this->setResultCacheKeys(array(
        "SECTIONS_COUNT",
        "SECTION",
    ));
	/*
	usort($arTemp, "sortDesc");	
	$arSect = array_splice($arTemp,0,12);
	sort($arSect);
	
	$html .= "count: " . $arResult["SECTIONS_COUNT"] . "<br>";
	$html .= "ind: " . $ind . "<br>";
	
	$file = fopen($_SERVER['DOCUMENT_ROOT'].'/fastlink.html', 'w+');
	fwrite($file, $html);
	fclose($file);
	*/
if(isset($_REQUEST['filter']['MANUFACTURER'])){	
	$MANUFACTURER = htmlspecialchars($_REQUEST['filter']['MANUFACTURER'], ENT_QUOTES);
	$arFilter = Array("IBLOCK_ID"=>5, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y","PROPERTY_STATE"=>110,"PROPERTY_MANUFACTURER"=>$MANUFACTURER);
	$model = true;
}else{
	
	$arFilter = Array("IBLOCK_ID"=>5, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y","PROPERTY_STATE"=>110);
	$model = false;
}

//============================	
$arMarka = array();
$arModel = array();

$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_STATE","PROPERTY_MANUFACTURER","PROPERTY_MODEL","IBLOCK_SECTION_ID");

$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($ob = $res->GetNextElement())
{
	
	$arFields = $ob->GetFields();

	if($model){
		$arModel[$arFields['PROPERTY_MODEL_VALUE']]['CODE'] = $arFields["PROPERTY_MODEL_VALUE"];
		$arModel[$arFields['PROPERTY_MODEL_VALUE']]['URL'] = '/auto/'.strtolower($arFields["PROPERTY_MANUFACTURER_VALUE"]).'/'.strtolower($arFields["PROPERTY_MODEL_VALUE"]).'/';
		$arModel[$arFields['PROPERTY_MODEL_VALUE']]['ITEMS'][] = true;
	 
	 }else{	
	 
		$arMarka[$arFields['PROPERTY_MANUFACTURER_VALUE']]['CODE'] = $arFields["PROPERTY_MANUFACTURER_VALUE"];
		$arMarka[$arFields['PROPERTY_MANUFACTURER_VALUE']]['URL'] = '/auto/'.strtolower($arFields["PROPERTY_MANUFACTURER_VALUE"]).'/';
		$arMarka[$arFields['PROPERTY_MANUFACTURER_VALUE']]['ITEMS'][] = true;

	}
}

	if($model){
		foreach($arModel as $k=>$v){ $arModel[$k]['COUNT'] = count($v['ITEMS']); }	
		usort($arModel, "sortDesc");	
		$arModel = array_splice($arModel,0,12);
		sort($arModel);
		
		$arResult['MODIFY'] = $arModel;
	}else{
		
		foreach($arMarka as $k=>$v){	$arMarka[$k]['COUNT'] = count($v['ITEMS']); }	
		usort($arMarka, "sortDesc");	
		$arMarka = array_splice($arMarka,0,12);
		sort($arMarka);
		
		$arResult['MODIFY'] = $arMarka;
	}

//============================

    $this->includeComponentTemplate();
}

if ($arResult["SECTIONS_COUNT"] > 0 || isset($arResult["SECTION"])) {
    if (
        $USER->IsAuthorized()
        && $APPLICATION->GetShowIncludeAreas()
        && Loader::includeModule("iblock")
    ) {
        $UrlDeleteSectionButton = "";
        if (isset($arResult["SECTION"]) && $arResult["SECTION"]['IBLOCK_SECTION_ID'] > 0) {
            $rsSection = CIBlockSection::GetList(
                array(),
                array("=ID" => $arResult["SECTION"]['IBLOCK_SECTION_ID']),
                false,
                array("SECTION_PAGE_URL")
            );
            $rsSection->SetUrlTemplates("", $arParams["SECTION_URL"]);
            $arSection = $rsSection->GetNext();
            $UrlDeleteSectionButton = $arSection["SECTION_PAGE_URL"];
        }

        if (empty($UrlDeleteSectionButton)) {
            $url_template = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "LIST_PAGE_URL");
            $arIBlock = CIBlock::GetArrayByID($arParams["IBLOCK_ID"]);
            $arIBlock["IBLOCK_CODE"] = $arIBlock["CODE"];
            $UrlDeleteSectionButton = CIBlock::ReplaceDetailUrl($url_template, $arIBlock, true, false);
        }

        $arReturnUrl = array(
            "add_section" => (
            '' != $arParams["SECTION_URL"] ?
                $arParams["SECTION_URL"] :
                CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_PAGE_URL")
            ),
            "add_element" => (
            '' != $arParams["SECTION_URL"] ?
                $arParams["SECTION_URL"] :
                CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_PAGE_URL")
            ),
            "delete_section" => $UrlDeleteSectionButton,
        );
        $arButtons = CIBlock::GetPanelButtons(
            $arParams["IBLOCK_ID"],
            0,
            $arResult["SECTION"]["ID"],
            array("RETURN_URL" => $arReturnUrl, "CATALOG" => true)
        );

        $this->addIncludeAreaIcons(CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arButtons));
    }

    if ($arParams["ADD_SECTIONS_CHAIN"] && isset($arResult["SECTION"]) && is_array($arResult["SECTION"]["PATH"])) {

        foreach ($arResult["SECTION"]["PATH"] as $arPath) {
            if ($arParams['CHAIN_FROM_META'] && isset($arPath["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"]) && $arPath["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"] != "")
                $APPLICATION->AddChainItem($arPath["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"], $arPath["~SECTION_PAGE_URL"]);
            else
                $APPLICATION->AddChainItem($arPath["NAME"], $arPath["~SECTION_PAGE_URL"]);
        }
    }


}