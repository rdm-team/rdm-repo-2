<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
{
	ShowError(GetMessage("CC_BCF_MODULE_NOT_INSTALLED"));
	return;
}
/*************************************************************************
	Processing of received parameters
*************************************************************************/
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;
if( !isset($arParams["FOLDER"]) || $arParams["FOLDER"] == ""){
	$arParams["FOLDER"] = "/auto/view/";
}
unset($arParams["IBLOCK_TYPE"]); //was used only for IBLOCK_ID setup with Editor
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);

$arParams["SAVE_IN_SESSION"] = $arParams["SAVE_IN_SESSION"]=="Y";

if(strlen($arParams["FILTER_NAME"])<=0|| !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
	$arParams["FILTER_NAME"] = "arrFilter";
$FILTER_NAME = $arParams["FILTER_NAME"];

global $$FILTER_NAME;
$$FILTER_NAME = array();

$arResult = array();
$arResult["CUR_PAGE"] = $APPLICATION->getCurPage(false);

if(strlen($_REQUEST['start_filter']) > 0) {
	//redirect to catalog
	if ($arResult["CUR_PAGE"] == $arParams["FOLDER"])
	{
		$arRedirect = array();
		if ($_REQUEST["filter"]["MANUFACTURER"])
		{
			$arRedirect[] = toLower($_REQUEST["filter"]["MANUFACTURER"]);

			if ($_REQUEST["filter"]["MODEL"])
			{
				$arRedirect[] = toLower($_REQUEST["filter"]["MODEL"]);

				if ($_REQUEST["filter"]["YEAR"]["FROM"] && $_REQUEST["filter"]["YEAR"]["TO"] &&
					$_REQUEST["filter"]["YEAR"]["FROM"] == $_REQUEST["filter"]["YEAR"]["TO"])
					$arRedirect[] = toLower($_REQUEST["filter"]["YEAR"]["FROM"]);
			}	
		}

		if (count($arRedirect) > 0)
		{
			$arQuery = explode('&', $_SERVER['QUERY_STRING']);
			foreach ($arQuery as $k => $value)
			{
				if (stripos($value, 'MANUFACTURER') !== false ||
					stripos($value, 'MODEL') !== false ||
					stripos($value, 'YEAR') !== false ||
					stripos($value, '=') == strlen($value) - 1
				) unset($arQuery[$k]);
			}
			$url = $arParams["FOLDER"]. implode('/', $arRedirect) . '/' . (count($arQuery) > 1 ? '?'. $_SERVER['QUERY_STRING'] : '');
			LocalRedirect($url, true, '301 Moved permanently');
		}
	}
	//

	if(isset($_REQUEST['filter']))
	{
		$arSetFilter = $_REQUEST['filter'];
		$arSetFilter['MANUFACTURER'] = str_replace('_', ' ', $arSetFilter['MANUFACTURER']);
		$arSetFilter['MODEL'] = str_replace('_', ' ', $arSetFilter['MODEL']);
	}
}
/*No action specified, so read from the session (if parameter is set)*/
elseif($arParams["SAVE_IN_SESSION"])
{
	if(isset($_SESSION[$FILTER_NAME]))
		$arSetFilter = $_SESSION[$FILTER_NAME];
}
/*
elseif(isset($_REQUEST["del_filter"]))
{

}
*/

/*Save filter values to the session*/
if($arParams["SAVE_IN_SESSION"])
{
	$_SESSION[$FILTER_NAME] = $arSetFilter;
}
	$arTaxonomy = YcawebHelper::getTaxonomy();
	$arResult["IBLOCK_ID"] = 3; //catalog

	$ID_FIRM_SELECTED = 0;
	foreach($arTaxonomy as $code=>$taxonomy){
		foreach($taxonomy as $itemID=>$taxItem){

			$arFilterItem = array(
				'for'	=> 'option-'.$taxItem["ID"],
				'name'	=> 'filter['.$code.']',
				'id'	=> $taxItem["ID"],
				'label'	=> $taxItem["NAME"],
				'value'	=> $taxItem["ID"],
			);
			$arFilterItem['selected'] = ((is_array($arSetFilter[$code]) && in_array($arFilterItem['value'], $arSetFilter[$code])) || $arSetFilter[$code] == $arFilterItem['value'] || stripos($arResult["CUR_PAGE"], $arFilterItem['value']) !== false);

			if($code == "MANUFACTURER" && $arFilterItem['selected'])
			{
				$ID_FIRM_SELECTED = $arFilterItem["id"];
				$setManufacturer = $arFilterItem["id"];
			}
			if($code == "YEAR")
			{
				$arFilterItem['selected'] = false;
			}
			if($arFilterItem['selected'])
			{
				$arResult[$code."_IS_SET"] = true;
			}
			if(isset($taxItem['CODE']))
			{
				$arFilterItem['code'] = $taxItem['CODE'];
			}
			if(isset($taxItem['COLOR_CODE']))
			{
				$arFilterItem['color'] = $taxItem['COLOR_CODE'];
			}
			if ($code == "CAR_BODY"){
				$arFilterItem['selected'] = in_array($arFilterItem['id'], $arSetFilter['CAR_BODY']);
				$arFilterItem['value'] = $taxItem["XML_ID"];
			}
			$arResult["SET"][$code][] = $arFilterItem;
		}
	}

	// инфоблок с объявлениями
	$adsIblockID = YcawebHelper::getIblockID("car-ads");


	// годы выпуска авто - из свойства типа "список"
	$arResult['SET']['YEAR'] = array();
	$yearPropID = YcawebHelper::getPropId($adsIblockID, 'MODEL_YEAR');
	$yearFrom = (int)$arSetFilter["YEAR"]['FROM'];
	$yearTo = (int)$arSetFilter["YEAR"]['TO'];

	$dbYears = CIBlockPropertyEnum::GetList(array("SORT" => "ASC"), array('IBLOCK_ID' => $adsIblockID, "PROPERTY_ID" => $yearPropID));

	while($arYear = $dbYears->fetch())
	{
		$arYearItem = array(
			'for'	=> 'option-'.$arYear["ID"],
			'name'	=> 'filter['.$arYear['PROPERTY_CODE'].']',
			'id'	=> $arYear["ID"],
			'label'	=> $arYear["VALUE"],
			'value'	=> $arYear["VALUE"],
		);
		$arYearItem['selected']['from'] = ($arSetFilter["YEAR"]['FROM'] == $arYearItem['value'] || stripos($arResult["CUR_PAGE"], $arYearItem['value']) !== false);
		$arYearItem['selected']['to'] = ($arSetFilter["YEAR"]['TO'] == $arYearItem['value']  || stripos($arResult["CUR_PAGE"], $arYearItem['value']) !== false);

		$arResult['SET']['YEAR'][] = $arYearItem;
	}

	// объем двигателя - из свойства типа "список"
	$arResult['SET']['ENGINE'] = array();
	$yearPropID = YcawebHelper::getPropId($adsIblockID, 'ENGINE');
	$dbYears = CIBlockPropertyEnum::GetList(array("SORT" => "ASC"), array('IBLOCK_ID' => $adsIblockID, "PROPERTY_ID" => $yearPropID));

	while($arYear = $dbYears->fetch())
	{
		$arYearItem = array(
			'for'	=> 'option-'.$arYear["ID"],
			'name'	=> 'filter['.$arYear['PROPERTY_CODE'].']',
			'id'	=> $arYear["ID"],
			'label'	=> $arYear["VALUE"],
			'value'	=> $arYear["VALUE"],
		);
		$arYearItem['selected']['from'] = ($arSetFilter["ENGINE"]['FROM'] == $arYearItem['value']);
		$arYearItem['selected']['to'] = ($arSetFilter["ENGINE"]['TO'] == $arYearItem['value']);

		$arResult['SET']['ENGINE'][] = $arYearItem;
	}

	// руль - из свойства типа "список"
	$arResult['SET']['STEERING_WHEEL'] = array();
	$wheelPropID = YcawebHelper::getPropId($adsIblockID, 'STEERING_WHEEL');
	$dbWheels = CIBlockPropertyEnum::GetList(array("SORT" => "ASC"), array('IBLOCK_ID' => $adsIblockID, "PROPERTY_ID" => $wheelPropID));

	while($arWheel = $dbWheels->fetch())
	{
		$arWheelItem = array(
			'for'	=> 'option-'.$arWheel["ID"],
			'name'	=> 'filter['.$arWheel['PROPERTY_CODE'].']',
			'id'	=> $arWheel["ID"],
			'label'	=> $arWheel["VALUE"],
			'value'	=> $arWheel["VALUE"],
		);

		if($arWheel['XML_ID'] == 'left')
		{
			$arWheelItem['selected'] = ($arSetFilter["STEERING_WHEEL"]['LEFT'] == $arWheelItem['value']);
			$arResult['SET']['STEERING_WHEEL']['LEFT'] = $arWheelItem;
		}
		elseif($arWheel['XML_ID'] == 'right')
		{
			$arWheelItem['selected'] = ($arSetFilter["STEERING_WHEEL"]['RIGHT'] == $arWheelItem['value']);
			$arResult['SET']['STEERING_WHEEL']['RIGHT'] = $arWheelItem;
		}
	}

	// модель авто, если выбрана марка
	$arResult['SET']['MODEL'] = array();

	if($arResult["MANUFACTURER_IS_SET"])
	{
	    foreach($arResult["SET"]["MODEL"] as $key=>$arItem)
		{
			if($arItem["parent"] !== $ID_FIRM_SELECTED)
			{
				unset($arResult["SET"]["MODEL"][$key]);
			}
	    }
		$arResult['SET']['MODEL'] = YcawebHelper::getModels($setManufacturer, $arSetFilter["MODEL"]);

		foreach($arResult["SET"]["MODEL"] as $key=>&$arItem)
		{
			if (stripos($arResult["CUR_PAGE"], $arItem['value']) !== false)
				$arItem['selected'] = true;
	    }
	}
	else
	{
		unset($arResult["SET"]["MODEL"]);
	}

	$powerMax = 300;
	
	// верхняя граница диапазона мощности
	/*
	$dbEl = CIBlockElement::GetList(array("PROPERTY_POWER" => "DESC"), array("IBLOCK_ID" => 5, "ACTIVE" => "Y"), false, array('nTopCount' => 1), array("IBLOCK_ID", "ID", "PROPERTY_POWER"));

	if($arEl = $dbEl->fetch())
	{
		$powerMax = $arEl['PROPERTY_POWER_VALUE'];
	}
	
	if($powerMax>500){$powerMax = 500;}
	*/
	
	$powerMax = 500;
	$powerStep = 50;
	$powerArr = array();
	for ($i = $powerStep; $i < $powerMax + $powerStep; $i += $powerStep)
	{
		$powerArr[] = array(
			'selected' => ($arSetFilter['POWER'] == $i),
			'value' => $i
		);
	}
	$arResult["SET"]["POWER"] = $powerArr;

	$arResult["SET"]["ENGINE_FROM"] = $arSetFilter["ENGINE"]["FROM"];
	$arResult["SET"]["ENGINE_TO"] = $arSetFilter["ENGINE"]["TO"];
	$arResult["SET"]["YEAR_FROM"] = $arSetFilter["YEAR"]["FROM"];
	$arResult["SET"]["YEAR_TO"] = $arSetFilter["YEAR"]["TO"];
	$arResult["SET"]["PRICE"]["FROM"] = $arSetFilter["PRICE"]["FROM"];
	$arResult["SET"]["PRICE"]["TO"] = $arSetFilter["PRICE"]["TO"];
	$arResult["SET"]["IS_FOREIGN"] = $arSetFilter["IS_FOREIGN"];
	$arResult["SET"]["IS_NEW"] = $arSetFilter["IS_NEW"];
	$arResult["SET"]["IS_HYBRID"] = $arSetFilter["IS_HYBRID"];
	$arResult["SET"]["HAS_RF_MILEAGE"] = $arSetFilter["HAS_RF_MILEAGE"];
	//$arResult["SET"]["POWER"] = $arSetFilter["POWER"];
	//$arResult["SET"]["STEERING_WHEEL"]["LEFT"] = $arSetFilter["STEERING_WHEEL"]["LEFT"];
	//$arResult["SET"]["STEERING_WHEEL"]["RIGHT"] = $arSetFilter["STEERING_WHEEL"]["RIGHT"];
		
	if(!empty($arSetFilter["STEERING_WHEEL"]['LEFT']))
	{
		${$FILTER_NAME}["=PROPERTY_STEERING_WHEEL_VALUE"][] = $arResult["SET"]["STEERING_WHEEL"]["LEFT"]['value'];
	}
	if(!empty($arSetFilter["STEERING_WHEEL"]['RIGHT']))
	{
		${$FILTER_NAME}["=PROPERTY_STEERING_WHEEL_VALUE"][] = $arResult["SET"]["STEERING_WHEEL"]["RIGHT"]['value'];
	}

	${$FILTER_NAME}["=PROPERTY_MANUFACTURER"] = $arSetFilter['MANUFACTURER'];
	${$FILTER_NAME}["=PROPERTY_MODEL"] = $arSetFilter['MODEL'];
	${$FILTER_NAME}["<=PROPERTY_POWER"] =  $arSetFilter['POWER'];
	${$FILTER_NAME}[">=PROPERTY_MODEL_YEAR_VALUE"] =  $arSetFilter['YEAR']['FROM'];
	${$FILTER_NAME}["<=PROPERTY_MODEL_YEAR_VALUE"] = $arSetFilter['YEAR']['TO'];
	${$FILTER_NAME}[">=PROPERTY_PRICE"] = str_replace(" ", "", $arSetFilter['PRICE']['FROM']);
	${$FILTER_NAME}["<=PROPERTY_PRICE"] = str_replace(" ", "", $arSetFilter['PRICE']['TO']);
	${$FILTER_NAME}[">=PROPERTY_ENGINE_VALUE"] = $arSetFilter['ENGINE']['FROM'];
	${$FILTER_NAME}["<=PROPERTY_ENGINE_VALUE"] = $arSetFilter['ENGINE']['TO'];
	${$FILTER_NAME}["=PROPERTY_FUEL"] = $arSetFilter['FUEL'];
	${$FILTER_NAME}["=PROPERTY_TRANSMISSION"] = $arSetFilter['TRANSMISSION'];
	${$FILTER_NAME}["=PROPERTY_DRIVE"] = $arSetFilter['DRIVE'];
	${$FILTER_NAME}["=PROPERTY_IS_FOREIGN_VALUE"] = $arSetFilter['IS_FOREIGN'];
	${$FILTER_NAME}["=PROPERTY_IS_NEW_VALUE"] = $arSetFilter['IS_NEW'];
	${$FILTER_NAME}["=PROPERTY_IS_HYBRID_VALUE"] = $arSetFilter['IS_HYBRID'];
	${$FILTER_NAME}["!PROPERTY_HAS_RF_MILEAGE_VALUE"] = $arSetFilter['HAS_RF_MILEAGE'];
	${$FILTER_NAME}["=PROPERTY_CAR_BODY"] = $arSetFilter['CAR_BODY'];
	${$FILTER_NAME}["=PROPERTY_COLOR"] = $arSetFilter['CAR-COLORS'];
	
	
	
//$arResult["FORM_ACTION"] = $arParams["FOLDER"].$arSection["PATH"]."/".($_SERVER['QUERY_STRING'] ? "?".$_SERVER['QUERY_STRING'] : "");
$arResult["FORM_ACTION"] = $arParams["FOLDER"].$arSection["PATH"].($_SERVER['QUERY_STRING'] ? "?".$_SERVER['QUERY_STRING'] : "");

/*
$arFilt = array();
$str = $arSection["PATH"].'?start_filter=filter&';

//if(isset($_REQUEST["adm"])){
	
	foreach($arSetFilter as $key=>$val){
		
		//echo "[".$key."]: " . is_array($val)."<br>";
		
		if(is_array($val)){
			
			foreach($val as $k=>$v){
			$arFilt.='&filter['.$key.']['.$k.']='.$v;
			}
			
		}else{
			
			$arFilt.='&filter['.$key.']='.$val;
		}
		
		
		
	} */
	//$arResult["FORM_ACTION"] = $str.$arFilt;
	//$arResult["FORM_ACTION"] = "";
	//$arFilt = $str.$arFilt;
	//echo $str.$arFilt;
	//echo"<pre>";
	//print_r($arSetFilter);
	//print_r($arParams["FOLDER"]);
	//print_r($arSection["PATH"]);
	//print_r($_SERVER["QUERY_STRING"]);
	//print_r($arResult);
	//echo"</pre>";
	
//} 

$this->IncludeComponentTemplate();
?>
