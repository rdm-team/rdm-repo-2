<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?CJSCore::Init(array("jquery"));?>
<?$this->addExternalJS($templateFolder."/jquery.validate.js");?>

<div class="webform__wrap">
<button class="webform__buttom webform-event"><?=$arResult["DESCRIPTION"]?></button>
</div>
<div class="modalForm modalForm-order">
    <div class="modalForm__head">
	<img class="modalForm__headerLogo" src="/local/assets/img/assets/header/logo.svg" alt="">
    <button class="modalForm__eventCancel"></button>
    </div>
    <form id="formConsultation" class="modalForm__body" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
        <div class="modalForm__row-buttons">
            <div class="modalForm__row-buttontitle">Кому отправить</div>
            <button class="modalForm__btn modalForm__btn-eventTo modalForm__btn-select" id="respond1">Отделу найма</button>
            <button class="modalForm__btn modalForm__btn-eventTo" id="respond2">Рекламе</button>
            <button class="modalForm__btn modalForm__btn-eventTo" id="respond3">Директору</button>
            <button class="modalForm__btn modalForm__btn-eventTo" id="respond4">Партнерство</button>
            <input type="hidden" name="respondTo" value="respond1" />            
        </div>
        
        <div class="modalForm__row-wrap">
        <div class="modalForm__row-title">Заполните форму</div>
        
        <div class="modalForm__wrapInput">
            <div class="modalForm__row modalForm__row-input">
                <input class="modalForm__input" type="text" name="yourname" value="" placeholder="Ваше имя" />
            </div>
            <div class="modalForm__row modalForm__row-input">
                <input class="modalForm__input"  type="text" name="yourphone" value="" placeholder="Контактный телефон" />
            </div>
            <div class="modalForm__row modalForm__row-input">
                <textarea class="modalForm__textarea"  type="text" name="yourcomment" value="" placeholder="Комментарий"></textarea>
            </div>
        </div>
        <div class="modalForm__row modalForm__row-submit">
            <input class="modalForm__submit modalForm__eventSubmit"  type="submit" name="submit" value="<?=$arResult["BUTTON"]?>" />
        </div>
        
        </div>        
    </form>
</div>

<div class="modalform__shadow"></div>