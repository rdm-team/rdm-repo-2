<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?CJSCore::Init(array("jquery"));?>

    <div class="evo__it-wrap">
<?if(count($arResult["ITEMS"])>0){
    $arItems = $arResult["ITEMS"];
    $ind=0;
	$count = count($arItems);
	
	if($count%2!=0){
		// Четное количество элементов
		$bPhoto=true;
		}else{		
		// Нечетное количество элементов		
		$bPhoto=false;		
		}
	
    foreach($arItems as $arItem){                
        
        if(count($arItem["ALBUM"])>0){
        $style = ($ind%2)?'evo__it-right':'evo__it-left';
		
		if($bPhoto){
			
			if(($count-1)==$ind){$style='evo__it-center';}
		}
		
	$this->AddEditAction($arItem['ID'], $arItem['ADD_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_ADD"));
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>    
        <div class="evo__it <?=$style?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <div class="evo_it-media">
                <div class="evo_it-title"><h2><?=$arItem["NAME"]?></h2></div>
				<div class="evo_it-overflow">
                <a id="<?=$arItem["ID"]?>" class="fancybox-EvoPopup evo__mobile-show" href="javascript:;"><img src="<?=$arItem["ALBUM"][0]["SRC"]?>" alt="" /></a>
                <a id="<?=$arItem["ID"]?>" class="fancybox-EvoPopup evo__desctop-show evo__it-imgWrap " href="javascript:;"><div class="evo__it-bgroundImage" style="background-image:url(<?=$arItem["ALBUM"][0]["SRC"]?>)" alt="" ></div></a>
                </div>
                
            </div>     
        </div>
		<?if($ind%2){?>
		<div class="evo__delimetr"></div>
		<?}?>		
        <?$ind++;}}}?>
             
    </div>
    <script>var jsEvoPic = <?=json_encode($arResult["JSON"]);?></script>