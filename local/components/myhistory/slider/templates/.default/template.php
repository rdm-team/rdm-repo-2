<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?CJSCore::Init(array("jquery"));?>
<? 
if(count($arResult["ITEMS"])>0){
	
	$this->addExternalCss($templateFolder."/swiper/swiper.min.css");
	$this->addExternalJS($templateFolder."/swiper/swiper.min.js");
?>

<div class="history-slider__wrap">
<!-- Swiper -->
    <div class="swiper-container">
        <div class="swiper-wrapper">
        <?
		$arItems = $arResult["ITEMS"];
		foreach($arItems as $arElem){
        ?>
            <div class="swiper-slide"><img src="<?=$arElem["PREVIEW_PICTURE"]["SRC"]?>"></div>
        <?}?>
        </div>
        <?if($arResult["boolSliderActive"]=="Y"){?>
		<!-- Add Arrows -->
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
        <?}?>
    </div>    
</div>
<script>
var jsonSlideSetting = <?=json_encode($arResult["jsonSlideSetting"]);?>;
var boolSliderActive = "<?=$arResult["boolSliderActive"];?>";
</script>
<?}?>