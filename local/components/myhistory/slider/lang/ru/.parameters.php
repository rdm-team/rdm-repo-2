<?
$MESS["IBLOCK_CACHE_FILTER"] = "Кешировать при установленном фильтре";
$MESS["T_IBLOCK_DESC_ASC"] = "По возрастанию";
$MESS["T_IBLOCK_DESC_DESC"] = "По убыванию";
$MESS["T_IBLOCK_DESC_FID"] = "ID";
$MESS["T_IBLOCK_DESC_FNAME"] = "Название";
$MESS["T_IBLOCK_DESC_FSORT"] = "Сортировка";
$MESS["T_IBLOCK_DESC_IBORD1"] = "Поле для сортировки слайдов";
$MESS["T_IBLOCK_DESC_IBBY1"] = "Направление для сортировки слайдов";
$MESS["T_IBLOCK_DESC_LIST_ID"] = "Код информационного блока";
$MESS["T_IBLOCK_DESC_LIST_TYPE"] = "Тип информационного блока";
$MESS["T_IBLOCK_FILTER"] = "Фильтр";
$MESS["CP_BNL_CACHE_GROUPS"] = "Учитывать права доступа";

$MESS["T_IBLOCK_USER_GROUPS"] = "Группы пользователей";
?>