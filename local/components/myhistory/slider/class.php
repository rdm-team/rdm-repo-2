<? 
    use Bitrix\Main\Loader;
    use Bitrix\Main\Application; 
    use Bitrix\Main\Web\Uri;
     
    if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

    class BlockList extends CBitrixComponent
    {
        
        public function executeComponent()
        {

                if(is_numeric($this->arParams["IBLOCK_ID"]))
                {
                    $rsIBlock = CIBlock::GetList(array(), array(
                        "ACTIVE" => "Y",
                        "ID" => $this->arParams["IBLOCK_ID"],
                        "CHECK_PERMISSIONS"=>"N"
                    ));

                }
                else
                {
                    $rsIBlock = CIBlock::GetList(array(), array(
                        "ACTIVE" => "Y",
                        "CODE" => $this->arParams["IBLOCK_ID"],
                        "SITE_ID" => SITE_ID,
                        "CHECK_PERMISSIONS"=>"N"
                    ));

                }

                    
                    $this->arResult["USER_HAVE_ACCESS"] = $bUSER_HAVE_ACCESS;

                    $arSelect = array(
                    );                
                    

                    //WHERE
                    $arFilter = array (
                        "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
                        "IBLOCK_LID" => SITE_ID,
                        "ACTIVE" => "Y",
                        "CHECK_PERMISSIONS" => $this->arParams['CHECK_PERMISSIONS'] ? "Y" : "N",
                    );

                    //ORDER BY
                    $arSort = array(
                        $this->arParams["SORT_BY1"]=>$this->arParams["SORT_ORDER1"],

                    );
					

                    /*if(!array_key_exists("ID", $arSort))
                        $arSort["ID"] = "DESC";*/

                    $this->arResult["ITEMS"] = array();

                    $arrFilter = array();


                    $rsElement = CIBlockElement::GetList($arSort, array_merge($arFilter, $arrFilter), false, false, $arSelect);

                    
                    
                    while($arItem = $rsElement->GetNext())
                    {
                       
                        $arItem['PREVIEW_PICTURE'] = CFile::GetFileArray($arItem['PREVIEW_PICTURE']);
                        $this->arResult["ITEMS"][] = $arItem;
                        
                    }

					$this->arResult["boolSliderActive"] = (count($this->arResult["ITEMS"])>1)?"Y":"N";
					
                    $this->arResult["jsonSlideSetting"] = array(
                        "nextButton"=> '.swiper-button-next',
                        "prevButton"=> '.swiper-button-prev',
                        "spaceBetween"=> 30,
                        "autoplayDisableOnInteraction"=>false,
                        "freeModeMinimumVelocity"=>0.02,
                        "loop"=> true
                    );
                                        
                   if(isset($this->arParams["INTERVAL"]) && count($this->arResult["ITEMS"])>1){
                      
                       $this->arResult["jsonSlideSetting"]["autoplay"] = (int)($this->arParams["INTERVAL"] * 1000);
                       
                   }
                   
                   if(isset($this->arParams["SPEED"])){
                       
                       $this->arResult["jsonSlideSetting"]["speed"] = (int)$this->arParams["SPEED"];                        
                       
                   }
				   
                   $this->includeComponentTemplate();
                    

        }    

        public function onPrepareComponentParams($arParams)
        {
            
            
            if(!isset($arParams["CACHE_TIME"]))
                $arParams["CACHE_TIME"] = 36000000;

            $arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
            if(strlen($arParams["IBLOCK_TYPE"])<=0)
                $arParams["IBLOCK_TYPE"] = "news";
            
            $arParams["IBLOCK_ID"] = trim($arParams["IBLOCK_ID"]);
            $arParams["SET_LAST_MODIFIED"] = $arParams["SET_LAST_MODIFIED"]==="Y";
            //$arParams["AJAX_MODE"] = "N";

            $arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
            if(strlen($arParams["SORT_BY1"])<=0)
                $arParams["SORT_BY1"] = "ACTIVE_FROM";
            if(!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["SORT_ORDER1"]))
                $arParams["SORT_ORDER1"]="DESC";

                $arrFilter = array();

            $arParams["CHECK_DATES"] = $arParams["CHECK_DATES"]!="N";

            $arParams["CHECK_PERMISSIONS"] = $arParams["CHECK_PERMISSIONS"]!="N";

            return $arParams;
        }
        
    }
?>