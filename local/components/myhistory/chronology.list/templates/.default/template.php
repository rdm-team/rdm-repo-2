<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?CJSCore::Init(array("jquery"));?>

<div class="chrono__wrap">

    <h2 class="chrono__title"><?=$arResult["NAME"]?></h2>

    <div class="chrono-list">
    <?if(count($arResult["ITEMS"])>0){
    $arItems = $arResult["ITEMS"];
    $ind=0;
    foreach($arItems as $arItem){

        $style = ($ind%2)?'chrono-list__right':'chrono-list__left';
        $this->AddEditAction($arItem['ID'], $arItem['ADD_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_ADD"));
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?> 
        <div class="chrono-list__item <?=$style?> <?if(count($arItem["ALBUM"])==0){?>chrono__noimage<?}?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <div class="chrono-list__baloon"><?=$arItem["NAME"]?></div>
            <div class="chrono-list__vline"></div>	
            <div class="chrono-list__colum chrono-text">
				<div class="chrono__table">
					<div class="chrono-text__valign"><?=$arItem["~DETAIL_TEXT"]?></div>
				</div>
            </div>
            <?if(count($arItem["ALBUM"])>0){?>
            <div class="chrono-list__media">
            <a id="<?=$arItem["ID"]?>" class="fancybox-ChronoPopup" href="javascript:;"><img src="<?=$arItem["ALBUM"][0]["SRC"]?>" alt="..." /></a>
            </div>
            <?}else{?>
			<div class="chrono-list__media"></div>
			<?}?>
        </div>
        <?$ind++;}}?>        
    </div>
</div>
<script>var jsChronoPic = <?=json_encode($arResult["JSON"]);?></script>        