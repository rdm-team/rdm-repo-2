<?
use \Bitrix\Main\Page\Asset,
	 \Bitrix\Main\Page\AssetLocation;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

/**
 * Bitrix vars
 *
 * @var CBitrixComponent         $component
 * @var CBitrixComponentTemplate $this
 * @var array                    $arParams
 * @var array                    $arResult
 * @var array                    $arLangMessages
 * @var array                    $templateData
 *
 * @var string                   $templateFile
 * @var string                   $templateFolder
 * @var string                   $parentTemplateFolder
 * @var string                   $templateName
 * @var string                   $componentPath
 *
 * @var CDatabase                $DB
 * @var CUser                    $USER
 * @var CMain                    $APPLICATION
 * @var CUserTypeManager         $USER_FIELD_MANAGER
 */

if(method_exists($this, 'setFrameMode'))
	$this->setFrameMode(true);

$FORM_ID           = trim($arParams['API_FEX_FORM_ID']);
$FORM_AUTOCOMPLETE = $arParams['FORM_AUTOCOMPLETE'] ? 'on' : 'off';

$INPUT_ROW_CLASS      = 'uk-form-row';
$INPUT_LABEL_CLASS    = 'uk-form-label';
$INPUT_CONTROLS_CLASS = 'uk-form-controls';
$FIELD_SIZE           = $arParams['FIELD_SIZE'] ? ' uk-form-' . $arParams['FIELD_SIZE'] : '';
$BUTTON_SIZE          = $arParams['FIELD_SIZE'] ? ' uk-button-' . $arParams['FIELD_SIZE'] : '';
$FIELD_NAME_POSITION  = $arParams['FIELD_NAME_POSITION'] ? ' uk-form-' . $arParams['FIELD_NAME_POSITION'] : ' uk-form-horizontal';

if($arParams['HIDE_FIELD_NAME'])
	$FIELD_NAME_POSITION = ' uk-form-stacked';

?>
<?if($arParams['USE_MODAL']):?>

	<?if($arParams['MODAL_BTN_TEXT']):?>
		<button <?=($arParams['MODAL_BTN_ID']?'id="'.$arParams['MODAL_BTN_ID'].'"':'')?>
			      class="<?=$arParams['MODAL_BTN_CLASS']?>"
		        onclick="jQuery.fn.apiModal('show',{id:'<?=$arParams['MODAL_ID']?>'});">
			<span class="<?=$arParams['MODAL_BTN_SPAN_CLASS']?>"></span><?=$arParams['MODAL_BTN_TEXT']?>
		</button>
	<?endif?>

	<div id="<?=ltrim($arParams['MODAL_ID'],'#')?>" class="api_modal">
		<div class="api_modal_dialog">
			<div class="api_modal_close"></div>
			<?if($arParams['MODAL_HEADER_TEXT']):?>
				<div class="api_modal_header"><?=$arParams['MODAL_HEADER_TEXT']?></div>
			<?endif?>
			<div class="api_modal_content">
<?endif?>

<!--form start 2222-->
<div id="API_FEX_<?=$FORM_ID?>" class="<?=trim($arParams['FORM_CLASS'] . ' api-feedbackex')?>">
	<div class="theme-uikit theme-<?=$arParams['THEME'];?> color-<?=$arParams['COLOR'];?>">
		<form id="<?=$FORM_ID?>"
		      class="uk-form <?=$FIELD_NAME_POSITION?>"
		      name="api_feedbackex_form"
		      enctype="multipart/form-data"
		      method="POST"
		      action="<?=POST_FORM_ACTION_URI;?>"
		      autocomplete="<?=$FORM_AUTOCOMPLETE?>">

			<input type="hidden" name="API_FEX_FORM_ID" value="<?=$FORM_ID?>">
			<input type="hidden" name="API_FEX_SUBMIT_ID" value="<?=$FORM_ID?>">
			<input type="text" name="ANTIBOT[NAME]" value="<?=$arResult['ANTIBOT']['NAME'];?>" class="api-antibot">

			<?=$arResult['FORM_TITLE']?>
			<?
			

			if(count($arParams['DISPLAY_FIELDS']) > 0)
			{
				foreach($arParams['DISPLAY_FIELDS'] as $FIELD)
				{
					
					
					$arField = $arParams['FORM_FIELDS'][ $FIELD ];

					$INPUT_NAME           = !empty($arParams[ 'LANG_' . $FIELD ]) ? $arParams[ 'LANG_' . $FIELD ] : $arField['NAME'];
					$INPUT_PLACEHOLDER    = ($arParams['USE_PLACEHOLDER'] || $arParams['HIDE_FIELD_NAME']) ? ' placeholder="' . $INPUT_NAME . ((empty($arParams["REQUIRED_FIELDS"]) || in_array($FIELD, $arParams["REQUIRED_FIELDS"])) ? ' *' : '') . '"' : '';
					$INPUT_ASTERISK       = ':<span class="api-asterisk">*</span>';

					$INPUT_ROW_ID         = $FORM_ID .'_ROW_'. $FIELD;
					$INPUT_ID             = $FORM_ID .'_FIELD_'. $FIELD;
					$INPUT_CLASS          = '';

					


					if(empty($arParams["REQUIRED_FIELDS"]) || in_array($FIELD, $arParams["REQUIRED_FIELDS"]))
					{
						$INPUT_CLASS .= ' required';
					}
					else
						$INPUT_ASTERISK = ':';


					$INPUT_CLASS = trim($INPUT_CLASS . ' '. $FIELD_SIZE);
					$INPUT_NAME  = $arParams['HIDE_ASTERISK'] ? $INPUT_NAME : $INPUT_NAME . $INPUT_ASTERISK;

					if($arField['TYPE'] == 'TEXTAREA')
					{
						?>
						<div class="<?=$INPUT_ROW_CLASS;?>" id="<?=$INPUT_ROW_ID?>">
							<? if(!$arParams['HIDE_FIELD_NAME']): ?>
								<label class="<?=$INPUT_LABEL_CLASS?>"><span class="api-label"><?=$INPUT_NAME?></span></label>
							<? endif; ?>
							<div class="<?=$INPUT_CONTROLS_CLASS?>">
								<textarea name="FIELDS[<?=$FIELD?>]" <?=$INPUT_PLACEHOLDER;?>
											 id="<?=$INPUT_ID?>"
											 rows="<?=$arParams['FORM_TEXTAREA_ROWS']?>"
								          class="<?=$INPUT_CLASS;?>"><?=$arResult['FIELDS'][ $FIELD ]?></textarea>
								<div class="api-field-error"></div>
							</div>
						</div>
						<?
					}
					elseif($arField['TYPE'] == 'PASSWORD')
					{
						?>
						<div class="<?=$INPUT_ROW_CLASS;?>" id="<?=$INPUT_ROW_ID?>">
							<? if(!$arParams['HIDE_FIELD_NAME']): ?>
								<label class="<?=$INPUT_LABEL_CLASS?>"><span class="api-label"><?=$INPUT_NAME?></span></label>
							<? endif; ?>
							<div class="<?=$INPUT_CONTROLS_CLASS?>">
								<input type="password"
								       id="<?=$INPUT_ID?>"
								       name="FIELDS[<?=$FIELD?>]"
								       value="<?=$arResult['FIELDS'][ $FIELD ]?>" <?=$INPUT_PLACEHOLDER;?>
								       class="<?=$INPUT_CLASS;?>">
								<div class="api-field-error"></div>
							</div>
						</div>
						<?
					}
					else
					{
						?>
						<div class="<?=$INPUT_ROW_CLASS;?>" id="<?=$INPUT_ROW_ID?>">
							<? if(!$arParams['HIDE_FIELD_NAME']): ?>
								<label class="<?=$INPUT_LABEL_CLASS?>"><span class="api-label"><?=$INPUT_NAME?></span></label>
							<? endif; ?>
							<div class="<?=$INPUT_CONTROLS_CLASS?>">
								<input type="text"
								       id="<?=$INPUT_ID?>"
							          name="FIELDS[<?=$FIELD?>]"
							          value="<?=$arResult['FIELDS'][ $FIELD ]?>"
										 <?=$INPUT_PLACEHOLDER;?>
								       class="<?=$INPUT_CLASS;?>">
								<div class="api-field-error"></div>
							</div>
						</div>
						<?
					}
				}
			}
			?>

			<div id="<?=$FORM_ID?>_ROW_BUTTON" class="<?=$INPUT_ROW_CLASS?> group-button">
				<? if(!$arParams['HIDE_FIELD_NAME']): ?>
					<label class="<?=$INPUT_LABEL_CLASS?>">&nbsp;</label>
				<? endif; ?>
				<div class="<?=$INPUT_CONTROLS_CLASS?>">
					<button id="API_FEX_FORM_SUBMIT_<?=$FORM_ID?>"
					        type="submit"
					        name="API_FEX_SUBMIT_BUTTON"
					        class="<?=$arParams['FORM_SUBMIT_CLASS'];?><?=$BUTTON_SIZE?>"
					        style="<?=$arParams['FORM_SUBMIT_STYLE'];?>"
					        value="<?=$FORM_ID?>"><i class="api-icon-load"></i> <?=$arParams['FORM_SUBMIT_VALUE'];?></button>
					<div class="api-field-warning"></div>
				</div>
			</div>
		</form>
	</div>
</div>
<!--form end-->

<?if($arParams['USE_MODAL']):?>
			</div><!--api_modal_content-->
			<?if($arParams['MODAL_FOOTER_TEXT']):?>
				<div class="api_modal_footer"><?=$arParams['MODAL_FOOTER_TEXT']?></div>
			<?endif?>
		</div><!--api_modal_dialog-->
	</div><!--api_modal-->
<?endif?>

<?
ob_start();
?>
	<script type="text/javascript">
		jQuery(document).ready(function ($) {

			$.fn.apiFeedbackex({
				wrapperId: '#API_FEX_<?=$FORM_ID?>',
				formId: '#<?=$FORM_ID?>',
				params: {
					siteId: '<?=SITE_ID?>',
					sessid: '<?=bitrix_sessid()?>'
				}
			});

			<?if($arParams['USE_MODAL']):?>
				$.fn.apiModal({
					id: '<?=$arParams['MODAL_ID']?>'
				});
			<?endif?>

		});
	</script>
<?
$script = ob_get_contents();
ob_end_clean();
Asset::getInstance()->addString($script, true, AssetLocation::AFTER_JS);
?>