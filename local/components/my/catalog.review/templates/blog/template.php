<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//p($arResult['REVIEWS']);
?>


<div class="wrap_comments blog<?if(empty($arResult['REVIEWS']))echo ' empty_block_reviews';?>">
	
    <?if(!empty($arResult['REVIEWS'])){?>
		<div class="comments_bottom_block">
			<div class="comments_clients_ttl">Комментарии</div>
            <?
			foreach($arResult['REVIEWS'] as $reviews) {
				$nameClient = explode(',',$reviews['PROPERTY_NAME_VALUE'], 2);
			?>
				<div class="comment clearfix">
					<div class="name_date">                     
						<div class="comment_date">
							<?=FormatDate("j F, H:i", MakeTimeStamp($reviews['DATE_CREATE']))?>
						</div>
                        <div class="comment_name_user">
							<?=$nameClient[0]?>
						</div>
                        <div class="comment_citate">
							цитировать
						</div> 
					</div>
                    <div class="text_photo_wrap">  
                        <div class="comment_text">
                            <?
                            $patterns = array();
                            $patterns[0] = '/>>/';
                            $patterns[1] = '/<</';
                            $replacements = array();
                            $replacements[2] = '<div class="comment_citate_text">';
                            $replacements[1] = '</div>';
                            $previewText =  preg_replace($patterns, $replacements, $reviews['~PREVIEW_TEXT']);
                            if(strlen($previewText) > 178 ){
                                ?>
                                <span class="preview_comment"><?=TruncateText(rtrim($previewText),178)?></span>
                                <span class="full_comment"><?=$previewText?></span>
                                <span class="comment_read_more">Показать дальше</span>
                                <?
                            }else{
                                echo $previewText;
                            }?>
                        </div>
                        <?if(count($reviews['PROPERTY_PHOTO_VALUE']) > 0){?>
                            <div class="photo_list">
                                <?
                                if(is_array($reviews['PROPERTY_PHOTO_VALUE'])){
                                    foreach($reviews['PROPERTY_PHOTO_VALUE'] as $photoId){
                                        $file = CFile::ResizeImageGet($photoId, array('width'=>150, 'height'=>150), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>            
                                        <img src="<?=$file['src']?>">
                                   <?}
                                }else{
                                    $file = CFile::ResizeImageGet($reviews['PROPERTY_PHOTO_VALUE'], array('width'=>150, 'height'=>150), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>            
                                    <img src="<?=$file['src']?>">    
                                <?}?>
                            </div>
                        <?}
                        
                        //p($reviews);
                        if(isset($reviews['~PROPERTY_ANSWER_REVIEW_VALUE']) && $reviews['~PROPERTY_ANSWER_REVIEW_VALUE']['TEXT']){?>
                            <span class="rdm_answer">
                                <span class="rdm_icon">Rdm Import</span>
                                <?=$reviews['~PROPERTY_ANSWER_REVIEW_VALUE']['TEXT']?>
                            </span>
                        <?}?>
                    </div>
				</div>
			<?}?>	
		</div>
	<?}?>	
       
    <?
    global $USER;
    if(!$USER->IsAuthorized()){?>       
        <div class="comments-user-auth">
            Для того чтобы оставить комментарий, пожалуйста 
			<a href="" class="_js-reg-lin eventOpenReg">зарегистрируйтесь</a> или 
			<a href="" class="_js-authorizatio eventOpenAuth">войдите</a> под своей учетной записью
        </div>
        <div class="social">
            <img src="/local/assets/img/social.png">
        </div>
    <?}else{?>
        <div class="comments-added">
            Оставить комментарий
        </div>
        <form id="comments_questions_form" class="comments_field_form<?/*if(!empty($arResult['REVIEWS']))echo ' hidden';*/?>" action="" enctype="multipart/form-data">
            <div id="comments_success"></div>
            <div id="comments_data">
                <div id="comments_error_field"></div>
                <div class="comments_form_inner">
                    <div class="user_name_wrap">
                        <p>Имя:&nbsp;<span class="mark_required_field">*</span></p>
                        <input class="user_name_field" id="user_name_field" name="name" value="<?=$arResult['NAME_USER']?>">
                        <input class="user_name_field hidden" id="user_name_field2" name="name2" value="">
                    </div>
                    <p><span class="comments_text_title">Текст комментария</span>:&nbsp;<span class="mark_required_field">*</span></p>
                    <textarea name="text" id="comments_text"></textarea>
                    <?/*<div class="user_phone_wrap">
                        <p>Контактные данные (телефон или email):&nbsp;<span class="mark_required_field">*</span></p>
                        <input class="user_phone_field" id="user_phone_field" name="phone" value="<?=$arResult['EMAIL_USER']?>">
                        <input class="user_phone_field display_none" id="user_phone_field2" name="phone2" value="">
                    </div>*/?>
                </div>
                
                <div class="comments_photo_wrap">
                    <div class="ttl-photo">Фотографии:(Вы можете добавить не больше 3 фотографий)</div>
                    
                    <div class="input-photo-files">
                        <div class="input-photo-file">
                            <input type="file" name="file[]" class="file" accept="image/*">
                        </div>
                    </div>
                    <div class="add-photo" id="add_photo">Добавить еще фото</div>
                </div>
                <input type="hidden" name="id_el" value="<?=$arResult["ELEMENT_ID"]?>">
                <input id="comments_submit" type="submit" value="Добавить комментарий" class="button_comment coomment_add">
            </div>
        </form>
    <?}?> 
</div>
