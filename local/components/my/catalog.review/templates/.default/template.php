<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="wrap_comments<?if(empty($arResult['REVIEWS']))echo ' empty_block_reviews';?>">
	
    <?if(!empty($arResult['REVIEWS'])){?>
		<div class="comments_bottom_block">
			<?/*<h4 class="comments_clients_head">Комментарии</h4>*/?>
            <?
			foreach($arResult['REVIEWS'] as $reviews) {
				$nameClient = explode(',',$reviews['PROPERTY_NAME_VALUE'], 2);
			?>
				<div class="comment">
					<div>
						<span class="comment_name_user">
							<?=$nameClient[0]?>
						</span>                        
						<span class="comment_date">
							, <?=FormatDate("j F, H:i", MakeTimeStamp($reviews['DATE_CREATE']))?>
						</span>
					</div>
					<?if(strlen($reviews['~PREVIEW_TEXT']) > 178 ){?>
						<span class="preview_comment"><?=TruncateText(rtrim($reviews['~PREVIEW_TEXT']),178)?></span>
						<span class="full_comment"><?=$reviews['~PREVIEW_TEXT']?></span>
						<span class="comment_read_more">Показать дальше</span>
						<?
					}else{
						echo $reviews['~PREVIEW_TEXT'];
					}
					//p($reviews);
					if(isset($reviews['~PROPERTY_ANSWER_REVIEW_VALUE']) && $reviews['~PROPERTY_ANSWER_REVIEW_VALUE']['TEXT']){?>
						<span class="rdm_answer">
							<span class="rdm_icon">Rdm Import</span>
							<?=$reviews['~PROPERTY_ANSWER_REVIEW_VALUE']['TEXT']?>
						</span>
					<?}?>
				</div>
			<?}?>	
		</div>
	<?}?>	
       
    <?
    global $USER;
    if(!$USER->IsAuthorized()){?>       
        <div class="comments-user-auth">
            Чтобы предложить свою цену, пожалуйста, <a href="#registration" class="_js-reg-link">зарегистрируйтесь</a> или <a href="#authorization" class="_js-authorization">войдите</a> под своей учетной записью
        </div>
    <?}else{?>
        <div class="comments-add" id="comment_add">
            Предложить свой вариант
        </div>
        <form id="comments_questions_form" class="comments_field_form<?if(!empty($arResult['REVIEWS']))echo ' hidden';?>" action="">
            <div id="comments_success"></div>
            <div id="comments_data">
                <div id="comments_error_field"></div>
                <div class="comments_form_inner">
                    <div class="user_name_wrap">
                        <p>Имя:&nbsp;<span class="mark_required_field">*</span></p>
                        <input class="user_name_field" id="user_name_field" name="name" value="<?=$arResult['NAME_USER']?>">
                        <input class="user_name_field hidden" id="user_name_field2" name="name2" value="">
                    </div>
                    <p>Ваше предложение:&nbsp;<span class="mark_required_field">*</span></p>
                    <textarea name="text" id="comments_text"></textarea>
                    <?/*<div class="user_phone_wrap">
                        <p>Контактные данные (телефон или email):&nbsp;<span class="mark_required_field">*</span></p>
                        <input class="user_phone_field" id="user_phone_field" name="phone" value="<?=$arResult['EMAIL_USER']?>">
                        <input class="user_phone_field display_none" id="user_phone_field2" name="phone2" value="">
                    </div>*/?>
                </div>
                <input type="hidden" name="id_el" value="<?=$arParams["ELEMENT_ID"]?>">
                <input id="comments_submit" type="submit" value="Отправить" class="button_comment">
            </div>
        </form>
    <?}?> 
</div>
