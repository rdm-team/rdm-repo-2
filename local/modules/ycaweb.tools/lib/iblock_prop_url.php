<?php

namespace Ycaweb;

IncludeModuleLangFile(__FILE__);

class CIBlockPropertyURL{

	public function GetUserTypeDescription(){
		return array(
			'PROPERTY_TYPE'			=> 'S',
			'USER_TYPE'				=> 'url',
			'DESCRIPTION'			=> GetMessage('YCAWEB_IBLOCK_PROP_URL'),
			'PrepareSettings'		=> array(__CLASS__, 'PrepareSettings'),
			'GetSettingsHTML' 		=> array(__CLASS__, 'GetSettingsHTML'),
			'GetAdminListViewHTML'	=> array(__CLASS__, 'GetAdminListViewHTML'),
			'GetPropertyFieldHtml'	=> array(__CLASS__, 'GetPropertyFieldHtml'),
			'GetPublicViewHTML'		=> array(__CLASS__, 'GetPublicViewHTML'),
			'GetPublicEditHTML'		=> array(__CLASS__, 'GetPublicEditHTML'),
			'ConvertToDB'			=> array(__CLASS__, 'ConvertToDB'),
			'ConvertFromDB'			=> array(__CLASS__, 'ConvertFromDB'),
		);
	}

	/**
	 * @param $arProperty
	 * @param $value
	 * @return mixed
	 */
	public static function ConvertToDB($arProperty, $value){

		return $value;
	}

	/**
	 * @param $arProperty
	 * @param $value
	 * @return mixed
	 */
	public static function ConvertFromDB($arProperty, $value){
		return $value;
	}

	public static function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName){
	?>

		<div class="adm-filter-box-sizing">
			<span class="adm-select-wrap">
				<select name="action" class="adm-select">
					<option value=""></option>
				</select>
			</span>
			<span class="adm-select-wrap">
				<input type="text" name="" value="">
			</span>
		</div>

	<?
	}

}