<?php

namespace Ycaweb;

/**
 * Class Holidays
 *
 * @author Shashenko Andrei
 * @package ycaweb.tools
 */

class Holidays{

	/**
	 * URL сервиса с данными о праздниках
	 *
	 * @var string
	 */
	protected $serverURL = 'http://kayaposoft.com/enrico/json/v1.0/index.php';

	public function __construct(){
		if(!extension_loaded('curl')){
			throw new \Exception('CURL extension is not loaded.');
		}
	}

	/**
	 * Данные о праздниках за определенный месяц
	 *
	 * @param int    $month
	 * @param int    $year
	 * @param string $country
	 * @return array
	 * @throws \Exception
	 */
	public function getForMonth($month, $year, $country = 'rus'){
		$arParams = array(
			'action'	=> 'getPublicHolidaysForMonth',
			'month'		=> $month,
			'year'		=> $year,
			'country'	=> $country,
			'region'	=> '',
		);
		$arData = $this->request($arParams);

		return $this->prepareData($arData);
	}

	/**
	 * Данные о праздниках за определенный год
	 *
	 * @param int    $year
	 * @param string $country
	 * @return array
	 * @throws \Exception
	 */
	public function getForYear($year, $country = 'rus'){
		$arParams = array(
			'action'	=> 'getPublicHolidaysForYear',
			'year'		=> $year,
			'country'	=> $country,
		);
		$arData = $this->request($arParams);

		return $this->prepareData($arData);
	}

	/**
	 * Данные о праздниках за определенный период
	 *
	 * @param string $fromDate
	 * @param string $toDate
	 * @param string $country
	 * @return array
	 * @throws Exception
	 * @throws \Exception
	 */
	public function getForDateRange($fromDate, $toDate, $country = 'rus'){
		$arParams = array(
			'action'	=> 'getPublicHolidaysForDateRange',
			'fromDate'	=> $this->parseDate($fromDate),
			'toDate'	=> $this->parseDate($toDate),
			'country'	=> $country,
			'region'	=> '',
		);

		$arData = $this->request($arParams);

		return $this->prepareData($arData);
	}

	/**
	 * Подготовка возвращаемых данных
	 *
	 * @param array $arData
	 * @return array
	 */
	protected function prepareData($arData){
		$arResult = array();

		if(!empty($arData)){
			foreach($arData as $objItem){
				$date = \DateTime::createFromFormat('j.n.Y', $objItem->date->day . '.' . $objItem->date->month . '.' . $objItem->date->year);
				$arResult[] = array(
					'NAME'		=> $objItem->localName,
					'NAME_EN'	=> $objItem->englishName,
					'DATE'		=> $date->format('d.m.Y')
				);
			}
		}

		return $arResult;
	}

	/**
	 * Проверка формата даты
	 *
	 * @param $date | format dd-mm-YYYY
	 * @return Date
	 * @throws Exception
	 */
	protected function parseDate($date){
		$parts = explode('-', $date);

		if(count($parts) != 3)
			throw new Exception('Invalid date ' . $date . '! Date string must be in format dd-mm-YYYY, e.g. 15-01-2012');

		return $date;
	}

	/**
	 * Выполнение запроса
	 *
	 * @param array $arParams
	 * @return array|mixed
	 * @throws \Exception
	 */
	protected function request($arParams = array()){
		$arResult = array();

		$url = $this->serverURL;
		$arQueryParams = array();

		if(!empty($arParams['action'])){

			foreach($arParams as $name=>$value){
				$arQueryParams[] = $name . '=' . $value;
			}
			$url .= '?' . implode('&', $arQueryParams);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$answer = curl_exec($ch);
			curl_close($ch);

			if($answer === false){
				throw new \Exception(curl_error($ch));
			}else{
				$arResult = json_decode($answer);
			}

		}else{

			throw new \Exception('No action');

		}

		return $arResult;
	}

}