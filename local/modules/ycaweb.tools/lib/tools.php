<?php

namespace Ycaweb;

/**
 * Class Tools
 *
 * @author Shashenko Andrei
 * @package ycaweb.tools
 */

class Tools{

	/*
	 * Libraries, temlates and blocks folders
	 */
	const	INCLUDE_PATH = '/local/php_interface/include',
			GLOBAL_INCLUDE_PATH = '/bitrix/php_interface/include';


	/*
	 * @var boolean
	 */
	protected static $bMainPage = null;

	/*
	 * Insert content block
	 * 
	 * @param string	$blockFile
	 * @param string	$mode
	 * @param string	$blockName
	 * @param array		$tplParams
	 * @param bool		$global
	 */
	public static function getBlock($blockFile, $blockName = 'блок', $mode = 'html', $tplParams = array(), $global = false){
		global $APPLICATION;

		if(!is_array($tplParams))
			$tplParams = array();

		$APPLICATION->IncludeFile(($global ? self::GLOBAL_INCLUDE_PATH : self::INCLUDE_PATH)."/blocks/{$blockFile}.block.php", $tplParams, array(
			'SHOW_BORDER'	=> true,
			'WORKFLOW'		=> false,
			'MODE'			=> $mode,
			'NAME'			=> $blockName,
			'TEMPLATE'		=> '/' //Fix
		));
	}

	/*
	 * Insert template block
	 * 
	 * @param string	$blockName
	 * @param bool		$global
	 */
	public static function getTpl($tplName, $global = false){			
		if(!empty($tplName)){
			global $APPLICATION;
			$path = $_SERVER['DOCUMENT_ROOT'].($global ? self::GLOBAL_INCLUDE_PATH : self::INCLUDE_PATH)."/tpl/{$tplName}.tpl.php";
			if(file_exists($path)){
				include($path);
			}
		}
	}

	/*
	 * Get site parameter
	 *
	 * @param string $paramCode
	 * @return string
	 */
	public static function getSiteParam($paramCode = ''){
		if(empty($paramCode))
			return '';

		return $GLOBALS['MAIN_LANGS_CACHE'][SITE_ID][strtoupper($paramCode)];
	}

	/*
	 * Render template
	 * 
	 * @param string $tplPath
	 * @param array $tplParams
	 * @return string
	 */
	public static function renderTpl($tplPath, $tplParams = array()){
		$tplPath = $_SERVER['DOCUMENT_ROOT'].str_replace('//', '/', SITE_DIR.$tplPath);
		$strRendered = '';
		if(is_file($tplPath)){
			ob_start();
			if(is_array($tplParams))
				extract($tplParams, EXTR_SKIP);

			include($tplPath);
			$strRendered = ob_get_contents();
			ob_end_clean();
		}
		return $strRendered;
	}

	/**
	 * @param integer|array $pic
	 * @param integer $w
	 * @param integer $h
	 * @param integer $type
	 * @return array|bool
	 */
	public static function getResizedPicArray($pic, $w, $h, $type = BX_RESIZE_IMAGE_EXACT){
		if($arResized = \CFile::ResizeImageGet(
			$pic,
			array(
				 'width'	=> $w,
				 'height'	=> $h
			),
			$type,
			true
		)){
			$arResized = @array_change_key_case($arResized, CASE_UPPER);

			if(is_array($pic)){
				return array_merge($pic, $arResized);
			}

			return $arResized;
		}

		return false;
	}

	/*
	 * Geoip
	 *
	 * @param boolean $bUseCookie
	 * @return array
	 */
	public static function geoIP($bUseCookie = true){
		$arData = array();
		if($bUseCookie && isset($_COOKIE['GEOBASE'])){
			$arData = unserialize($_COOKIE['GEOBASE']);
		}else{
			require_once(__DIR__ . '/geoip.php');
			$objGeo = new \Ycaweb\GeoIP();
			$arData = $objGeo->getValue(false, $bUseCookie);
		}

		return $arData;
	}

	/*
	 * Check H1 title of current page
	 */
	public static function checkH1Title(){
		global $APPLICATION;
		$bNotShow = trim($APPLICATION->GetProperty('not_show_page_title'));

		if($bNotShow == 'Y') return;

		$h1 = $APPLICATION->GetTitle();

		$section_file = $_SERVER['DOCUMENT_ROOT'].$APPLICATION->GetCurDir().'.section.php';
		if(empty($h1) && file_exists($section_file))
		{
			include($section_file);
			$h1 = $sSectionName;
		}
		self::SetH1Title($h1);
	}

	/*
	 * Show H1 title of current page
	 */
	public static function showH1Title($additionalClass = ''){
		global $APPLICATION;
		echo $APPLICATION->AddBufferContent(array(__CLASS__, 'getH1Title'), $additionalClass);
	}

	/*
	 * Set H1 title of current page
	 */
	public static function setH1Title($h1 = ''){
		global $h1_title;
		$h1_title = $h1;
	}

	/*
	 * Return H1 title of current page
	 * 
	 * @return string
	 */
	public static function getH1Title($additionalClass){
		global $h1_title;
		$className = 'b-title'.(!empty($additionalClass) ? ' '.$additionalClass : '');
		return (!empty($h1_title)) ? sprintf('<div class="%s"><h1>%s</h1></div>', $className, $h1_title) : '';
	}

	/*
	 * Check main page
	 * @return boolean
	 */
	public static function checkMainPage(){
		global $APPLICATION;

		if(!isset(self::$bMainPage))
			self::$bMainPage = ($APPLICATION->GetCurPage(false) == SITE_DIR);

		return self::$bMainPage;
	}

	/*
	 *
	 * @return void
	 */
	public static function checkPage404(){
		if(
			!defined('ADMIN_SECTION') &&
			(\CHTTP::GetLastStatus() == '404 Not Found' || defined("ERROR_404")) &&
			file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/templates/' . SITE_ID . '_404/header.php')
		){
			global $APPLICATION;
			$APPLICATION->RestartBuffer();
			\CHTTP::SetStatus('404 Not Found');
			include($_SERVER['DOCUMENT_ROOT'] . '/local/templates/' . SITE_ID . '_404/header.php');
			include($_SERVER['DOCUMENT_ROOT'] . SITE_DIR . '404.php');
			self::checkH1Title();
			include($_SERVER['DOCUMENT_ROOT'] . '/local/templates/' . SITE_ID . '_404/footer.php');
			die();
		}
	}

	/*
	 * 
	 */
	public static function checkPageOptions(){
		global $APPLICATION;
		self::checkMainPage();
		self::checkH1Title();
		self::checkPage404();
		if(strpos($APPLICATION->GetCurPage(true), 'index.php') === false){
			$APPLICATION->AddChainItem($APPLICATION->GetTitle());
		}
	}

}