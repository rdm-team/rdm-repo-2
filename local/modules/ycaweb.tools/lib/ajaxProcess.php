<?php

namespace Ycaweb;

define('AJX_PRCS', true);

/**
 * Errors defines
 */
define('SESSION_ERROR', 1);
define('SESSION_ERROR_TEXT', 'Ошибка при проверке ключа безопасности.');

define('EMPTY_MODULE', 2);
define('EMPTY_MODULE_TEXT', 'Модуль не найден');

define('NOT_FOUND_MODULE', 3);
define('NOT_FOUND_MODULE_TEXT', '');

define('NOT_FOUND_ACTION', 4);
define('NOT_FOUND_ACTION_TEXT', 'Не найдено действие.');

define('KERNEL_PANIC', 99);

/**
 * Class ajaxProcess
 * @package ycaweb.tools
 */
class ajaxProcess{
	/**
	 *
	 * @var array
	 */
	protected $request = array();
	protected $strRequestPrefx = "jxprocess";

	/**
	 *
	 * @var array
	 */
	protected $return;

	/**
	 *
	 * @var string
	 */
	protected $sessid;

	/**
	 * @var bool
	 */
	protected $debug = false;

	/**
	 * @return void
	 */
	protected function send(){
		if($this->return == null){
			$this->return = array(
				'result' => false
			);
		}
		print(json_encode($this->return));
		exit();
	}

	/**
	 *
	 * @param int $errorType
	 * @param array $returnArray
	 */
	public function returnError($errorType, $returnArray){
		$this->return = array(
			'error'		=> true,
			'type'		=> $errorType,
			'answer'	=> $returnArray
		);
		$this->send();
	}

	/**
	 * @return void
	 */
	public function init(){
		if(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) === $_SERVER['SERVER_NAME']){
			if(isset($this->request['module']) && !empty($this->request['module'])){
				if(preg_match('/[a-zA-Z0-9_-]+/', $this->request['module'])){
					$module_path = $_SERVER['DOCUMENT_ROOT'] . '/local/ajax/classes/'. $this->request['module'] .'.class.php';
					if(file_exists($module_path)){
						if(!isset($this->request['action']) || empty($this->request['action'])){
							$this->returnError(NOT_FOUND_ACTION, array(
																	  'message' => NOT_FOUND_ACTION_TEXT
																 ));
						}else{
							$action = $this->request['action'];
							include_once($module_path);
							$module = new $this->request['module'];

							$module->doAction($action);
						}
					}else{
						$this->returnError(NOT_FOUND_MODULE, array(
																  'message' => 'Модуль ' . $this->request['module'] . ' не найден'
															 ));
					}
				}else{
					$this->returnError(KERNEL_PANIC, array(
														  'message' => 'Incorrect request'
													 ));
				}
			}else{
				$this->returnError(NOT_FOUND_MODULE, array(
														  'message' => 'В запросе не указан модуль'
													 ));
			}
		}else{
			$this->returnError(KERNEL_PANIC, array(
												  'message' => 'Incorrect request'
											 ));
		}
	}
}

/**
 * Class ajaxRequest
 */
class ajaxRequest extends ajaxProcess{
	/**
	 * @return \ajaxRequest
	 */
	public function __construct(){
		$this->request['action'] = $_REQUEST[$this->strRequestPrefx]['action'];
		$this->request['module'] = $_REQUEST[$this->strRequestPrefx]['module'];

		$this->sessid = $_REQUEST[$this->strRequestPrefx]['sessid'];

		if(isset($_REQUEST[$this->strRequestPrefx]['debug']) && !empty($_REQUEST[$this->strRequestPrefx]['debug'])){
			$this->debug = true;
		}

		/**
		 * Check sessid
		 */
		if(!$this->debug){
			/**
			 * Check action for getting new session identifer
			 */
			if($this->request['action'] != 'getNewSessid' && $this->request['module'] != 'kernel'){
				if($this->sessid != bitrix_sessid()){
					$this->returnError(SESSION_ERROR, array(
														   'message' => SESSION_ERROR_TEXT
													  ));
				}
			}
		}
	}
}

class ajaxModules extends ajaxProcess{
	/**
	 * @param string $action
	 * @return void
	 */
	public function doAction($action){
		if(strpos($action, "_") !== 0){
			if(is_callable(array($this, $action))){
				$this->$action();
			}else{
				$this->returnError(NOT_FOUND_ACTION, array(
														  'message' => NOT_FOUND_ACTION_TEXT
													 ));
			}
		}
	}

	/**
	 * @param array $resultArray
	 * @return void
	 */
	protected function moduleResult($resultArray){
		$this->return = array(
			'error'		=> false,
			'answer'	=> $resultArray
		);
		$this->send();
	}

}