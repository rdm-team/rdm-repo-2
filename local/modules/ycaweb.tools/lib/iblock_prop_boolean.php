<?php

namespace Ycaweb;

use Bitrix\Main;
use Bitrix\Main\Localization\Loc;


Loc::loadMessages(__FILE__);

class CIBlockPropertyBoolean{

	const	VALUE_TRUE = 1,
			VALUE_FALSE = 0;

	public static function GetUserTypeDescription(){
		return array(
			'PROPERTY_TYPE'			=> 'N',
			'USER_TYPE'				=> 'boolean',
			'DESCRIPTION'			=> Loc::getMessage('YCAWEB_IBLOCK_PROP_BOOLEAN'),
			'PrepareSettings'		=> array(__CLASS__, 'PrepareSettings'),
			'GetSettingsHTML' 		=> array(__CLASS__, 'GetSettingsHTML'),
			'GetAdminListViewHTML'	=> array(__CLASS__, 'GetAdminListViewHTML'),
			'GetPropertyFieldHtml'	=> array(__CLASS__, 'GetPropertyFieldHtml'),
			'GetPublicViewHTML'		=> array(__CLASS__, 'GetPublicViewHTML'),
			'GetPublicEditHTML'		=> array(__CLASS__, 'GetPublicEditHTML'),
			'ConvertToDB'			=> array(__CLASS__, 'ConvertToDB'),
			'ConvertFromDB'			=> array(__CLASS__, 'ConvertFromDB'),
		);
	}

	/**
	 * @param $arProperty
	 * @param $value
	 * @return mixed
	 */
	public static function ConvertToDB($arProperty, $value){
		$value['VALUE'] = intval($value['VALUE']);
		if ($value['VALUE'] != self::VALUE_TRUE) {
			$value['VALUE'] = self::VALUE_FALSE;
		}
		return $value;
	}

	/**
	 * @param $arProperty
	 * @param $value
	 * @return mixed
	 */
	public static function ConvertFromDB($arProperty, $value){
		if ($value['VALUE'] != '') {
			$value['VALUE'] = intval($value['VALUE']);
			if ($value['VALUE'] != self::VALUE_TRUE) {
				$value['VALUE'] = self::VALUE_FALSE;
			}
		}
		return $value;
	}

	public static function GetSettingsHTML($arFields,$strHTMLControlName, &$arPropertyFields) {
		$arPropertyFields = array(
			'HIDE' => array('ROW_COUNT', 'COL_COUNT', 'MULTIPLE_CNT', 'WITH_DESCRIPTION'),
			'USER_TYPE_SETTINGS_TITLE' => Loc::getMessage('YCAWEB_IBLOCK_PROP_BOOLEAN_SETTING_TITLE'),
		);
		$arSettings = self::PrepareSettings($arFields);

		ob_start();
		?>
		<tr>
			<td><?php echo Loc::getMessage('YCAWEB_IBLOCK_PROP_BOOLEAN_VALUE_FALSE'); ?></td>
			<td>
				<input type="text" name="<?php echo $strHTMLControlName['NAME']; ?>[VIEW][<?php echo self::VALUE_FALSE; ?>]" value="<?php echo htmlspecialcharsbx($arSettings['VIEW'][self::VALUE_FALSE]); ?>">
			</td>
		</tr>
		<tr>
			<td><?php echo Loc::getMessage('YCAWEB_IBLOCK_PROP_BOOLEAN_VALUE_TRUE'); ?></td>
			<td>
				<input type="text" name="<?php echo $strHTMLControlName['NAME']; ?>[VIEW][<?php echo self::VALUE_TRUE; ?>]" value="<?php echo htmlspecialcharsbx($arSettings['VIEW'][self::VALUE_TRUE]); ?>">
			</td>
		</tr>
		<?
		$strResult = ob_get_clean();

		return $strResult;
	}

	/**
	 * @param $arProperty
	 * @param $arValue
	 * @param $strHTMLControlName
	 * @return string
	 */
	public static function GetPropertyFieldHTML($arProperty, $arValue, $strHTMLControlName) {
		if ($arValue['VALUE'] === null || $arValue['VALUE'] === false || $arValue['VALUE'] === '') {
			$newID = (!isset($_REQUEST['ID']) || (int)$_REQUEST['ID'] <= 0 || (isset($_REQUEST['action']) && $_REQUEST['action'] == 'copy'));
			if ($newID) {
				$arValue['VALUE'] = $arProperty['DEFAULT_VALUE'];
			} else {
				$arValue['VALUE'] = self::VALUE_FALSE;
			}
			unset($index);
		}
		$arValue['VALUE'] = (int)$arValue['VALUE'];
		if ($arValue['VALUE'] != self::VALUE_TRUE) {
			$arValue['VALUE'] = self::VALUE_FALSE;
		}

		$strResult = '<input type="hidden" name="'.htmlspecialcharsbx($strHTMLControlName['VALUE']).'" id="'.$strHTMLControlName['VALUE'].'_N" value="'.self::VALUE_FALSE.'" />'
			.'<input type="checkbox" name="'.htmlspecialcharsbx($strHTMLControlName['VALUE']).'" id="'.$strHTMLControlName['VALUE'].'_Y" value="'.self::VALUE_TRUE.'" '.($arValue['VALUE'] == self::VALUE_TRUE ? 'checked="checked"' : '').'/>';
		return $strResult;
	}

	/**
	 * @param $arProperty
	 * @param $arValue
	 * @param $strHTMLControlName
	 * @return string
	 */
	public static function GetAdminListViewHTML($arProperty, $arValue, $strHTMLControlName) {
		$arSettings = static::PrepareSettings($arProperty);
		if ('|'.$arValue['VALUE'] != '|'.(int)$arValue['VALUE']) {
			return Loc::getMessage('YCAWEB_IBLOCK_PROP_BOOLEAN_ABSENT');
		}
		if ($arValue['VALUE'] != self::VALUE_TRUE && $arValue['VALUE'] != self::VALUE_FALSE) {
			return Loc::getMessage('YCAWEB_IBLOCK_PROP_BOOLEAN_ABSENT');
		}
		return htmlspecialcharsex($arSettings['VIEW'][$arValue['VALUE']]);
	}

	/**
	 * @param $arProperty
	 * @param $strHTMLControlName
	 * @return string
	 */
	public static function GetAdminFilterHTML($arProperty, $strHTMLControlName) {

		$arSettings = static::PrepareSettings($arProperty);

		$strCurValue = '';
		if (array_key_exists($strHTMLControlName['VALUE'], $_REQUEST) && ($_REQUEST[$strHTMLControlName['VALUE']] == self::VALUE_TRUE || $_REQUEST[$strHTMLControlName['VALUE']] == self::VALUE_FALSE)) {
			$strCurValue = $_REQUEST[$strHTMLControlName['VALUE']];
		} elseif (isset($GLOBALS[$strHTMLControlName['VALUE']]) && ($GLOBALS[$strHTMLControlName['VALUE']] == self::VALUE_TRUE || $GLOBALS[$strHTMLControlName['VALUE']] == self::VALUE_FALSE)) {
			$strCurValue = $GLOBALS[$strHTMLControlName['VALUE']];
		}

		$strResult = '<select name="'.htmlspecialcharsbx($strHTMLControlName['VALUE']).'" id="filter_'.htmlspecialcharsbx($strHTMLControlName['VALUE']).'">';
		$strResult .= '<option value=""'.('' == $strCurValue ? ' selected="selected"' : '').'>'.htmlspecialcharsex(Loc::getMessage('YCAWEB_IBLOCK_PROP_BOOLEAN_EMPTY')).'</option>';
		foreach ($arSettings['VIEW'] as $key => $value) {
			$strResult .= '<option value="'.intval($key).'"'.($strCurValue != '' && $key == $strCurValue ? ' selected="selected"' : '').'>'.htmlspecialcharsex($value).'</option>';
		}
		$strResult .= '</select>';

		return $strResult;
	}

	/**
	 * @param $arProperty
	 * @param $arValue
	 * @param $strHTMLControlName
	 * @return string
	 */
	public static function GetPublicViewHTML($arProperty, $arValue, $strHTMLControlName) {
		$arSettings = static::PrepareSettings($arProperty);
		if($arValue['VALUE'] !== self::VALUE_TRUE) {
			$arValue['VALUE'] = self::VALUE_FALSE;
		}
		return htmlspecialcharsex($arSettings['VIEW'][$arValue['VALUE']]);
	}

	/**
	 * @param $arProperty
	 * @param $arValue
	 * @param $strHTMLControlName
	 * @return string
	 */
	public static function GetPublicEditHtml($arProperty, $arValue, $strHTMLControlName) {
		if ($arValue['VALUE'] === null || $arValue['VALUE'] === false || $arValue['VALUE'] === '') {
			$arValue['VALUE'] = $arProperty['DEFAULT_VALUE'];
		}

		$arValue['VALUE'] = (int)$arValue['VALUE'];
		if ($arValue['VALUE'] != self::VALUE_TRUE) {
			$arValue['VALUE'] = self::VALUE_FALSE;
		}

		$strResult = '<input type="hidden" name="'.htmlspecialcharsbx($strHTMLControlName['VALUE']).'" id="'.$strHTMLControlName['VALUE'].'_N" value="'.self::VALUE_FALSE.'" />'.
			'<input type="checkbox" name="'.htmlspecialcharsbx($strHTMLControlName['VALUE']).'" id="'.$strHTMLControlName['VALUE'].'_Y" value="'.self::VALUE_TRUE.'" '.($arValue['VALUE'] == self::VALUE_TRUE ? 'checked="checked"' : '').'/>';
		return $strResult;
	}

	/**
	 * @param $arFields
	 * @return array
	 */
	public static function PrepareSettings($arFields) {
		$arDefView = self::GetDefaultListValues();
		$arView = array();
		if (
			array_key_exists('USER_TYPE_SETTINGS', $arFields) && is_array($arFields['USER_TYPE_SETTINGS']) &&
			array_key_exists('VIEW', $arFields['USER_TYPE_SETTINGS']) &&
			!empty($arFields['USER_TYPE_SETTINGS']['VIEW']) && is_array($arFields['USER_TYPE_SETTINGS']['VIEW'])
		) {
			$arView = $arFields['USER_TYPE_SETTINGS']['VIEW'];
		}

		if (empty($arView)) {
			$arView = $arDefView;
		}

		return array(
			'VIEW' => $arView
		);
	}

	/**
	 * @param $arProperty
	 * @param $strHTMLControlName
	 * @return string
	 */
	public static function GetPublicFilterHTML($arProperty, $strHTMLControlName) {
		$arSettings = static::PrepareSettings($arProperty);

		$strCurValue = '';
		if (isset($_REQUEST[$strHTMLControlName['VALUE']]) && ($_REQUEST[$strHTMLControlName['VALUE']] == self::VALUE_TRUE || $_REQUEST[$strHTMLControlName['VALUE']] == self::VALUE_FALSE)) {
			$strCurValue = $_REQUEST[$strHTMLControlName['VALUE']];
		}

		$strResult = '<select name="'.htmlspecialcharsbx($strHTMLControlName['VALUE']).'" id="filter_'.htmlspecialcharsbx($strHTMLControlName['VALUE']).'">';
		$strResult .= '<option value=""'.('' == $strCurValue ? ' selected="selected"' : '').'>'.htmlspecialcharsex(Loc::getMessage('YCAWEB_IBLOCK_PROP_BOOLEAN_VALUE_EMPTY')).'</option>';
		foreach ($arSettings['VIEW'] as $key => $value) {
			$strResult .= '<option value="'.intval($key).'"'.($strCurValue != '' && $key == $strCurValue ? ' selected="selected"' : '').'>'.htmlspecialcharsex($value).'</option>';
		}
		$strResult .= '</select>';

		return $strResult;
	}

	/**
	 * @return array
	 */
	protected function GetDefaultListValues() {
		return array(
			self::VALUE_FALSE	=> Loc::getMessage('YCAWEB_IBLOCK_PROP_BOOLEAN_VALUE_FALSE'),
			self::VALUE_TRUE	=> Loc::getMessage('YCAWEB_IBLOCK_PROP_BOOLEAN_VALUE_TRUE')
		);
	}

}