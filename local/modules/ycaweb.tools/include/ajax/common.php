<?php
if(strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') die('Forbidden');

//Turn off all error reporting
//error_reporting(0);
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/local/modules/ycaweb.tools/lib/ajaxProcess.php');

//Init ajaxRequest
$ajax = new \Ycaweb\ajaxRequest;
$ajax->init();