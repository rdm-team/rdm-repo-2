<?php
$MESS['YCAWEB_TOOLS_MODULE_NAME'] = 'Инструменты Юка';
$MESS['YCAWEB_TOOLS_MODULE_DESCRIPTION'] = '';
$MESS['YCAWEB_TOOLS_PARTNER_NAME'] = 'Агентство интернет-решений "Юка"';
$MESS['YCAWEB_TOOLS_MODULE_INSTALL_TITLE'] = 'Установка модуля "' . $MESS['YCAWEB_TOOLS_MODULE_NAME'] . '"';
$MESS['YCAWEB_TOOLS_MODULE_UNINSTALL_TITLE'] = 'Деинсталляция модуля "' . $MESS['YCAWEB_TOOLS_MODULE_NAME'] . '"';
$MESS['NOT_SHOW_PAGE_TITLE'] = 'Скрывать заголовок страницы';
$MESS['NOT_SHOW_NAV_CHAIN'] = 'Скрывать цепочку навигации';

$MESS['MOD_INST_CUSTOM_EMAIL_TPL'] = 'Использовать собственный шаблон писем в формате html';
$MESS['MOD_INST_ACCEPT'] = 'Установить';
$MESS['MOD_INST_SUCCESS'] = 'Модуль успешно установлен';
$MESS['MOD_INST_ERROR'] = 'При установке модуля произошла ошибка';

$MESS['MOD_ACCESS_DENIED'] = "Доступ закрыт";
$MESS['MOD_ACCESS_OPENED'] = "Доступ открыт";
$MESS['MOD_ACCESS_FULL'] = "Полный доступ";