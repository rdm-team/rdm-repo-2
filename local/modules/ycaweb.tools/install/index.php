<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main;

$strPath2Lang = str_replace("\\", "/", __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang)-strlen("/install/index.php")).'/install.php';

Loc::loadMessages($strPath2Lang);

if (class_exists('ycaweb_tools'))
	return;

/*
 *
 *
 * @author Shashenko Andrei
 * @copyright http://www.ycaweb.ru
 * @package ycaweb.tools
 */

class ycaweb_tools extends \CModule{

	public $MODULE_ID = 'ycaweb.tools';
	public $MODULE_NAME;
	public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
	public $MODULE_DESCRIPTION;
	public $MODULE_GROUP_RIGHTS = 'Y';
	public $errors;
	public $modulePath = '';

	public function __construct(){
		$arModuleVersion = array();
		$path = str_replace('\\', '/', __FILE__);
		$path = substr($path, 0, strlen($path) - strlen('/index.php'));
		include($path.'/version.php');

		$this->modulePath = $_SERVER['DOCUMENT_ROOT'].'/local/modules/'.$this->MODULE_ID;

		$this->MODULE_VERSION = $arModuleVersion['VERSION'];
		$this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];

		$this->MODULE_NAME = Loc::getMessage('YCAWEB_TOOLS_MODULE_NAME');
		$this->MODULE_DESCRIPTION = Loc::getMessage('YCAWEB_TOOLS_MODULE_DESCRIPTION');
		$this->PARTNER_NAME = Loc::getMessage('YCAWEB_TOOLS_PARTNER_NAME');
		$this->PARTNER_URI = 'http://www.ycaweb.ru';
	}
	
	public function DoInstall(){
		global $APPLICATION, $step, $errors;

		if (!\Bitrix\Main\ModuleManager::isModuleInstalled($this->MODULE_ID))
		{
			/*
			$step = intval($step);
			if ($step < 2)
			{
				$APPLICATION->IncludeAdminFile(Loc::getMessage('YCAWEB_TOOLS_MODULE_INSTALL_TITLE'), $this->modulePath.'/install/step1.php');
			}
			elseif($step == 2)
			{*/
				$errors = false;

				$this->InstallDB();
				$this->InstallFiles();
				$this->InstallEvents();

				$APPLICATION->IncludeAdminFile(Loc::getMessage('YCAWEB_TOOLS_MODULE_INSTALL_TITLE'), $this->modulePath.'/install/step2.php');
			//}
		}
	}

	public function DoUnInstall(){
		$this->UnInstallDB();
		$this->UnInstallFiles();
		$this->UnInstallEvents();
	}
	
	public function InstallDB(){
		\Bitrix\Main\ModuleManager::registerModule($this->MODULE_ID);

		//Add site prop types
		$arSitesID = array('');
		$rsSites = CSite::GetList($by = 'sort', $order = 'desc', array());
		while ($arSite = $rsSites->Fetch())
		{
			$arSitesID[] = $arSite['ID'];
		}

		foreach($arSitesID as $siteID){
			$propstypes = \Bitrix\Main\Config\Option::get('fileman', 'propstypes', false, $siteID);
			$propstypes = unserialize(stripslashes($propstypes));
			$propstypes['not_show_page_title'] = Loc::getMessage('NOT_SHOW_PAGE_TITLE');
			$propstypes['not_show_nav_chain'] = Loc::getMessage('NOT_SHOW_NAV_CHAIN');
			\Bitrix\Main\Config\Option::set('fileman', 'propstypes', addslashes(serialize($propstypes)), $siteID);
		}

		//Add developer info
		if(file_exists($this->modulePath . '/include/gadgets_admin_index.php')){
			$developerInfo = '';
			ob_start();
			include $this->modulePath . '/include/gadgets_admin_index.php';
			$developerInfo = ob_get_clean();
			if(!empty($developerInfo)){
				$rsUserOptions = CUserOptions::GetList(array('ID' => 'ASC'), array('CATEGORY' => 'intranet', 'NAME' => '~gadgets_admin_index'));
				while($arOptions = $rsUserOptions->Fetch()){
					$arGadgetSettings = unserialize($arOptions['VALUE']);
					if(isset($arGadgetSettings[0]['GADGETS']['HTML_AREA@444444444'])){
						$arGadgetSettings[0]['GADGETS']['HTML_AREA@444444444']['USERDATA']['content'] = $developerInfo;
						CUserOptions::SetOption($arOptions['CATEGORY'], $arOptions['NAME'], $arGadgetSettings, ($arOptions['COMMON'] == 'Y'), ($arOptions['USER_ID'] > 0 ? $arOptions['USER_ID'] : false));
					}
				}
			}
		}

		return true;
	}
	
	public function UnInstallDB(){
		\Bitrix\Main\ModuleManager::unRegisterModule($this->MODULE_ID);

		return true;
	}
	
	public function InstallEvents(){
		RegisterModuleDependences('main', 'OnEpilog', $this->MODULE_ID, '\Ycaweb\Tools', 'checkPageOptions');
		RegisterModuleDependences('main', 'OnUserTypeBuildList', $this->MODULE_ID, '\Ycaweb\CUserTypeHTMLEditor', 'GetUserTypeDescription');
		RegisterModuleDependences('iblock', 'OnIBlockPropertyBuildList', $this->MODULE_ID, '\Ycaweb\CIBlockPropertyBoolean', 'GetUserTypeDescription');
		RegisterModuleDependences('iblock', 'OnIBlockPropertyBuildList', $this->MODULE_ID, '\Ycaweb\CIBlockPropertyColor', 'GetUserTypeDescription');
		RegisterModuleDependences('iblock', 'OnIBlockPropertyBuildList', $this->MODULE_ID, '\Ycaweb\CIBlockPropertyMap2GIS', 'GetUserTypeDescription');

		return true;
	}
	
	public function UnInstallEvents(){
		UnRegisterModuleDependences('main', 'OnEpilog', $this->MODULE_ID, '\Ycaweb\Tools', 'CheckPageOptions');
		UnRegisterModuleDependences('main', 'OnUserTypeBuildList', $this->MODULE_ID, '\Ycaweb\CUserTypeHTMLEditor', 'GetUserTypeDescription');
		UnRegisterModuleDependences('iblock', 'OnIBlockPropertyBuildList', $this->MODULE_ID, '\Ycaweb\CIBlockPropertyBoolean', 'GetUserTypeDescription');
		UnRegisterModuleDependences('iblock', 'OnIBlockPropertyBuildList', $this->MODULE_ID, '\Ycaweb\CIBlockPropertyColor', 'GetUserTypeDescription');
		UnRegisterModuleDependences('iblock', 'OnIBlockPropertyBuildList', $this->MODULE_ID, '\Ycaweb\CIBlockPropertyMap2GIS', 'GetUserTypeDescription');

		return true;
	}
	
	public function InstallFiles(){
		CopyDirFiles($this->modulePath.'/install/components', $_SERVER['DOCUMENT_ROOT'].'/local/components', false, true);
		CopyDirFiles($this->modulePath.'/install/js', $_SERVER['DOCUMENT_ROOT'].'/bitrix/js/', true, true);

		return true;
	}
	
	public function UnInstallFiles($arParams = array()){
		DeleteDirFilesEx('/bitrix/js/'.$this->MODULE_ID.'/');

		return true;
	}
	
	public function GetModuleRightList(){
        global $MESS;
        $arr = array(
            'reference_id' => array('D','R','W'),
            'reference' => array(
                Loc::getMessage('MOD_ACCESS_DENIED'),
                Loc::getMessage('MOD_ACCESS_OPENED'),
                Loc::getMessage('MOD_ACCESS_FULL'))
            );
        return $arr;
    }
	
}