<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	'NAME' => GetMessage('LP_BLOCK_COMPONENT_NAME'),
	'DESCRIPTION' => GetMessage('LP_BLOCK_COMPONENT_DESC'),
	'ICON' => '/images/lp-block.gif',
	'PATH' => array(
		'ID' => 'ycaweb',
	),
);