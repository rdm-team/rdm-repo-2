<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arDefaultParams = include __DIR__ . '/default_params.php';

$site = ($_REQUEST["site"] <> ''? $_REQUEST["site"] : ($_REQUEST["src_site"] <> ''? $_REQUEST["src_site"] : false));

$arEventTypes = array();
$arFilter = array("LID" => SITE_ID);
$rsET = CEventType::GetList(array());
while($arType = $rsET->Fetch($arFilter)){
	$arEventTypes[$arType["EVENT_NAME"]] = $arType["NAME"] . " [".$arType["EVENT_NAME"]."]";
}

$arFilter = array("ACTIVE" => "Y");
$arFilter["TYPE_ID"] = ($arCurrentValues["EVENT_TYPE_NAME"]) ? $arCurrentValues["EVENT_TYPE_NAME"] : $arDefaultParams['EVENT_TYPE_NAME'];
$arEventMessages = Array();
$dbType = CEventMessage::GetList($by="ID", $order="DESC", $arFilter);
while($arEvent = $dbType->GetNext())
	$arEventMessages[$arEvent["ID"]] = "[".$arEvent["ID"]."] ".$arEvent["SUBJECT"];

$arComponentParameters = array(
	"GROUPS" => array(
		"SAVE_DATA_SETTINGS" => array(
			"NAME" => GetMessage("MFP_SAVE_DATA_GROUP")
		)
	),
	"PARAMETERS" => array(
		"FORM_TITLE" => Array(
			"NAME" => GetMessage("MFP_FORM_TITLE"),
			"TYPE" => "STRING",
			"DEFAULT" => "",
			"PARENT" => "BASE",
		),
		"OK_TEXT" => Array(
			"NAME" => GetMessage("MFP_OK_MESSAGE"),
			"TYPE" => "STRING",
			"DEFAULT" => GetMessage("MFP_OK_TEXT"),
			"PARENT" => "BASE",
		),
		"REQUIRED_FIELDS" => Array(
			"NAME" => GetMessage("MFP_REQUIRED_FIELDS"),
			"TYPE"=>"LIST",
			"MULTIPLE"=>"Y",
			"VALUES" => array_merge(array("NONE" => GetMessage("MFP_ALL_REQ")), $arDefaultParams['FIELDS']),
			"DEFAULT"=>"",
			"COLS"=>25,
			"PARENT" => "BASE",
		),
		"EMAIL_TO" => Array(
			"NAME" => GetMessage("MFP_EMAIL_TO"),
			"TYPE" => "STRING",
			"DEFAULT" => htmlspecialchars(COption::GetOptionString("main", "email_from")),
			"PARENT" => "BASE",
		),
		"EVENT_TYPE_NAME" => Array(
			"NAME" => GetMessage("MFP_EMAIL_TYPE"),
			"TYPE" =>"LIST",
			"VALUES" => $arEventTypes,
			"DEFAULT" => $arDefaultParams['EVENT_TYPE_NAME'],
			"COLS" => 25,
			"PARENT" => "BASE",
			"REFRESH" => "Y"
		),
		"EVENT_MESSAGE_ID" => Array(
			"NAME" => GetMessage("MFP_EMAIL_TEMPLATES"),
			"TYPE"=>"LIST",
			"VALUES" => $arEventMessages,
			"DEFAULT"=>"",
			"MULTIPLE"=>"Y",
			"COLS"=>25,
			"PARENT" => "BASE",
		),
		"USE_CAPTCHA" => Array(
			"NAME" => GetMessage("MFP_CAPTCHA"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
			"PARENT" => "BASE",
		),
		"WRAPPER_CLASSNAME" => Array(
			"NAME" => GetMessage("MFP_WRAPPER_CLASSNAME"),
			"TYPE" => "STRING",
			"DEFAULT" => "",
			"PARENT" => "BASE",
		),
		"SAVE_DATA" => Array(
			"NAME" => GetMessage("MFP_SAVE_DATA"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
			"PARENT" => "BASE",
			"REFRESH" => "Y",
		),
		"AJAXPROCESS_MODE" => Array(
			"NAME" => GetMessage("MFP_AJAXPROCESS_MODE"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
			"PARENT" => "AJAX_SETTINGS",
		),
		"AJAX_MODE" => array(),
	)
);

if($arCurrentValues["SAVE_DATA"] == "Y"){
	$arAddParameters = array();
	if(CModule::IncludeModule("iblock")){

		$arIBlockType = CIBlockParameters::GetIBlockTypes();

		$arIBlock = array();
		$rsIBlock = CIBlock::GetList(Array("sort" => "asc"), Array("TYPE" => $arCurrentValues["IBLOCK_TYPE"], "ACTIVE"=>"Y"));
		while($arr=$rsIBlock->Fetch())
			$arIBlock[$arr["ID"]] = "[".$arr["ID"]."] ".$arr["NAME"];

		$arAddParameters = array(
			"SAVE_IBLOCK_TYPE" => array(
				"PARENT" => "SAVE_DATA_SETTINGS",
				"NAME" => GetMessage("MFP_SAVE_IBLOCK_TYPE"),
				"TYPE" => "LIST",
				"VALUES" => $arIBlockType,
				"REFRESH" => "Y",
			),
			"SAVE_IBLOCK_ID" => array(
				"PARENT" => "SAVE_DATA_SETTINGS",
				"NAME" => GetMessage("MFP_SAVE_IBLOCK_IBLOCK"),
				"TYPE" => "LIST",
				"VALUES" => $arIBlock,
				"REFRESH" => "N",
			),
		);

	}



	$arComponentParameters["PARAMETERS"] = array_merge($arComponentParameters["PARAMETERS"], $arAddParameters);
}