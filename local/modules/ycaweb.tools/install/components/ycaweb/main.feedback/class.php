<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/*
 * class CFeedbackComponent
 */

class CFeedbackComponent extends CBitrixComponent{

	public $arParams = array();

	protected $arFormFields = array();

	protected $pseudoFieldName = 'email';

	/*
	 * Prepare parameters
	 *
	 * @param array $arParams
	 * @return array
	 */
	public function onPrepareComponentParams($arParams){
		global $USER;

		$arDefaultParams = include __DIR__ . '/default_params.php';

		$arParams['FORM_TITLE'] = trim($arParams['FORM_TITLE']);
		$arParams['USE_CAPTCHA'] = (($arParams['USE_CAPTCHA'] != 'N' && !$USER->IsAuthorized()) ? true : false);
		$arParams['EMAIL_TO'] = trim($arParams['EMAIL_TO']);
		$arParams['EVENT_TYPE_NAME'] = trim($arParams['EVENT_TYPE_NAME']);
		$arParams['OK_TEXT'] = trim($arParams['OK_TEXT']);
		$arParams['FIELDS'] = $arDefaultParams['FIELDS'];

		$arParams['WRAPPER_CLASSNAME'] = trim($arParams['WRAPPER_CLASSNAME']);

		if(strlen($arParams['EMAIL_TO']) <= 0)
			$arParams['EMAIL_TO'] = COption::GetOptionString('main', 'email_from');

		if(strlen($arParams['EVENT_TYPE_NAME']) <= 0)
			$arParams['EVENT_TYPE_NAME'] = $arDefaultParams['EVENT_TYPE_NAME'];

		if(strlen($arParams['OK_TEXT']) <= 0)
			$arParams['OK_TEXT'] = GetMessage('MF_OK_MESSAGE');

		//Save data params
		$arParams['SAVE_DATA'] = ($arParams['SAVE_DATA'] == 'Y');
		$arParams['SAVE_IBLOCK_TYPE'] = trim($arParams['SAVE_IBLOCK_TYPE']);
		$arParams['SAVE_IBLOCK_ID'] = (int)$arParams['SAVE_IBLOCK_ID'];

		$this->arParams = $arParams;
		$this->getFormFields();

		return $arParams;
	}

	/*
	 *
	 */
	public function getFieldForPseudoCAPTCHA(){
		$strField = '';
		if(!$this->arParams['USE_CAPTCHA']){
			$strField = '<input style="position: absolute !important; top: 0; left: -9999px; opacity: 0 !important; height: 0 !important" type="text" name="' . $this->pseudoFieldName . '" value="">';
		}
		return $strField;
	}

	/*
	 * Get form values
	 *
	 * return void
	 */
	private function getFormFields(){
		foreach($this->arParams['FIELDS'] as $fieldName=>$fieldLabel){
			$fieldName = strtoupper($fieldName);
			$this->arFormFields[$fieldName] = array(
				'ID'		=> 'feedback-field_' . strtolower($fieldName),
				'NAME'		=> $fieldName,
				'LABEL' 	=> $fieldLabel,
				'REQUIRED'	=> (in_array($fieldName, $this->arParams['REQUIRED_FIELDS'])),
				'VALUE'		=> (isset($_POST[$fieldName])) ? htmlspecialcharsEx($_POST[$fieldName]) : '',
				'ERROR'		=> false
			);
		}
	}

	public function fillResult(){
		$arResult = array();
		if(!empty($this->arFormFields)){
			foreach($this->arFormFields as $name=>$arField){
				$arResult[$arField['NAME']] = $arField;
			}
		}

		return $arResult;
	}

	/*
	 * Validate form values
	 *
	 * @return array
	 */
	protected function validate(){
		$arErrors = array();

		if(check_bitrix_sessid()){
			if(empty($this->arParams['REQUIRED_FIELDS']) || !in_array('NONE', $this->arParams['REQUIRED_FIELDS'])){

				//Checking the emptiness
				foreach($this->arFormFields as $name=>$arField){
					if($arField['REQUIRED'] && empty($arField['VALUE'])){
						$arErrors[$arField['NAME']] = GetMessage('MF_REQ_'.$arField['NAME']);
						$this->arFormFields[$arField['NAME']]['ERROR'] = true;
					}
				}
			}
		}else{
			$arErrors["SESS"] = GetMessage("MF_SESS_EXP");
		}

		//Specific checking
		if(($this->arFormFields['AUTHOR_EMAIL']['REQUIRED'] || !empty($this->arFormFields['AUTHOR_EMAIL']['VALUE'])) && !check_email($this->arFormFields['AUTHOR_EMAIL']['VALUE'])){
			$arErrors['AUTHOR_EMAIL'] = GetMessage('MF_EMAIL_NOT_VALID');
			$this->arFormFields['AUTHOR_EMAIL']['ERROR'] = true;
		}
		if($this->arParams['USE_CAPTCHA']){
			include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/classes/general/captcha.php');
			$captcha_code = $_POST['CAPTCHA_SID'];
			$captcha_word = $_POST['CAPTCHA_WORD'];
			$cpt = new CCaptcha();
			$captchaPass = COption::GetOptionString('main', 'captcha_password', '');
			if (strlen($captcha_word) > 0 && strlen($captcha_code) > 0){
				if (!$cpt->CheckCodeCrypt($captcha_word, $captcha_code, $captchaPass))
					$arErrors['CAPTCHA'] = GetMessage('MF_CAPTCHA_WRONG');
			}else{
				$arErrors['CAPTCHA'] = GetMessage('MF_CAPTHCA_EMPTY');
			}
		}elseif(!empty($_POST[$this->pseudoFieldName]) && !$GLOBALS['$USER']->IsAuthorized()){
			$arErrors['CAPTCHA'] = 'Error!';
		}

		return $arErrors;
	}

	/*
	 * Get author ip address
	 *
	 * @return string
	 */
	protected function getIP(){
		$IP = '';
		if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			$arIPs[] = trim(strtok($_SERVER['HTTP_X_FORWARDED_FOR'], ','));
		if(isset($_SERVER['HTTP_CLIENT_IP']))
			$arIPs[] = $_SERVER['HTTP_CLIENT_IP'];
		if(isset($_SERVER['REMOTE_ADDR']))
			$arIPs[] = $_SERVER['REMOTE_ADDR'];
		if(isset($_SERVER['HTTP_X_REAL_IP']))
			$arIPs[] = $_SERVER['HTTP_X_REAL_IP'];

		//проверяем ip-адреса на валидность начиная с приоритетного.
		foreach($arIPs as $ipa){
			if(preg_match("#^([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})$#", $ipa)){
				$IP = $ipa;
				break;
			}
		}
	}

	/*
	 * Save form data
	 *
	 * @param array $arSaveData
	 * @return integer
	 */
	protected function saveData($arSaveData = array()){
		$ELEMENT_ID = 0;

		if(\Bitrix\Main\Loader::includeModule('iblock') && !empty($arSaveData) && $this->arParams['SAVE_IBLOCK_ID']){
			$arIBlockFields = CIBlock::GetFields($this->arParams['SAVE_IBLOCK_ID']);

			$arElementFields = array(
				'IBLOCK_ID'      	=> $this->arParams['SAVE_IBLOCK_ID'],
				'IBLOCK_SECTION_ID' => false,
				'ACTIVE'         	=> 'N',
				'NAME'           	=> $arIBlockFields['NAME']['DEFAULT_VALUE'],
				'PREVIEW_TEXT'   	=> $arSaveData['MESSAGE']['VALUE'],
				'PROPERTY_VALUES'	=> array(
					'AUTHOR_IP'		=> $this->getIP(),
				)
			);

			$resProp = \CIBlock::GetProperties($this->arParams['SAVE_IBLOCK_ID'], array(), array('ACTIVE' => 'Y'));
			while($arProp = $resProp->Fetch()){
				if(isset($arSaveData[$arProp['CODE']])){
					$arElementFields['PROPERTY_VALUES'][$arProp['CODE']] = $arSaveData[$arProp['CODE']]['VALUE'];
				}
			}

			$objElement = new \CIBlockElement();
			$ELEMENT_ID = $objElement->Add($arElementFields, false, false, false);
		}

		return (int)$ELEMENT_ID;
	}

	/*
	 * Prepare form data before send
	 *
	 * @return array
	 */
	public function prepareEventFields(){
		$arEventFields = array(
			'DATE'			=> strtolower(FormatDate('j F Y', time() + CTimeZone::GetOffset())),
			'TIME'			=> FormatDate('H:i', time() + CTimeZone::GetOffset()),
			'EMAIL_TO'		=> $this->arParams['EMAIL_TO'],
			'AUTHOR_IP'		=> $this->getIP(),
			'AUTHOR'		=> $this->arFormFields['AUTHOR_NAME']['VALUE'],
			'TEXT'			=> $this->arFormFields['MESSAGE']['VALUE'],
			'ADMIN_EDIT_URL'=> ''
		);
		$arEventFields['DATETIME'] = $arEventFields['DATE'] . ' ' . $arEventFields['TIME'];

		if(!empty($this->arFormFields)){
			foreach($this->arFormFields as $arField){
				$arEventFields[$arField['NAME']] = $arField['VALUE'];
			}
		}

		return $arEventFields;
	}

	/*
	 * Send email
	 *
	 * @param array $arFields
	 * @param string $eventName
	 * @param array $arMessages
	 * @return void
	 */
	public function eventProcess($arFields = array(), $eventName = '', $arMessages = array()){
		if(empty($eventName)) return;

		if(!empty($arMessages)){
			foreach($arMessages as $v)
				if(IntVal($v) > 0)
					CEvent::Send($eventName, SITE_ID, $arFields, "N", IntVal($v));
		}else{
			CEvent::Send($eventName, SITE_ID, $arFields);
		}
	}

	/*
	 * Execute component
	 */
	public function executeComponent(){
		global $APPLICATION, $USER;

		$this->arResult["PARAMS_HASH"] = md5(serialize($this->arParams).$this->GetTemplateName());

		$this->arResult['FIELDS'] = $this->fillResult();

		if(isset($_POST['submit']) && (!isset($_POST['PARAMS_HASH']) || $this->arResult['PARAMS_HASH'] === $_POST['PARAMS_HASH']))
		{
			$this->arResult['ERROR_MESSAGES'] = $this->validate();
			$this->arResult['FIELDS'] = $this->fillResult();

			if(empty($this->arResult['ERROR_MESSAGES']))
			{
				$arEventFields = $this->prepareEventFields();

				if($this->arParams['SAVE_DATA']){
					$SAVED_ELEMENT_ID = $this->saveData($this->arFormFields);
					if($SAVED_ELEMENT_ID){
						$arEventFields['ADMIN_EDIT_URL'] = sprintf('http://%s/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=%d&type=%s&ID=%d&lang=%s', $_SERVER['SERVER_NAME'], $this->arParams['SAVE_IBLOCK_ID'], $this->arParams['SAVE_IBLOCK_TYPE'], $SAVED_ELEMENT_ID, LANGUAGE_ID);
					}
				}

				$this->eventProcess($arEventFields, $this->arParams['EVENT_TYPE_NAME'], $this->arParams['EVENT_MESSAGE_ID']);

				$_SESSION['MF_NAME'] = $this->arFormFields['AUTHOR_NAME']['VALUE'];
				$_SESSION['MF_PHONE'] = $this->arFormFields['AUTHOR_PHONE']['VALUE'];
				$_SESSION['MF_EMAIL'] = $this->arFormFields['AUTHOR_EMAIL']['VALUE'];

				if($this->arParams['AJAXPROCESS_MODE'] != 'Y')
					LocalRedirect($APPLICATION->GetCurPageParam('success=Y', array('success')));
			}

			if(empty($this->arResult['ERROR_MESSAGES'])){
				$this->arResult['OK_MESSAGE'] = $this->arParams['OK_TEXT'];
			}
		}elseif($_REQUEST['success'] == 'Y'){
			$this->arResult['OK_MESSAGE'] = $this->arParams['OK_TEXT'];
		}

		if(empty($this->arResult['ERROR_MESSAGES'])){
			if($USER->IsAuthorized()){
				$rsUser = \CUser::GetByID($USER->GetID());
				$arUser = $rsUser->Fetch();

				$this->arResult['FIELDS']['AUTHOR_NAME']['VALUE'] = htmlspecialcharsEx($arUser['NAME']);
				$this->arResult['FIELDS']['AUTHOR_EMAIL']['VALUE'] = htmlspecialcharsEx($arUser['EMAIL']);
				$this->arResult['FIELDS']['AUTHOR_PHONE']['VALUE'] = htmlspecialcharsEx($arUser['PERSONAL_PHONE']);
			}else{
				if(strlen($_SESSION['MF_NAME']) > 0)
					$this->arResult['FIELDS']['AUTHOR_NAME']['VALUE'] = htmlspecialcharsEx($_SESSION['MF_NAME']);
				if(strlen($_SESSION['MF_PHONE']) > 0)
					$this->arResult['FIELDS']['AUTHOR_PHONE']['VALUE'] = htmlspecialcharsEx($_SESSION['MF_PHONE']);
				if(strlen($_SESSION['MF_EMAIL']) > 0)
					$this->arResult['FIELDS']['AUTHOR_EMAIL']['VALUE'] = htmlspecialcharsEx($_SESSION['MF_EMAIL']);
			}
		}

		if($this->arParams["USE_CAPTCHA"])
			$this->arResult["capCode"] =  htmlspecialcharsbx($APPLICATION->CaptchaGetCode());

		$this->IncludeComponentTemplate();
	}

} 