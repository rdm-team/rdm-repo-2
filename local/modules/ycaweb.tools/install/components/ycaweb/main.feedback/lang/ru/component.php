<?
$MESS ['MF_OK_MESSAGE'] = "Спасибо, ваше сообщение принято.";
$MESS ['MF_REQ_AUTHOR_NAME'] = "Укажите ваше имя";
$MESS ['MF_REQ_AUTHOR_DATE_OF_BIRTH'] = "Укажите дату рождения";
$MESS ['MF_REQ_AUTHOR_COMPANY'] = "Укажите название компании";
$MESS ['MF_REQ_AUTHOR_POST'] = "Укажите вашу должность";
$MESS ['MF_REQ_AUTHOR_PHONE'] = "Укажите корректный телефон";
$MESS ['MF_REQ_AUTHOR_EMAIL'] = "Укажите E-mail, на который хотите получить ответ";
$MESS ['MF_REQ_MESSAGE'] = "Вы не написали сообщение";
$MESS ['MF_REQ_VACANCY'] = "Вы не прикрепили файл";
$MESS ['MF_EMAIL_NOT_VALID'] = "Указанный E-mail некорректен";
$MESS ['MF_CAPTCHA_WRONG'] = "Неверно указан код защиты от автоматических сообщений.";
$MESS ['MF_CAPTHCA_EMPTY'] = "Не указан код защиты от автоматических сообщений.";
$MESS ['MF_SESS_EXP'] = "Ваша сессия истекла. Отправьте сообщение повторно.";
$MESS ['MF_DEFAULT_ELEMENT_NAME'] = "Элемент";

//Additional specific fields
$MESS ['MF_REQ_VACANCY'] = "Укажите вакансию";
$MESS ['MF_REQ_SERVICE'] = "Укажите услугу";
$MESS ['MF_REQ_FILE_CV'] = "Вы не прикрепили файл с резюме";
$MESS ['MF_REQ_FILE_ANKETA'] = "Вы не прикрепили файл с анкетой";
?>