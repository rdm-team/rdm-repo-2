<?
$MESS ['MFP_CAPTCHA'] = "Использовать защиту от автоматических сообщений (CAPTCHA) для неавторизованных пользователей";
$MESS ['MFP_OK_MESSAGE'] = "Сообщение, выводимое пользователю после отправки";
$MESS ['MFP_OK_TEXT'] = "Спасибо, ваше сообщение принято.";
$MESS ['MFP_EMAIL_TO'] = "Email, на который будет отправлено письмо";
$MESS ['MFP_REQUIRED_FIELDS'] = "Обязательные поля для заполнения";
$MESS ['MFP_ALL_REQ'] = "(все необязательные)";
$MESS ['MFP_FORM_TITLE'] = "Заголовок формы";
$MESS ['MFP_EMAIL_TYPE'] = "Тип почтового события";
$MESS ['MFP_EMAIL_TEMPLATES'] = "Почтовые шаблоны для отправки письма";
$MESS ['MFP_WRAPPER_CLASSNAME'] = "Класс обертки";
$MESS ['MFP_AJAXPROCESS_MODE'] = "Режим ajaxProcess";

$MESS ['MFP_SAVE_DATA_GROUP'] = "Параметры сохранения данных";
$MESS ['MFP_SAVE_DATA'] = "Сохранить данные формы";
$MESS ['MFP_SAVE_IBLOCK_TYPE'] = "Тип инфоблока";
$MESS ['MFP_SAVE_IBLOCK_IBLOCK'] = "Инфоблок";