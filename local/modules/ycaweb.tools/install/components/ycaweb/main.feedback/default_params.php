<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

return array(
	'EVENT_TYPE_NAME' => 'FEEDBACK_FORM',
	'FIELDS' => array(
		'AUTHOR_NAME'			=> Loc::getMessage('MFP_AUTHOR_NAME'),
		'AUTHOR_DATE_OF_BIRTH'	=> Loc::getMessage('MFP_AUTHOR_DATE_OF_BIRTH'),
		'AUTHOR_CITY'			=> Loc::getMessage('MFP_AUTHOR_CITY'),
		'AUTHOR_COMPANY'		=> Loc::getMessage('MFP_AUTHOR_COMPANY'),
		'AUTHOR_POST'			=> Loc::getMessage('MFP_AUTHOR_POST'),
		'AUTHOR_PHONE'			=> Loc::getMessage('MFP_AUTHOR_PHONE'),
		'AUTHOR_EMAIL'			=> Loc::getMessage('MFP_AUTHOR_EMAIL'),
		'MESSAGE'				=> Loc::getMessage('MFP_MESSAGE'),
		'FILE'					=> Loc::getMessage('MFP_FILE'),
	)
);