<?php if(!check_bitrix_sessid()) return;
use Bitrix\Main\Localization\Loc;
?>

<form action="<?=$APPLICATION->GetCurPage()?>">
	<?=bitrix_sessid_post()?>
	<input type="hidden" name="lang" value="<?=LANGUAGE_ID;?>">
	<input type="hidden" name="id" value="ycaweb.tools">
	<input type="hidden" name="install" value="Y">
	<input type="hidden" name="step" value="2">
	<p><input id="custom_email_tpl" type="checkbox" name="custom_email_tpl" value="Y"><label for="custom_email_tpl"><?=Loc::getMessage('MOD_INST_CUSTOM_EMAIL_TPL')?></label></p>
	<input type="submit" name="inst" value="<?=GetMessage('MOD_INST_ACCEPT')?>">
</form>