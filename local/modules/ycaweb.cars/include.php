<?php
$moduleId = "ycaweb.cars";

\Bitrix\Main\Loader::registerAutoLoadClasses($moduleId, array(
    'Ycaweb\OptionsTable'=>'/lib/db/optionsdb.php',
    'Ycaweb\Cars\EditForm'=>'/lib/edit_form.php',
    'Ycaweb\Cars\Cars'=>'/lib/cars.php',
    'Ycaweb\Cars\CarCharacteristicTable'=>'/lib/db/car_characteristicdb.php',
    'Ycaweb\Cars\CarCharacteristicValueTable'=>'/lib/db/car_characteristic_valuedb.php',
    'Ycaweb\Cars\CarEquipmentTable'=>'/lib/db/car_equipmentdb.php',
    'Ycaweb\Cars\CarGenerationTable'=>'/lib/db/car_generationdb.php',
    'Ycaweb\Cars\CarMarkTable'=>'/lib/db/car_markdb.php',
    'Ycaweb\Cars\CarModelTable'=>'/lib/db/car_modeldb.php',
    'Ycaweb\Cars\CarModificationTable'=>'/lib/db/car_modificationdb.php',
    'Ycaweb\Cars\CarOptionValueTable'=>'/lib/db/car_option_valuedb.php',
    'Ycaweb\Cars\CarOptionTable'=>'/lib/db/car_optiondb.php',
    'Ycaweb\Cars\CarSerieTable'=>'/lib/db/car_seriedb.php',
    'Ycaweb\Cars\CarTypeTable'=>'/lib/db/car_typedb.php',
));