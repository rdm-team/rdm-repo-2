<?
namespace Ycaweb\Cars;

use Bitrix\Main\Config\Option,
    Bitrix\Main\Loader;

IncludeModuleLangFile(__FILE__);
Loader::includeModule("iblock");

class Cars
{
    const MODULE_ID = "ycaweb.cars";
    private $arResult = array();

    function __construct()
    {

    }
	
    /**
     * Получает массив характеристик автомобиля
     * @return mixed
     */
    public static function getAllCharacteristics()
    {
        $arCharacteristics = array();
        $rsCharacteristics = CarCharacteristicTable::getList(
            array(
                'select' => array('id_car_characteristic', 'name'),
            )
        );
		
        while($arCharacteristic = $rsCharacteristics->Fetch())
		{
            $arCharacteristics[] =  $arCharacteristic;
        }

        if(!empty($arCharacteristics))
		{
            return $arCharacteristics;
        }

        return false;
    }


    /**
     * Получает список заполненных свойств-характеристик из настроек модуля
     * @return mixed
     */
    public static function getAutoOptions()
    {
        $arAutoOptions = array('car_mark', 'car_model', 'car_generation', 'car_serie', 'car_modification');
        $adsIblockID = Option::get(self::MODULE_ID, 'carIblock');

        foreach($arAutoOptions as $key=>$option)
        {
            $optionValue =  Option::get(self::MODULE_ID, $option);

            $prop = \Bitrix\Iblock\PropertyTable::getList(array(
                    'filter' => array('IBLOCK_ID' => $adsIblockID, 'CODE' => $optionValue),
                )
            );
            if($arProp = $prop->fetch())
            {
                $propTMP = array(
                    'NAME' => $arProp['NAME'],
                    'IBLOCK_CODE' => $arProp['CODE'],
                );
            }

            $arAutoOptions[$key] = $propTMP;
        }
        if(!empty($arAutoOptions))
        {
            return $arAutoOptions;
        }

        return false;
    }


    /**
     * Получает список заполненных свойств-характеристик из настроек модуля
     * @return mixed
     */
    public static function getChosenOptions()
    {
        $arProps = array();
        $arCharacteristics = self::getAllCharacteristics();
        $adsIblockID = Option::get(self::MODULE_ID, 'carIblock');

        foreach($arCharacteristics as $arCharacteristic)
        {
            $propCode = Option::get(self::MODULE_ID, $arCharacteristic['id_car_characteristic']);

            if(!empty($propCode))
            {
                $propTMP = array(
                    'TABLE_ID' => $arCharacteristic['id_car_characteristic'],
                    'IBLOCK_CODE' => $propCode,
                );

                $prop = \Bitrix\Iblock\PropertyTable::getList(array(
                        'filter' => array('IBLOCK_ID' => $adsIblockID, 'CODE' => $propCode),
                    )
                );
                if($arProp = $prop->fetch())
                {
                    $propTMP['IBLOCK_ID'] = $arProp['ID'];
                    $propTMP['NAME'] = $arProp['NAME'];
                    $propTMP['TYPE'] = $arProp['PROPERTY_TYPE'];
                    $propTMP['TABLE_ID'] = $arCharacteristic['id_car_characteristic'];
                    if($arProp['PROPERTY_TYPE'] == 'E')
                    {
                        $propTMP['LINK_IBLOCK_ID'] = $arProp['LINK_IBLOCK_ID'];
                    }
                }
                $arProps[$arCharacteristic['id_car_characteristic']] = $propTMP;
            }
        }

        if(!empty($arProps))
        {
            return $arProps;
        }

        return false;
    }


    /**
     * Получает массив с марками автомобилей
     * @return mixed
     */
    public static function getManufacturers()
    {
        $arManufacturers = array();
        $rsManufacturers = CarMarkTable::getList(
            array(
                'select' => array('id_car_mark', 'name', 'name_rus'),
            )
        );

        while($arManufacturer = $rsManufacturers->Fetch())
        {
            $arManufacturers[] =  array(
                'ID' => $arManufacturer['id_car_mark'],
                'NAME' => $arManufacturer['name'],
                'NAME_RU' => $arManufacturer['name_rus'],
            );

        }

        if(!empty($arManufacturers))
        {
            return $arManufacturers;
        }

        return false;
    }

    /**
     * Получает массив с моделями автомобилей
     * @param integer $manufacturerID
     * @return mixed
     */
    public static function getModels($manufacturerID)
    {
        $arModels = array();
        $rsModels = CarModelTable::getList(
            array(
                'select' => array('id_car_model', 'name'),
                'filter' => array('id_car_mark' => $manufacturerID),
            )
        );

        while($arModel = $rsModels->Fetch())
        {
            $arModels[] =  array(
                'ID' => $arModel['id_car_model'],
                'NAME' => $arModel["name"],
            );
        }

        if(!empty($arModels))
        {
            return $arModels;
        }

        return false;
    }

    /**
     * Получает массив с поколениями автомобилей
     * @param integer $modelID
     * @return mixed
     */
    public static function getGenerations($modelID)
    {
        $arGenerations = array();
        $rsGenerations = CarGenerationTable::getList(
            array(
                'select' => array('id_car_generation', 'name', 'year_begin', 'year_end'),
                'filter' => array('id_car_model' => $modelID),
            )
        );

        while($arGeneration = $rsGenerations->Fetch())
        {
            $arGenerations[] =  array(
                'ID' => $arGeneration['id_car_generation'],
                'NAME' => $arGeneration["name"],
                'YEAR_BEGIN' => $arGeneration['year_begin'],
                'YEAR_END' => $arGeneration['year_end'],
            );
        }

        if(!empty($arGenerations))
        {
            return $arGenerations;
        }

        return false;
    }

    /**
     * Получает массив с сериями автомобилей
     * @param integer $generationID
     * @return mixed
     */
    public static function getSeries($generationID)
    {
        $arSeries = array();

        $rsSeries = CarSerieTable::getList(
            array(
                'select' => array('id_car_serie', 'name'),
                'filter' => array('id_car_generation' => $generationID),
                'order' => array('name' => 'asc'),
            )
        );

        while($arSerie = $rsSeries->Fetch())
        {
            $arSeries[] =  array(
                'ID' => $arSerie['id_car_serie'],
                'NAME' => $arSerie['name'],
            );
        }

        if(!empty($arSeries))
        {
            return $arSeries;
        }

        return false;
    }

    /**
     * Получает массив с модификациями автомобилей
     * @param integer $serieID
     * @return mixed
     */
    public static function getModifications($serieID)
    {
        $arModifications = array();

        $rsModifications = CarModificationTable::getList(
            array(
                'select' => array('id_car_modification', 'name'),
                'filter' => array('id_car_serie' => $serieID),
                'order' => array('name' => 'asc'),
            )
        );

        while($arModification = $rsModifications->Fetch())
        {
            $arModifications[] =  array(
                'ID' => $arModification['id_car_modification'],
                'NAME' => $arModification['name'],
            );
        }

        if(!empty($arModifications))
        {
            return $arModifications;
        }

        return false;
    }

    /**
     * Получает массив характеристик автомобиля
     * @param integer $modificationID
     * @return mixed
     */
    public static function getCharacteristics($modificationID)
    {
        $arChosenOptions = self::getChosenOptions();
        $mainIblockID = Option::get(self::MODULE_ID, 'carIblock');

        if(!empty($arChosenOptions))
        {
            $characteristicsIDs = $arPropsCode = array();
            foreach($arChosenOptions as $key=>$option)
            {
                $arPropsCode[] = $option['IBLOCK_CODE'];
                $characteristicsIDs[] = $option['TABLE_ID'];

                $dbProps = \Bitrix\Iblock\PropertyTable::getList(array(
                    'filter' => array('IBLOCK_ID' => $mainIblockID, 'CODE' => $option['IBLOCK_CODE']),
                    )
                );
                while($arProp = $dbProps->fetch())
                {
                    $arChosenOptions[$key]['PROPERTY_TYPE'] = $arProp['PROPERTY_TYPE'];
                    $arChosenOptions[$key]['IBLOCK_ID'] = $arProp['ID'];
                }
            }

            $arCharacteristics = array();
            $rsCharacteristics = CarCharacteristicValueTable::getList(array(
                'filter' => array('id_car_modification' => $modificationID, 'id_car_characteristic' => $characteristicsIDs),
                'select' => array('id_car_characteristic', 'value', 'unit'),
            ));

            while($arCharacteristic = $rsCharacteristics->fetch())
            {
                $arCharacteristics[] =  $arCharacteristic;

                // в БД объем двигателя - с точностью до см3, а нужно - до десятых
                if($arChosenOptions[$arCharacteristic['id_car_characteristic']]['IBLOCK_CODE'] == 'ENGINE')
                {
                    $powerVal = round((int)$arCharacteristic['value']/1000, 1);
                    if(strlen($powerVal) == 1)
                    {
                        $powerVal .= ".0";

                    }
                    $arChosenOptions[$arCharacteristic['id_car_characteristic']]["VALUE"] = $powerVal;
                }
                else
                {
                    $arChosenOptions[$arCharacteristic['id_car_characteristic']]["VALUE"] = $arCharacteristic['value'];
                }
                $arChosenOptions[$arCharacteristic['id_car_characteristic']]["UNIT"] = $arCharacteristic['unit'];
            }

            if(!empty($arChosenOptions))
            {

                return array_values($arChosenOptions);
            }
        }
        return false;
    }

    /**
     * Получает значение свойства по его ID
     * @param integer $propID
     * @param string $propName
     * @return mixed
     */
    public static function getPropValueByID($propID, $propName)
    {
        switch($propName)
        {
            case "MANUFACTURER":
                $dbProp = CarMarkTable::getList(
                    array(
                        'select' => array('id_car_mark', 'name'),
                        'filter' => array('id_car_mark' => $propID),
                    )
                );
                break;
            case "MODEL":
                $dbProp = CarModelTable::getList(
                    array(
                        'select' => array('id_car_model', 'name'),
                        'filter' => array('id_car_model' => $propID),
                    )
                );
                break;
            case "GENERATION":
                $dbProp = CarGenerationTable::getList(
                    array(
                        'select' => array('id_car_generation', 'name'),
                        'filter' => array('id_car_generation' => $propID),
                    )
                );
                break;
            case "SERIE":
                $dbProp = CarSerieTable::getList(
                    array(
                        'select' => array('id_car_serie', 'name'),
                        'filter' => array('id_car_serie' => $propID),
                    )
                );
                break;
            case "MODIFICATION":
                $dbProp = CarModificationTable::getList(
                    array(
                        'select' => array('id_car_modification', 'name'),
                        'filter' => array('id_car_modification' => $propID),
                    )
                );
                break;
        }

        while($arProp = $dbProp->Fetch())
        {
            $prop =  $arProp['name'];
        }

        if(!empty($prop))
        {
            return $prop;
        }

        return false;
    }

    /**
     * Получает марку, модель, поколение, серию и модификацию автомобиля по ID объявления
     * @param integer $elementID
     * @return mixed
     */
    public static function getAdProps($elementID)
    {
        $adsIblockID = Option::get(self::MODULE_ID, 'carIblock');
        $arResult = $arSelect = array();
        /*
        $arProperties = self::getAutoOptions();

        foreach($arProperties as $arProp)
        {
            if(!empty($arProp))
            {
                $arSelect[$arProp['IBLOCK_CODE']] = $arProp['IBLOCK_CODE'];
            }
        }
        */
        $arFilter = array(
            'IBLOCK_ID' => $adsIblockID,
            'ID' => $elementID
        );
        $arSelect = array('IBLOCK_ID', 'ID', 'PROPERTY_MANUFACTURER', 'PROPERTY_MODEL', 'PROPERTY_GENERATION', 'PROPERTY_SERIE', 'PROPERTY_MODIFICATION');
        $dbElement = \CIBlockElement::GetList(false, $arFilter, false, false, $arSelect);

        if($arElement = $dbElement->fetch())
        {
            $arResult = array(
                'MANUFACTURER' => $arElement['PROPERTY_MANUFACTURER_VALUE'],
                'MODEL' => $arElement['PROPERTY_MODEL_VALUE'],
                'GENERATION' => $arElement['PROPERTY_GENERATION_VALUE'],
                'SERIE' => $arElement['PROPERTY_SERIE_VALUE'],
                'MODIFICATION' => $arElement['PROPERTY_MODIFICATION_VALUE'],
            );
        }

        if(!empty($arResult))
        {
            return $arResult;
        }

        return false;
    }

    /**
     * Получает марку, модель, поколение, серию и модификацию автомобиля по ID объявления
     * @param integer $elementID
     * @param string $propCode
     * @return mixed
     */
    public static function getAdProp($elementID, $propCode)
    {
        $adsIblockID = Option::get(self::MODULE_ID, 'carIblock');

        $arFilter = array(
            'IBLOCK_ID' => $adsIblockID,
            'ID' => $elementID
        );
        $arSelect = array('IBLOCK_ID', 'ID', 'PROPERTY_'.$propCode);
        $dbElement = \CIBlockElement::GetList(false, $arFilter, false, false, $arSelect);

        if($arElement = $dbElement->fetch())
        {
            $arResult = $arElement['PROPERTY_'.$propCode.'_VALUE'];
        }

        if(!empty($arResult))
        {
            return $arResult;
        }

        return false;
    }


    /**
     * Проверка на существование элемента инфоблока
     * @param integer $elementID
     * @return bool
     */
    public static function elementExists($elementID)
    {
        $adsIblockID = Option::get(self::MODULE_ID, 'carIblock');

        $arFilter = array(
            'IBLOCK_ID' => $adsIblockID,
            'ID' => $elementID
        );
        $arSelect = array('IBLOCK_ID', 'ID');
        $dbElement = \CIBlockElement::GetList(false, $arFilter, false, false, $arSelect);

        if($arElement = $dbElement->fetch())
        {
            return true;
        }

        return false;
    }
}

?>