<?
namespace Ycaweb\Cars;

use Bitrix\Main;
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Type;

Loc::loadMessages(__FILE__);

class CarGenerationTable extends Entity\DataManager
{
    public static function getFilePath()
    {
        return __FILE__;
    }

    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'car_generation';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'id_car_generation' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
            ),
            'id_car_model' => array(
                'data_type' => 'integer',
              //  'required' => true,
            ),
            'name' => array(
                "data_type" => "string",
                "required" => false,
                'title' => Loc::getMessage("YCAWEB_CAR_GENERATION_NAME")
            ),
            'year_begin' => array(
                "data_type" => "string",
                "required" => false,
                'title' => Loc::getMessage("YCAWEB_CAR_GENERATION_YEAR_BEGIN")
            ),
            'year_end' => array(
                "data_type" => "string",
                "required" => false,
                'title' => Loc::getMessage("YCAWEB_CAR_GENERATION_YEAR_END")
            ),
            'id_car_type'      => array(
                "data_type" => "integer",
                //"required" => false
            ),
        );
    }
}
?>


