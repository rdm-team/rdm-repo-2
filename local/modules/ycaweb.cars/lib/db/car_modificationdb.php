<?
namespace Ycaweb\Cars;

use Bitrix\Main;
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Type;

Loc::loadMessages(__FILE__);

class CarModificationTable extends Entity\DataManager
{
    public static function getFilePath()
    {
        return __FILE__;
    }

    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'car_modification';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'id_car_modification' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
            ),
            'id_car_serie' => array(
                'data_type' => 'integer',
              //  'required' => true,
            ),
            'id_car_model' => array(
                'data_type' => 'integer',
              //  'required' => true,
            ),
            'name' => array(
                "data_type" => "string",
                "required" => false,
                'title' => Loc::getMessage("YCAWEB_CAR_MODIFICATION_NAME")
            ),
            'id_car_type'      => array(
                "data_type" => "integer",
                //"required" => false
            ),
        );
    }
}
?>


