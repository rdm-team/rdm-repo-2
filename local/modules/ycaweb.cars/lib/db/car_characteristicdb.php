<?
namespace Ycaweb\Cars;

use Bitrix\Main;
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Type;

Loc::loadMessages(__FILE__);

class CarCharacteristicTable extends Entity\DataManager
{
    public static function getFilePath()
    {
        return __FILE__;
    }

    /**
     * Returns DB table name for entity.'
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'car_characteristic';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'id_car_characteristic' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
            ),
            'id_parent' => array(
                'data_type' => 'integer',
              //  'required' => true,
                'title' => Loc::getMessage("YCAWEB_CAR_CHARACTERISTIC_ID_PARENT")
            ),
            'name' => array(
                "data_type" => "string",
                "required" => false,
                'title' => Loc::getMessage("YCAWEB_CAR_CHARACTERISTIC_NAME"),
            ),
            'id_car_type'      => array(
                "data_type" => "integer",
               // "required" => true,
            ),
            'date_create'      => array(
                "data_type" => "integer",
                //"required" => false
            ),
            'date_update'      => array(
                "data_type" => "integer",
                //"required" => false
            ),
        );
    }
}
?>


