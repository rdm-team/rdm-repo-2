<?
namespace Ycaweb\Cars;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class OptionsTable extends Entity\DataManager
{
    public static function getFilePath()
    {
        return __FILE__;
    }
    public static function getTableName()
    {
        return 'ycaweb_module_options';
    }

    public static function getMap()
    {
        return array(
            'OPTION_CODE' => array(
                'data_type' => 'string',
                'primary' => true,
            ),
            "OPTION_GROUP"	=> array(
                "data_type" => "integer",
                "required" => true
            ),
            "OPTION_SORT"	=> array(
                "data_type" => "integer",
                "required" => false
            ),
            "VALUE"	=> array(
                "data_type" => "string",
                "required" => false,

            )
        );
    }


/*
    public function GetInvoiceSpecialMark(){
        $optionId =  \Bitrix\Main\Config\Option::get("ycaweb.orders","YCAWEB_INVOICE_SPECIAL_MARK_OPTION_ID");
        $res = $this->getList(array("filter"=>array("OPTION_GROUP"=>$optionId),"select"=>array("OPTION_CODE","VALUE"), "order"=>array("OPTION_SORT")));
        $arOptions = array();
        while($value = $res->fetch()){
            $arOptions [$value["OPTION_CODE"]] = $value["VALUE"];
        }
        return $arOptions;
    }
*/

}