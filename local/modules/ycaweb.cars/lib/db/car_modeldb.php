<?
namespace Ycaweb\Cars;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class CarModelTable extends Entity\DataManager
{
    public static function getFilePath()
    {
        return __FILE__;
    }

    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'car_model';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    { 
        return array(
            'id_car_model'      => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
            ),
            'id_car_mark'      => array(
                "data_type" => "integer",
                "required" => false
            ),
            'id_car_type'      => array(
                "data_type" => "integer",
                //"required" => false
            ),
            "name"	=> array(
                "data_type" => "string",
                "required" => false,
                "title"	=> Loc::getMessage("YCAWEB_CAR_MODEL_NAME")
            )
        );
    }
}