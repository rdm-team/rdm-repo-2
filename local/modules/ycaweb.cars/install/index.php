<?php

use Bitrix\Main\Localization\Loc;

global $MESS;
$PathInstall = str_replace("\\", "/", __FILE__);
$PathInstall = substr($PathInstall, 0, strlen($PathInstall)-strlen("/index.php"));
IncludeModuleLangFile($PathInstall."/install.php");
include($PathInstall."/version.php");


Class ycaweb_cars extends CModule
{
    public $MODULE_ID = "ycaweb.cars";
    public $MODULE_NAME;
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_DESCRIPTION;
    public $MODULE_GROUP_RIGHTS = "Y";
    public $MODULE_PATH = "";
    private $errors;

    function ycaweb_cars()
    {
        global $arModuleVersion;
        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->MODULE_NAME = Loc::getMessage("YCAWEB_CAR_DB_MODULE_NAME");
        $this->MODULE_DESCRIPTION = Loc::getMessage("YCAWEB_CAR_DB_MODULE_DESCRIPTION");
        $this->PARTNER_NAME = Loc::getMessage('YCAWEB_CAR_DB_PARTNER_NAME');
		$this->MODULE_PATH = $_SERVER['DOCUMENT_ROOT'].'/local/modules/'.$this->MODULE_ID;
    }
    function UnInstallDB($arParams = array())
    {
        global $DB, $APPLICATION;
        $this->errors = false;
        // если не отключен chexbox сохранения таблиц модуля в DB
        // или ключ savedata отсутствует в массиве arParams
        if(!array_key_exists("savedata", $arParams) || $arParams["savedata"] != "Y")
        {
        // удаляем все таблицы модуля
            $this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/local/modules/ycaweb.cars/install/db/".strtolower($DB->type)."/uninstall.sql");
        }

        if($this->errors !== false)
        {
            $APPLICATION->ThrowException(implode("<br>", $this->errors));
            return false;
        }
        return true;
    }
    function InstallDB($arParams = array())
    {
        global $DB, $DBType, $APPLICATION;
        $this->errors = false;

        // выполняет пакет запросов из файла, возвращаем false в случае успеха или массив ошибок
       // $this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/local/modules/ycaweb.cars/install/db/".strtolower($DB->type)."/cars_cut.sql"); //auto_cut

        // если при выполнении запроса были ошибки выдаем их в сообщении
        if($this->errors !== false)
        {
            $APPLICATION->ThrowException(implode("<br>", $this->errors));
            return false;
        }


            return true;
    }
	
	public function InstallFiles()
    {
	    CopyDirFiles($this->MODULE_PATH.'/install/admin_page',  $_SERVER["DOCUMENT_ROOT"].'/bitrix/admin/', false, true);
        CopyDirFiles($this->MODULE_PATH.'/install/js',  $_SERVER["DOCUMENT_ROOT"].'/bitrix/js/'.$this->MODULE_ID.'/', false, true);
		CopyDirFiles($this->MODULE_PATH.'/install/js',  $_SERVER["DOCUMENT_ROOT"].'/bitrix/js/'.$this->MODULE_ID.'/', false, true);
	
	    return true;
	}

    public function UnInstallFiles($arParams = array()){
        DeleteDirFilesEx('/bitrix/js/'.$this->MODULE_ID.'/');
        \Bitrix\Main\IO\File::deleteFile("/bitrix/admin/ad_element_edit.php");

        return true;
    }

    function DoInstall()
    {
        global $DB, $APPLICATION, $step;

        if($this->InstallDB())
        {
            $this->InstallFiles();
            $this->InstallEvents();
            \Bitrix\Main\ModuleManager::registerModule($this->MODULE_ID);
           // \Bitrix\Main\Config\Option::set($this->MODULE_ID,"YCAWEB_ORDER_STATUS_OPTION_ID","1");
        }

        else
            return false;
    }
    function DoUninstall()
    {
        global $DB, $APPLICATION, $step;

        $this->UnInstallFiles();
        $this->UnInstallEvents();
        $this->UnInstallDB();
        \Bitrix\Main\ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    public function InstallEvents(){
        RegisterModuleDependences('main', 'OnAdminContextMenuShow', $this->MODULE_ID, 'Ycaweb\Cars\EditForm', 'CreateEditFormBtn');
		_vardump("sdf", 'f', false);
        return true;
    }

    public function UnInstallEvents(){
        UnRegisterModuleDependences('main', 'OnAdminContextMenuShow', $this->MODULE_ID, 'Ycaweb\Cars\EditForm', 'CreateEditFormBtn');

        return true;
    }
}

?>
