<?php
/**
 * User: LaDi666
 * Date: 29.12.2016
 */
use Bitrix\Main\Loader;
CModule::IncludeModule("iblock");


class YcawebHelper {

    public static function getPropId($iblockId, $propCode){
        $property = CIBlockProperty::GetList(Array(), Array("ACTIVE" => "Y", "IBLOCK_ID" => $iblockId, "CODE" => $propCode))->fetch();
        return $property["ID"];
    }

    public static function getIblockID($code){

        $obCache = new CPHPCache();
        $cacheLifetime = 3600*24*7; $cacheID = 'allIbloksId'; $cachePath = '/'.SITE_ID."/".$cacheID;
        if( $obCache->InitCache($cacheLifetime, $cacheID, $cachePath) )
        {
            $vars = $obCache->GetVars();
            $arIbloks = $vars['allIbloksId'];
        }
        elseif( $obCache->StartDataCache()  )
        {
            $el = CIBlock::GetList(array(), array(), false, false);
            while($arIblock  = $el->Fetch()){
                $arIbloks[$arIblock["CODE"]] = $arIblock["ID"];
            }
            $obCache->EndDataCache(array('allIbloksId' => $arIbloks));
        }
        return $arIbloks[$code];
    }

    public function priceFormat($number){
        return number_format((int)$number, 0, '.', ' ');
    }

    public function getTaxonomy($code='', $use_cache=true){
        $code = strtoupper(trim($code));

        $obCache = new CPHPCache;
        $cache_time = 60*60*24*30;
        $cache_id = "taxonomy";
        $cache_path = '/'.SITE_ID."/taxonomy/";

        if($use_cache && $obCache->InitCache($cache_time, $cache_id, $cache_path)){
            $cacheVars = $obCache->GetVars();
            $arrTaxonomy = $cacheVars["arrTaxonomy"];
        }else{
            CModule::IncludeModule("iblock");

            $arReferenceIblocksType = 'reference';
            $arrTaxonomy = $arIBlocks = array();

            $resBlock = CIBlock::GetList(array(), array("TYPE" => $arReferenceIblocksType, "SITE_ID" => SITE_ID, "ACTIVE" => "Y"), false);

            while($arBlock = $resBlock->Fetch())
            {
                $arIBlocks[$arBlock["ID"]] = $arBlock;
            }

            $arSelect = array('IBLOCK_ID', 'IBLOCK_CODE', 'ID', 'CODE', 'NAME', 'PROPERTY_COLOR', 'XML_ID');

            $resEl = CIBlockElement::GetList(array("SORT" => "ASC", "NAME" => "ASC"), array("IBLOCK_TYPE" => $arReferenceIblocksType, "ACTIVE" => "Y"), false, false, $arSelect);

            while($objEl = $resEl->fetch())
            {
                $arrTaxonomy[strtoupper($objEl["IBLOCK_CODE"])][$objEl["ID"]] = array(
                    "ID" 	=> $objEl["ID"],
                    "NAME" 	=> $objEl["NAME"],
                );
                if(!empty($objEl["PROPERTY_COLOR_VALUE"]))
                {
                    $arrTaxonomy[strtoupper($objEl["IBLOCK_CODE"])][$objEl["ID"]]['COLOR_CODE'] = $objEl["PROPERTY_COLOR_VALUE"];
                }
                if(!empty($objEl["CODE"]))
                {
                    $arrTaxonomy[strtoupper($objEl["IBLOCK_CODE"])][$objEl["ID"]]['CODE'] = $objEl["CODE"];
                }
                if(!empty($objEl["XML_ID"]))
                {
                    $arrTaxonomy[strtoupper($objEl["IBLOCK_CODE"])][$objEl["ID"]]["XML_ID"] = $objEl["XML_ID"];
                }
            }

            $adsIblockID = YcawebHelper::getIblockID('ads');
            $arrTaxonomy["MANUFACTURER"] = array();

            $res = CIBlockElement::GetList(array("PROPERTY_MANUFACTURER" => "ASC"), array("IBLOCK_ID" => $adsIblockID, "ACTIVE" => "Y"), array("PROPERTY_MANUFACTURER"), false, array("ID", "IBLOCK_ID", "PROPERTY_MANUFACTURER"));

            while($ar_res = $res->Fetch())
            {
                if(!empty($ar_res["PROPERTY_MANUFACTURER_VALUE"])){
                    $arrTaxonomy["MANUFACTURER"][$ar_res["PROPERTY_MANUFACTURER_VALUE"]] = array(
                        "ID" => $ar_res["PROPERTY_MANUFACTURER_VALUE"],
                        "NAME" => $ar_res["PROPERTY_MANUFACTURER_VALUE"],
                    );
                }
            }
            ksort($arrTaxonomy["VOLUME"]);
        }

        if($obCache->StartDataCache()){
            $obCache->EndDataCache(array("arrTaxonomy"=>$arrTaxonomy));
        }


        return (!empty($code)) ? $arrTaxonomy[strtoupper($code)] : $arrTaxonomy;
    }

	public static function agentUpdateRates(){
		$queryStr = date('d.m.Y');
		$adminDate = date($GLOBALS['DB']->DateFormatToPHP(CLang::GetDateFormat('SHORT')));
		require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/classes/general/xml.php');
		$strQueryText = QueryGetData('www.cbr.ru', 80, '/scripts/XML_daily.asp', $queryStr, $errno, $errstr);
		$arNewRates = array();
		
		$arCurrency = self::getTaxonomy('currency');
		$objXML = new CDataXML();

		if($objXML->LoadString($strQueryText)){
			$arXMLData = $objXML->GetArray();
			if(!empty($arXMLData) && is_array($arXMLData)){
				foreach($arXMLData['ValCurs']['#']['Valute'] as $arC){
					$arNewRates[$arC["#"]["CharCode"][0]["#"]] = array(
						'CURRENCY'	=> $arC["#"]["CharCode"][0]["#"],
						'RATE_CNT'	=> intval($arC['#']['Nominal'][0]['#']),
						'RATE'		=> doubleval(str_replace(',', '.', $arC['#']['Value'][0]['#'])),
						'DATE_RATE' => $adminDate,
					);
				}
				foreach($arCurrency as $currency){
					if(!isset($arNewRates[$currency["CODE"]])) continue;
					$arRate = $arNewRates[$currency["CODE"]];
					$PROPS = array(
						"RATE"			=> $arRate["RATE"]/$arRate["RATE_CNT"],
						"DATE_UPDATE"	=> $arRate["DATE_RATE"],
					);
					CIBlockElement::SetPropertyValuesEx($currency["ID"], false, $PROPS);
				}

			}

			//Clear cache
			BXClearCache(true, "/s1/yca/");
			BXClearCache(true, "/yca/");
		}

		return 'YcawebHelper::agentUpdateRates();';
	}

	
	public static function getCurrencyPrice($currencyCODE = '')
	{
		$arFilter = array(
			'IBLOCK_ID' => self::getIblockID("currency"),
			'ACTIVE' => 'Y',
		);
		if(!empty($currencyCODE))
		{
			$arFilter['CODE'] = $currencyCODE;
		}
        $resEl = CIBlockElement::GetList(array(), $arFilter, false, false, array("ID", "IBLOCK_ID", "CODE", "PROPERTY_RATE", "PROPERTY_SYMBOL"));
		
		$arResult = array();
        while($arEl = $resEl->fetch())
		{
			if(!empty($arEl["PROPERTY_RATE_VALUE"]))
			{
				$arResult[$arEl['CODE']] = array(
					"RATE" => $arEl["PROPERTY_RATE_VALUE"],
					"SYMBOL" => $arEl["PROPERTY_SYMBOL_VALUE"],
				);
			}
		}
		
		if(!empty($arResult))
		{
			return $arResult;
		}

        return false;
    }
	

	
    public static function getModels($manufacturer, $chosenModel = '')
    {
        $adsIblockID = self::getIblockID("car-ads");

        $arModelSelect = array('IBLOCK_ID', 'ID', 'PROPERTY_MANUFACTURER', 'PROPERTY_MODEL');
        $arModelFilter = array("IBLOCK_ID" => $adsIblockID, "ACTIVE" => "Y", "PROPERTY_MANUFACTURER" => trim($manufacturer));
        $resEl = CIBlockElement::GetList(array("SORT" => "ASC", "NAME" => "ASC"), $arModelFilter, false, false, $arModelSelect);
		$arModels = array();
        while($arElement = $resEl->fetch())
        {
			$modelValue = trim($arElement["PROPERTY_MODEL_VALUE"]);
            $arModelItem = array(
                'for'	=> 'option-'.$arElement["ID"],
                'name'	=> 'filter['.$arElement['ID'].']',
                'id'	=> $arElement["ID"],
                'label'	=> $modelValue,
                'value'	=> $modelValue,
            );
            $arModelItem['selected'] = ($chosenModel == $arElement['PROPERTY_MODEL_VALUE']);

			$alreadyExists = false;
			foreach($arModels as $model)
			{
				if($model['value'] == $modelValue)
				{
					$alreadyExists = true;
					break;
				}
			}
			if(!$alreadyExists)
			{
				$arModels[] = $arModelItem;
			}
        }

        if(!empty($arModels))
        {
            return $arModels;
        }

        return false;
    }


    public static function morpher($text, $padeg)
    {/*
        $credentials = array('Username'=>'test',
            'Password'=>'test');

        $header = new SOAPHeader('http://morpher.ru/', 'Credentials', $credentials);

        $url = 'http://morpher.ru/WebService.asmx?WSDL';

        $client = new SoapClient($url);

        $client->__setSoapHeaders($header);

        $params = array('parameters'=>array('s'=>$text));

        $result = (array) $client->__soapCall('GetXml', $params);

        $singular = (array) $result['GetXmlResult'];

        return $singular[$padeg];*/
    }
	
	public static function parsePhone($phone)
	{
		$result = $phone;
		$result = explode(",", $result);
		$result = preg_replace('![^0-9]+!', '', $result[0]);
		if(strlen($result) > 10){
			$result = substr($result, 1, 10);
		}
		$result = "".substr($result, 0, 3)." ".substr($result, 3, 3)." ".substr($result, 6, 2)." ".substr($result, 8, 2);
		
		return $result;
	}
}