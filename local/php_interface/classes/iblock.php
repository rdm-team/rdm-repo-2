<?
namespace Classes\Handlers;

class Iblock
{
	
	private static $aсtiveElement = false;

	/**
     * OnAfterIBlockElementUpdate - вызывается после попытки изменения элемента информационного блока методом CIBlockElement::Update.
	  @param &arFields
	 OnAfterIBlockElementAdd - вызывается после попытки добавления нового элемента информационного блока методом CIBlockElement::Add. 
	 @param &arFields
     * @return void
    */
	public static function BXIBlockAfterSave(&$arFields)
	{
////arraytofile($_REQUEST,$_SERVER['DOCUMENT_ROOT']."/local/php_interface/classes/handlers/ImportLog.txt", "_REQUEST");	
		
		if (@$_REQUEST['mode'] == 'import')
		{
            if($arFields["IBLOCK_ID"] == 5)
            {
                $arSelect = array('ID', 
				'IBLOCK_ID', 
				'NAME', 
				'PROPERTY_MANUFACTURER', 
				'PROPERTY_MANUFACTURER_OLD', 
				'PROPERTY_MODIFICATION', 
				'PROPERTY_GENERATION', 
				'PROPERTY_SERIE', 
				'PROPERTY_MODEL',
				'PROPERTY_MARKA_NAME',
				'PROPERTY_MODEL_NAME',
				'PROPERTY_CAR_BODY'
				);
                $arFilter = array ('IBLOCK_ID'  => 5, 'ID'  => $arFields['ID']);
                $res = \CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                $hLCarMark = new \HLBlock('CarMark');
                $hLCarModel = new \HLBlock('CarModel');
                $hLCarGeneration = new \HLBlock('CarGeneration');
                $hLCarSerie = new \HLBlock('CarSerie');
                $hLCarModification = new \HLBlock('CarModification');
                if ($arEl = $res->fetch())
                {

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Проверяем марку в HL. Если нет такой, то добавляем
                    $arCarMark = $hLCarMark->hasToCreateField(array('UF_XML_ID' => $arEl['PROPERTY_MANUFACTURER_VALUE']));

					if($arCarMark['STATUS']){
						
						if($arCarMark['UF_NAME'])
							$arProps['MANUFACTURER'] = $arCarMark['UF_NAME'];
					
					}else{
						
						if(!empty($arEl['PROPERTY_MARKA_NAME_VALUE'])){
							
							$hLCarMark->addFields(array(
							'UF_XML_ID' => $arEl['PROPERTY_MANUFACTURER_VALUE'],		
							'UF_NAME' => $arEl['PROPERTY_MARKA_NAME_VALUE']
							));
							$arProps['MANUFACTURER'] = $arEl['PROPERTY_MARKA_NAME_VALUE'];
						}
					}
					
					
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Проверяем модель в HL. Если нет такой, то добавляем
                    $arCarModel = $hLCarModel->hasToCreateField(array('UF_XML_ID' => $arEl['PROPERTY_MODEL_VALUE']));
					
					if($arCarModel['STATUS']){
                    if($arCarModel['UF_NAME'])
                        $arProps['MODEL'] = $arCarModel['UF_NAME'];
					}else{
						
						if(!empty($arEl['PROPERTY_MODEL_NAME_VALUE'])){
							
							$hLCarModel->addFields(array(
							'UF_XML_ID' => $arEl['PROPERTY_MODEL_VALUE'],
							'UF_CAR_MARK' => $arEl['PROPERTY_MANUFACTURER_VALUE'],
							'UF_NAME' => $arEl['PROPERTY_MODEL_NAME_VALUE']
							));
							$arProps['MODEL'] = $arEl['PROPERTY_MODEL_NAME_VALUE'];
						}
					}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                    $arCarGeneration = $hLCarGeneration->getField(array('UF_XML_ID' => $arEl['PROPERTY_GENERATION_VALUE']));
                    if($arCarGeneration['UF_NAME'])
                        $arProps['GENERATION'] = $arCarGeneration['UF_NAME'];

                    $arCarSerie = $hLCarSerie->getField(array('UF_XML_ID' => $arEl['PROPERTY_SERIE_VALUE']));
                    if($arCarSerie['UF_NAME'])
                        $arProps['SERIE'] = $arCarSerie['UF_NAME'];

                
                    $arCarModification = $hLCarModification->getField(array('UF_XML_ID' => $arEl['PROPERTY_MODIFICATION_VALUE']));
                    if($arCarModification['UF_NAME'])
                        $arProps['MODIFICATION'] = $arCarModification['UF_NAME'];

                    
                    //p($arProps, 'arProps',1);
					//BXClearCache(false, "/bitrix/cache/s1/bitrix/news.detail/");
					
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						$CAR_BODY = self::getKuzov($arEl['PROPERTY_CAR_BODY_VALUE']);
						$arProps['CAR_BODY'] = $CAR_BODY;
						ob_start();
						echo $CAR_BODY."\r\n";
						echo"<pre>\r\n";				
						print_r($arEl);					
						print_r($arProps);						
						echo"</pre>\r\n";
						$content = ob_get_contents();  	
						ob_end_clean();
						
						$fp = fopen($_SERVER["DOCUMENT_ROOT"]."/local/logs/".'BXIBlockAfterSave-'.$arFields['XML_ID'].'.txt', 'w+');
						fwrite($fp, $content);
						fclose($fp);
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\CIBlockElement::SetPropertyValuesEx($arFields["ID"],$arFields["IBLOCK_ID"], $arProps);
                } 
				
				
                $arSelect = array('ID','IBLOCK_ID','NAME','ACTIVE','PREVIEW_PICTURE','DETAIL_PICTURE','PROPERTY_MORE_PHOTO');
                $arFilter = array ('IBLOCK_ID'  => 5, 'ID'  => $arFields['ID']);
                $res = \CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                if ($arEl = $res->GetNextElement())
                {
						$arField = $arEl->GetFields();
						$arProp = $arEl->GetProperties();
						$arPhoto = array();
						$Preview = array();
						if(isset($arField['PREVIEW_PICTURE'])){
							$Preview = \CFile::GetFileArray($arField['PREVIEW_PICTURE']);
						}
						
						if(isset($arProp['MORE_PHOTO']['VALUE'])){
							
							foreach($arProp['MORE_PHOTO']['VALUE'] as $v){
								
								$arPhoto[] = \CFile::GetFileArray($v);
							}
						}
						/*
						ob_start();
						
						echo"<pre>";				
						print_r($arEl);				
						print_r($Preview);
						print_r($arPhoto);
						print_r($arProp);
						echo"</pre>";
						$content = ob_get_contents();  	
						ob_end_clean();
						
						$fp = fopen($_SERVER["DOCUMENT_ROOT"]."/local/logs/".'BXIBlockAfterSave-'.$arFields['XML_ID'].'.txt', 'w+');
						fwrite($fp, $content);
						fclose($fp);
						*/
				}		
            }
        }
	}
	
	public static function getKuzov($id){
                $arSelect = array('ID','IBLOCK_ID','NAME','ACTIVE','PROPERTY_CAR_BODY','XML_ID');
                $arFilter = array ('IBLOCK_ID'  => 6, 'XML_ID'  => $id);
                $res = \CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
				if ($arEl = $res->GetNextElement())
                {
					$arField = $arEl->GetFields();
					return $arField['ID'];
				}
				return $id;
	}			
				
	public static function BXIBlockBeforeSave(&$arFields)
	{
		if (@$_REQUEST['mode'] == 'import')
		{
				if($arFields["IBLOCK_ID"] == 5)
				{		
					ob_start();		
					echo"<pre>";
					print_r($arFields);
					echo"</pre>";
					$content = ob_get_contents();  	
					ob_end_clean();
					
					$fp = fopen($_SERVER["DOCUMENT_ROOT"]."/local/logs/".'BXIBlockBeforeSave-'.$arFields['XML_ID'].'.txt', 'w+');
					fwrite($fp, $content);
					fclose($fp);
			}
		}
	}
	
}