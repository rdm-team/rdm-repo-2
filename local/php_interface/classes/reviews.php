<?
namespace Classes\Handlers;

//use Classes\Helpers;
use Bitrix\Main\Data\TaggedCache;

class Reviews
{
    static $review_compare = '';
    static $status_active = '';

    /**
     * OnAfterIBlockElementDelete
     * @param $arFields
     * @return void
     */
    public static function OnBeforeIBlockElementDeleteHandler($ID)
    { 
        // ������
        \Bitrix\Main\Loader::includeModule('iblock');
		
        $res = \CIBlockElement::GetByID($ID);
        $arEl = $res->GetNext();
        if ($arEl["IBLOCK_ID"] == REVIEWS_IBLOCK_ID && $arEl['ACTIVE'] == 'Y'){
    
            if (defined('BX_COMP_MANAGED_CACHE')){              
                $dbPropsIdEl = \CIBlockElement::GetProperty($arEl['IBLOCK_ID'], $ID, Array('sort'=>'asc'), array('CODE' => 'ID_ELEMENT'));
                if($arProps = $dbPropsIdEl->Fetch()){
                    $REVIEW_ID_ELEMENT = IntVal($arProps['VALUE']);                                      
 
					$tagCache = new TaggedCache();
					$tagCache->ClearByTag('review_'.$REVIEW_ID_ELEMENT);
                }
            }
        }
    } 
    /**
     * OnBeforeIBlockElementUpdate
     * @param $arFields
     * @return void
     */
    public static function OnBeforeIBlockElementUpdateHandler($arFields)
    { 
        // ������
        if ($arFields['ID'] && $arFields["IBLOCK_ID"] == REVIEWS_IBLOCK_ID){   
            //���������� ������ ������
            \Bitrix\Main\Loader::includeModule('iblock');
          
            $resEl = \CIBlockElement::GetByID($arFields['ID']);
            if($element = $resEl->GetNext()){         
                $dbPropsAnswerReview  = \CIBlockElement::GetProperty($arFields['IBLOCK_ID'], $arFields['ID'], Array('sort'=>'asc'), array('CODE' => 'ANSWER_REVIEW'));
                if($arProps = $dbPropsAnswerReview->Fetch()){
                    $ansver_review_text = $arProps['VALUE']['TEXT'];
                }
                self::$review_compare = md5(serialize(implode(";", array($element['ACTIVE'], $element['~PREVIEW_TEXT'], $element['PREVIEW_TEXT_TYPE'],$ansver_review_text))));
                self::$status_active = $element['ACTIVE'];
            }
        }
    }     
    /**
     * OnAfterIBlockElementUpdate
     * @param $arFields
     * @return void
     */
    public static function OnAfterIBlockElementUpdateHandler($arFields)
    {
        // ������
        if ($arFields["IBLOCK_ID"] == REVIEWS_IBLOCK_ID){ 

            //����� ���� �������� ������, ���� ����� �������  
            \Bitrix\Main\Loader::includeModule('iblock');
			/*foreach($arFields['PROPERTY_VALUES']['1011'] as $ansver_review){//����� �� �����
                $ansver_review_text = $ansver_review['VALUE']['TEXT'];
            }*/
			$dbPropsAnswerReview  = \CIBlockElement::GetProperty($arFields['IBLOCK_ID'], $arFields['ID'], Array('sort'=>'asc'), array('CODE' => 'ANSWER_REVIEW'));
			if($arProps = $dbPropsAnswerReview->Fetch()){
				$ansver_review_text = $arProps['VALUE']['TEXT'];
			}
       
            if( self::$review_compare != '' && self::$review_compare != md5(serialize(implode(";", array($arFields['ACTIVE'],$arFields['PREVIEW_TEXT'],$arFields['PREVIEW_TEXT_TYPE'], $ansver_review_text)))) ){           
                if (defined('BX_COMP_MANAGED_CACHE')){
                   //\Bitrix\Main\Loader::includeModule('iblock');
                    $dbPropsIdGood  = \CIBlockElement::GetProperty($arFields['IBLOCK_ID'], $arFields['ID'], Array('sort'=>'asc'), array('CODE' => 'ID_ELEMENT'));
                    if($arProps = $dbPropsIdGood->Fetch()){
                        $REVIEW_ID_ELEMENT = IntVal($arProps['VALUE']);

						$tagCache = new TaggedCache();
						$tagCache->ClearByTag('review_'.$REVIEW_ID_ELEMENT);						
                    }  
                }
                self::$review_compare = '';               

                self::$status_active = '';
            }
        }
		
		// ��������� �������� ����� ����������
		/*if ($arFields["IBLOCK_ID"] == 5){ 
			BXClearCache(false, "/bitrix/cache/s1/bitrix/news.detail/");
		
			ob_start();
			
			echo"<pre>";
			print_r($arFields);
			echo"</pre>";
			$content = ob_get_contents();  	
			ob_end_clean();
			
			$fp = fopen($_SERVER["DOCUMENT_ROOT"]."/local/logs/".'debug_update-'.$arFields['XML_ID'].'.txt', 'w+');
			fwrite($fp, $content);
			fclose($fp);	
		}*/	
    }      
    /**
     * OnAfterIBlockElementAdd
     * @param $arFields
     * @return void
     */
    public static function OnAfterIBlockElementAddHandler($arFields)
    {
        // ������
        if ($arFields["IBLOCK_ID"] == REVIEWS_IBLOCK_ID && $arFields["RESULT"] ){ 
            \Bitrix\Main\Loader::includeModule('iblock');
            $dbPropsReview  = \CIBlockElement::GetProperty($arFields['IBLOCK_ID'], $arFields['ID']);
            while($arProps = $dbPropsReview->Fetch()){
        
                if($arProps['CODE'] == 'ID_ELEMENT')
                    $ID_ELEMENT = IntVal($arProps['VALUE']);
                elseif($arProps['CODE'] == 'EMAIL')
                    $EMAIL = $arProps['VALUE'];  
                elseif($arProps['CODE'] == 'NAME')
                    $NAME = $arProps['VALUE'];                
				elseif($arProps['CODE'] == 'NAME_TOVAR')
                    $NAME_TOVAR = $arProps['VALUE'];				
				elseif($arProps['CODE'] == 'DETAIL_URL')
                    $DETAIL_URL = $arProps['VALUE'];
            } 
							
            $arEventFields = array(
                //"NAME"        => $arFields["NAME"],
                "NAME"        => $NAME,
                "ID_ELEMENT"     => $ID_ELEMENT,
                "NAME_ELEMENT"     => $NAME_TOVAR,
                "DETAIL_PAGE_URL"     => $DETAIL_URL,
                "TEXT"        => $arFields['PREVIEW_TEXT'],
                "EMAIL"       => $EMAIL,
            );
            if($arFields["BLOG"] == "Y"){
                \CEvent::Send("COMMENT_SEND_MANAGER", "s1", $arEventFields );
            }
            else{
                //����� ��� ������ "s1" $arrSites = array();$objSites = CSite::GetList();while ($arrSite = $objSites->Fetch())$arrSites[] = $arrSite["ID"];
                \CEvent::Send("REVIEWS_SEND_MANAGER", "s1", $arEventFields );
            }
        }

    }  
}