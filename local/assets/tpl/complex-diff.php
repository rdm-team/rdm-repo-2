<div class="complex__header">
	<img class="complex__headerLogo" src="/local/assets/img/assets/header/logo.svg" alt="">
</div>	
<div class="diffe__bigtitle">Чем отличается автокредит в&nbsp;РДМ&nbsp;-&nbsp;Импорт?</div>

<div class="diffe__header">
	<div class="diffe__head-name"></div>
	<div class="diffe__head-rdm">РДМ-Импорт</div>
	<div class="diffe__head-broker">Кредитный брокер</div>
	<div class="diffe__head-bank">Банк</div>
</div>

<div class="diffe__body">
	<div class="diffe__row">
		<div class="diffe__body-name">
			<strong>ВЫГОДНО</strong>
			<span>Дилерская процентная ставка - 7,5%</span>
		</div>
		<div class="diffe__body-col diffe__icon-good"><div class="diffe__col-name">РДМ-Импорт</div></div>
		<div class="diffe__body-col diffe__icon-bad"><div class="diffe__col-name">Кредитный брокер</div></div>
		<div class="diffe__body-col diffe__icon-bad"><div class="diffe__col-name">Банк</div></div>
	</div>
	<div class="diffe__row">
		<div class="diffe__body-name">
<strong>ДЕЙСТВИТЕЛЬНО ПОМОГАЕМ</strong>

<span>Искренняя заинтересованность специалиста</span>

<span>Советы, как правильно заполнить заявку, чтобы одобрили</span>
		</div>
		<div class="diffe__body-col diffe__icon-good"><div class="diffe__col-name">РДМ-Импорт</div></div>
		<div class="diffe__body-col diffe__icon-good"><div class="diffe__col-name">Кредитный брокер</div></div>
		<div class="diffe__body-col diffe__icon-bad"><div class="diffe__col-name">Банк</div></div>
	</div>
	
	<div class="diffe__row">
		<div class="diffe__body-name">
<strong>ДЕЙСТВИТЕЛЬНО ЭКОНОМИМ</strong>

<span>Низкая комиссия за оформление или её отсутствие</span>

<span>Возможность подать заявку сразу во все банки и сэкономить</span>

<span>Отсутствие процентов за выдачу денег</span>
		</div>
		<div class="diffe__body-col diffe__icon-good"><div class="diffe__col-name">РДМ-Импорт</div></div>
		<div class="diffe__body-col diffe__icon-quest"><div class="diffe__col-name">Кредитный брокер</div></div>
		<div class="diffe__body-col diffe__icon-bad"><div class="diffe__col-name">Банк</div></div>
	</div>
	
	<div class="diffe__row">
		<div class="diffe__body-name">
<strong>У НАС СВОБОДА ВЫБОРА</strong>

<span>Минимальное количество попутных страховок</span>

<span>Отсутствие желания как можно больше заработать на кредите</span>

<span>Желание получить кредит, чтобы продать вам хороший автомобиль</span>

<span>Разнообразие кредитных программ от разных банков</span>

<span>Минимальный пакет документов для кредита</span>
		</div>
		<div class="diffe__body-col diffe__icon-good"><div class="diffe__col-name">РДМ-Импорт</div></div>
		<div class="diffe__body-col diffe__icon-quest"><div class="diffe__col-name">Кредитный брокер</div></div>
		<div class="diffe__body-col diffe__icon-bad"><div class="diffe__col-name">Банк</div></div>
	</div>
	
	<div class="diffe__row">
		<div class="diffe__body-name">
<strong>РАБОТАЕМ БЫСТРО</strong>

<span>Ускоренная процедура получения кредита</span>

<span>Ускоренная процедура оформления кредита</span>
		</div>
		<div class="diffe__body-col diffe__icon-good"><div class="diffe__col-name">РДМ-Импорт</div></div>
		<div class="diffe__body-col diffe__icon-good"><div class="diffe__col-name">Кредитный брокер</div></div>
		<div class="diffe__body-col diffe__icon-quest"><div class="diffe__col-name">Банк</div></div>
	</div>
	
	<div class="diffe__row">
		<div class="diffe__body-name">
<strong>ВСЁ В ОДНОМ МЕСТЕ</strong>

<span>Всё в одном месте (получение, оформление, автомобиль)</span>

<span>Один ответственный за весь процесс от А до Я</span>
		</div>
		<div class="diffe__body-col diffe__icon-good"><div class="diffe__col-name">РДМ-Импорт</div></div>
		<div class="diffe__body-col diffe__icon-bad"><div class="diffe__col-name">Кредитный брокер</div></div>
		<div class="diffe__body-col diffe__icon-bad"><div class="diffe__col-name">Банк</div></div>
	</div>
	
	<div class="diffe__row">
		<div class="diffe__body-name">
		<strong>ПОЛНАЯ ОТВЕТСТВЕННОСТЬ ЗА ВЕСЬ ПРОЦЕСС</strong>

		<span>Отсутствие непредсказуемого скоринга</span>
		<span>Высокая вероятность одобрения</span>
		</div>
		<div class="diffe__body-col diffe__icon-good"><div class="diffe__col-name">РДМ-Импорт</div></div>
		<div class="diffe__body-col diffe__icon-quest"><div class="diffe__col-name">Кредитный брокер</div></div>
		<div class="diffe__body-col diffe__icon-bad"><div class="diffe__col-name">Банк</div></div>
	</div>	
</div>

<div class="diffe__nav">
	<button class="btn-submit" id="onCreditBack">Подать заявку</button>
</div>
