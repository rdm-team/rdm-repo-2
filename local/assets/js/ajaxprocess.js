/**
 * AJAX lib
 *
 * @constructor
 * @this {ajaxProcess}
 * @version 1.1.0
 * @requires jQuery
 */
var ajaxProcess;
ajaxProcess = function () {
	var self = this;

	var globalContext = window;

	// Options
	self.processorUrl = '/local/ajax/common.php';
	self.timeout = 5;
	/* in minutes */
	self.KERNEL_PANIC_ERR_ID = 99;
	self.reqPrefix = 'jxprocess';
	/**
	 * @type {boolean} debug
	 */
	var debug = false;
	//

	//
	var loading = false, date = new Date(), lastSendTime = 0;

	var sessID = 0;

	var requestStack = [];
	requestStack.add2Stack = function (OBJ) {
		var stack = this;
		return stack.push(OBJ) - 1;
	};
	requestStack.getByID = function (id) {
		var stack = this;
		return stack[id];
	};
	requestStack.removeByID = function (id) {
		var stack = this;
		delete stack[id];
	};

	/**
	 * @private
	 * @param {function} optionalCallback
	 */
	var refreshSessID = function (optionalCallback) {
		var reqID = requestStack.add2Stack({
			rsend:    {
				module: 'kernel',
				action: 'getNewSessid'
			},
			callback: function (dataSESS) {
				sessID = dataSESS.answer.newSessId;
				if (typeof(optionalCallback) == 'function') {
					optionalCallback.call(globalContext);
				}
			}
		});
		_Request(reqID);
	};

	/**
	 * @private
	 * @param {object} $block
	 */
	var complete = function ($block) {
		if (typeof($showBlock) == 'object' && $showBlock != null) {
			$showBlock.removeClass('disabled-area');
		}
	}

	/**
	 * @param {integer} reqID
	 * @private
	 */
	var _Request = function (reqID) {
		var reqOBJ = requestStack.getByID(reqID);

		// Prepare
		var jxProcess = {};
		if (reqOBJ.rsend.hasOwnProperty('module')) {
			var v = reqOBJ.rsend.module;
			jxProcess.module = v;
			delete reqOBJ.rsend['module'];
		}
		if (reqOBJ.rsend.hasOwnProperty('action')) {
			var v = reqOBJ.rsend.action;
			jxProcess.action = v;
			delete reqOBJ.rsend['action'];
		}
		jxProcess.sessid = sessID;
		if (debug == true) {
			jxProcess.debug = 1;
		}
		reqOBJ.rsend[self.reqPrefix] = jxProcess;

		/* Send request */
		$.ajax({
			url:      self.processorUrl,
			data:     reqOBJ.rsend,
			type:     'POST',
			context:  globalContext,
			cache:    false,
			dataType: 'text',
			success:  function (data, textStatus, jqXHR) {
				lastSendTime = date.getTime();
				if (debug) {
					console.log(data, textStatus, jqXHR, reqOBJ);
				}
				try{
					var tmp = $.parseJSON(data);
				}catch(e){
					if(debug){
						console.log("Data is not valid JSON string");
						console.log(data);
					}
				};
				if (typeof(tmp) == 'object') {
					data = tmp;
					if (data.error == false) {
						if (typeof(reqOBJ.callback) == 'function') {
							reqOBJ.callback.call(globalContext, data, textStatus, jqXHR);
							//Delete from stack
							if (typeof(data.answer.jsCallback) == 'string') {
								/* Eval is EVIL */
								$.globalEval(data.answer.jsCallback);
							}
						}
					} else {
						if (window.sendNotify) {
							var notify = function (data) {
								sendNotify({
									title: "Ошибка",
									text:  data.answer.message
								});
							}
						}

						/* ERROR */
						if (data.type == self.KERNEL_PANIC_ERR_ID) {
							if (notify) {
								notify(data);
							} else {
								console.log(data);
							}
							requestStack.removeByID(reqID);
							loading = false;
							complete(reqOBJ.disabler);
							return;
						}
						if (typeof(reqOBJ.errorCallback) == 'function') {
							reqOBJ.errorCallback.call(globalContext, data, textStatus, jqXHR);
						} else {
							if (notify) {
								notify(data);
							} else {
								console.log(data);
							}
						}
					}
				}
				requestStack.removeByID(reqID);
				loading = false;
				complete(reqOBJ.disabler);
			}
		});
	};

	// Exports
	/**
	 * @return {void}
	 */
	self.init = function () {
		refreshSessID();
		setInterval(function () {
			refreshSessID.call(self);
		}, self.timeout * 60000);

		window.jQuery.fn.serializeJSON = function () {
			var json = {};
			jQuery.map($(this).serializeArray(), function (n, i) {
				json[n['name']] = n['value'];
			});
			return json;
		};
	};

	/**
	 * Make new AJAX Request
	 *
	 * @param {object} basicRqData
	 * @param {function} basicRqCallback
	 * @param {object} $showBlock
	 * @param {function} basicErrorCallback
	 */
	self.send = function (basicRqData, basicRqCallback, $showBlock, basicErrorCallback) {
		loading = true;
		if (typeof($showBlock) == 'object' && $showBlock != null) {
			$showBlock.addClass('disabled-area');
		}

		var reqID = requestStack.add2Stack({
			rsend:         basicRqData,
			callback:      basicRqCallback,
			disabler:      $showBlock,
			errorCallback: basicErrorCallback
		});

		if (sessID != 0) {
			_Request(reqID);
		} else {
			refreshSessID(function () {
				_Request(reqID);
			});
		}
	};

	//Prepare form data
	self.prepareFormData = function($formObject){
		var formData = $formObject.serializeArray(),
			result = [];

		for(var i = 0, count = formData.length; i < count; i++){
			var fname = formData[i].name;

			if(fname.indexOf('[]') !== -1){
				if(typeof result[fname] == 'undefined'){
					result[fname] = [];
				}
				result[fname].push(formData[i].value);
			}else{
				result[fname] = formData[i].value;
			}
		}
		return result;
	};
};

var AJAX = new ajaxProcess;
AJAX.init();