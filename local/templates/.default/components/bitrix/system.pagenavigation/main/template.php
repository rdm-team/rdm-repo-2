<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}
ob_start();?>
<div class="page__main-wrapper">
	<div class="pagination">
		<div class="pagination__list">
		<?
		$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
		$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
		?>
		<span class="modern-page-title"><?=GetMessage("page")?></span>
		<?
		if($arResult["bDescPageNumbering"] === true):
			$bFirst = true;
			if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):
				if($arResult["bSavePage"]):
					?>
					<div class="pagination__list-item _prev">

					<a class="pagination__list-item-link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"><?=GetMessage("nav_prev")?></a>
					</div>
					<?
				else:
					if ($arResult["NavPageCount"] == ($arResult["NavPageNomer"]+1) ):
						?>
			<div class="pagination__list-item _prev">
						<a class="pagination__list-item-link" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=GetMessage("nav_prev")?></a></div>
						<?
					else:
						?><div class="pagination__list-item _prev">
						<a class="pagination__list-item-link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"><?=GetMessage("nav_prev")?></a>
						</div>
						<?
					endif;
				endif;

				if ($arResult["nStartPage"] < $arResult["NavPageCount"]):
					$bFirst = false;
					if($arResult["bSavePage"]):
						?>
						<div class="pagination__list-item">
							<a class="modern-page-first" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>">1</a>
						</div><?
					else:
						?>
						<div class="pagination__list-item">
							<a class="modern-page-first" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">1</a>
						</div><?
					endif;
					if ($arResult["nStartPage"] < ($arResult["NavPageCount"] - 1)):
						/*?>
									<span class="modern-page-dots">...</span>
						<?*/
						?>
						<a class="modern-page-dots" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=intVal($arResult["nStartPage"] + ($arResult["NavPageCount"] - $arResult["nStartPage"]) / 2)?>">...</a>
						<?
					endif;
				endif;
			endif;
			do
			{
				$NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1;

				if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):
					?>
					<span class="<?=($bFirst ? "modern-page-first " : "")?>modern-page-current"><?=$NavRecordGroupPrint?></span>
					<?
				elseif($arResult["nStartPage"] == $arResult["NavPageCount"] && $arResult["bSavePage"] == false):
					?>
					<a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>" class="<?=($bFirst ? "modern-page-first" : "")?>"><?=$NavRecordGroupPrint?></a>
					<?
				else:
					?>
					<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"<?
					?> class="<?=($bFirst ? "modern-page-first" : "")?>"><?=$NavRecordGroupPrint?></a>
					<?
				endif;

				$arResult["nStartPage"]--;
				$bFirst = false;
			} while($arResult["nStartPage"] >= $arResult["nEndPage"]);

			if ($arResult["NavPageNomer"] > 1):
				if ($arResult["nEndPage"] > 1):
					if ($arResult["nEndPage"] > 2):
						/*?>
								<span class="modern-page-dots">...</span>
						<?*/
						?>
						<div class="pagination__list-item _prev">
							<a class="modern-page-dots" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=round($arResult["nEndPage"] / 2)?>">...</a>
						</div><?
					endif;
					?>
					<a class="modern-page-last" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1"><?=$arResult["NavPageCount"]?></a>
					<?
				endif;

				?>
				<a class="modern-page-next" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><?=GetMessage("nav_next")?></a>
				<?
			endif;

		else:
			$bFirst = true;

			if ($arResult["NavPageNomer"] > 1):
				if($arResult["bSavePage"]):
					?>
					<a class="modern-page-previous" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><?=GetMessage("nav_prev")?></a>
					<?
				else:
					if ($arResult["NavPageNomer"] > 2):
						?><div class="pagination__list-item _prev">
						<a class="pagination__list-item-link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><?=GetMessage("nav_prev")?></a>
						</div><?
					else:
						?><div class="pagination__list-item _prev">
						<a class="pagination__list-item-link" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=GetMessage("nav_prev")?></a>
		</div><?
					endif;

				endif;

				if ($arResult["nStartPage"] > 1):
					$bFirst = false;
					if($arResult["bSavePage"]):
						?>
						<a class="modern-page-first" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1">1</a>
						<?
					else:
						?>
						<a class="modern-page-first" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">1</a>
						<?
					endif;
					if ($arResult["nStartPage"] > 2):
						/*?>
									<span class="modern-page-dots">...</span>
						<?*/
						?>
						<a class="modern-page-dots" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=round($arResult["nStartPage"] / 2)?>">...</a>
						<?
					endif;
				endif;
			endif;

			do
			{
				if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):
					?>
					<div class="pagination__list-item _active">
						<a href="#" class="pagination__list-item-link"><?=$arResult["nStartPage"]?></a>
					</div>
					<?
				elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):
					?>
					<div class="pagination__list-item">
						<a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>" class="pagination__list-item-link"><?=$arResult["nStartPage"]?></a>
					</div><?
				else:
					?>
					<div class="pagination__list-item">
						<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>" class="pagination__list-item-link">
							<?=$arResult["nStartPage"]?>
						</a>
					</div>
					<?
				endif;
				$arResult["nStartPage"]++;
				$bFirst = false;
			} while($arResult["nStartPage"] <= $arResult["nEndPage"]);

			if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):
				if ($arResult["nEndPage"] < $arResult["NavPageCount"]):
					if ($arResult["nEndPage"] < ($arResult["NavPageCount"] - 1)):
						/*?>
								<span class="modern-page-dots">...</span>
						<?*/
						?>
						<a class="modern-page-dots" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=round($arResult["nEndPage"] + ($arResult["NavPageCount"] - $arResult["nEndPage"]) / 2)?>">...</a>
						<?
					endif;
					?>
					<a class="modern-page-last" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>"><?=$arResult["NavPageCount"]?></a>
					<?
				endif;
				?>
				<div class="pagination__list-item _next">
					<a class="pagination__list-item-link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"><?=GetMessage("nav_next")?></a>
				</div>
				<?
			endif;
		endif;

		if ($arResult["bShowAll"]):
			if ($arResult["NavShowAll"]):
				?>
				<a class="modern-page-pagen" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>SHOWALL_<?=$arResult["NavNum"]?>=0"><?=GetMessage("nav_paged")?></a>
				<?
			else:
				?>
				<a class="modern-page-all" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>SHOWALL_<?=$arResult["NavNum"]?>=1"><?=GetMessage("nav_all")?></a>
				<?
			endif;
		endif
		?>
		</div>
	</div>
</div>
<?
$paging = ob_get_contents();
$paging = preg_replace_callback('/href="([^"]+)"/is', function($matches) {
	$url = $matches[1];
	$newUrl = '';
	if ($arUrl = parse_url($url)) {
		$newUrl .= $arUrl['path'];
		if (substr($newUrl, -1) != '/') {
			$newUrl .= '/';
		}
		$newUrl = preg_replace('#(page[\d]+/)#is', '', $newUrl);
		parse_str(htmlspecialcharsback($arUrl['query']), $arQuery);
		foreach ($arQuery as $k => $v) {
			if (substr($k, 0, 5)=='PAGEN') {
				$newUrl .= 'page'.intval($v).'/';
				unset($arQuery[$k]);
			}
			elseif (strpos($_SERVER['REQUEST_URI'], $k) === false) {
				unset($arQuery[$k]);
			}
		}
		$buildQuery = http_build_query($arQuery, '', '&');
		if (strlen($buildQuery)) {
			$newUrl .= '?'.$buildQuery;
		}
	}
	return 'href="'.$newUrl.'"';
}, $paging);
ob_end_clean();
echo $paging;