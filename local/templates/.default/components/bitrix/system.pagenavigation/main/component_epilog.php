<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$next = intval($arResult["NavPageNomer"]) + 1;
$prev = intval($arResult["NavPageNomer"]) - 1;
$host = $_SERVER["HTTP_HOST"];
$canonical = $APPLICATION->GetCurPage(false);

if ($arResult["NavPageNomer"] > 1)
{
	$APPLICATION->AddHeadString('<link rel="canonical" href="https://'.$host.$canonical.'">');
}
if ($next <= $arResult["NavPageCount"])
{
	$APPLICATION->AddHeadString('<link rel="next" href="https://'.$host.$arResult["sUrlPath"].'page'.$next.'/">');
}
if ($prev > 1)
{
	$APPLICATION->AddHeadString('<link rel="prev" href="https://'.$host.$arResult["sUrlPath"].'page'.$prev.'/">');
}
elseif ($prev == 1)
{
	$APPLICATION->AddHeadString('<link rel="prev" href="https://'.$host.$arResult["sUrlPath"].'">');
}
?>