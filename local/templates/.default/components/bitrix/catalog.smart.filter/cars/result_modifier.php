<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arParams["TEMPLATE_THEME"] = "blue";

$arParams["FILTER_VIEW_MODE"] = (isset($arParams["FILTER_VIEW_MODE"]) && toUpper($arParams["FILTER_VIEW_MODE"]) == "HORIZONTAL") ? "HORIZONTAL" : "VERTICAL";
$arParams["POPUP_POSITION"] = (isset($arParams["POPUP_POSITION"]) && in_array($arParams["POPUP_POSITION"], array("left", "right"))) ? $arParams["POPUP_POSITION"] : "left";

$arFilterProps = $arCarBodies = $arColors = array();

foreach($arResult["ITEMS"] as $id=>$arItem)
{
    $arFilterProps[$id] = $arItem['CODE'];

    if($arItem['CODE'] == 'CAR_BODY')
    {
        $carBodyPropID = $arItem["ID"];

        foreach($arItem['VALUES'] as $key=>$arValue)
        {
            $arCarBodies[] = $key;
        }
    }
    else if($arItem['CODE'] == 'COLOR')
    {
        $colorPropID = $arItem["ID"];

        foreach($arItem['VALUES'] as $key=>$arValue)
        {
            $arColors[] = $key;
        }
    }
}
$arResult['FILTER_PROPS'] = $arFilterProps;
$colorsIblockID = YcawebHelper::getIblockID('car-colors');
$carBodiesIblockID = YcawebHelper::getIblockID('car-body');

if($colorsIblockID)
{
    $dbColors = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $colorsIblockID, 'ACTIVE' => 'Y'), false, false, array('IBLOCK_ID', 'ID', 'PROPERTY_COLOR'));
    while($arColors = $dbColors->fetch())
    {
        if(isset($arResult['ITEMS'][$colorPropID]['VALUES'][$arColors['ID']]))
        {
            $arResult['ITEMS'][$colorPropID]['VALUES'][$arColors['ID']]['COLOR_HEX'] = $arColors['PROPERTY_COLOR_VALUE'];
        }
    }
}


if($carBodiesIblockID)
{
    $dbCarBodies = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $carBodiesIblockID, 'ACTIVE' => 'Y'), false, false, array('IBLOCK_ID', 'ID', 'CODE'));
    while ($arCarBodies = $dbCarBodies->fetch())
    {
        if(isset($arResult['ITEMS'][$carBodyPropID]['VALUES'][$arCarBodies['ID']]))
        {
            $arResult['ITEMS'][$carBodyPropID]['VALUES'][$arCarBodies['ID']]['CODE'] =  $arCarBodies['CODE'];
        }
    }
}

