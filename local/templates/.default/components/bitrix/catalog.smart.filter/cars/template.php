<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/colors.css',
	'TEMPLATE_CLASS' => 'bx-'.$arParams['TEMPLATE_THEME']
);
/*
?>
<div class="bx-filter <?=$templateData["TEMPLATE_CLASS"]?> <?if ($arParams["FILTER_VIEW_MODE"] == "HORIZONTAL") echo "bx-filter-horizontal"?>">
	<div class="bx-filter-section container-fluid">
		<div class="row"><div class="<?if ($arParams["FILTER_VIEW_MODE"] == "HORIZONTAL"):?>col-sm-6 col-md-4<?else:?>col-lg-12<?endif?> bx-filter-title"><?echo GetMessage("CT_BCSF_FILTER_TITLE")?></div></div>
		<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="smartfilter">
			<?foreach($arResult["HIDDEN"] as $arItem):?>
			<input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" />
			<?endforeach;?>
			<div class="row">
				<?
					//not prices
					foreach($arResult["ITEMS"] as $key=>$arItem)
					{
						if(
							empty($arItem["VALUES"])
							|| isset($arItem["PRICE"])
						)
							continue;

						if (
							$arItem["DISPLAY_TYPE"] == "A"
							&& (
								$arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
							)
						)
							continue;
				?>
					<div class="<?if ($arParams["FILTER_VIEW_MODE"] == "HORIZONTAL"):?>col-sm-6 col-md-4<?else:?>col-lg-12<?endif?> bx-filter-parameters-box <?if ($arItem["DISPLAY_EXPANDED"]== "Y"):?>bx-active<?endif?>">
						<span class="bx-filter-container-modef"></span>
						<div class="bx-filter-parameters-box-title" onclick="smartFilter.hideFilterProps(this)">
							<span class="bx-filter-parameters-box-hint"><?=$arItem["NAME"]?>
								<?if ($arItem["FILTER_HINT"] <> ""):?>
									<i id="item_title_hint_<?echo $arItem["ID"]?>" class="fa fa-question-circle"></i>
									<script type="text/javascript">
										new top.BX.CHint({
											parent: top.BX("item_title_hint_<?echo $arItem["ID"]?>"),
											show_timeout: 10,
											hide_timeout: 200,
											dx: 2,
											preventHide: true,
											min_width: 250,
											hint: '<?= CUtil::JSEscape($arItem["FILTER_HINT"])?>'
										});
									</script>
								<?endif?>
								<i data-role="prop_angle" class="fa fa-angle-<?if ($arItem["DISPLAY_EXPANDED"]== "Y"):?>up<?else:?>down<?endif?>"></i>
							</span>
						</div>

						<div class="bx-filter-block" data-role="bx_filter_block">
							<div class="bx-filter-parameters-box-container">
							<?
							$arCur = current($arItem["VALUES"]);
							switch ($arItem["DISPLAY_TYPE"])
							{
								case "A"://NUMBERS_WITH_SLIDER
									?>
									<div class="col-xs-6 bx-filter-parameters-box-container-block bx-left">
										<i class="bx-ft-sub"><?=GetMessage("CT_BCSF_FILTER_FROM")?></i>
										<div class="bx-filter-input-container">
											<input
												class="min-price"
												type="text"
												name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
												id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
												value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
												size="5"
												onkeyup="smartFilter.keyup(this)"
											/>
										</div>
									</div>
									<div class="col-xs-6 bx-filter-parameters-box-container-block bx-right">
										<i class="bx-ft-sub"><?=GetMessage("CT_BCSF_FILTER_TO")?></i>
										<div class="bx-filter-input-container">
											<input
												class="max-price"
												type="text"
												name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
												id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
												value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
												size="5"
												onkeyup="smartFilter.keyup(this)"
											/>
										</div>
									</div>

									<div class="col-xs-10 col-xs-offset-1 bx-ui-slider-track-container">
										<div class="bx-ui-slider-track" id="drag_track_<?=$key?>">
											<?
											$precision = $arItem["DECIMALS"]? $arItem["DECIMALS"]: 0;
											$step = ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"]) / 4;
											$value1 = number_format($arItem["VALUES"]["MIN"]["VALUE"], $precision, ".", "");
											$value2 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step, $precision, ".", "");
											$value3 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * 2, $precision, ".", "");
											$value4 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * 3, $precision, ".", "");
											$value5 = number_format($arItem["VALUES"]["MAX"]["VALUE"], $precision, ".", "");
											?>
											<div class="bx-ui-slider-part p1"><span><?=$value1?></span></div>
											<div class="bx-ui-slider-part p2"><span><?=$value2?></span></div>
											<div class="bx-ui-slider-part p3"><span><?=$value3?></span></div>
											<div class="bx-ui-slider-part p4"><span><?=$value4?></span></div>
											<div class="bx-ui-slider-part p5"><span><?=$value5?></span></div>

											<div class="bx-ui-slider-pricebar-vd" style="left: 0;right: 0;" id="colorUnavailableActive_<?=$key?>"></div>
											<div class="bx-ui-slider-pricebar-vn" style="left: 0;right: 0;" id="colorAvailableInactive_<?=$key?>"></div>
											<div class="bx-ui-slider-pricebar-v"  style="left: 0;right: 0;" id="colorAvailableActive_<?=$key?>"></div>
											<div class="bx-ui-slider-range" 	id="drag_tracker_<?=$key?>"  style="left: 0;right: 0;">
												<a class="bx-ui-slider-handle left"  style="left:0;" href="javascript:void(0)" id="left_slider_<?=$key?>"></a>
												<a class="bx-ui-slider-handle right" style="right:0;" href="javascript:void(0)" id="right_slider_<?=$key?>"></a>
											</div>
										</div>
									</div>
									<?
									$arJsParams = array(
										"leftSlider" => 'left_slider_'.$key,
										"rightSlider" => 'right_slider_'.$key,
										"tracker" => "drag_tracker_".$key,
										"trackerWrap" => "drag_track_".$key,
										"minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
										"maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
										"minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
										"maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
										"curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
										"curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
										"fltMinPrice" => intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"] ,
										"fltMaxPrice" => intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
										"precision" => $arItem["DECIMALS"]? $arItem["DECIMALS"]: 0,
										"colorUnavailableActive" => 'colorUnavailableActive_'.$key,
										"colorAvailableActive" => 'colorAvailableActive_'.$key,
										"colorAvailableInactive" => 'colorAvailableInactive_'.$key,
									);
									?>
									<script type="text/javascript">
										BX.ready(function(){
											window['trackBar<?=$key?>'] = new BX.Iblock.SmartFilter(<?=CUtil::PhpToJSObject($arJsParams)?>);
										});
									</script>
									<?
									break;
								case "B"://NUMBERS
									?>
									<div class="col-xs-6 bx-filter-parameters-box-container-block bx-left">
										<i class="bx-ft-sub"><?=GetMessage("CT_BCSF_FILTER_FROM")?></i>
										<div class="bx-filter-input-container">
											<input
												class="min-price"
												type="text"
												name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
												id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
												value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
												size="5"
												onkeyup="smartFilter.keyup(this)"
												/>
										</div>
									</div>
									<div class="col-xs-6 bx-filter-parameters-box-container-block bx-right">
										<i class="bx-ft-sub"><?=GetMessage("CT_BCSF_FILTER_TO")?></i>
										<div class="bx-filter-input-container">
											<input
												class="max-price"
												type="text"
												name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
												id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
												value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
												size="5"
												onkeyup="smartFilter.keyup(this)"
												/>
										</div>
									</div>
									<?
									break;
								case "G"://CHECKBOXES_WITH_PICTURES
									?>
									<div class="bx-filter-param-btn-inline">
									<?foreach ($arItem["VALUES"] as $val => $ar):?>
										<input
											style="display: none"
											type="checkbox"
											name="<?=$ar["CONTROL_NAME"]?>"
											id="<?=$ar["CONTROL_ID"]?>"
											value="<?=$ar["HTML_VALUE"]?>"
											<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
										/>
										<?
										$class = "";
										if ($ar["CHECKED"])
											$class.= " bx-active";
										if ($ar["DISABLED"])
											$class.= " disabled";
										?>
										<label for="<?=$ar["CONTROL_ID"]?>" data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label <?=$class?>" onclick="smartFilter.keyup(BX('<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')); BX.toggleClass(this, 'bx-active');">
											<span class="bx-filter-param-btn bx-color-sl">
												<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
												<span class="bx-filter-btn-color-icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
												<?endif?>
											</span>
										</label>
									<?endforeach?>
									</div>
									<?
									break;
								case "H"://CHECKBOXES_WITH_PICTURES_AND_LABELS
									?>
									<div class="bx-filter-param-btn-block">
									<?foreach ($arItem["VALUES"] as $val => $ar):?>
										<input
											style="display: none"
											type="checkbox"
											name="<?=$ar["CONTROL_NAME"]?>"
											id="<?=$ar["CONTROL_ID"]?>"
											value="<?=$ar["HTML_VALUE"]?>"
											<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
										/>
										<?
										$class = "";
										if ($ar["CHECKED"])
											$class.= " bx-active";
										if ($ar["DISABLED"])
											$class.= " disabled";
										?>
										<label for="<?=$ar["CONTROL_ID"]?>" data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label<?=$class?>" onclick="smartFilter.keyup(BX('<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')); BX.toggleClass(this, 'bx-active');">
											<span class="bx-filter-param-btn bx-color-sl">
												<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
													<span class="bx-filter-btn-color-icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
												<?endif?>
											</span>
											<span class="bx-filter-param-text" title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?><?
											if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):
												?> (<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<?
											endif;?></span>
										</label>
									<?endforeach?>
									</div>
									<?
									break;
								case "P"://DROPDOWN
									$checkedItemExist = false;
									?>
									<div class="bx-filter-select-container">
										<div class="bx-filter-select-block" onclick="smartFilter.showDropDownPopup(this, '<?=CUtil::JSEscape($key)?>')">
											<div class="bx-filter-select-text" data-role="currentOption">
												<?
												foreach ($arItem["VALUES"] as $val => $ar)
												{
													if ($ar["CHECKED"])
													{
														echo $ar["VALUE"];
														$checkedItemExist = true;
													}
												}
												if (!$checkedItemExist)
												{
													echo GetMessage("CT_BCSF_FILTER_ALL");
												}
												?>
											</div>
											<div class="bx-filter-select-arrow"></div>
											<input
												style="display: none"
												type="radio"
												name="<?=$arCur["CONTROL_NAME_ALT"]?>"
												id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
												value=""
											/>
											<?foreach ($arItem["VALUES"] as $val => $ar):?>
												<input
													style="display: none"
													type="radio"
													name="<?=$ar["CONTROL_NAME_ALT"]?>"
													id="<?=$ar["CONTROL_ID"]?>"
													value="<? echo $ar["HTML_VALUE_ALT"] ?>"
													<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
												/>
											<?endforeach?>
											<div class="bx-filter-select-popup" data-role="dropdownContent" style="display: none;">
												<ul>
													<li>
														<label for="<?="all_".$arCur["CONTROL_ID"]?>" class="bx-filter-param-label" data-role="label_<?="all_".$arCur["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape("all_".$arCur["CONTROL_ID"])?>')">
															<? echo GetMessage("CT_BCSF_FILTER_ALL"); ?>
														</label>
													</li>
												<?
												foreach ($arItem["VALUES"] as $val => $ar):
													$class = "";
													if ($ar["CHECKED"])
														$class.= " selected";
													if ($ar["DISABLED"])
														$class.= " disabled";
												?>
													<li>
														<label for="<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label<?=$class?>" data-role="label_<?=$ar["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')"><?=$ar["VALUE"]?></label>
													</li>
												<?endforeach?>
												</ul>
											</div>
										</div>
									</div>
									<?
									break;
								case "R"://DROPDOWN_WITH_PICTURES_AND_LABELS
									?>
									<div class="bx-filter-select-container">
										<div class="bx-filter-select-block" onclick="smartFilter.showDropDownPopup(this, '<?=CUtil::JSEscape($key)?>')">
											<div class="bx-filter-select-text fix" data-role="currentOption">
												<?
												$checkedItemExist = false;
												foreach ($arItem["VALUES"] as $val => $ar):
													if ($ar["CHECKED"])
													{
													?>
														<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
															<span class="bx-filter-btn-color-icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
														<?endif?>
														<span class="bx-filter-param-text">
															<?=$ar["VALUE"]?>
														</span>
													<?
														$checkedItemExist = true;
													}
												endforeach;
												if (!$checkedItemExist)
												{
													?><span class="bx-filter-btn-color-icon all"></span> <?
													echo GetMessage("CT_BCSF_FILTER_ALL");
												}
												?>
											</div>
											<div class="bx-filter-select-arrow"></div>
											<input
												style="display: none"
												type="radio"
												name="<?=$arCur["CONTROL_NAME_ALT"]?>"
												id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
												value=""
											/>
											<?foreach ($arItem["VALUES"] as $val => $ar):?>
												<input
													style="display: none"
													type="radio"
													name="<?=$ar["CONTROL_NAME_ALT"]?>"
													id="<?=$ar["CONTROL_ID"]?>"
													value="<?=$ar["HTML_VALUE_ALT"]?>"
													<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
												/>
											<?endforeach?>
											<div class="bx-filter-select-popup" data-role="dropdownContent" style="display: none">
												<ul>
													<li style="border-bottom: 1px solid #e5e5e5;padding-bottom: 5px;margin-bottom: 5px;">
														<label for="<?="all_".$arCur["CONTROL_ID"]?>" class="bx-filter-param-label" data-role="label_<?="all_".$arCur["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape("all_".$arCur["CONTROL_ID"])?>')">
															<span class="bx-filter-btn-color-icon all"></span>
															<? echo GetMessage("CT_BCSF_FILTER_ALL"); ?>
														</label>
													</li>
												<?
												foreach ($arItem["VALUES"] as $val => $ar):
													$class = "";
													if ($ar["CHECKED"])
														$class.= " selected";
													if ($ar["DISABLED"])
														$class.= " disabled";
												?>
													<li>
														<label for="<?=$ar["CONTROL_ID"]?>" data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label<?=$class?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')">
															<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
																<span class="bx-filter-btn-color-icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
															<?endif?>
															<span class="bx-filter-param-text">
																<?=$ar["VALUE"]?>
															</span>
														</label>
													</li>
												<?endforeach?>
												</ul>
											</div>
										</div>
									</div>
									<?
									break;
								case "K"://RADIO_BUTTONS
									?>
									<div class="radio">
										<label class="bx-filter-param-label" for="<? echo "all_".$arCur["CONTROL_ID"] ?>">
											<span class="bx-filter-input-checkbox">
												<input
													type="radio"
													value=""
													name="<? echo $arCur["CONTROL_NAME_ALT"] ?>"
													id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
													onclick="smartFilter.click(this)"
												/>
												<span class="bx-filter-param-text"><? echo GetMessage("CT_BCSF_FILTER_ALL"); ?></span>
											</span>
										</label>
									</div>
									<?foreach($arItem["VALUES"] as $val => $ar):?>
										<div class="radio">
											<label data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label" for="<? echo $ar["CONTROL_ID"] ?>">
												<span class="bx-filter-input-checkbox <? echo $ar["DISABLED"] ? 'disabled': '' ?>">
													<input
														type="radio"
														value="<? echo $ar["HTML_VALUE_ALT"] ?>"
														name="<? echo $ar["CONTROL_NAME_ALT"] ?>"
														id="<? echo $ar["CONTROL_ID"] ?>"
														<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
														onclick="smartFilter.click(this)"
													/>
													<span class="bx-filter-param-text" title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?><?
													if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):
														?>&nbsp;(<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<?
													endif;?></span>
												</span>
											</label>
										</div>
									<?endforeach;?>
									<?
									break;
								case "U"://CALENDAR
									?>
									<div class="bx-filter-parameters-box-container-block"><div class="bx-filter-input-container bx-filter-calendar-container">
										<?$APPLICATION->IncludeComponent(
											'bitrix:main.calendar',
											'',
											array(
												'FORM_NAME' => $arResult["FILTER_NAME"]."_form",
												'SHOW_INPUT' => 'Y',
												'INPUT_ADDITIONAL_ATTR' => 'class="calendar" placeholder="'.FormatDate("SHORT", $arItem["VALUES"]["MIN"]["VALUE"]).'" onkeyup="smartFilter.keyup(this)" onchange="smartFilter.keyup(this)"',
												'INPUT_NAME' => $arItem["VALUES"]["MIN"]["CONTROL_NAME"],
												'INPUT_VALUE' => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
												'SHOW_TIME' => 'N',
												'HIDE_TIMEBAR' => 'Y',
											),
											null,
											array('HIDE_ICONS' => 'Y')
										);?>
									</div></div>
									<div class="bx-filter-parameters-box-container-block"><div class="bx-filter-input-container bx-filter-calendar-container">
										<?$APPLICATION->IncludeComponent(
											'bitrix:main.calendar',
											'',
											array(
												'FORM_NAME' => $arResult["FILTER_NAME"]."_form",
												'SHOW_INPUT' => 'Y',
												'INPUT_ADDITIONAL_ATTR' => 'class="calendar" placeholder="'.FormatDate("SHORT", $arItem["VALUES"]["MAX"]["VALUE"]).'" onkeyup="smartFilter.keyup(this)" onchange="smartFilter.keyup(this)"',
												'INPUT_NAME' => $arItem["VALUES"]["MAX"]["CONTROL_NAME"],
												'INPUT_VALUE' => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
												'SHOW_TIME' => 'N',
												'HIDE_TIMEBAR' => 'Y',
											),
											null,
											array('HIDE_ICONS' => 'Y')
										);?>
									</div></div>
									<?
									break;
								default://CHECKBOXES
									?>
									<?foreach($arItem["VALUES"] as $val => $ar):?>
										<div class="checkbox">
											<label data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label <? echo $ar["DISABLED"] ? 'disabled': '' ?>" for="<? echo $ar["CONTROL_ID"] ?>">
												<span class="bx-filter-input-checkbox">
													<input
														type="checkbox"
														value="<? echo $ar["HTML_VALUE"] ?>"
														name="<? echo $ar["CONTROL_NAME"] ?>"
														id="<? echo $ar["CONTROL_ID"] ?>"
														<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
														onclick="smartFilter.click(this)"
													/>
													<span class="bx-filter-param-text" title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?><?
													if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):
														?>&nbsp;(<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<?
													endif;?></span>
												</span>
											</label>
										</div>
									<?endforeach;?>
							<?
							}
							?>
							</div>
							<div style="clear: both"></div>
						</div>
					</div>
				<?
				}
				?>
			</div><!--//row-->
			<div class="row">
				<div class="col-xs-12 bx-filter-button-box">
					<div class="bx-filter-block">
						<div class="bx-filter-parameters-box-container">
							<input
								class="btn btn-themes"
								type="submit"
								id="set_filter"
								name="set_filter"
								value="<?=GetMessage("CT_BCSF_SET_FILTER")?>"
							/>
						</div>
					</div>
				</div>
			</div>
			<div class="clb"></div>
		</form>
	</div>
</div>
<?*/?>
<script>
	var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);
</script>





<?/*foreach($arItem["VALUES"] as $val => $ar):?>
	<div class="checkbox">
		<label data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label <? echo $ar["DISABLED"] ? 'disabled': '' ?>" for="<? echo $ar["CONTROL_ID"] ?>">
			<span class="bx-filter-input-checkbox">
				<input
					type="checkbox"
					value="<? echo $ar["HTML_VALUE"] ?>"
					name="<? echo $ar["CONTROL_NAME"] ?>"
					id="<? echo $ar["CONTROL_ID"] ?>"
					<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
					onclick="smartFilter.click(this)"
				/>
				<span class="bx-filter-param-text" title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?><?
				if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):
					?>&nbsp;(<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<?
				endif;?></span>
			</span>
		</label>
	</div>
<?endforeach;
*/

_vardump( $arResult['ITEMS']);
?>

<div class="main__auto-form">
	<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>"  id="auto-form" method="GET" class="auto-form _js-auto-form smartfilter">
		<div class="auto-form__wrapper">
			<div class="auto-form__item _first">
				<div class="auto-form__item-title">
					Марка
				</div>
				<?
				$propId = array_search('MANUFACTURER', $arResult['FILTER_PROPS']);
				$arItem = $arResult['ITEMS'][$propId];
				$arCur = current($arItem["VALUES"]);
				//_vardump($arItem);
				?>
				<div class="auto-form__item-select-wrapper _js-select-wrapper">
					<select class="auto-form__item-select _js-select" name="<?=$arCur['CONTROL_NAME_ALT']?>">
						<option class="auto-form__item-select-var" value="">--</option>
						<?foreach($arItem["VALUES"] as $val => $ar):?>
							<option class="auto-form__item-select-var" <?=($ar["CHECKED"] ? 'selected' : '')?> value="<?=$ar["HTML_VALUE"] ?>" ?>
								<?=$ar["VALUE"];?>
							</option>
						<?endforeach;?>
					</select>
				</div>
			</div>
			<div class="auto-form__item">
				<div class="auto-form__item-title">
					Модель
				</div>
				<div class="auto-form__item-select-wrapper _model">
					<select class="auto-form__item-select _js-car-model" disabled>
						<option class="auto-form__item-select-var" value="">--</option>
					</select>
				</div>
			</div>
			<div class="auto-form__item">
				<div class="auto-form__item-title">
					Цена
				</div>
				<div class="auto-form__item-input-wrapper">
					<input type="text" placeholder="200 000"  class="auto-form__item-input _js-price-input">
				</div>
				<span class="auto-form__item-text">—</span>
				<div class="auto-form__item-input-wrapper">
					<input type="text" placeholder="3 000 000" class="auto-form__item-input _js-price-input">
				</div>
			</div>
			<div class="auto-form__item _year">
				<div class="auto-form__item-title">
					Год
				</div>
				<?
				$propId = array_search('MODEL_YEAR', $arResult['FILTER_PROPS']);
				$arItem = $arResult['ITEMS'][$propId];

				?>
				<div class="auto-form__item-select-wrapper _year">
					<select class="auto-form__item-select">
						<?foreach($arItem["VALUES"] as $val => $ar):?>
							<option class="auto-form__item-select-var" <?=($ar["CHECKED"] ? 'selected' : '')?> value="<? echo $ar["HTML_VALUE"] ?>" name="<? echo $ar["CONTROL_NAME"] ?>">
								<?=$ar["VALUE"];?>
							</option>
						<?endforeach;?>
					</select>
				</div>
				<span class="auto-form__item-text">—</span>
				<div class="auto-form__item-select-wrapper _year">
					<select class="auto-form__item-select">
						<?foreach($arItem["VALUES"] as $val => $ar):?>
							<option class="auto-form__item-select-var" <?=($ar["CHECKED"] ? 'selected' : '')?> value="<? echo $ar["HTML_VALUE"] ?>" name="<? echo $ar["CONTROL_NAME"] ?>">
								<?=$ar["VALUE"];?>
							</option>
						<?endforeach;?>
					</select>
				</div>
			</div>
			<div class="auto-form__item _volume _js-mobile-volume">
				<div class="auto-form__item-title">
					Объем
				</div>
				<?
				$propId = array_search('ENGINE', $arResult['FILTER_PROPS']);
				$arItem = $arResult['ITEMS'][$propId];
				?>
				<div class="auto-form__item-select-wrapper _volume">
					<select class="auto-form__item-select">
						<?foreach($arItem["VALUES"] as $val => $ar):?>
							<option class="auto-form__item-select-var" <?=($ar["CHECKED"] ? 'selected' : '')?> value="<? echo $ar["HTML_VALUE"] ?>" name="<? echo $ar["CONTROL_NAME"] ?>">
								<?=$ar["VALUE"];?>
							</option>
						<?endforeach;?>
					</select>
				</div>
				<span class="auto-form__item-text">—</span>
				<div class="auto-form__item-select-wrapper _volume">
					<select class="auto-form__item-select">
						<?foreach($arItem["VALUES"] as $val => $ar):?>
							<option class="auto-form__item-select-var" <?=($ar["CHECKED"] ? 'selected' : '')?> value="<? echo $ar["HTML_VALUE"] ?>" name="<? echo $ar["CONTROL_NAME"] ?>">
								<?=$ar["VALUE"];?>
							</option>
						<?endforeach;?>
					</select>
				</div>
			</div>
		</div>
		<div class="auto-form__wrapper">
			<!-- <div class="auto-form__item-wrapper"> -->
			<div class="auto-form__item _first _js-mobile-fuel">
				<div class="auto-form__item-title">
					Топливо
				</div>
				<?
				$propId = array_search('FUEL', $arResult['FILTER_PROPS']);
				$arItem = $arResult['ITEMS'][$propId];
				?>
				<div class="auto-form__item-select-wrapper _fuel">
					<select class="auto-form__item-select">
						<option class="auto-form__item-select-var" value="">--</option>
						<?foreach($arItem["VALUES"] as $val => $ar):?>
							<option class="auto-form__item-select-var" <?=($ar["CHECKED"] ? 'selected' : '')?> value="<? echo $ar["HTML_VALUE"] ?>" name="<? echo $ar["CONTROL_NAME"] ?>">
								<?=$ar["VALUE"];?>
							</option>
						<?endforeach;?>
					</select>
				</div>
			</div>
			<div class="auto-form__item _js-mobile-unit">
				<div class="auto-form__item-title">
					Привод
				</div>
				<?
				$propId = array_search('DRIVE', $arResult['FILTER_PROPS']);
				$arItem = $arResult['ITEMS'][$propId];
				//_vardump($arItem);
				?>
				<div class="auto-form__item-select-wrapper _unit">
					<select class="auto-form__item-select">
						<option class="auto-form__item-select-var" value="">--</option>
						<?foreach($arItem["VALUES"] as $val => $ar):?>
							<option class="auto-form__item-select-var" <?=($ar["CHECKED"] ? 'selected' : '')?> value="<? echo $ar["HTML_VALUE"] ?>" name="<? echo $ar["CONTROL_NAME"] ?>">
								<?=$ar["VALUE"];?>
							</option>
						<?endforeach;?>
					</select>
				</div>
			</div>
			<div class="auto-form__item _js-mobile-kpp">
				<div class="auto-form__item-title">
					КПП
				</div>
				<?
				$propId = array_search('TRANSMISSION', $arResult['FILTER_PROPS']);
				$arItem = $arResult['ITEMS'][$propId];
				?>
				<div class="auto-form__item-select-wrapper _kpp">
					<select class="auto-form__item-select">
						<option class="auto-form__item-select-var" value="">--</option>
						<?foreach($arItem["VALUES"] as $val => $ar):?>
							<option class="auto-form__item-select-var" <?=($ar["CHECKED"] ? 'selected' : '')?> value="<? echo $ar["HTML_VALUE"] ?>" name="<? echo $ar["CONTROL_NAME"] ?>">
								<?=$ar["VALUE"];?>
							</option>
						<?endforeach;?>
					</select>
				</div>
			</div>
			<!-- 		<div class="auto-form__item _js-mobile-status">
                <div class="auto-form__item-title">
                    Статус
                </div>
                <div class="auto-form__item-select-wrapper _status">
                    <select class="auto-form__item-select">
                        <option class="auto-form__item-select-var" value="">--</option>
                        <option class="auto-form__item-select-var" value="">1</option>
                        <option class="auto-form__item-select-var" value="">2</option>
                        <option class="auto-form__item-select-var" value="">3</option>
                    </select>
                </div>
            </div> -->
			<div class="auto-form__item-controls">
				<div class="auto-form__item _rudder _js-mobile-rudder">
					<input type="checkbox" class="auto-form__item-check" id="rudder" value="Y" />
					<label class="auto-form__item-label" for="rudder" >Левый руль</label>
				</div>
				<div class="auto-form__item _foreign _js-mobile-foreign">
					<input type="checkbox" class="auto-form__item-check" id="foreign" value="Y" />
					<label class="auto-form__item-label" for="foreign">Иномарки</label>
				</div>
				<div class="auto-form__more _js-more">
					<span class="auto-form__more-text">Больше параметров</span>
				</div>
				<a href="#" class="auto-form__full _js-form-show">Расширенный поиск</a>
				<div class="auto-form__item-wrapper _mobile _js-mobile _hidden">
				</div>
				<button class="auto-form__submit _js-auto-submit" type="submit" id="set_filter"	name="set_filter">
					Подобрать
				</button>
			</div>
		</div>
		<div class="auto-form__full-form _hidden _js-full-form-hide">
			<div class="full-form">
				<div class="full-form__item">
					<div class="full-form__item-title">
						Цвет автомобиля
					</div>
					<?
					$propId = array_search('COLOR', $arResult['FILTER_PROPS']);
					$arItem = $arResult['ITEMS'][$propId];
					?>
					<div class="full-form__item-wrapper">
						<div class="full-form__item-checkbox _color">
							<input type="checkbox" class="full-form__item-checkbox-input" id="check" />
							<label class="full-form__item-checkbox-label _any" for="check">Любой</label>
						</div>
						<?foreach($arItem["VALUES"] as $val => $ar):?>
							<div class="full-form__item-checkbox _color">
								<input type="checkbox" class="full-form__item-checkbox-input"
									   id="<?=$ar["CONTROL_ID"] ?>"
									   name="<? echo $ar["CONTROL_NAME"] ?>"
									   <? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
									/>
								<label class="full-form__item-checkbox-label" for="<?=$ar["CONTROL_ID"] ?>" style="background-color: <?=$ar['COLOR_HEX']?>"></label>
							</div>
						<?endforeach;?>
					</div>
				</div>
				<!-- 	<div class="full-form__item">
                        <div class="full-form__item-title">
                            Состояние
                        </div>
                        <div class="full-form__item-wrapper">
                            <div class="full-form__item-checkbox _common">
                                <input type="checkbox" class="full-form__item-checkbox-input" id="state1"/>
                                <label class="full-form__item-checkbox-label" for="state1">Любое</label>
                            </div>
                            <div class="full-form__item-checkbox _common">
                                <input type="checkbox" class="full-form__item-checkbox-input" id="state2"/>
                                <label class="full-form__item-checkbox-label" for="state2">Небитиые</label>
                            </div>
                            <div class="full-form__item-checkbox _common">
                                <input type="checkbox" class="full-form__item-checkbox-input" id="state3"/>
                                <label class="full-form__item-checkbox-label" for="state3">Битые</label>
                            </div>
                            <div class="full-form__item-checkbox _common">
                                <input type="checkbox" class="full-form__item-checkbox-input" id="state4"/>
                                <label class="full-form__item-checkbox-label" for="state4">Не на ходу</label>
                            </div>
                        </div>
                    </div> -->
				<!-- 	<div class="full-form__item">
                        <div class="full-form__item-title">
                            Наличие ПТС
                        </div>
                        <div class="full-form__item-wrapper">
                            <div class="full-form__item-checkbox _common">
                                <input type="checkbox" class="full-form__item-checkbox-input" id="pts1"/>
                                <label class="full-form__item-checkbox-label" for="pts1">С документами</label>
                            </div>
                            <div class="full-form__item-checkbox _common">
                                <input type="checkbox" class="full-form__item-checkbox-input" id="pts2"/>
                                <label class="full-form__item-checkbox-label" for="pts2">Без документов</label>
                            </div>
                            <div class="full-form__item-checkbox _common">
                                <input type="checkbox" class="full-form__item-checkbox-input" id="pts3"/>
                                <label class="full-form__item-checkbox-label" for="pts3">Не важно</label>
                            </div>
                        </div>
                    </div> -->
				<div class="full-form__item">
					<div class="full-form__item-title">
						Тип кузова
					</div>
					<?
					$propId = array_search('CAR_BODY', $arResult['FILTER_PROPS']);
					$arItem = $arResult['ITEMS'][$propId];
					?>
					<div class="full-form__item-wrapper">
						<?foreach($arItem["VALUES"] as $val => $ar):?>
							<div class="full-form__item-checkbox _common _<?=$ar['CODE']?>">
								<input type="checkbox" class="full-form__item-checkbox-input" name="<? echo $ar["CONTROL_NAME"] ?>"
									   id="<?=$ar["CONTROL_ID"] ?>"
									<? echo $ar["CHECKED"]? 'checked="checked"': '' ?> />
								<label class="full-form__item-checkbox-label" for="<?=$ar["CONTROL_ID"] ?>">
									<?=$ar["VALUE"] ?>
									<div class="full-form__item-checkbox-label-pic"></div>
								</label>
							</div>
						<?endforeach;?>
					</div>
				</div>
				<!-- 	<div class="full-form__item _exchange">
                        <div class="full-form__item-title">
                            Обмен
                        </div>
                        <div class="full-form__item-wrapper"> -->
				<!-- 			<div class="full-form__item-checkbox _common">
                                <input type="checkbox" class="full-form__item-checkbox-input" id="exchange1"/>
                                <label class="full-form__item-checkbox-label" for="exchange1">Готов меняться</label>
                            </div> -->
				<!-- 			<div class="full-form__item-checkbox _common">
                                <input type="checkbox" class="full-form__item-checkbox-input" id="exchange2"/>
                                <label class="full-form__item-checkbox-label" for="exchange2">На более дорогую</label>
                            </div>
                            <div class="full-form__item-checkbox _common">
                                <input type="checkbox" class="full-form__item-checkbox-input" id="exchange3"/>
                                <label class="full-form__item-checkbox-label" for="exchange3">На равноценную</label>
                            </div>
                            <div class="full-form__item-checkbox _common">
                                <input type="checkbox" class="full-form__item-checkbox-input" id="exchange4"/>
                                <label class="full-form__item-checkbox-label" for="exchange4">На более дешевую</label>
                            </div> -->
				<!-- 		</div>
                    </div> -->
				<div class="full-form__item">
					<div class="full-form__item-title">
						Мощность по ПТС
					</div>
					<div class="full-form__item-wrapper">
						<div class="full-form__item-select-wrapper">
							<select class="full-form__item-select">
								<option>1</option>
								<option>2</option>
							</select>
						</div>
					</div>
				</div>
				<div class="full-form__item">
					<div class="full-form__item-title">
						Дополнительно
					</div>
					<div class="full-form__item-wrapper">
						<div class="full-form__item-checkbox _common">
							<?
							$propId = array_search('IS_NEW', $arResult['FILTER_PROPS']);
							$arItem = $arResult['ITEMS'][$propId];
							$arCur = current($arItem["VALUES"]);
							?>
							<input type="checkbox" class="full-form__item-checkbox-input" id="more1" value="<?=$arCur['HTML_VALUE']?>" name="<?=$arCur['CONTROL_ID']?>"/>
							<label class="full-form__item-checkbox-label" for="more1">Новые</label>
						</div>
						<div class="full-form__item-checkbox _common">
							<?
							$propId = array_search('IS_HYBRID', $arResult['FILTER_PROPS']);
							$arItem = $arResult['ITEMS'][$propId];
							$arCur = current($arItem["VALUES"]);
							?>
							<input type="checkbox" class="full-form__item-checkbox-input" id="more2" value="<?=$arCur['HTML_VALUE']?>" name="<?=$arCur['CONTROL_ID']?>"/>
							<label class="full-form__item-checkbox-label" for="more2">Гибрид</label>
						</div>
						<div class="full-form__item-checkbox _common">
							<input type="checkbox" class="full-form__item-checkbox-input" id="more3" />
							<label class="full-form__item-checkbox-label" for="more3">Правый руль</label>
						</div>
						<div class="full-form__item-checkbox _common">
							<input type="checkbox" class="full-form__item-checkbox-input" id="more4" value=""/>
							<label class="full-form__item-checkbox-label" for="more4">Без пробега по РФ</label>
						</div>
					</div>
				</div>
				<div class="full-form__controls">
					<div class="full-form__button _js-button-reset">
						Сбросить фильтры
					</div>
					<a href="#" class="full-form__hide _js-form-hide">Скрыть расширенный поиск</a>
				</div>
				<button class="full-form__submit" type="submit" id="set_filter"	name="set_filter">
					Подобрать
				</button>
			</div>
		</div>
		<!-- </div> -->


	</form>
</div>
