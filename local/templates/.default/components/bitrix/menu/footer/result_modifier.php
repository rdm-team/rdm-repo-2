<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
if(!empty($arResult))
{
    $arMenuLinks = $arResult;
    $arResultNew = $arTitles = $arLinks = array();

    foreach ($arMenuLinks as $arItem)
    {
        if(empty($arItem['LINK']))
        {
            $arTitles[] = $arItem;
        }
        else
        {
            $arLinks[] = $arItem;
        }
    }
    $arResultNew["LINKS"] = $arLinks;
    $arResultNew["TITLE"] = $arTitles;
    $arResult = $arResultNew;

}