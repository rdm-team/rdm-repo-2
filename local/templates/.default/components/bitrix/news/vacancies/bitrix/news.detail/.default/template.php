<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$bImage = false;
?>
<div class="vacancy-detail">
<?if( isset($arResult["DETAIL_PICTURE"]) && $arResult["DETAIL_PICTURE"] ){
	$bImage = true;
	$img = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"], array('width'=>300, 'height'=>500), BX_RESIZE_IMAGE_PROPORTIONAL); 
}?>
<?//if( $bImage ){?>
	
	<div class="xxx" style="margin-top: -25px;">
		<h1><?=$arResult['NAME']?></h1>
		<div class="fllft">
			<img src="<?=$img['src']?>" alt="<?=$arResult['NAME']?>" />
			<div class="offers-list__item-action potr block mrglft">
				<div data-src="#vacancyRequest" onclick="yaCounter4175221.reachGoal('vacancy'); return true;" class="offers-list__item-action-link fancy-inline">
					Оставить отклик
				</div>
			</div>
		</div>
		<div class="fllft">
		<div class="offers-list__item-text">
			<p>Позвоните нам или отправьте <br>своё резюме нам на почту</p>
			<? if ($arResult['DISPLAY_PROPERTIES']['PHONE']['VALUE']){?><p class="vacancy-phone"><?=$arResult['DISPLAY_PROPERTIES']['PHONE']['VALUE'];?></p><?}?>
			<p class="vacancy-email"><a href="mailto:<?=$arResult['DISPLAY_PROPERTIES']['EMAIL']['VALUE'];?>"><?=$arResult['DISPLAY_PROPERTIES']['EMAIL']['VALUE'];?></a></p>
		</div>
		</div>
		<div class="cls"></div>
		
		<div class="vacancy-detail-text offers-list__item-text"><?=$arResult['DETAIL_TEXT']?></div>
	</div>
<?//}?>
	<!--div class="vacancy-detail-content" <?=($bImage ? '' : 'style="width: 100%;"')?>>
		<h1><?=$arResult['NAME']?></h1>
		<div class="offers-list__item-text">
			<p>Позвоните нам или отправьте <br>своё резюме нам на почту</p>
			<p class="vacancy-phone"><?=$arResult['DISPLAY_PROPERTIES']['PHONE']['VALUE'];?></p>
			<p class="vacancy-email"><a href="mailto:<?=$arResult['DISPLAY_PROPERTIES']['EMAIL']['VALUE'];?>"><?=$arResult['DISPLAY_PROPERTIES']['EMAIL']['VALUE'];?></a></p>
		</div>
		<div class="vacancy-detail-text offers-list__item-text"><?=$arResult['DETAIL_TEXT']?></div>
		<center>
			<div class="offers-list__item-action potr">
				<div data-src="#vacancyRequest" onclick="yaCounter4175221.reachGoal('vacancy'); return true;" class="offers-list__item-action-link fancy-inline">
					Оставить заявку
				</div>
		   </div>
		</center>
	</div-->
</div>

<?$APPLICATION->IncludeFile(
	SITE_DIR."include/forms/vacancy.php",
	Array("VACANCY"=>$arResult['NAME']),
	Array("MODE"=>"text")
);?>