<?
$MESS ['T_NEWS_DETAIL_BACK'] = "Возврат к списку";
$MESS ['CATEGORIES'] = "Материалы по теме:";
$MESS ['T_NEWS_DETAIL_PROP_ENGINE'] = "Двигатель";
$MESS ['T_NEWS_DETAIL_PROP_POWER'] = "Мощность";
$MESS ['T_NEWS_DETAIL_PROP_TRANSMISSON'] = "Трансмиссия";
$MESS ['T_NEWS_DETAIL_PROP_DRIVE'] = "Привод";
$MESS ['T_NEWS_DETAIL_PROP_HAS_RF_MILEAGE'] = "Пробег по России";
$MESS ['T_NEWS_DETAIL_PROP_COLOR'] = "Цвет";
$MESS ['T_NEWS_DETAIL_PROP_MILEAGE'] = "Пробег, км";
$MESS ['T_NEWS_DETAIL_PROP_STEERING_WHEEL'] = "Руль";
$MESS ['T_NEWS_DETAIL_PROP_EXTRA'] = "Дополнительно";

$MESS ['T_NEWS_DETAIL_AUTO_TECH_CHARACTERISTICS'] = "Технические характериски авто";
$MESS ['T_NEWS_DETAIL_DIAGNOSTIC'] = "Диагностическая карта";
$MESS ['T_NEWS_DETAIL_WHEREABOUTS'] = "Расположение";
$MESS ['T_NEWS_DETAIL_ADD_TO_COMPARE_LIST'] = "В список сравнения";
$MESS ['T_NEWS_DETAIL_SIMULAR_PRICE'] = "Похожие по цене";
$MESS ['T_NEWS_DETAIL_SIMULAR_TECH'] = "Похожие по тех. характеристикам";
$MESS ['T_NEWS_DETAIL_RESERVE_AUTO'] = "Зарезервировать авто";
$MESS ['T_NEWS_DETAIL_ADD_TO_FAVOURITE'] = "В избранное";
$MESS ['T_NEWS_DETAIL_SHOW_PHONE'] = "Показать телефон";
$MESS ['T_NEWS_DETAIL_OFFER_YOUR_PRICE'] = "Предложите свою цену";
$MESS ['T_NEWS_DETAIL_COMPARE_LIST'] = "Список сравнения";
$MESS ['T_NEWS_DETAIL_ADD_TO_COMPARE_LIST'] = "Возврат к списку";
$MESS ['T_NEWS_DETAIL_YEAR'] = "Год";

?>