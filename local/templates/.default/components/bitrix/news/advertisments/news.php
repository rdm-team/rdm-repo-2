<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
    <h1 class="main__title">
        <?$APPLICATION->showTitle(false)?>
    </h1>
    <div class="page__main-wrapper _filter _js-main-part" id="auto-form">
        <div class="main">
            <div class="main__wrapper">

                <? $APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "top-main",
                    array(
                        "0" => "",
                        "1" => false,
                        "ALLOW_MULTI_SELECT" => "N",
                        "CHILD_MENU_TYPE" => "top.main",
                        "DELAY" => "N",
                        "MAX_LEVEL" => "1",
                        "MENU_CACHE_GET_VARS" => array(),
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "ROOT_MENU_TYPE" => "topmain",
                        "USE_EXT" => "N",
                        "COMPONENT_TEMPLATE" => "top-main"
                    ),
                    false
                ); ?>
                <? $APPLICATION->IncludeComponent(
                    "ycaweb:ads.filter",
                    ".default",
                    array(
                        "FILTER_NAME" => "arrAdsFilter",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_GROUPS" => "Y",
                        "SAVE_IN_SESSION" => "N",
                        "COMPONENT_TEMPLATE" => ".default",
                        "FOLDER" => $arParams["SEF_FOLDER"]
                    ),
                    false
                ); ?>
            </div>
        </div>
    </div>

<?
$sectionCode = '';
if ($_REQUEST['filter']['MANUFACTURER']) {
    $sectionCode = $_REQUEST['filter']['MANUFACTURER'];
    $level = "MARK";
}
if ($_REQUEST['filter']['MODEL']) {
    $parentSectionCode = $sectionCode;
    $sectionCode = $_REQUEST['filter']['MODEL'];
    $level = "MODEL";
}
if ($_REQUEST['filter']['MODEL'] && $_REQUEST['filter']['YEAR']['FROM'] && $_REQUEST['filter']['YEAR']['FROM'] == $_REQUEST['filter']['YEAR']['TO']) {
    $parentSectionCode = $sectionCode;
    $sectionCode = $_REQUEST['filter']['YEAR']['FROM'];
    $level = "YEAR";
}

$APPLICATION->IncludeComponent(
	"ycaweb:catalog.section.list", 
	"intelsib", 
	array(
		"PARENT_SECTION_CODE" => $parentSectionCode,
		"SECTION_CODE" => $sectionCode,
		"IBLOCK_ID" => "5",
		"LEVEL" => $level,
		"CHAIN_FROM_META" => false,
		"MYTEST" => "TT2",
		"COMPONENT_TEMPLATE" => "intelsib",
		"IBLOCK_TYPE" => "ads",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"COUNT_ELEMENTS" => "Y",
		"TOP_DEPTH" => "2",
		"SECTION_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"VIEW_MODE" => "LINE",
		"SHOW_PARENT_NAME" => "Y",
		"SECTION_URL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "N",
		"ADD_SECTIONS_CHAIN" => "Y"
	),
	false
);


if (!empty($_REQUEST['method'])) {
    $sortMethod = $_REQUEST['method'];

    if ($_REQUEST['method'] == 'ASC') {
        $sortMethodTmp = 'DESC';
    } elseif ($_REQUEST['method'] == 'DESC') {
        $sortMethodTmp = 'ASC';
    }
} else {
    $sortMethod = $sortMethodTmp = 'ASC';
}

$arCurrencies = YcawebHelper::getTaxonomy('currency');

switch ($arParams['SEF_FOLDER']) {
    case '/order/':
        $GLOBALS['arrAdsFilter']['PROPERTY_STATE_VALUE'] = 'Под заказ';
		$strTitle = 'Под заказ';
		$boolStatus = false;
        break;
    case '/archive/':
        $GLOBALS['arrAdsFilter']['PROPERTY_STATE_VALUE'] = 'Архив';
		$strTitle = 'Продано';
		$boolStatus = false;
        break;
    default:
        $GLOBALS['arrAdsFilter']['PROPERTY_STATE_VALUE'] = 'В наличии';
        $strTitle = 'Автомобили в наличии';
		$boolStatus = true;
        break;
}
?>
    <div class="page__main-wrapper _auto" id="auto_list">
	<?
	if($boolStatus){
	if(!isset($_REQUEST['PAGEN_2'])){?>
	<h2><?=$strTitle?></h2>
	<?}}else{?>
	<h2><?=$strTitle?></h2>
	<?}

	?>
    <div class="auto">
    <div class="auto__header">
        <div class="auto__header-item _date <?= ($_REQUEST['sort'] == "DATE_CREATE" ? "_active" : "") ?>">
				<span class="auto__header-item-text _<?= strtolower($sortMethod) ?>">
					<a href="<? echo $APPLICATION->GetCurPageParam("method=" . $sortMethodTmp . "&sort=DATE_CREATE", array(
                        "method",
                        "sort")); ?>" class="auto__header-item-link">
						дата публикации
					</a>
				</span>
        </div>
        <div class="auto__header-item _picture ">
        </div>
        <div class="auto__header-item _model <?= ($_REQUEST['sort'] == "MANUFACTURER" ? "_active" : "") ?>">
				<span class="auto__header-item-text _<?= strtolower($sortMethod) ?>">
					<a href="<? echo $APPLICATION->GetCurPageParam("method=" . $sortMethodTmp . "&sort=MANUFACTURER", array(
                        "method",
                        "sort")); ?>" class="auto__header-item-link">
						модель
					</a>
				</span>
        </div>
        <div class="auto__header-item _year <?= ($_REQUEST['sort'] == "MODEL_YEAR" ? "_active" : "") ?>">
				<span class="auto__header-item-text _<?= strtolower($sortMethod) ?>">
					<a href="<? echo $APPLICATION->GetCurPageParam("method=" . $sortMethodTmp . "&sort=MODEL_YEAR", array(
                        "method",
                        "sort")); ?>" class="auto__header-item-link">
						год
					</a>
				</span>
        </div>
        <div class="auto__header-item _engine <?= ($_REQUEST['sort'] == "ENGINE" ? "_active" : "") ?>">
				<span class="auto__header-item-text _<?= strtolower($sortMethod) ?>">
					<a href="<? echo $APPLICATION->GetCurPageParam("method=" . $sortMethodTmp . "&sort=ENGINE", array(
                        "method",
                        "sort")); ?>" class="auto__header-item-link">
						двигатель
					</a>
				</span>
        </div>
        <div class="auto__header-item _mileage <?= ($_REQUEST['sort'] == "MILEAGE" ? "_active" : "") ?>">
				<span class="auto__header-item-text _<?= strtolower($sortMethod) ?>">
					<a href="<? echo $APPLICATION->GetCurPageParam("method=" . $sortMethodTmp . "&sort=MILEAGE", array(
                        "method",
                        "sort")); ?>" class="auto__header-item-link">
						пробег
						<span class="auto__header-item-text-km">, тыс. км</span>
					</a>
				</span>
        </div>
        <div class="auto__header-item _price <?= ($_REQUEST['sort'] == "price" ? "_active" : "") ?>">
				<span class="auto__header-item-text<?= strtolower($sortMethod) ?>">
					<a style="margin-right:26px;"
                       href="<? echo $APPLICATION->GetCurPageParam("method=" . $sortMethodTmp . "&sort=price", array(
                           "method",
                           "sort")); ?>" class="auto__header-item-link">
					цена</a></span>
            <span class="auto__header-item-text _hidden"></span>
            <select class="auto__header-item-select _js-select _currency">
                <? foreach ($arCurrencies as $arCurrency): ?>
                    <option class="auto__header-item-select"
                            value="<?= $arCurrency['CODE'] ?>" <?= ($arCurrency['CODE'] == "RUB" ? "selected" : "") ?>><?= $arCurrency['CODE'] ?></option>
                <? endforeach; ?>
            </select>
        </div>
    </div>
<?
$sortSecondBy = $arParams["SORT_BY2"];
$sortSecondOrder = $arParams["SORT_ORDER2"];

if ($_REQUEST['sort']) {
    $sortBy = $_REQUEST['sort'];

    if ($sortBy != 'DATE_CREATE') {
        $sortFirstBy = 'PROPERTY_' . $_REQUEST['sort'];

        if ($sortFirstBy == 'PROPERTY_MANUFACTURER') {
            $sortSecondBy = 'PROPERTY_MODEL';
        }
    } else {
        $sortFirstBy = $_REQUEST['sort'];
    }

    if (!empty($_REQUEST['method'])) {
        $sortFirstOrder = $_REQUEST['method'];
    } else {
        $sortFirstOrder = $arParams["SORT_ORDER1"];
    }
} else {
    $sortFirstBy = $arParams["SORT_BY1"];
    $sortFirstOrder = $arParams["SORT_ORDER1"];
}
?>

<? $APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "",
    Array(
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "NEWS_COUNT" => $arParams["NEWS_COUNT"],
        "SORT_BY1" => $sortFirstBy,
        "SORT_ORDER1" => $sortFirstOrder,
        "SORT_BY2" => $sortSecondBy,
        "SORT_ORDER2" => $arParams["SORT_ORDER2"],
        "FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
        "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
        "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["detail"],
        "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
        "IBLOCK_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["news"],
        "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
        "SET_TITLE" => $arParams["SET_TITLE"],
        "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
        "MESSAGE_404" => $arParams["MESSAGE_404"],
        "SET_STATUS_404" => $arParams["SET_STATUS_404"],
        "SHOW_404" => $arParams["SHOW_404"],
        "FILE_404" => $arParams["FILE_404"],
        "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "CACHE_FILTER" => $arParams["CACHE_FILTER"],
        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
        "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
        "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
        "PAGER_TITLE" => $arParams["PAGER_TITLE"],
        "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
        "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
        "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
        "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
        "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
        "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
        "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
        "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
        "DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
        "DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
        "PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
        "ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
        "USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
        "GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
        "FILTER_NAME" => "arrAdsFilter",
        "HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
        "CHECK_DATES" => $arParams["CHECK_DATES"],
    ),
    $component
); 
$db_list = CIBlockSection::GetList(Array(), Array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "CODE" => $sectionCode));
$sect = $db_list->GetNext();

if (!preg_match('/\/page([\d]+)\//', $_SERVER['ORIGINAL_URI'])):?>
    <div class="page__main-wrapper _auto section-text">
    <?=$sect["DESCRIPTION"];?>
    </div>
<?endif?>