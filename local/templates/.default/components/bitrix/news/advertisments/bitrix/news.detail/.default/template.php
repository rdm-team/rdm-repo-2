<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
        $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","DETAIL_PAGE_URL");
        $arFilter = Array("IBLOCK_ID"=>5, "EXTERNAL_ID"=>"000000229","ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        if($ob = $res->GetNextElement())
        {
            $arRes = $ob->GetFields();
            $arAuto = '<a href="http://'.$_SERVER["HTTP_HOST"].$arRes["DETAIL_PAGE_URL"].'" >'.$arRes["NAME"].'</a>';
        }

?>
<script>
	$("._breadcrumbs").addClass("_item");
</script>
<style>
	.info__parameters-data._color:after {
		background-color: <?=$arResult['COLOR']['VALUE']?>;
	}
	.page__main-wrpSearch._search._js-search {
		display: none !important;
	}
</style>
<div class="item" data-id="<?=$arResult['ID']?>">

	<div class="item__wrapper">
		<div class="item__title">
			<a href="<?=$arResult['LIST_PAGE_URL']?>" class="item__title-back"><?=GetMessage("AD_DETAIL_BACK_TO_LIST")?></a>
			<?// '.item__title-text._expected' - авто ожидается ?>
			<h1 class="item__title-text"><?=GetMessage("AD_DETAIL_SELLING")?> <?=$arResult['DISPLAY_PROPERTIES']['MANUFACTURER']['VALUE']?> <?=$arResult['DISPLAY_PROPERTIES']['MODEL']['VALUE']?><?if(isset($arResult['MORPHER'])):?> в <?=$arResult['MORPHER']?><?endif;?> в Новосибирске</h1>
<? $name = $arResult['DISPLAY_PROPERTIES']['MANUFACTURER']['VALUE']." ".$arResult['DISPLAY_PROPERTIES']['MODEL']['VALUE'].", ".
				$arResult["DISPLAY_PROPERTIES"]["MODEL_YEAR"]["VALUE"]."&nbsp;".GetMessage("AD_DETAIL_YEAR");
				
				$name = trim($name);
				?>
				
			 
			<div class="info__title-text mobile buy_title_mobile"><div class="detail__compare-wrap desctop__compare-btn" id="compare__wrap" data-id="<?=$arResult["ID"]?>"></div><?=$name;?>

			</div>
		 	<div class="info__date">
				<span class="info__date-nid"><?=GetMessage("AD_DETAIL_ADVERTISMENT")?> <?=$arResult['ID']?></span> 
                <span class="info__date-date"><?=GetMessage("AD_DETAIL_FROM")." ".FormatDate("d F Y", MakeTimeStamp($arResult["TIMESTAMP_X"]));?></span>	
			</div>

			<div class="item__share">
			<button class="share_btn onShare">Поделиться</button>
			<div class="share__list">
				<div class="share__title">Поделиться</div>
				<div class="share_box">
				<div class="ya-share2" data-services="whatsapp,telegram,odnoklassniki,vkontakte,twitter,facebook" data-size="m"></div>
				<button class="share__print onPrint"></button>
				<a class="share__mail" href="mailto:?body=<?='http://'.$_SERVER['HTTP_HOST'].$arResult['DETAIL_PAGE_URL']?>"></a>
				</div>
				<div class="share__link onCopy">
					<input type="text" class="share__copytext" id="share__copytext" value="<?='http://'.$_SERVER['HTTP_HOST'].$arResult['DETAIL_PAGE_URL']?>"/>
				</div>
				<button class="share__copy onCopy">КОПИРОВАТЬ</button>
			</div>
			</div>
		</div>
		<div class="item__left">
			<div class="item__left-slider">
				<div class="slider">
					<?if(!empty($arResult["GALLERY"])):?>
						<ul class="slider__list _js-slider">
							<?foreach($arResult["GALLERY"] as $img):?>
								<li class="slider__list-item">
									<a class="slider__list-item-link fancy-image" data-fancybox="group" href="<?=$img['SRC_ORIG']?>">
										<img src="<?=$img['SRC_MIN']?>" class="slider__list-item-link-img" alt="" />
									</a>
								</li>
							<?endforeach;?>
						</ul>
						<?if(!$arResult["NO_PHOTO"]):?>
							<div class="slider__pager _js-slider-pager">
								<div class="slider__pager-wrapper _js-slider-pager-wrapper">
									<?foreach($arResult["GALLERY"] as $key=>$img):?>
										<?if($key < 10):?>
											<a data-slide-index="<?=$key?>" href="<?=$img['SRC_MIN']?>" class="slider__pager-link">
												<div class="slider__pager-link-wrapper"></div>
												<img src="<?=$img['SRC_MIN_LIST']?>" class="slider__pager-link-img" alt="" />
											</a>
										<?endif;?>
									<?endforeach;?>
								</div>
								<?if($arResult["HAS_MANY_PHOTOS"]):?>
									<div class="slider__pager-all _js-show-all-photos"><?=GetMessage("AD_DETAIL_EXPAND_ALL_PHOTOS")?></div>
									<div class="slider__pager-wrapper _all _js-slider-all-photos">
										<?foreach($arResult["GALLERY"] as $key=>$img):?>
											<?if($key >= 10):?>
												<a data-fancybox="images" data-slide-index="<?=$key?>" href="<?=$img['SRC_MIN']?>" class="slider__pager-link">
													<div class="slider__pager-link-wrapper"></div>
													<img src="<?=$img['SRC_MIN_LIST']?>" class="slider__pager-link-img" alt="" />
												</a>
											<?endif;?>
										<?endforeach;?>
									</div>
								<?endif;?>
							</div>
						<?endif;?>
					<?endif;?>
				</div>
			</div>

<!-- -->
			<div class="info__price-mobile">
			
				<div class="info__price-wrapper">
					<div class="info__price-number">
						<span class="info__price-number-text">
							<?=YcawebHelper::priceFormat($arResult["DISPLAY_PROPERTIES"]["PRICE"]["VALUE"])?>
						</span>
						<span class="info__price-number-rouble"></span>
					</div>
				</div>

				<?
					$percent = .7; // Процент суммы в кредит
					$yearpecent = .099; // Процентная ставка (9.9%)
					$t = (1/12)*$yearpecent;
				
					$summa = (int)$arResult["DISPLAY_PROPERTIES"]["PRICE"]["VALUE"]*$percent;								
					$itogo = $summa *($t+($t/(pow((1+$t),60)-1)));					
				?>
			<?if($arResult["PROPERTIES"]["STATE"]["VALUE_XML_ID"] == "can_archive"){?>
			<a class="info__sales" href="<?=$arResult['SIMILAR_LINK']['CHARACTERISTICS']?>">

				<div class="info__credit-wrapper">
					<div class="info__credit-number info__credit-link">
						<span class="info__credit-number-text auto__sales">
							АВТО ПРОДАНО. ПОСМОТРЕТЬ ПОХОЖИЕ В&nbsp;НАЛИЧИИ
						</span>					
						
					</div>
				</div>
				
			</a>
			<?}else{?>
			<button class="info__credit onFormOpen" data-type="credit" data-target="yakartcredit" data-cpth="true" data-layout="layout-1">
				<input type="hidden" name="credit_autolink" value="<?=(int)$arResult["ID"]?>" />
				<input type="hidden" name="credit_summa" value="<?=YcawebHelper::priceFormat($itogo)?>" />
				<div class="info__credit-wrapper">
					<div class="info__credit-number">
						<span class="info__credit-number-text">
							КУПИТЬ В&nbsp;КРЕДИТ ОТ
						</span>
						<span class="info__credit-rouble"><?=YcawebHelper::priceFormat($itogo)?></span> в&nbsp;мес.
						
					</div>
				</div> 
			</button>						
			<?}?>
			</div>						
<!-- -->			

			<div class="item__left-item-controls controls-desctop">
				<div class="item-controls">
					<div class="item-controls__wrapper">
						<div class="item-controls__views">
							<?=$arResult['SHOW_COUNTER'];?>
						</div>
						<div class="item-controls__links">
						
							<a href="<?=$arResult['SIMILAR_LINK']['PRICE']?>" class="item-controls__links-link" target="_blank">
								<span class="item-controls__links-link-text _price"><?=GetMessage("AD_DETAIL_SIMILAR_PRICE")?></span>
							</a>
							<a href="<?=$arResult['SIMILAR_LINK']['CHARACTERISTICS']?>" class="item-controls__links-link" target="_blank">
								<span class="item-controls__links-link-text _technical"><?=GetMessage("AD_DETAIL_SIMILAR_CHARACTERISTICS")?></span>
							</a>
							<?if($arResult["PROPERTIES"]["STATE"]["VALUE_XML_ID"] != "can_archive"){?>
							<button class="item-controls__links-link onFormOpen" data-type="reserve" data-mtype="box-3" data-target="false" data-cpth="false" data-layout="layout-3">
                                <input type="hidden" name="reserve_autolink" value="<?=$arResult["ID"]?>" />
								<span class="item-controls__links-link-text _reserve">Зарезервировать авто</span>
							</button>
							<button class="item-controls__links-link onFormOpen" data-type="testdrive" data-mtype="box-3" data-target="false" data-cpth="false" data-layout="layout-3">
                                <input type="hidden" name="testdrive_autolink" value="<?=$arResult["ID"]?>" />
								<span class="item-controls__links-link-text _testdrive">Записаться на тест-драйв</span>
							</button>
							<?}?>
						</div>
					</div>
				</div>
			</div>
			
			<div class="item__left-share">
				<div class="share">
					<div class="share__wrapper">
						<div class="share__text"><?=GetMessage("AD_DETAIL_SHARE")?></div>
						<div class="share__icons" id="shareIcons">
							<a href="#" class="share__icons-icon _vk">604</a>
							<a href="#" class="share__icons-icon _fb">246</a>
							<a href="#" class="share__icons-icon _tw">83</a>
							<a href="#" class="share__icons-icon _odnkl">218</a>
							<a href="#" class="share__icons-icon _mail">66</a>
						</div>
					</div>
				</div>
			</div>

            <div class="item__youtube">
                <a href="https://www.youtube.com/channel/UCAc8BkGqUL1TlaLrHEsdAyQ" class="item__youtube-link" target="_blank">Видеоканал: свежие обзоры, полезные советы</a>
            </div>

			<div style="z-index:9000;margin-top: 32px;display: block;">
				<? if($arResult['DISPLAY_PROPERTIES']['YOUTUBE']['VALUE']){
					?><div class="vidos">
					<?foreach($arResult['DISPLAY_PROPERTIES']['YOUTUBE']['VALUE'] as $row){
							?><div class="vidos__item"> <iframe class="vidos__iframe" src="https://www.youtube.com/embed/<?=$row;?>"> </iframe> </div>
							<?}?>
						</div>
						<?}?>
			</div> 
		</div>
		<div class="item__right">
			<div class="item__right-info">
				<div class="info">
					<div class="info__wrapper">
						<div class="info__title">
							<div class="info__title-text mob-title">

								<div class="detail__compare-wrap desctop__compare-btn" id="compare__wrap" data-id="<?=$arResult["ID"]?>"></div>
								<?=$arResult['DISPLAY_PROPERTIES']['MANUFACTURER']['VALUE']?> <?=$arResult['DISPLAY_PROPERTIES']['MODEL']['VALUE']?>,
								<?=$arResult["DISPLAY_PROPERTIES"]["MODEL_YEAR"]["VALUE"]?>&nbsp;<?=GetMessage("AD_DETAIL_YEAR")?>
							</div>
						</div>
						
						<div class="info__price">
						<div class="info__uslugi">
							<div class="info__price-wrapper">
								<div class="info__price-number">
									<span class="info__price-number-text">
										<?=YcawebHelper::priceFormat($arResult["DISPLAY_PROPERTIES"]["PRICE"]["VALUE"])?>
									</span>
									<span class="info__price-number-rouble"></span>
								</div>
							</div>

							<div class="info__video">
								<a class="info__video-link eventVideo" href="javascript:void(0);" data-src="HtknFS5aIH4">
									<span class="info__video-linktext">Смотреть видео</span>
									<span class="info__video-linkicon"></span>
								</a>
							</div>							
							<?
								$percent = .7; // Процент суммы в кредит
								$yearpecent = .099; // Процентная ставка (9.9%)
								$t = (1/12)*$yearpecent;
							
								$summa = (int)$arResult["DISPLAY_PROPERTIES"]["PRICE"]["VALUE"]*$percent;								
								$itogo = $summa *($t+($t/(pow((1+$t),60)-1)));					
							?>
						<?if($arResult["PROPERTIES"]["STATE"]["VALUE_XML_ID"] == "can_archive"){?>
						<a class="info__credit" href="<?=$arResult['SIMILAR_LINK']['CHARACTERISTICS']?>">                
							<div class="info__credit-wrapper">
								<div class="info__credit-number info__credit-link">
									<span class="info__credit-number-text auto__sales">
										АВТО ПРОДАНО. ПОСМОТРЕТЬ ПОХОЖИЕ В&nbsp;НАЛИЧИИ
									</span>								
								</div>
							</div>							
						</a>
						<?}else{?>
						<button class="info__credit onFormOpen" data-type="credit" data-mtype="" data-target="yakartcredit" data-cpth="true"  data-layout="layout-1">
                            <input type="hidden" name="credit_autolink" value="<?=(int)$arResult["ID"]?>" />
                            <input type="hidden" name="credit_summa" value="<?=YcawebHelper::priceFormat($itogo)?>" />
							<div class="info__credit-wrapper">
								<div class="info__credit-number">
									<span class="info__credit-number-text">
										КУПИТЬ В КРЕДИТ ОТ
									</span>
									<span class="info__credit-rouble"><?=YcawebHelper::priceFormat($itogo)?></span> в&nbsp;мес.
									
								</div>
							</div> 
						</button>						
						<?}?>
						                     
                        <?if($arResult["PROPERTIES"]["STATE"]["VALUE_XML_ID"] != "can_archive"){?>
							<div class="info__trade-in">
							<div class="info__trade-in-wrapper">
							<a 
							
							class="eventCalc info__trade-in-link" 
							data-form="kartobmen" 
							data-target="yakartobmen" 
							data-type="obmen" 
							data-target="yakartobmen"							
							data-mtype="box-2"
							>Обмен на ваш автомобиль</a>
							</div>
							</div>
							<?}?>
						</div> 
                        <div class="clearfix"></div> 
						<dl class="info__parameters">
							<?if(!empty($arResult["DISPLAY_PROPERTIES"]["ENGINE"]["VALUE"])):?>
								<div class="info__parameters-wrapper">
									<dt class="info__parameters-term">
										<?=GetMessage('T_NEWS_DETAIL_PROP_ENGINE')?>:
									</dt>
									<dd class="info__parameters-data">
										<?if(!empty($arResult["DISPLAY_PROPERTIES"]["FUEL"]["DISPLAY_VALUE"])):?>
											<?=strtolower(strip_tags($arResult["DISPLAY_PROPERTIES"]["FUEL"]["DISPLAY_VALUE"]));?>, 
										<?endif;?>
										<?=$arResult["DISPLAY_PROPERTIES"]["ENGINE"]["VALUE"];?> л.
									</dd>
								</div>
							<?endif;?>
							<?if(!empty($arResult["DISPLAY_PROPERTIES"]["POWER"]["VALUE"])):?>
								<div class="info__parameters-wrapper">
									<dt class="info__parameters-term">
										<?=GetMessage('T_NEWS_DETAIL_PROP_POWER')?>:
									</dt>
									<dd class="info__parameters-data">
										<?=$arResult["DISPLAY_PROPERTIES"]["POWER"]["VALUE"];?> <?=GetMessage('AD_DETAIL_HORSE_POWER')?>
									</dd>
								</div>
							<?endif;?>
							<?if(!empty($arResult["DISPLAY_PROPERTIES"]["TRANSMISSION"]["DISPLAY_VALUE"])):?>
								<div class="info__parameters-wrapper">
									<dt class="info__parameters-term">
										<?=GetMessage('T_NEWS_DETAIL_PROP_TRANSMISSON')?>:
									</dt>
									<dd class="info__parameters-data">
										<?=strtolower(strip_tags($arResult["DISPLAY_PROPERTIES"]["TRANSMISSION"]["DISPLAY_VALUE"]));?>
									</dd>
								</div>
							<?endif;?>
							<?if(!empty($arResult["DISPLAY_PROPERTIES"]["DRIVE"]["VALUE"])):?>
								<div class="info__parameters-wrapper">
									<dt class="info__parameters-term">
										<?=GetMessage('T_NEWS_DETAIL_PROP_DRIVE')?>:
									</dt>
									<dd class="info__parameters-data">
										<?=strtolower(strip_tags($arResult["DISPLAY_PROPERTIES"]["DRIVE"]["DISPLAY_VALUE"]));?>
									</dd>
								</div>
							<?endif;?>
							<?if(!empty($arResult['COLOR']['NAME'])):?>
								<div class="info__parameters-wrapper">
									<dt class="info__parameters-term">
										<?=GetMessage('T_NEWS_DETAIL_PROP_COLOR')?>:
									</dt>
									<dd class="info__parameters-data _color">
										<?=strtolower($arResult['COLOR']['NAME']);?>
									</dd>
								</div>
							<?endif;?>
							<?if(!empty($arResult["DISPLAY_PROPERTIES"]["HAS_RF_MILEAGE"]["VALUE"])):?>
								<div class="info__parameters-wrapper">
									<dt class="info__parameters-term">
										<?=GetMessage('T_NEWS_DETAIL_PROP_HAS_RF_MILEAGE')?>:
									</dt>
									<dd class="info__parameters-data">
										<?=GetMessage('AD_DETAIL_HAS_RF_MILEAGE')?>
									</dd>
								</div>
							<?endif;?>						
							<?if(!empty($arResult["DISPLAY_PROPERTIES"]["STEERING_WHEEL"]["VALUE"])):?>
								<div class="info__parameters-wrapper">
									<dt class="info__parameters-term">
										<?=GetMessage('T_NEWS_DETAIL_PROP_STEERING_WHEEL')?>:
									</dt>
									<dd class="info__parameters-data">
										<?=strtolower($arResult["DISPLAY_PROPERTIES"]["STEERING_WHEEL"]["VALUE"])?>
									</dd>
								</div>
							<?endif;?>
							<?
							if($arResult["PROPERTIES"]["DIAGNOZ"]["MODIFY"] != "N" ||
							$arResult["PROPERTIES"]["DIAGNOZ"]["MODIFY"] != "N" ||
							$arResult["PROPERTIES"]["DIAGNOZ"]["MODIFY"] != "N")
							{?>
                            <div class="info__parameters-wrapper">
                                <div class="info__parameters-item">
                                    <?if($arResult["PROPERTIES"]["DIAGNOZ"]["MODIFY"] != "N"){?><a class="info__parameters-itemLink _view fancybox" data-fancybox href="<?=$arResult["PROPERTIES"]["DIAGNOZ"]["MODIFY"]["SRC"]?>">Посмотреть диагностическую карту</a><?}?>
                                    <?if($arResult["PROPERTIES"]["INSPECTIA"]["MODIFY"] != "N"){?><a class="info__parameters-itemLink _view fancybox" data-fancybox href="<?=$arResult["PROPERTIES"]["INSPECTIA"]["MODIFY"]["SRC"]?>">Посмотреть инспекцию компании</a><?}?>
                                    <?if($arResult["PROPERTIES"]["AUTOCODE"]["MODIFY"] != "N"){?><a class="info__parameters-itemLink _view fancybox" data-fancybox href="<?=$arResult["PROPERTIES"]["AUTOCODE"]["MODIFY"]["SRC"]?>">Отчет Автокод</a><?}?>
                                </div>
                            </div>
							<?}?>
                       
							<?if(
							!empty($arResult["PROPERTIES"]["PROBEG"]["VALUE"])||
							!empty($arResult["PROPERTIES"]["OWNERS"]["VALUE"])||
							!empty($arResult["PROPERTIES"]["PTS"]["VALUE"])||
							!empty($arResult["PROPERTIES"]["VIN"]["VALUE"])
							){?>
								<span class="info__parameters-name info__parameters-title">ГАРАНТИРОВАНО РДМ-ИМПОРТ</span>
								<?
								 // Заведено два свойства с пробегом. Если в одном нет пробега, берется из другого свойства
								?>
								<?if(!empty($arResult["PROPERTIES"]["PROBEG"]["VALUE"])){?>								
									<div class="info__parameters-wrapper">										
										<dt class="info__parameters-term">
											Пробег:
										</dt>
										<dd class="info__parameters-data">
											<?=strtolower($arResult["PROPERTIES"]["PROBEG"]["VALUE"])?>
										</dd>										
									</div>
								<?}elseif(!empty($arResult["PROPERTIES"]["MILEAGE"]["VALUE"])){?>
									<div class="info__parameters-wrapper">										
										<dt class="info__parameters-term">
											Пробег:
										</dt>
										<dd class="info__parameters-data">
											<?=strtolower($arResult["PROPERTIES"]["MILEAGE"]["VALUE"])?>
										</dd>										
									</div>
								<?}?>
								
								<?if(!empty($arResult["PROPERTIES"]["OWNERS"]["VALUE"])):?>								
									<div class="info__parameters-wrapper">										
										<dt class="info__parameters-term">
											Количество владельцев:
										</dt>
										<dd class="info__parameters-data">
											<?=strtolower($arResult["PROPERTIES"]["OWNERS"]["VALUE"])?>
										</dd>										
									</div>
								<?endif;?>
								
								<?if(!empty($arResult["PROPERTIES"]["PTS"]["VALUE"])):?>								
									<div class="info__parameters-wrapper">										
										<dt class="info__parameters-term">
											ПТС:
										</dt>
										<dd class="info__parameters-data">
											<?=strtolower($arResult["PROPERTIES"]["PTS"]["VALUE"])?>
										</dd>										
									</div>
								<?endif;?>
								
								<?if(!empty($arResult["PROPERTIES"]["VIN"]["VALUE"])):?>								
									<div class="info__parameters-wrapper">										
										<dt class="info__parameters-term">
											VIN:
										</dt>
										<dd class="info__parameters-data info__parameters-uppercase">
											<?=strtolower($arResult["PROPERTIES"]["VIN"]["VALUE"])?>
										</dd>										
									</div>
								<?endif;?>
								<?}?>
								
                                <div class="probeg__wrap">
									<button class="probeg__btn" id="onGarantProbeg">Узнать больше о гарантии</button>
                                </div>
							
								<?if(
								$arResult["PROPERTIES"]["INSPECTION"]["MODIFY"] != "N" ||
								$arResult["PROPERTIES"]["STO"]["MODIFY"] != "N" ||
								$arResult["PROPERTIES"]["NOT_PALETE"]["MODIFY"] != "N" ||
								$arResult["PROPERTIES"]["GIBDD"]["MODIFY"] != "N" ||
								$arResult["PROPERTIES"]["PRISTAV"]["MODIFY"] != "N" ||
								$arResult["PROPERTIES"]["SERTIFICAT"]["MODIFY"] != "N"){?>
								<span class="info__parameters-name info__parameters-title">ДОКУМЕНТЫ</span>
								<div class="info__parameters-wrapper">
									<div class="info__parameters-item">
										<?if($arResult["PROPERTIES"]["INSPECTION"]["MODIFY"] != "N"){?><a class="info__parameters-itemLink _view fancybox" data-fancybox href="<?=$arResult["PROPERTIES"]["INSPECTION"]["MODIFY"]["SRC"]?>">Инспекция компании</a><?}?>
										<?if($arResult["PROPERTIES"]["STO"]["MODIFY"] != "N"){?><a class="info__parameters-itemLink _view fancybox" data-fancybox href="<?=$arResult["PROPERTIES"]["STO"]["MODIFY"]["SRC"]?>">Отчет СТО</a><?}?>
										<?if($arResult["PROPERTIES"]["NOT_PALETE"]["MODIFY"] != "N"){?><a class="info__parameters-itemLink _view fancybox" data-fancybox href="<?=$arResult["PROPERTIES"]["NOT_PALETE"]["MODIFY"]["SRC"]?>">Отчет Нотариальной палаты</a><?}?>
										<?if($arResult["PROPERTIES"]["GIBDD"]["MODIFY"] != "N"){?><a class="info__parameters-itemLink _view fancybox" data-fancybox href="<?=$arResult["PROPERTIES"]["GIBDD"]["MODIFY"]["SRC"]?>">Отчет ГИБДД</a><?}?>
										<?if($arResult["PROPERTIES"]["PRISTAV"]["MODIFY"] != "N"){?><a class="info__parameters-itemLink _view fancybox" data-fancybox href="<?=$arResult["PROPERTIES"]["PRISTAV"]["MODIFY"]["SRC"]?>">Отчет судебных приставов</a><?}?>
										<?if($arResult["PROPERTIES"]["SERTIFICAT"]["MODIFY"] != "N"){?><a class="info__parameters-itemLink _view fancybox" data-fancybox href="<?=$arResult["PROPERTIES"]["SERTIFICAT"]["MODIFY"]["SRC"]?>">Подробный отчет Автокод</a><?}?>
									</div>
								</div>
								<?}?>
								<div class="youtube__mobile">
									<a class="youtube__mobile-link eventVideo" href="javascript:void(0);" data-src="HtknFS5aIH4">
										Смотреть видео										
									</a>
								</div>
							<?if(strlen($arResult['DETAIL_TEXT']) > 0):?>
								<div class="info__parameters-wrapper _more" id="printText">
									<span class="info__parameters-name info__parameters-title">ДОПОЛНИТЕЛЬНО:</span>
									<span class="info__parameters-text">
										<?=$arResult["DETAIL_TEXT"];?>
									</span>
								</div>
							<?endif;?>
						</dl>
					
						<div class="info__parameters">z
						<?if(!empty($arResult["DETAIL_PICTURE"]["SRC"])):?>
							<div class="info__more">
								<!--
								<a href="#" class="info__more-link">
									<span class="info__more-link-text"><?=GetMessage('AD_DETAIL_AUTO_CHARACTERISTICS')?></span>
								</a>
								-->
								<a href="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" class="info__more-link _js-diagnostic">
									<span class="info__more-link-text"><?=GetMessage('AD_DETAIL_DIAGNOSTIC_CARD')?></span>
									<div class="info__more-link-image">
										<img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" alt="" />
									</div>
								</a>
							</div>
						<?endif;?>
						<?if(!empty($arResult["DISPLAY_PROPERTIES"]["CITY"]["VALUE"])):?>
							<div class="info__location">
								<?=$arResult["DISPLAY_PROPERTIES"]["CITY"]["VALUE"];?>
							</div>
						<?endif;?>
						<?if(!empty($arResult["DISPLAY_PROPERTIES"]["CONTACT_NAME"]["VALUE"])):?>
							<div class="info__contact-name">
								<?=$arResult["DISPLAY_PROPERTIES"]["CONTACT_NAME"]["VALUE"];?>
							</div>
						<?endif;?>
						
						<?/*if(!empty($arResult['PHONES'])):?>
							<?foreach($arResult['PHONES'] as $number):?>
								<div class="info__phone">
								
									<div class="info__phone-number">
											<div class="info__phone-code">+7</div> 
											<a class="info__phone-full" href="tel:<?=str_replace(' ','',$number)?>"><?=$number?></a>
											<div class="info__phone-button onShowphone"><?=GetMessage('AD_DETAIL_SHOW_PHONE')?></div>
									</div>									
								</div>
							<?endforeach;?>
						<?endif;*/?>
						<div class="info__phone">
							<div class="info__phone-number">
								<div class="info__phone-code">+7</div> 
								<a class="info__phone-full" href="tel:+73833596159">+7 (383) 359-61-59</a>
								<div class="info__phone-button onShowphone"><?=GetMessage('AD_DETAIL_SHOW_PHONE')?></div>
							</div>									
						</div>

						<?if($arResult["PROPERTIES"]["STATE"]["VALUE_XML_ID"] != "can_archive" && false){?>
                        <div class="trade__wrap">
                            <button class="comments-ttl onClickTradeIn" data-target="yakartcredit">
                                Торгуйтесь с нами, предложив свою цену
                            </button>                          
                        </div>
						<?}?>
						
					</div>
					</div>
				</div>
			</div>
			
		</div>
		
		<div class="item__left-item-controls controls-mobile">
			<div class="item-controls">
				<div class="item-controls__wrapper">
					<div class="item-controls__views">
						<?=$arResult['SHOW_COUNTER'];?>
					</div>
					<div class="item-controls__links">
					
						<a href="<?=$arResult['SIMILAR_LINK']['PRICE']?>" class="item-controls__links-link" target="_blank">
							<span class="item-controls__links-link-text _price"><?=GetMessage("AD_DETAIL_SIMILAR_PRICE")?></span>
						</a>
						<a href="<?=$arResult['SIMILAR_LINK']['CHARACTERISTICS']?>" class="item-controls__links-link" target="_blank">
							<span class="item-controls__links-link-text _technical"><?=GetMessage("AD_DETAIL_SIMILAR_CHARACTERISTICS")?></span>
						</a>
						<?if($arResult["PROPERTIES"]["STATE"]["VALUE_XML_ID"] != "can_archive"){?>
						<button class="item-controls__links-link onFormOpen" data-type="reserve" data-mtype="box-1" data-cpth="false" data-layout="layout-3">
							<input type="hidden" name="reserve_autolink" value="<?=$arResult["ID"]?>" />
							<span class="item-controls__links-link-text _reserve">Зарезервировать авто</span>
						</button>
						<button class="item-controls__links-link onFormOpen" data-type="testdrive" data-mtype="box-1" data-cpth="false" data-layout="layout-3">
							<input type="hidden" name="testdrive_autolink" value="<?=$arResult["ID"]?>" />
							<span class="item-controls__links-link-text _testdrive">Записаться на тест-драйв</span>
						</button>
						<?}?>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>
<script>
var oPrint = <?=json_encode(autoID["PRINT"]);?>;
var carName = "<?=$arResult['DISPLAY_PROPERTIES']['MANUFACTURER']['VALUE'] . " " . $arResult['DISPLAY_PROPERTIES']['MODEL']['VALUE'] .", ". $arResult["DISPLAY_PROPERTIES"]["MODEL_YEAR"]["VALUE"] . " " . GetMessage("AD_DETAIL_YEAR");?>"
var cPrice = <?=$arResult["DISPLAY_PROPERTIES"]["PRICE"]["VALUE"]?>;
var autoID = <?=$arResult['ID']?>;
</script>
<div class="share__shadow onShareCancel"></div>