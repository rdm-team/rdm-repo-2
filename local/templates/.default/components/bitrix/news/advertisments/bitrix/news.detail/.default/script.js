//var copytext = document.getElementById("share__link");
$(document).ready(function(){
			
	$('body').on('click','#onGarantProbeg', function(){

		$.fancybox.open([$('#allgarant')], {
		wrapCSS:'styleWrap',                  
		touch:true				
		});
		
	});				
				
	/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
	$('body').on('click','.onEventCompare', function(){
			$.post('/compare-add/',{id:$(this).data('id')},function(data){		
				if(data.status){$('.detail__compare-wrap').html(data.html);}
			},'json');
		
	});
	
	/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
	
	var id = $('#compare__wrap').data('id');

	$.post('/compare-get/',{id:id},function(data){		
		if(data.status){$('.detail__compare-wrap').html(data.html);}
	},'json');
	
	/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
	
	$('body').on('click','.onCopy', function(){
		// Выделяем текст в поле
		$('#share__copytext').select();
		// Копируем текст в буфер обмена
		document.execCommand('copy');
	});
	
	/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
	
	$('body').on('click','.onShare', function(){
		$('.share__list').css({display:'block'});
		$('.share__shadow').css({display:'block'});
	});
	
	/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
	
	$('body').on('click','.onShareCancel', function(){
				
		$('.share__list').css({display:'none'});
		$('.share__shadow').css({display:'none'});
	
	});		
	
	$('body').on('touchend','.onShareCancel', function(){
				
		$('.share__list').css({display:'none'});
		$('.share__shadow').css({display:'none'});
	
	});	
	
	/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
	
	$('body').on('click','.onPrint', function(){				
		$('.share__list').css({display:'none'});
		$('.share__shadow').css({display:'none'});
		
		MyPrint();
		return false;
	});
	
});
	
function MyPrint(){

	var table = '<table cellpadding="0" cellspacing="0" border="0">'+
	'<tr><td colspan="3" ><strong id="1">Продажа Honda CR-V в Новосибирске</strong><p id="2">Объявление 15573 от 2 Ноября 2017</p></td></tr>'+
	'<tr>'+
	'	<td id="0" colspan="2" style="width:67%;"></td>'+
	'	<td colspan="1" style="width:33%;">'+
	'	<h3 id="3"></h3>'+
	'	<b id="4">1 480 000 Р</b>'+
	'		<div id="5"></div>'+
	'	</td>'+
	'</tr>'+
	'<tr>'+
	'<td id="6" style="width:33%;padding: 20px 10px 10px 0;font-size:16px;"></td>'+
	'<td id="7" style="width:33%;padding: 20px 0;font-size:16px;"></td>'+
	'<td id="8" style="width:33%;padding: 20px 0;font-size:16px;"></td>'+
	'</tr>'+
	'</table>';

	var style = '<style>'+
	'@page rotated {size: landscape}'+
	'*{font-family:"Arial";}'+
	'table{width:190mm; font-size:1.3pc;}'+
	'th{text-align:center; vertical-align:center; fonw-weight:bold;}'+
	'td:nth-child(even){width:37%;height:60px; padding-left:0; text-align:left; vertical-align:top;} '+
	'td:nth-child(odd){ width:0%;height:60px; text-align:left; vertical-align:top;} '+
	'img{height:80mm;}'+
	'h3{margin:0;font-size:24px;}'+
	'h4{margin:0;font-size:16px;}'+
	'strong{font-family: "Arial";font-size: 24px;margin: 0 0 5px 0;display: inline-block;}'+
	'p{margin:0;font-family:"Arial";font-size:16px;}'+
	'b{margin: 10px 0;display: inline-block;font-size: 24px;color:#ff0000}'+
	'label{color: #8c8c8c;font-size: 16px;padding-bottom: 5px;display: inline-block;}'+
	'div{font-size:16px;}'+
	'</style>';

	var iframe=$('<iframe id="print_frame">'); // создаем iframe в переменную
	
	$('body').append(iframe); //добавляем эту переменную с iframe в наш body (в самый конец)
	
	var doc = $('#print_frame')[0].contentDocument || $('#print_frame')[0].contentWindow.document;
	var win = $('#print_frame')[0].contentWindow || $('#print_frame')[0];
	doc.getElementsByTagName('body')[0].innerHTML=style+table;// Вставили таблицу для печати в фрейм
	
	doc.getElementById(0).innerHTML = '<img src="'+oPrint[0]+'" />';
	doc.getElementById(6).innerHTML = $('#printText').html();
	doc.getElementById(8).innerHTML = oPrint[6];
	for(i = 1; i <= 5; i++){	
		doc.getElementById(i).innerHTML = oPrint[i];
	}
	win.print();
	$('iframe').remove();
	
}