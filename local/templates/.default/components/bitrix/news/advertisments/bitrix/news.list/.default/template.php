<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(empty($arResult["ITEMS"])){
?>
<p class="not_found_text">
	К сожалению, в настоящий момент у нас нет данных автомобилей в наличии.<br/> Мы рекомендуем вам не расстраиваться и продолжить поиск, нажав на одну из кнопок ниже:
</p>
<div class="buttons_wrap">
	<a href="?filter%5BENGINE%5D%5BFROM%5D=<?=$_REQUEST['filter']['ENGINE']['FROM']?>
	&filter%5BENGINE%5D%5BTO%5D=<?=$_REQUEST['filter']['ENGINE']['TO']?>
	&filter%5BFUEL%5D=<?=$_REQUEST['filter']['FUEL']?>
	&filter%5BDRIVE%5D=<?=$_REQUEST['filter']['DRIVE']?>
	&filter%5BTRANSMISSION%5D=<?=$_REQUEST['filter']['TRANSMISSION']?>
	&start_filter=filter
	&filter%5BPOWER%5D=<?=$_REQUEST['filter']['POWER']?>
	&filter%5BIS_FOREIGN%5D=<?=$_REQUEST['filter']['IS_FOREIGN']?>
	&filter%5BSTEERING_WHEEL%5D%5BLEFT%5D=<?=$_REQUEST['filter']['STEERING_WHEEL']['LEFT']?>
" class="btn-subscription">Посмотреть похожие автомобили <b>по техническим характеристикам</b></a>
	<a href="?filter%5BPRICE%5D%5BFROM%5D=<?=$_REQUEST['filter']['PRICE']['FROM']?>&filter%5BPRICE%5D%5BTO%5D=<?=$_REQUEST['filter']['PRICE']['TO']?>
	&filter%5BYEAR%5D%5BFROM%5D=<?=$_REQUEST['filter']['YEAR']['FROM']?>
	&filter%5BYEAR%5D%5BTO%5D=<?=$_REQUEST['filter']['YEAR']['TO']?>
	&start_filter=1" class="btn-subscription">Искать такие же автомобили <b>по цене и году выпуска</b></a>
	<a href="javascript:void(0);" class="btn-subscription onEventSubscript">Подписаться на <b>новые поступления</b> автомобилей в автосалон</a>
</div>
<?}else{?>	
<?$sCurPage = $APPLICATION->GetCurPage(false);?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?$inxImage = 1;
$arCookie = $APPLICATION->get_cookie('compare');
if( $sCurPage != '/order/' && $sCurPage != '/archive/'){
	$ordArhive = true;
}else{
	$ordArhive = false;
}

foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	if(isset($arItem["DATE_CREATE"])){$strDate = $arItem["DATE_CREATE"];}else{$strDate = $arItem["ACTIVE_FROM"];}
	
	if(in_array($arItem['ID'], $arCookie)){$style='fave-active';}else{$style='fave-inactive';}
	
	$_state = $arItem["DISPLAY_PROPERTIES"]["STATE"]["VALUE"];
	
	if($sCurPage =='/order/'){$_state = 'Ожидается';}elseif($sCurPage =='/archive/'){$_state = 'Продано';}
	?>
	<a href="<?= "/auto/view/".$arItem["EXTERNAL_ID"]."/"?>" class="auto__item _main  <?=$style?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>" data-fave="<?=$arItem['ID']?>">
		<div class="auto__item-cell _date" data-date="archive">
			<?=FormatDate("d/m", MakeTimeStamp($strDate))?>
			<?if($ordArhive){?>
			<div class="fave__btn fave__btn-desctop onEventCompare" data-id="<?=$arItem['ID']?>"></div>
			<?}?>
		</div>
		<div class="auto__item-cell _picture">
			<?/*if(!empty($arItem["PREVIEW_PICTURE_MIN"]["SRC"])):?>
				<img src="<?=$arItem["PREVIEW_PICTURE_MIN"]["SRC"]?>" alt="" class="auto__item-picture-img" />
			<?endif*/?>

				<img id="<?=$inxImage?>" src="/local/assets/img/noimg.png" data-src="<?=$arItem["PREVIEW_PICTURE_MIN"]["SRC"]?>" alt="" class="auto__item-picture-img img-load" />

		</div>
		<div class="auto__item-cell _model">
			<span class="auto__item-cell-important">
				<?=$arItem['DISPLAY_PROPERTIES']['MANUFACTURER']['VALUE']." ".$arItem['DISPLAY_PROPERTIES']['MODEL']['VALUE'];?>
			</span>
			
			<span class="auto__item-cell-state"><?=$_state?></span>
			
		</div>
		<div class="auto__item-cell _year">
			<span class="auto__item-cell-important">
				<?=$arItem['DISPLAY_PROPERTIES']['MODEL_YEAR']['VALUE'];?>
			</span>
		</div>
		<div class="auto__item-cell _engine">
			<span class="auto__item-cell-important">
				<?=$arItem['DISPLAY_PROPERTIES']['ENGINE']['VALUE']." ".GetMessage("AD_LIST_ENGINE_VOLUME_MEASURE");?>
			</span>
			<span class="auto__item-cell-about">
				<?=strip_tags($arItem["DISPLAY_PROPERTIES"]["FUEL"]["DISPLAY_VALUE"]);?>
			</span>
			</span>
			<span class="auto__item-cell-about">
				<?=strip_tags($arItem["DISPLAY_PROPERTIES"]["TRANSMISSION"]["DISPLAY_VALUE"]);?>
			</span>
			<span class="auto__item-cell-about">
				<?=strip_tags($arItem["DISPLAY_PROPERTIES"]["DRIVE"]["DISPLAY_VALUE"]);?>
			</span>
		</div>
		<div class="auto__item-cell _mileage">
			<span class="auto__item-cell-important <?=(!empty($arItem['DISPLAY_PROPERTIES']['IS_NEW']['VALUE'])) ? "_new" : ""?>">
				<?
					if(!empty($arItem['DISPLAY_PROPERTIES']['IS_NEW']['VALUE']))
					{
						echo GetMessage("AD_LIST_MILEAGE_NEW_CAR");
					}
					elseif(!empty($arItem['DISPLAY_PROPERTIES']['MILEAGE']['VALUE']))
					{
						if($arItem['DISPLAY_PROPERTIES']['MILEAGE']['VALUE']!=1){echo $arItem['DISPLAY_PROPERTIES']['MILEAGE']['VALUE'];}
					}
					else
					{
						echo GetMessage("AD_LIST_MILEAGE_NULL");
					}
				?>
			</span>
		</div>
		<div class="auto__item-cell _price">
			<span class="auto__item-cell-important _price _show _rub">
				<?=$arItem["FORMATTED_PRICE_RUB"];?>
			</span>
			<?if(!empty($arItem["PRICE"]["EUR"])):?>
				<span class="auto__item-cell-important _price _eur _hidden">
					<?=$arItem["PRICE"]["EUR"]["VALUE"]." ".$arItem["PRICE"]["EUR"]["SYMBOL"]?> 
				</span>
			<?endif;?>
			<?if(!empty($arItem["PRICE"]["USD"])):?>
				<span class="auto__item-cell-important _price _usd _hidden">
					<?=$arItem["PRICE"]["USD"]["VALUE"]." ".$arItem["PRICE"]["USD"]["SYMBOL"]?> 
				</span>
			<?endif;?>
			<?if(!empty($arItem["DISPLAY_PROPERTIES"]["CITY"]["VALUE"])):?>
				<span class="auto__item-cell-city">
				<?
				/* Временное решение, по в выгрузке не исправят это свойство */
					
					$arItem["DISPLAY_PROPERTIES"]["CITY"]["VALUE"] = str_replace(', ул.Фрунзе 61/2','',$arItem["DISPLAY_PROPERTIES"]["CITY"]["VALUE"]);
					$arItem["DISPLAY_PROPERTIES"]["CITY"]["VALUE"] = str_replace(', ул. Фрунзе 61/2','',$arItem["DISPLAY_PROPERTIES"]["CITY"]["VALUE"]);
				?>
					<?=$arItem["DISPLAY_PROPERTIES"]["CITY"]["VALUE"]?>
				</span>
			<?endif;?>

		</div>
	</a>
	<?$inxImage++;?>
<?endforeach;?>
<?}?>
<div class="auto__item-wrapper">
	<?$inxImage=1000?>
	<?foreach($arResult["ITEMS"] as $arItem):?>
	<?if(in_array($arItem['ID'], $arCookie)){$style='fave-active';}else{$style='fave-inactive';}
	$_state = $arItem["DISPLAY_PROPERTIES"]["STATE"]["VALUE"];
	
	if($sCurPage =='/order/'){$_state = 'Ожидается';}elseif($sCurPage =='/archive/'){$_state = 'Продано';}
	?>
		<a href="<?= "/auto/view/".$arItem["EXTERNAL_ID"]."/" /*$arItem['DETAIL_PAGE_URL']*/ ?>" class="auto__item _mobile <?=$style?>" data-fave="<?=$arItem['ID']?>">
			<div class="auto__item-left">
				<div class="auto__item-left-wrapper">
					<div class="auto__item-left-model">
						<?=$arItem['DISPLAY_PROPERTIES']['MANUFACTURER']['VALUE']." ".$arItem['DISPLAY_PROPERTIES']['MODEL']['VALUE'];?>
					</div>
					<div class="auto__item-left-state"><?=$_state?></div>
					<?if($ordArhive){?><div class="fave__btn fave__btn-mobile onEventCompare" data-id="<?=$arItem['ID']?>"></div><?}?>
				</div>
				<div class="auto__item-left-picture">
					<?if(is_array($arItem["PREVIEW_PICTURE_MIN"])):?>
						<!-- img src="<?=$arItem["PREVIEW_PICTURE_MIN"]['SRC']?>" alt="" class="auto__item-left-picture-img" / -->
						<img id="<?=$inxImage?>" src="/local/assets/img/noimg.png" data-src="<?=$arItem["PREVIEW_PICTURE_MIN"]["SRC"]?>" alt="" class="auto__item-picture-img img-load" />
					<?endif;?>
				</div>
			</div>
			<div class="auto__item-rigth">
				<div class="auto__item-rigth-favorites">
			
				</div>
				<div class="auto__item-rigth-price">
					<?=$arItem["FORMATTED_PRICE_RUB"];?>
				</div>
				<div class="auto__item-rigth-about">
					<?if(!empty($arItem['DISPLAY_PROPERTIES']['MODEL_YEAR']['VALUE'])):?>
						<?=$arItem['DISPLAY_PROPERTIES']['MODEL_YEAR']['NAME'].": ".$arItem['DISPLAY_PROPERTIES']['MODEL_YEAR']['VALUE'];?>,<br>
					<?endif;?>
					
					<?if(!empty($arItem['DISPLAY_PROPERTIES']['ENGINE']['VALUE'])):?>
						<?=$arItem['DISPLAY_PROPERTIES']['ENGINE']['NAME'].": ".$arItem['DISPLAY_PROPERTIES']['ENGINE']['VALUE']." ".GetMessage("AD_LIST_ENGINE_VOLUME_MEASURE");?>,<br>
					<?endif;?>
					<?if(!empty($arItem["DISPLAY_PROPERTIES"]["FUEL"]["DISPLAY_VALUE"])):?>
						<?=$arItem['DISPLAY_PROPERTIES']['FUEL']['NAME'].": ".strip_tags($arItem["DISPLAY_PROPERTIES"]["FUEL"]["DISPLAY_VALUE"]);?>,<br>
					<?endif;?>
					<?if(!empty($arItem["DISPLAY_PROPERTIES"]["TRANSMISSION"]["DISPLAY_VALUE"])):?>
						<?=$arItem['DISPLAY_PROPERTIES']['TRANSMISSION']['NAME'].": ".strip_tags($arItem["DISPLAY_PROPERTIES"]["TRANSMISSION"]["DISPLAY_VALUE"]);?>,<br>
					<?endif;?>
					<?if(!empty($arItem["DISPLAY_PROPERTIES"]["DRIVE"]["DISPLAY_VALUE"])):?>
						<?=$arItem['DISPLAY_PROPERTIES']['DRIVE']['NAME'].": ".strip_tags($arItem["DISPLAY_PROPERTIES"]["DRIVE"]["DISPLAY_VALUE"])." ".GetMessage("AD_LIST_DRIVE");?>,<br>
					<?endif;?>
					<?=$arItem['DISPLAY_PROPERTIES']['MILEAGE']['VALUE']." ".GetMessage("AD_LIST_MILEAGE_MEASURE");?>
					
				</div>
				<?if(!empty($arItem["DISPLAY_PROPERTIES"]["CITY"]["VALUE"])):?>
				<?
				/* Временное решение, по в выгрузке не исправят это свойство */
					
					$arItem["DISPLAY_PROPERTIES"]["CITY"]["VALUE"] = str_replace(', ул.Фрунзе 61/2','',$arItem["DISPLAY_PROPERTIES"]["CITY"]["VALUE"]);
					$arItem["DISPLAY_PROPERTIES"]["CITY"]["VALUE"] = str_replace(', ул. Фрунзе 61/2','',$arItem["DISPLAY_PROPERTIES"]["CITY"]["VALUE"]);
				?>
					<div class="auto__item-rigth-city">
						<?=$arItem["DISPLAY_PROPERTIES"]["CITY"]["VALUE"];?>
					</div>
				<?endif;?>
			</div>
		</a>
		<?$inxImage++;?>
	<?endforeach;?>
</div>
</div>
</div>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>

<?if( $sCurPage != '/order/' && $sCurPage != '/archive/'){?>

	<?
		global $arrAdsFilter;
		global $arrAdsFilterOrder;
		$arrAdsFilterOrder = $arrAdsFilter;
		$arrAdsFilterOrder['PROPERTY_STATE_VALUE'] = 'Под заказ';
	?>

	<?$APPLICATION->IncludeComponent(
			"bitrix:news.list",
			"order",
			Array(
				"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"NEWS_COUNT" => 5,
				"SORT_BY1" => "DATE_CREATE",
				"SORT_ORDER1" => "DESC",
				"SORT_BY2" => $arParams["SORT_BY2"],
				"SORT_ORDER2" => $arParams["SORT_ORDER2"],
				"FIELD_CODE" => $arParams["FIELD_CODE"],
				"PROPERTY_CODE" => $arParams["PROPERTY_CODE"],
				"DETAIL_URL" => "/auto/view/#EXTERNAL_ID#/",
				"SECTION_URL" =>"/auto/view/",
				"IBLOCK_URL" => "/auto/view/",
				"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
				"SET_TITLE" => "N",
				"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
				"MESSAGE_404" => $arParams["MESSAGE_404"],
				"SET_STATUS_404" => $arParams["SET_STATUS_404"],
				"SHOW_404" => $arParams["SHOW_404"],
				"FILE_404" => $arParams["FILE_404"],
				"INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
				"CACHE_TYPE" => $arParams["CACHE_TYPE"],
				"CACHE_TIME" => $arParams["CACHE_TIME"],
				"CACHE_FILTER" => $arParams["CACHE_FILTER"],
				"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" =>  "Y",
				"PAGER_TITLE" => $arParams["PAGER_TITLE"],
				"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
				"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
				"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
				"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
				"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
				"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
				"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
				"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
				"DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
				"DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
				"PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
				"ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
				"USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
				"GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
				"FILTER_NAME" => "arrAdsFilterOrder",
				"HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
				"CHECK_DATES" => $arParams["CHECK_DATES"],
			),
			false				
		);?>
		
<div class="auto__list-nav">
<button class="btn-subscription onEventSubscript">Подписаться на новые поступления авто</button>
</div>					
	<?
		global $arrAdsFilter;
		global $arrAdsFilterArchive;
		$arrAdsFilterArchive = $arrAdsFilter;
		$arrAdsFilterArchive['PROPERTY_STATE_VALUE'] = 'Архив';
	?>

	<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"archive", 
	array(
		"IBLOCK_TYPE" => "-",
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"NEWS_COUNT" => 5,
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "DESC",
		"FIELD_CODE" => array(
			0 => "",
			1 => $arParams["FIELD_CODE"],
			2 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => $arParams["PROPERTY_CODE"],
			2 => "",
		),
		"DETAIL_URL" => "/auto/view/#EXTERNAL_ID#/",
		"SECTION_URL" => "/auto/view/",
		"IBLOCK_URL" => "/auto/view/",
		"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
		"SET_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"MESSAGE_404" => $arParams["MESSAGE_404"],
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"FILE_404" => $arParams["FILE_404"],
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => $arParams["PAGER_TITLE"],
		"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
		"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
		"ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
		"USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
		"GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
		"FILTER_NAME" => "arrAdsFilterArchive",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"CHECK_DATES" => "N",
		"COMPONENT_TEMPLATE" => "archive",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"STRICT_SECTION_CHECK" => "N"
	),
	false
);?>
<?}?>