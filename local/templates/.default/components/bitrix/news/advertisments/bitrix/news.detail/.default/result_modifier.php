<?//test
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(strstr($_SERVER['HTTP_HOST'],'rdm-import.local') || strstr($_SERVER['HTTP_HOST'],'marketica-dev.ru')){
	
	$domain = 'http://'.$_SERVER['HTTP_HOST'];
	
}else if(!strstr($_SERVER['HTTP_HOST'],'rdm-import.ru')){
	
	$domain = 'https://rdm-import.ru';
	
}else{
	
	$domain = '';
}

if(!empty($arResult['DISPLAY_PROPERTIES']['COLOR']['VALUE']))
{
    $colorIblockID = YcawebHelper::getIblockID('car-colors');
    $colorElementID = key($arResult['DISPLAY_PROPERTIES']['COLOR']['LINK_ELEMENT_VALUE']);

    $dbColor = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $colorIblockID,'ID' => $colorElementID), false, false, array('ID', 'IBLOCK_ID', 'NAME' ,'PROPERTY_COLOR'));
    while($arColor = $dbColor->fetch())
    {
        $arResult['COLOR'] = array(
            'NAME' => $arColor['NAME'],
            'VALUE' => $arColor['PROPERTY_COLOR_VALUE']
        );
    }
}
$arResult['FORMATTED_DATE'] = CIBlockFormatProperties::DateFormat("j F Y", MakeTimeStamp($arResult["DATE_CREATE"], CSite::GetDateFormat()));

$arResult["HAS_MANY_PHOTOS"] = false;

// склонение города для заголовка
if(!empty($arResult['DISPLAY_PROPERTIES']['CITY']['VALUE']))
{
    $arResult['MORPHER'] = YcawebHelper::morpher($arResult['DISPLAY_PROPERTIES']['CITY']['VALUE'], 'П');
}


$arResult["GALLERY"] = array();

$arResult["NO_PHOTO"] = false;

if(!empty($arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO"]))
{
	if(!empty($arParams["RESIZE_PHOTO_WIDTH"]))
	{
		$imgWidth = $arParams["RESIZE_PHOTO_WIDTH"];
	}
	else
	{
		$imgWidth = 1200;
	}
	if(!empty($arParams["RESIZE_PHOTO_HEIGHT"]))
	{
		$imgHeight = $arParams["RESIZE_PHOTO_HEIGHT"];
	}
	else
	{
		$imgHeight = 700;
	}
    
    $imgWidth = 583;
    $imgHeight = 392;

//p($arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["FILE_VALUE"]);
    
    if(isset($arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["FILE_VALUE"][0]))
    {
        if(count($arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["FILE_VALUE"]) > 10)
        {
            $arResult["HAS_MANY_PHOTOS"] = true;
        }
		foreach($arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["FILE_VALUE"] as $img)
		{
			
			$arImgTmp = CFile::ResizeImageGet(
				$img,
				array("width" => $imgWidth, "height" => $imgHeight),
				BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
				true
			);

			
            $imgList = CFile::ResizeImageGet(
				$img,
				array("width" => 113, "height" => 80),
				BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
				true
			);
		
			
			$arResult["GALLERY"][] = array( 'SRC_MIN' => $domain.$arImgTmp["src"], 'SRC_ORIG' => $domain.$img["SRC"], 'SRC_MIN_LIST' => $domain.$imgList["src"] );
		}
    }
	else
	{
		$imgNew = CFile::ResizeImageGet(
            $arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["FILE_VALUE"],
            array("width" => $imgWidth, "height" => $imgHeight),
            BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
            true
        );
        $imgList = CFile::ResizeImageGet(
            $arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["FILE_VALUE"],
            array("width" => 113, "height" => 80),
            BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
            true
        );  
        $arResult["GALLERY"][] = array( 'SRC_MIN' => $domain.$imgNew['src'], 'SRC_ORIG' => $domain.$arResult["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["FILE_VALUE"]["SRC"], 'SRC_MIN_LIST' => $domain.$imgList["src"]);
	}
	
}
else
{
	$arResult["NO_PHOTO"] = true;
	if(empty($arResult["PREVIEW_PICTURE"]["SRC"]))
	{
		$arResult["GALLERY"][] = NO_PHOTO_PATH;
	}
	else
	{
		$arResult["GALLERY"][] = $domain.$arResult["PREVIEW_PICTURE"]["SRC"];
	}
}

// $arResult['PHONES'] = array();
// if(!empty($arResult["DISPLAY_PROPERTIES"]['PHONE']))
// {
// 	foreach($arResult["DISPLAY_PROPERTIES"]['PHONE']["~VALUE"] as $phone)
// 	{
// 		$arResult['PHONES'][] = '+7 ' . YcawebHelper::parsePhone($phone);
// 	}
// }

// для блока "похожие по цене/ тех.характеристикам"
$arResult["SIMILAR"]["MAX_PRICE"] = (int)$arResult["DISPLAY_PROPERTIES"]["PRICE"]["VALUE"] + 50000;

if($arResult["DISPLAY_PROPERTIES"]["PRICE"]["VALUE"] > 50000)
{
    $arResult["SIMILAR"]["MIN_PRICE"] = (int)$arResult["DISPLAY_PROPERTIES"]["PRICE"]["VALUE"] - 50000;
}

// Диагностическая карта
if(!empty($arResult["PROPERTIES"]["DIAGNOZ"]["VALUE"]))
{
    $arResult["PROPERTIES"]["DIAGNOZ"]["MODIFY"] = CFile::GetFileArray($arResult["PROPERTIES"]["DIAGNOZ"]["VALUE"]);
}else{	
	$arResult["PROPERTIES"]["DIAGNOZ"]["MODIFY"] = "N";
}

// Автокод
if(!empty($arResult["PROPERTIES"]["AUTOCODE"]["VALUE"]))
{
    $arResult["PROPERTIES"]["AUTOCODE"]["MODIFY"] = CFile::GetFileArray($arResult["PROPERTIES"]["AUTOCODE"]["VALUE"]);
}else{
	
	$arResult["PROPERTIES"]["AUTOCODE"]["MODIFY"] = "N";
}

// Инспекция
if(!empty($arResult["PROPERTIES"]["INSPECTIA"]["VALUE"]))
{
    $arResult["PROPERTIES"]["INSPECTIA"]["MODIFY"] = CFile::GetFileArray($arResult["PROPERTIES"]["INSPECTIA"]["VALUE"]);
}else{
	
	$arResult["PROPERTIES"]["INSPECTIA"]["MODIFY"] = "N";
}

//Инспекция компании
if(!empty($arResult["PROPERTIES"]["INSPECTION"]["VALUE"]))
{
    $arResult["PROPERTIES"]["INSPECTION"]["MODIFY"] = CFile::GetFileArray($arResult["PROPERTIES"]["INSPECTION"]["VALUE"]);
}else{
	
	$arResult["PROPERTIES"]["INSPECTION"]["MODIFY"] = "N";
}
//Отчет СТО
if(!empty($arResult["PROPERTIES"]["STO"]["VALUE"]))
{
    $arResult["PROPERTIES"]["STO"]["MODIFY"] = CFile::GetFileArray($arResult["PROPERTIES"]["STO"]["VALUE"]);
}else{
	
	$arResult["PROPERTIES"]["STO"]["MODIFY"] = "N";
}
//Отчет Нотариальной палаты
if(!empty($arResult["PROPERTIES"]["NOT_PALETE"]["VALUE"]))
{
    $arResult["PROPERTIES"]["NOT_PALETE"]["MODIFY"] = CFile::GetFileArray($arResult["PROPERTIES"]["NOT_PALETE"]["VALUE"]);
}else{
	
	$arResult["PROPERTIES"]["NOT_PALETE"]["MODIFY"] = "N";
}
//Отчет ГИБДД
if(!empty($arResult["PROPERTIES"]["GIBDD"]["VALUE"]))
{
    $arResult["PROPERTIES"]["GIBDD"]["MODIFY"] = CFile::GetFileArray($arResult["PROPERTIES"]["GIBDD"]["VALUE"]);
}else{
	
	$arResult["PROPERTIES"]["GIBDD"]["MODIFY"] = "N";
}
//Отчет судебных приставов
if(!empty($arResult["PROPERTIES"]["PRISTAV"]["VALUE"]))
{
    $arResult["PROPERTIES"]["PRISTAV"]["MODIFY"] = CFile::GetFileArray($arResult["PROPERTIES"]["PRISTAV"]["VALUE"]);
}else{
	
	$arResult["PROPERTIES"]["PRISTAV"]["MODIFY"] = "N";
}
//Сертификат
if(!empty($arResult["PROPERTIES"]["SERTIFICAT"]["VALUE"]))
{
    $arResult["PROPERTIES"]["SERTIFICAT"]["MODIFY"] = CFile::GetFileArray($arResult["PROPERTIES"]["SERTIFICAT"]["VALUE"]);
}else{
	
	$arResult["PROPERTIES"]["SERTIFICAT"]["MODIFY"] = "N";
}


$filter = '';
//$filter .= "&filter[MANUFACTURER]=".$arResult["DISPLAY_PROPERTIES"]["MANUFACTURER"]["VALUE"];
//$filter .= "&filter[MODEL]=".$arResult["DISPLAY_PROPERTIES"]["MODEL"]["VALUE"];
//$filter .= "&filter[YEAR][FROM]=".($arResult["DISPLAY_PROPERTIES"]["MODEL_YEAR"]["VALUE"]-1)."&filter[YEAR][TO]=".($arResult["DISPLAY_PROPERTIES"]["MODEL_YEAR"]["VALUE"]+1);
if(!empty($arResult["DISPLAY_PROPERTIES"]['DRIVE']["VALUE"]))
{
    $filter .= "&filter[DRIVE]=".$arResult["DISPLAY_PROPERTIES"]['DRIVE']["VALUE"];
}
if(!empty($arResult["DISPLAY_PROPERTIES"]['FUEL']["VALUE"]))
{
    $filter .= "&filter[FUEL]=".$arResult["DISPLAY_PROPERTIES"]['FUEL']["VALUE"];
}
if(!empty($arResult["DISPLAY_PROPERTIES"]['TRANSMISSION']["VALUE"]))
{
    $filter .= "&filter[TRANSMISSION]=".$arResult["DISPLAY_PROPERTIES"]['TRANSMISSION']["VALUE"];
}
if(!empty($arResult["DISPLAY_PROPERTIES"]['ENGINE']["VALUE"]))
{
    $filter .= "&filter[ENGINE][FROM]=".$arResult["DISPLAY_PROPERTIES"]['ENGINE']["VALUE"]."&filter[ENGINE][TO]=".$arResult["DISPLAY_PROPERTIES"]['ENGINE']["VALUE"];
}
if(!empty($arResult["DISPLAY_PROPERTIES"]['CAR_BODY']["VALUE"]))
{
$filter .= "&filter[CAR_BODY][]=".$arResult["DISPLAY_PROPERTIES"]["CAR_BODY"]["VALUE"];
}

$filterPriceFrom = (isset($arResult["SIMILAR"]["MIN_PRICE"]) ? "&filter[PRICE][FROM]=".$arResult["SIMILAR"]["MIN_PRICE"] : "");

$arResult['SIMILAR_LINK']['CHARACTERISTICS'] = "/auto/?start_filter=filter".$filter;

$arResult['SIMILAR_LINK']['PRICE'] = "/auto/?start_filter=filter".$filterPriceFrom."&filter[PRICE][TO]=".$arResult["SIMILAR"]["MAX_PRICE"];

// Clear cookie
if(false){
	$arCookie = $APPLICATION->get_cookie('compare');
	foreach($arCookie as $k=>$arCook){
		$APPLICATION->set_cookie('compare['.$k.']', '',time()+(86400* 5));
	}
}

/* ~~~ Переменные для принта ~~~*/
	$arPrint = array();
	if(isset($arResult["GALLERY"][0])){
		$arPrint[] = $arResult["GALLERY"][0]["SRC_MIN"];
		
	}
	//$arPrint[] = $arResult["GALLERY"][0]["SRC_MIN"];
	
	$name = GetMessage("AD_DETAIL_SELLING")." ".$arResult['DISPLAY_PROPERTIES']['MANUFACTURER']['VALUE']." ".$arResult['DISPLAY_PROPERTIES']['MODEL']['VALUE'];
	if(isset($arResult['MORPHER'])){$name = $arResult['MORPHER']; }
	$arPrint[] = $name . " в Новосибирске";
	$arPrint[] = GetMessage("AD_DETAIL_ADVERTISMENT")." ".$arResult['ID']." ".GetMessage("AD_DETAIL_FROM")." ".$arResult['FORMATTED_DATE'];
	
	$arPrint[] = $arResult['DISPLAY_PROPERTIES']['MANUFACTURER']['VALUE']." ".$arResult['DISPLAY_PROPERTIES']['MODEL']['VALUE'].", ".
			     $arResult["DISPLAY_PROPERTIES"]["MODEL_YEAR"]["VALUE"]."&nbsp;".GetMessage("AD_DETAIL_YEAR");
	
	
	$arPrint[] = YcawebHelper::priceFormat($arResult["DISPLAY_PROPERTIES"]["PRICE"]["VALUE"])." руб.";
	$prop = "<div>";
if(!empty($arResult["DISPLAY_PROPERTIES"]["ENGINE"]["VALUE"])){
	$prop .= "<label>".GetMessage('T_NEWS_DETAIL_PROP_ENGINE')."</label>: ";
	if(!empty($arResult["DISPLAY_PROPERTIES"]["FUEL"]["DISPLAY_VALUE"])){
		$prop .= strtolower(strip_tags($arResult["DISPLAY_PROPERTIES"]["FUEL"]["DISPLAY_VALUE"])).", ";
	};
	$prop .= $arResult["DISPLAY_PROPERTIES"]["ENGINE"]["VALUE"]. "л.<br/>";
}

if(!empty($arResult["DISPLAY_PROPERTIES"]["POWER"]["VALUE"])){							
	$prop .= "<label>".GetMessage('T_NEWS_DETAIL_PROP_POWER')."</label>: ";
	$prop .= $arResult["DISPLAY_PROPERTIES"]["POWER"]["VALUE"]." ".GetMessage('AD_DETAIL_HORSE_POWER')."<br/>";
	//$arPrint[] = $prop;
};

if(!empty($arResult["DISPLAY_PROPERTIES"]["TRANSMISSION"]["DISPLAY_VALUE"])){
	$prop .= "<label>".GetMessage('T_NEWS_DETAIL_PROP_TRANSMISSON')."</label>: ";
	$prop .= strtolower(strip_tags($arResult["DISPLAY_PROPERTIES"]["TRANSMISSION"]["DISPLAY_VALUE"]))."<br/>";
	//$arPrint[] = $prop;
};
if(!empty($arResult["DISPLAY_PROPERTIES"]["DRIVE"]["VALUE"])){
	$prop .= "<label>".GetMessage('T_NEWS_DETAIL_PROP_DRIVE')."</label>: ";
	$prop .= strtolower(strip_tags($arResult["DISPLAY_PROPERTIES"]["DRIVE"]["DISPLAY_VALUE"]))."<br/>";
	//$arPrint[] = $prop;
};
if(!empty($arResult['COLOR']['NAME'])){
	$prop .= GetMessage('T_NEWS_DETAIL_PROP_COLOR')."</label>: ";
	$prop .= strtolower($arResult['COLOR']['NAME']);
	//$arPrint[] = $prop;
};
if(!empty($arResult["DISPLAY_PROPERTIES"]["HAS_RF_MILEAGE"]["VALUE"])){
	$prop .= "<label>".GetMessage('T_NEWS_DETAIL_PROP_HAS_RF_MILEAGE')."</label>: ";
	$prop .= GetMessage('AD_DETAIL_HAS_RF_MILEAGE')."<br/>";
	//$arPrint[] = $prop;
};

if(!empty($arResult["DISPLAY_PROPERTIES"]["MILEAGE"]["VALUE"])){
	$prop .= "<label>".GetMessage('T_NEWS_DETAIL_PROP_MILEAGE')."</label>: ";
	$prop .= $arResult["DISPLAY_PROPERTIES"]["MILEAGE"]["VALUE"]."<br/>";
	//$arPrint[] = $prop;
};

if(!empty($arResult["DISPLAY_PROPERTIES"]["STEERING_WHEEL"]["VALUE"])){
	$prop .= "<label>".GetMessage('T_NEWS_DETAIL_PROP_STEERING_WHEEL')."</label>: ";
	$prop .= strtolower($arResult["DISPLAY_PROPERTIES"]["STEERING_WHEEL"]["VALUE"])."<br/>";
	//$arPrint[] = $prop;
};
	$prop .= "</div>";
	
	$arPrint[] = $prop;
	
/* GARANT */	
	
	$garant = '<h4>ГАРАНТИРОВАНО РДМ&nbsp;-&nbsp;ИМПОРТ</h4>';
	if(!empty($arResult["PROPERTIES"]["PROBEG"]["VALUE"])){								
		$garant.= '<label>Пробег:</label> '.strtolower($arResult["PROPERTIES"]["PROBEG"]["VALUE"])."<br/>";								
	};
	
	if(!empty($arResult["PROPERTIES"]["OWNERS"]["VALUE"])){								
		$garant.= '<label>Количество владельцев:</label> '.strtolower($arResult["PROPERTIES"]["OWNERS"]["VALUE"])."<br/>";								
	};
	
	if(!empty($arResult["PROPERTIES"]["PTS"]["VALUE"])){							
		$garant.= '<label>ПТС:</label> '.strtolower($arResult["PROPERTIES"]["PTS"]["VALUE"])."<br/>";								
	};
	
	if(!empty($arResult["PROPERTIES"]["VIN"]["VALUE"])){							
		$garant.= '<label>VIN:</label> '.strtolower($arResult["PROPERTIES"]["VIN"]["VALUE"])."<br/>";							
	};
	
	$arPrint[] = $garant;
	
	
	ob_start();
	echo"<pre>";
	print_r($arResult);
	echo"</pre>";
	$content = ob_get_contents();  	
	ob_end_clean();

	$fp = fopen($_SERVER["DOCUMENT_ROOT"]."/local/logs/".'detail-'.$arResult['ID'].'.txt', 'w+');
	fwrite($fp, $content);
	fclose($fp);