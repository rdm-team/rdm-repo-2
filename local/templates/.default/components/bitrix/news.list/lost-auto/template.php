<?//if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?><div class="news-list">
	<? $j=0;?>
	
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?

	foreach($arItem['PROPERTIES'] as $row=>$value){
		$a[$i][$j]=$value['VALUE'];

		$j++;
	} 
?>

<script>var arMorePhoto = <?=json_encode($arItem["PROPERTIES"]["MORE_PHOTO"]["_SRC"])?>;</script>


<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>

<div class="offers-list__item">
	<div class="top-banner__info-title">
		<div class="b-title">
			<h2>Выданные автомобили</h2>
		</div>
	</div>
	 <!--div class="offers-list__item-info"-->
	<div class="offers-list__item-desc">
		<div class="offers-list__item-text">
			<p>
				 Мы считаем, что сказанное всегда должно находить подтверждение в сделанном. Любую компанию, как и отдельного человека, можно судить только по количеству качественно предоставленных услуг или проданной продукции. Мы гордимся и радуемся каждым выданным автомобилем. И у нас их очень много! Приходите к нам за машиной: хорошим автомобилям требуется заботливый хозяин.
			</p>
			<p>
			</p>
		</div>
		<div class="offers-list__item-info">
			<div class="bx-wrapper" style="max-width: 100%;">
				<a class="showPhoto" href="javascript:;"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"></a>
			<p>
				<a class="all_photo_vidacha showPhoto" href="javascript:;">Смотреть все фото с выдачи</a>
			</p>

		</div>
	</div>
	<div class="offers-list__item-info pdlft">
		<div class="offers-list__item-text">
		<span class="car_mark">
			<? echo $a[$i][0].' '.$a[$i][1]; ?></span><a class="analog-auto" href="/buy/?start_filter=filter&filter[MANUFACTURER]=<? //echo $a[$i][0]; ?>&filter[MODEL]=<? echo $a[$i][1]; ?>">Найти такую же машину в нашем каталоге</a>
			
			<p class="date_auto">
				 Выдана <? echo $a[$i][4]; ?>
			</p>
			<div class="manager_list">
				<div class="manager_name">
					<? echo $a[$i][2]; ?>
				</div>
				<div class="class_manager">
					 <? echo $a[$i][3]; ?>				</div>
			</div>
			<div class="helper_list">
				<div class="helper_class">
					 Сотрудник RDM, который помогал
				</div>
				<div class="class_manager">
					 <a href="#" style="color:#4790c6;"><? echo $a[$i][5]; ?></a>				

				</div>
				<a href="/vidannye-auto/" class="all_cars" style="color:#4790c6; font-size:17px">Список выданных автомобилей</a>
				</div>
				
		</div>
		<div class="offers-list__item-desc">
			<div class="offers-list__item-text">
			</div>
		</div>
	</div>
</div>
</div>
<?
	$j=0;
	break;
	$i++;
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<!--p class="news-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		
				<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img
						class="preview_picture"
						border="0"
						src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
						width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
						height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
						alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
						title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
						style="float:left"
						/></a-->
			<?else:?>

			<?endif;?>
		<?endif?>
		<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
	<!--span class="news-date-time"><?//echo $arItem["DISPLAY_ACTIVE_FROM"]?></span-->
		<?endif?>
		<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
			<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
				<!--a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><b><?echo $arItem["NAME"]?></b></a><br /-->
			<?else:?>
				<!--b><?echo $arItem["NAME"]?></b><br /-->
			<?endif;?>
		<?endif;?>
		<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
	<?//echo $arItem["PREVIEW_TEXT"];?>
		<?endif;?>
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			<!--div style="clear:both"></div-->
		<?endif?>
		<?foreach($arItem["FIELDS"] as $code=>$value):?>
			<!--small>
			<?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?>
</small><br /-->
		<?endforeach;?>
		<?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
			<!--small>
			<?=$arProperty["NAME"]?>:&nbsp;
			<?if(is_array($arProperty["DISPLAY_VALUE"])):?>
				<?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
			<?else:?>
				<?=$arProperty["DISPLAY_VALUE"];?>
			<?endif?>
			</small><br /-->
		<?endforeach;?>
<?endforeach; ?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>
	<pre><?//print_r($a);?></pre>
</div><? //$cnt=$arItem["IBLOCK_ID"];?>

<script>
$(document).ready(function() {

    $(".showPhoto").click(function() {
		$.fancybox(arMorePhoto			
		, {
			'padding'			: 0,
			'transitionIn'		: 'none',
			'transitionOut'		: 'none',
			'type'              : 'image',
			'changeFade'        : 0,
            helpers:{overlay: {
                        locked: false
                    }}
		}
        
        );
	});
	
	});

</script>