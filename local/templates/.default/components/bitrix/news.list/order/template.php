<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(count($arResult['ITEMS'])>0){?>
<div class="page__main-wrapper">
	<div class="pagination">
		<h2>Скоро ожидается</h2>
	</div>
</div>
<div class="page__main-wrapper _auto">
	<div class="auto">
	<div class="auto__header">
			<div class="auto__header-item _date <?=($_REQUEST['sort'] == "DATE_CREATE" ? "_active" : "")?>">
				<span class="auto__header-item-text _<?=strtolower($sortMethod)?>">
					<a href="<?echo $APPLICATION->GetCurPageParam("method=".$sortMethodTmp."&sort=DATE_CREATE", array(
						"method",
						"sort"));?>" class="auto__header-item-link">
						дата публикации
					</a>
				</span>
			</div>
			<div class="auto__header-item _picture ">
			</div>
			<div class="auto__header-item _model <?=($_REQUEST['sort'] == "MANUFACTURER" ? "_active" : "")?>">
				<span class="auto__header-item-text _<?=strtolower($sortMethod)?>">
					<a href="<?echo $APPLICATION->GetCurPageParam("method=".$sortMethodTmp."&sort=MANUFACTURER", array(
						"method",
						"sort"));?>" class="auto__header-item-link">
						модель
					</a>
				</span>
			</div>
			<div class="auto__header-item _year <?=($_REQUEST['sort'] == "MODEL_YEAR" ? "_active" : "")?>">
				<span class="auto__header-item-text _<?=strtolower($sortMethod)?>">
					<a href="<?echo $APPLICATION->GetCurPageParam("method=".$sortMethodTmp."&sort=MODEL_YEAR", array(
						"method",
						"sort"));?>" class="auto__header-item-link">
						год
					</a>
				</span>
			</div>
			<div class="auto__header-item _engine <?=($_REQUEST['sort'] == "ENGINE" ? "_active" : "")?>">
				<span class="auto__header-item-text _<?=strtolower($sortMethod)?>">
					<a href="<?echo $APPLICATION->GetCurPageParam("method=".$sortMethodTmp."&sort=ENGINE", array(
						"method",
						"sort"));?>" class="auto__header-item-link">
						двигатель
					</a>
				</span>
			</div>
			<div class="auto__header-item _mileage <?=($_REQUEST['sort'] == "MILEAGE" ? "_active" : "")?>">
				<span class="auto__header-item-text _<?=strtolower($sortMethod)?>">
					<a href="<?echo $APPLICATION->GetCurPageParam("method=".$sortMethodTmp."&sort=MILEAGE", array(
						"method",
						"sort"));?>" class="auto__header-item-link">
						пробег
						<span class="auto__header-item-text-km">, тыс. км</span>
					</a>
				</span>
			</div>
			<div class="auto__header-item _price <?=($_REQUEST['sort'] == "price" ? "_active" : "")?>">
				<span class="auto__header-item-text<?=strtolower($sortMethod)?>">
					<a style="margin-right:26px;" href="<?echo $APPLICATION->GetCurPageParam("method=".$sortMethodTmp."&sort=price", array(
						"method",
						"sort"));?>" class="auto__header-item-link">
					цена</a></span>
				<span class="auto__header-item-text _hidden"></span>
				<select class="auto__header-item-select _js-select _currency">
					<?foreach($arCurrencies as $arCurrency):?>
						<option class="auto__header-item-select" value="<?=$arCurrency['CODE']?>" <?=($arCurrency['CODE'] == "RUB" ? "selected" : "")?>><?=$arCurrency['CODE']?></option>
					<?endforeach;?>
				</select>
			</div>
		</div>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="auto__item _main" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<div class="auto__item-cell _date" data-date="view">
			<?=FormatDate("d/m", MakeTimeStamp($arItem["DATE_CREATE"]))?>
		</div>
		<div class="auto__item-cell _picture">
			<?if(!empty($arItem["PREVIEW_PICTURE_MIN"]["SRC"])):?>
				<img src="<?=$arItem["PREVIEW_PICTURE_MIN"]["SRC"]?>" alt="" class="auto__item-picture-img" />
			<?endif?>
		</div>
		<div class="auto__item-cell _model">
			<span class="auto__item-cell-important">
				<?=$arItem['DISPLAY_PROPERTIES']['MANUFACTURER']['VALUE']." ".$arItem['DISPLAY_PROPERTIES']['MODEL']['VALUE'];?>
			</span>
			<span class="auto__item-cell-state auto__item-cell-red">Ожидается</span>
		</div>
		<div class="auto__item-cell _year">
			<span class="auto__item-cell-important">
				<?=$arItem['DISPLAY_PROPERTIES']['MODEL_YEAR']['VALUE'];?>
			</span>
		</div>
		<div class="auto__item-cell _engine">
			<span class="auto__item-cell-important">
				<?=$arItem['DISPLAY_PROPERTIES']['ENGINE']['VALUE']." ".GetMessage("AD_LIST_ENGINE_VOLUME_MEASURE");?>
			</span>
			<span class="auto__item-cell-about">
				<?=strip_tags($arItem["DISPLAY_PROPERTIES"]["FUEL"]["DISPLAY_VALUE"]);?>
			</span>		
			<span class="auto__item-cell-about">
				<?=strip_tags($arItem["DISPLAY_PROPERTIES"]["TRANSMISSION"]["DISPLAY_VALUE"]);?>
			</span>
			<span class="auto__item-cell-about">
				<?=strip_tags($arItem["DISPLAY_PROPERTIES"]["DRIVE"]["DISPLAY_VALUE"]);?>
			</span>
		</div>
		<div class="auto__item-cell _mileage">
			<span class="auto__item-cell-important <?=(!empty($arItem['DISPLAY_PROPERTIES']['IS_NEW']['VALUE'])) ? "_new" : ""?>">
				<?
					if(!empty($arItem['DISPLAY_PROPERTIES']['IS_NEW']['VALUE']))
					{
						echo GetMessage("AD_LIST_MILEAGE_NEW_CAR");
					}
					elseif(!empty($arItem['DISPLAY_PROPERTIES']['MILEAGE']['VALUE']))
					{
						//echo $arItem['DISPLAY_PROPERTIES']['MILEAGE']['VALUE'];
						if($arItem['DISPLAY_PROPERTIES']['MILEAGE']['VALUE']!=1){echo $arItem['DISPLAY_PROPERTIES']['MILEAGE']['VALUE'];}
					}
					else
					{
						echo GetMessage("AD_LIST_MILEAGE_NULL");
					}
				?>
			</span>
		</div>
		<div class="auto__item-cell _price">
			<span class="auto__item-cell-important _price _show _rub">
				<?=$arItem["FORMATTED_PRICE_RUB"];?>
			</span>
			<?if(!empty($arItem["PRICE"]["EUR"])):?>
				<span class="auto__item-cell-important _price _eur _hidden">
					<?=$arItem["PRICE"]["EUR"]["VALUE"]." ".$arItem["PRICE"]["EUR"]["SYMBOL"]?> 
				</span>
			<?endif;?>
			<?if(!empty($arItem["PRICE"]["USD"])):?>
				<span class="auto__item-cell-important _price _usd _hidden">
					<?=$arItem["PRICE"]["USD"]["VALUE"]." ".$arItem["PRICE"]["USD"]["SYMBOL"]?> 
				</span>
			<?endif;?>
			<?if(!empty($arItem["DISPLAY_PROPERTIES"]["CITY"]["VALUE"])):?>
				<span class="auto__item-cell-city">
				<?
				/* Временное решение, по в выгрузке не исправят это свойство */
					
					$arItem["DISPLAY_PROPERTIES"]["CITY"]["VALUE"] = str_replace(', ул.Фрунзе 61/2','',$arItem["DISPLAY_PROPERTIES"]["CITY"]["VALUE"]);
					$arItem["DISPLAY_PROPERTIES"]["CITY"]["VALUE"] = str_replace(', ул. Фрунзе 61/2','',$arItem["DISPLAY_PROPERTIES"]["CITY"]["VALUE"]);
				?>
					<?=$arItem["DISPLAY_PROPERTIES"]["CITY"]["VALUE"]?>
				</span>
			<?endif;?>
		</div>
	</a>
<?endforeach;?>

<div class="auto__item-wrapper">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="auto__item _mobile">
			<div class="auto__item-left">
				<div class="auto__item-left-wrapper">
					<div class="auto__item-left-model">
						<?=$arItem['DISPLAY_PROPERTIES']['MANUFACTURER']['VALUE']." ".$arItem['DISPLAY_PROPERTIES']['MODEL']['VALUE'];?>
					</div>
					<div class="auto__item-left-state auto__item-cell-red">Ожидается</div>					
				</div>
				<div class="auto__item-left-picture">
					<?if(is_array($arItem["PREVIEW_PICTURE_MIN"])):?>
						<img src="<?=$arItem["PREVIEW_PICTURE_MIN"]['SRC']?>" alt="" class="auto__item-left-picture-img" />
					<?endif;?>
				</div>
			</div>
			<div class="auto__item-rigth">
				<div class="auto__item-rigth-favorites">
			
				</div>
				<div class="auto__item-rigth-price">
					<?=$arItem["FORMATTED_PRICE_RUB"];?>
				</div>	
				<div class="auto__item-rigth-about">
				
					<?if(!empty($arItem['DISPLAY_PROPERTIES']['MODEL_YEAR']['VALUE'])):?>
						<?=$arItem['DISPLAY_PROPERTIES']['MODEL_YEAR']['NAME'].": ".$arItem['DISPLAY_PROPERTIES']['MODEL_YEAR']['VALUE'];?>,<br>
					<?endif;?>
					
					<?if(!empty($arItem['DISPLAY_PROPERTIES']['ENGINE']['VALUE'])):?>
						<?=$arItem['DISPLAY_PROPERTIES']['ENGINE']['NAME'].": ".$arItem['DISPLAY_PROPERTIES']['ENGINE']['VALUE']." ".GetMessage("AD_LIST_ENGINE_VOLUME_MEASURE");?>,<br>
					<?endif;?>
					<?if(!empty($arItem["DISPLAY_PROPERTIES"]["FUEL"]["DISPLAY_VALUE"])):?>
						<?=$arItem['DISPLAY_PROPERTIES']['FUEL']['NAME'].": ".strip_tags($arItem["DISPLAY_PROPERTIES"]["FUEL"]["DISPLAY_VALUE"]);?>,<br>
					<?endif;?>
					<?if(!empty($arItem["DISPLAY_PROPERTIES"]["TRANSMISSION"]["DISPLAY_VALUE"])):?>
						<?=$arItem['DISPLAY_PROPERTIES']['TRANSMISSION']['NAME'].": ".strip_tags($arItem["DISPLAY_PROPERTIES"]["TRANSMISSION"]["DISPLAY_VALUE"]);?>,<br>
					<?endif;?>
					<?if(!empty($arItem["DISPLAY_PROPERTIES"]["DRIVE"]["DISPLAY_VALUE"])):?>
						<?=$arItem['DISPLAY_PROPERTIES']['DRIVE']['NAME'].": ".strip_tags($arItem["DISPLAY_PROPERTIES"]["DRIVE"]["DISPLAY_VALUE"])." ".GetMessage("AD_LIST_DRIVE");?>,<br>
					<?endif;?>
					<?="Пробег: ".$arItem['DISPLAY_PROPERTIES']['MILEAGE']['VALUE']." ".GetMessage("AD_LIST_MILEAGE_MEASURE");?>	
				</div>
				<?if(!empty($arItem["PROPERTIES"]["CITY"]["VALUE"])):?>
					<div class="auto__item-rigth-city">
				<?
				/* Временное решение, по в выгрузке не исправят это свойство */
					
					$arItem["PROPERTIES"]["CITY"]["VALUE"] = str_replace(', ул.Фрунзе 61/2','',$arItem["PROPERTIES"]["CITY"]["VALUE"]);
					$arItem["PROPERTIES"]["CITY"]["VALUE"] = str_replace(', ул. Фрунзе 61/2','',$arItem["PROPERTIES"]["CITY"]["VALUE"]);
				?>
						<?=$arItem["PROPERTIES"]["CITY"]["VALUE"];?>
					</div>
				<?endif;?>
			</div>
		</a>
	<?endforeach;?>
</div>
</div>
</div>

<div class="page__main-wrapper">
	<div class="pagination">
		<a class="auto__item-moreLink" href="<?=str_replace( $APPLICATION->GetCurPage(false),"/order/",$APPLICATION->GetCurPageParam('',array('PAGEN_1'),false));?>">Смотреть все ожидаемые автомобили</a>
	</div>
</div>
<?}?>