<?
//==============================================================================
// Super leads
//==============================================================================
if(isset($arParams['LEAD_CODE'])){
	include $_SERVER['DOCUMENT_ROOT'].'/local/configpage.php';
	$strUrl = '<a href="http://'.$_SERVER['HTTP_HOST'].'">РДМ-ИМПОРТ</a>';
	
	$arSelect = Array(
	"ID", 
	"NAME",
	"CODE", 
	"DATE_ACTIVE_FROM", 
	"DETAIL_PAGE_URL", 
	"PREVIEW_TEXT", 
	"PREVIEW_PICTURE", 
	"PROPERTY_MEDIA_SHOW",
	"PROPERTY_MEDIA_YOUTUBE",
	"PROPERTY_MEDIA_NAME",
	"PROPERTY_MEDIA_BLOCK",
	"PROPERTY_MEDIA_URL",
	);
	$arFilter = Array("IBLOCK_ID"=>24, "CODE"=>$arParams['LEAD_CODE'], "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	while($ob = $res->GetNext())
	{	
		//$item = $ob->GetFields();
		//$this->arResult["lead"][] = $ob;
		
		$arMedia = false;
		$strText = false;
		$blockStyle='two';
		//$mediaStyle='media-half';
		if($ob['PROPERTY_MEDIA_SHOW_ENUM_ID']==114){
			//Показать видео
			
			if(!empty($ob['PROPERTY_MEDIA_YOUTUBE_VALUE'])){
				
				$arMedia = array(
				'value'=>$ob['PROPERTY_MEDIA_YOUTUBE_VALUE'],
				'type'=>'video',
				'thumb'=>'https://img.youtube.com/vi/'.$ob['PROPERTY_MEDIA_YOUTUBE_VALUE'].'/0.jpg'
				);
				
			}
			
		}elseif($ob['PROPERTY_MEDIA_SHOW_ENUM_ID']==115){
			//Показать фото
			if(!empty($ob['PREVIEW_PICTURE'])){
				$arMedia = array(
				'value'=>CFile::GetFileArray($ob['PREVIEW_PICTURE']),
				'type'=>'photo'
				);	
			}
		}else{
			//Ничего не показывать
			
		$blockStyle='one';
	
		}
		
		if(empty($ob['~PREVIEW_TEXT'])){
		$textStyle='';
		$mediaStyle='';	
		}else{
			
			$strText = $ob['~PREVIEW_TEXT'];
		}
		
		$arResult["leads"][$ob['CODE']] = array(
		'TITLE'=>!empty($ob['~PROPERTY_MEDIA_NAME_VALUE']['TEXT'])?$ob['~PROPERTY_MEDIA_NAME_VALUE']['TEXT']:false,
		'TEXTBLOCK'=>!empty($ob['PROPERTY_MEDIA_BLOCK_VALUE'])?$ob['PROPERTY_MEDIA_BLOCK_VALUE']:false,				
		'NAME'=>$ob['NAME'],
		'CODE'=>$ob['CODE'],		
		'SHOW'=>$ob['PROPERTY_MEDIA_SHOW_ENUM_ID'],			
		'MEDIA' => $arMedia,
		'TEXT' => $strText,
		'STYLE' => $blockStyle,
		);
		
		
		$strUrl = $ob['PROPERTY_MEDIA_URL_VALUE'];			
		$strName = $ob['NAME'];
		//$strUrl .= '<a href="http://'.$_SERVER['HTTP_HOST'].$ob['PROPERTY_MEDIA_URL_VALUE'].'">'.$ob['NAME'].'</a>';
		
		$arResult['json'][$ob['CODE']] = array('page'=>$strUrl,'name'=>$strName);
		$arResult['linkpage'] = $strUrl;
		$arResult['linkname'] = $strName;
	}
}

?>