<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?><?//elements list?>
<!--a name="top"></a-->
<?//foreach ($arResult['ITEMS'] as $key=>$val):?>
	<!--li class="point-faq"><a href="#<?=$val["ID"]?>"><?=$val['NAME']?></a><br/></li-->
<?//endforeach;?>
<!--div class="primer"-->
<?foreach ($arResult['ITEMS'] as $key=>$val):?>
<?
	$this->AddEditAction($val['ID'],$val['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($val['ID'],$val['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BSFE_ELEMENT_DELETE_CONFIRM')));
?>
<a name="<?=$val["ID"]?>"></a>
<!--table cellpadding="0" cellspacing="0" class="data-table" width="100%"  id="<?=$this->GetEditAreaId($val['ID']);?>"-->
		<div class="title-vopros">
		<?=$val['NAME']?> 
		</div>
<div class="accordeon_slide" style="padding:0px 0 20px">
		<?=$val['PREVIEW_TEXT']?>
		<?=$val['DETAIL_TEXT']?>
		</div>


<!--/table-->
<!--/div--><?endforeach;?>