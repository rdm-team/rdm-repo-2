$(function(){
    $('body').on('change', '._js-mark-select', function(e){

        var modelsSelect = '._js-model-select';

        if($(this).val() != '')
        {
            var manufacturerID = $(this).val();
            var ajaxUrl =  '/local/ajax/getModels.php';
            var params = { 'manufacturerID' : manufacturerID };

            $.post(ajaxUrl, params, function(data)
            {
                var result = JSON.parse(data);
				//console.log(result);
				//console.log('yauznatceny2');
				//yaCounter4175221.reachGoal('yauznatceny2');
                if(result.ERRORS)
                {
                    console.log(result.ERRORS);
                }
                else if(result.MODELS)
                {
                    $(modelsSelect).attr('disabled', false).find('option').remove();

                    if(result.MODELS.length)
                    {
                        $("<option>--</option>").appendTo(modelsSelect).val('');

                        for (var i=0; i< result.MODELS.length; i++)
                        {
                            $("<option class='auto-form__item-select-var'>" + result.MODELS[i].label + "</option>").appendTo(modelsSelect).val(result.MODELS[i].value);
                        }
                        $(modelsSelect).selectOrDie("update");
                        $('._model .sod_select').removeClass('disabled').find('.sod_list').css({'max-height': '165px'});
                    }

                }
            })
        }
        else
        {
            $(modelsSelect).find('option').remove();
            $("<option>--</option>").appendTo(modelsSelect).val('');
            $(modelsSelect).attr('disabled', true)
            $(modelsSelect).selectOrDie("update");
        }
    });
});
