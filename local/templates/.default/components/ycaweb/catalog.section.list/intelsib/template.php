<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
global $APPLICATION;
$this->setFrameMode(true);
?>
<div class="category_list" id="category-list">
    <ul class="column">
        <?foreach ($arResult['MODIFY'] as $key => $item) {?>
			<li data-count="<?=$item['COUNT']?>">
				<a href="<?= str_replace('auto1/', 'auto/', $item['URL']) ?>"><?= $item['CODE'] ?></a>
			</li> 
		<?} ?>
    </ul>
</div>