<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arViewModeList = array('LIST', 'LINE', 'TEXT', 'TILE');

$arDefaultParams = array(
    'VIEW_MODE' => 'LIST',
    'SHOW_PARENT_NAME' => 'Y',
    'HIDE_SECTION_NAME' => 'N'
);

$arParams = array_merge($arDefaultParams, $arParams);

if (!in_array($arParams['VIEW_MODE'], $arViewModeList))
    $arParams['VIEW_MODE'] = 'LIST';
if ('N' != $arParams['SHOW_PARENT_NAME'])
    $arParams['SHOW_PARENT_NAME'] = 'Y';
if ('Y' != $arParams['HIDE_SECTION_NAME'])
    $arParams['HIDE_SECTION_NAME'] = 'N';

$arResult['VIEW_MODE_LIST'] = $arViewModeList;

if (0 < $arResult['SECTIONS_COUNT']) {
    if ('LIST' != $arParams['VIEW_MODE']) {
        $boolClear = false;
        $arNewSections = array();
        foreach ($arResult['SECTIONS'] as &$arOneSection) {
            if (1 < $arOneSection['RELATIVE_DEPTH_LEVEL']) {
                $boolClear = true;
                continue;
            }
            $arNewSections[] = $arOneSection;
        }
        unset($arOneSection);
        if ($boolClear) {
            $arResult['SECTIONS'] = $arNewSections;
            $arResult['SECTIONS_COUNT'] = count($arNewSections);
        }
        unset($arNewSections);
    }
}

if (0 < $arResult['SECTIONS_COUNT']) {
    $boolPicture = false;
    $boolDescr = false;
    $arSelect = array('ID');
    $arMap = array();
    if ('LINE' == $arParams['VIEW_MODE'] || 'TILE' == $arParams['VIEW_MODE']) {
        reset($arResult['SECTIONS']);
        $arCurrent = current($arResult['SECTIONS']);
        if (!isset($arCurrent['PICTURE'])) {
            $boolPicture = true;
            $arSelect[] = 'PICTURE';
        }
        if ('LINE' == $arParams['VIEW_MODE'] && !array_key_exists('DESCRIPTION', $arCurrent)) {
            $boolDescr = true;
            $arSelect[] = 'DESCRIPTION';
            $arSelect[] = 'DESCRIPTION_TYPE';
        }
    }
    if ($boolPicture || $boolDescr) {
        foreach ($arResult['SECTIONS'] as $key => $arSection) {
            $arMap[$arSection['ID']] = $key;
        }
        $rsSections = CIBlockSection::GetList(array(), array('ID' => array_keys($arMap)), false, $arSelect);
        while ($arSection = $rsSections->GetNext()) {
            if (!isset($arMap[$arSection['ID']]))
                continue;
            $key = $arMap[$arSection['ID']];
            if ($boolPicture) {
                $arSection['PICTURE'] = intval($arSection['PICTURE']);
                $arSection['PICTURE'] = (0 < $arSection['PICTURE'] ? CFile::GetFileArray($arSection['PICTURE']) : false);
                $arResult['SECTIONS'][$key]['PICTURE'] = $arSection['PICTURE'];
                $arResult['SECTIONS'][$key]['~PICTURE'] = $arSection['~PICTURE'];
            }
            if ($boolDescr) {
                $arResult['SECTIONS'][$key]['DESCRIPTION'] = $arSection['DESCRIPTION'];
                $arResult['SECTIONS'][$key]['~DESCRIPTION'] = $arSection['~DESCRIPTION'];
                $arResult['SECTIONS'][$key]['DESCRIPTION_TYPE'] = $arSection['DESCRIPTION_TYPE'];
                $arResult['SECTIONS'][$key]['~DESCRIPTION_TYPE'] = $arSection['~DESCRIPTION_TYPE'];
            }
        }
    }
}
if (is_array($arResult["SECTION"])) {

    $arSelect = Array("PROPERTY_PRICE_VALUE");
    $arFilter = Array(
        "IBLOCK_ID" => $arParams['IBLOCK_ID'],
        "ACTIVE" => "Y",
        'PROPERTY_STATE' => '110',
        'SECTION_CODE' => $arResult["SECTION"]["CODE"],
        'INCLUDE_SUBSECTIONS' => 'Y');
    $res = CIBlockElement::GetList(Array('PROPERTY_PRICE' => 'ASC'), $arFilter, false, Array("nPageSize" => 50), $arSelect);
    $ob = $res->GetNextElement();
    if ($ob) {
        $arFields = $ob->GetFields();
        $minPrice = $arFields['PROPERTY_PRICE_VALUE'];
        $ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues(
            $arResult["SECTION"]["IBLOCK_ID"],
            $arResult["SECTION"]["ID"]
        );
        $iProps = $ipropValues->getValues();

        $iProps['SECTION_META_TITLE'] = str_replace("#MIN_PRICE#", $minPrice, $iProps['SECTION_META_TITLE']);
        $arParams['LEVEL'] != "YEAR" ? $yearRepl = '' : $yearRepl = ' года';
        $iProps['SECTION_META_TITLE'] = str_replace(" #YEAR#", $yearRepl, $iProps['SECTION_META_TITLE']);
        $iProps['SECTION_META_DESCRIPTION'] = str_replace(" #YEAR#", $yearRepl, $iProps['SECTION_META_DESCRIPTION']);
        $iProps['SECTION_PAGE_TITLE'] = str_replace(" #YEAR#", $yearRepl, $iProps['SECTION_PAGE_TITLE']);

        $arResult["SECTION"]["IPROPERTY_VALUES"] = $iProps;

    }
}
//Тут генерим тайтл для марок и моделей автомобилей
$uri = explode('/',trim(parse_url($_SERVER['REQUEST_URI'])['path'], '/')); 
switch (count($uri)) {
    case '2':
        $mark = ucwords(urldecode(str_replace('_', ' ', $uri[1])));
        $arResult['TITLE'] = str_replace('MARK',$mark, explode('$',$arResult["SECTION"]["IPROPERTY_VALUES"]['SECTION_META_TITLE'])[0]);
        $arResult['DESCRIPTION'] = str_replace('MARK',$mark, explode('$',$arResult["SECTION"]["IPROPERTY_VALUES"]['SECTION_META_DESCRIPTION'])[0]);
        $main__title = str_replace('MARK','<span class="main__title-important">'.$mark.'</span>', explode('$',$arResult["SECTION"]["IPROPERTY_VALUES"]['SECTION_PAGE_TITLE'])[0]);
        break;
    case '3':
        $mark = ucwords(urldecode(str_replace('_', ' ', $uri[1])));
        $model = ucwords(urldecode(str_replace('_', ' ', $uri[2])));
        $arResult['TITLE'] = str_replace('MARK',$mark, str_replace('MODEL',$model,explode('$',$arResult["SECTION"]["IPROPERTY_VALUES"]['SECTION_META_TITLE'])[1]));
        $arResult['DESCRIPTION'] = str_replace('MARK',$mark, str_replace('MODEL',$model,explode('$',$arResult["SECTION"]["IPROPERTY_VALUES"]['SECTION_META_DESCRIPTION'])[1]));
        $main__title = str_replace('MARK','<span class="main__title-important">'.$mark, str_replace('MODEL',$model.'</span>',explode('$',$arResult["SECTION"]["IPROPERTY_VALUES"]['SECTION_PAGE_TITLE'])[1]));
        break;
    default:
        //$arResult['TITLE'] = "Объявления"; 
        $arResult['TITLE'] = "Купить автомобиль|Крупнейший в Сибири автосалон подержанных автомобилей в наличии"; 
        break;
}

// Что бы заголовки и описание стабильно работали, подготавливаем инфу и передаем в component_epilog.php
$cp = $this->__component; // объект компонента
if (is_object($cp))
{

    $cp->arResult['MY_TITLE'] = $arResult['TITLE'];
    $cp->arResult['MY_DESCRIPTION'] = $arResult['DESCRIPTION'];
    $cp->SetResultCacheKeys(array('MY_TITLE'));
    $arResult['MY_TITLE'] = $cp->arResult['MY_TITLE'];
    $arResult['MY_DESCRIPTION'] = $cp->arResult['MY_DESCRIPTION'];
}
 

?>