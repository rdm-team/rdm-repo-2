<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
$sCurPage = $APPLICATION->GetCurPage(false);
?>
<!DOCTYPE html>
<html lang="<?=LANGUAGE_ID?>" class="no-js fix-<?=detectBrowser()?>">
	<head>

		<?\Ycaweb\Tools::getTpl('head');?>
		<? if (!$USER->IsAuthorized()){ ?>
			<style>
				._disabled{display:none};
			</style>
		<?}?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
 
 ga('create', 'UA-110238090-1', 'auto');
  ga('send', 'pageview');
</script>
<script type="text/javascript">
(function (w, d, nv, ls) {
var lwait = function (w, on, trf, dly, ma, orf, osf) {var pfx = "ct_await_", sfx = "_completed";if (!w[pfx + on + sfx]) {var ci = clearInterval, si = setInterval, st = setTimeout, cmld = function() {if (!w[pfx + on + sfx]) { w[pfx + on + sfx] = true;if ((w[pfx + on] && (w[pfx + on].timer))) { ci(w[pfx + on].timer);w[pfx + on] = null;}orf(w[on]);}};if (!w[on] || !osf) {if (trf(w[on])){cmld();} else {if (!w[pfx + on]) {w[pfx + on] = {timer: si(function () {if (trf(w[on]) || ma < ++w[pfx + on].attempt) {cmld();}}, dly), attempt: 0};}}} else {if (trf(w[on])) {cmld();} else {osf(cmld);st(function(){lwait(w, on, trf, dly, ma, orf);}, 0);}}} else {orf(w[on]);}};
var ct = function (w, d, e, c, n) {var a = 'all', b = 'tou', src = b + 'c' + 'h';src = 'm' + 'o' + 'd.c' + a + src; var jsHost = "https://" + src, p = d.getElementsByTagName(e)[0], s = d.createElement(e); var jsf = function (w, d, p, s, h, c, n) {s.async = 1;s.src = jsHost + "." + "r" + "u/d_client.js?param;" + (c ? "client_id" + c + ";" : "") + "ref" + escape(d.referrer) + ";url" + escape(d.URL) + ";cook" + escape(d.cookie) + ";attrs" + escape("{\"attrh\":" + n + ",\"ver\":170615}") + ";"; p.parentNode.insertBefore(s, p);};if (!w.jQuery) {var jq = d.createElement(e);jq.src = jsHost + "." + "r" + 'u/js/jquery-1.7.min.js';jq.onload = function () {lwait(w, 'jQuery', function (obj) {return (obj ? true : false);}, 30, 100, function () {jsf(w, d, d.getElementsByTagName(e)[0], s, jsHost, c, n);});};p.parentNode.insertBefore(jq, p);} else {jsf(w, d, p, s, jsHost, c, n);}};
var gaid = function (w, d, o, ct, n) {if (!!o) {lwait(w, o, function (obj) {return (obj && obj.getAll ? true : false);}, 200, (nv.userAgent.match(/Opera|OPR\//) ? 10 : 20), function (gaCounter) {var clId = null;try {var cnt = gaCounter && gaCounter.getAll ? gaCounter.getAll() : null;clId = cnt && cnt.length > 0 && !!cnt[0] && cnt[0].get ? cnt[0].get('clientId') : null;} catch (e) {console.warn("Unable to get clientId, Error: " + e.message);}ct(w, d, 'script', clId, n);}, function (f) {w[o](function () {f(w[o]);})});} else {ct(w, d, 'script', null, n);}};
var cid = function () {try {var m1 = d.cookie.match('(?:^|;)\\s*_ga=([^;]*)');if (!(m1 && m1.length > 1)) return null;var m2 = decodeURIComponent(m1[1]).match(/(\d+\.\d+)$/);if (!(m2 && m2.length > 1)) return null;return m2[1]} catch (err) {}}();
if (cid === null && !!w.GoogleAnalyticsObject) {
   if (w.GoogleAnalyticsObject == 'ga_ckpr') w.ct_ga = 'ga'; else w.ct_ga = w.GoogleAnalyticsObject;
   if (typeof Promise !== "undefined" && Promise.toString().indexOf("[native code]") !== -1) { new Promise(function (resolve) {var db, on = function () {resolve(true)}, off = function () {resolve(false)}, tryls = function tryls() {try {ls && ls.length ? off() : (ls.x = 1, ls.removeItem("x"), off());} catch (e) {nv.cookieEnabled ? on() : off();}}; w.webkitRequestFileSystem ? webkitRequestFileSystem(0, 0, off, on) : "MozAppearance" in d.documentElement.style ? (db = indexedDB.open("test"), db.onerror = on, db.onsuccess = off) : /constructor/i.test(w.HTMLElement) ? tryls() : !w.indexedDB && (w.PointerEvent || w.MSPointerEvent) ? on() : off();}).then(function (pm) {
       if (pm) {gaid(w, d, w.ct_ga, ct, 2);} else {gaid(w, d, w.ct_ga, ct, 3);}})} else {gaid(w, d, w.ct_ga, ct, 4);}
} else {ct(w, d, 'script', cid, 1);}})
(window, document, navigator, localStorage);


</script>
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = 'https://vk.com/rtrg?p=VK-RTRG-211158-dukt4';</script>

	</head>
<?\Ycaweb\Tools::getTpl('iebody');?>
	<body class="page">
	<script>
		if( document.getElementById('map') ){
			ymaps.ready(function () {
				var myMap = new ymaps.Map('map', {
						center: [55.040636, 82.952120],
						zoom: 14.8
					}, {
						searchControlProvider: 'yandex#search'
					}),

				// Создаём макет содержимого.
				MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
					'<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
				),
				myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
					hintContent: 'РДМ-ИМПОРТ',
					balloonContent: 'РДМ-ИМПОРТ'
				}, {
					// Опции.
					// Необходимо указать данный тип макета.
					iconLayout: 'default#image',

					// Размеры метки.
					iconImageSize: [30, 42],
					// Смещение левого верхнего угла иконки относительно
					// её "ножки" (точки привязки).
					iconImageOffset: [-5, -38]
				});
				myMap.behaviors.disable('scrollZoom');
				if( myPlacemark.length > 0 ){;
					myMap.geoObjects.add(myPlacemark)
				}
				if( typeof myPlacemarkWithContent != "undefined" && myPlacemarkWithContent.length > 0 ){;
					myMap.geoObjects.add(myPlacemarkWithContent)
				}
			});
		}
	</script>
	<style>
		#map{height:340px;}
	</style>
<?if( $sCurPage != '/kompany/history/'){?>	
<section class="page__wrapper" date-page="<?=$sCurPage?>">
<?}?>

<?if( $sCurPage == '/kompany/history/'){?>
	<? \Ycaweb\Tools::getTpl('header-history');?>
<?}else{?>
	<? \Ycaweb\Tools::getTpl('header');?>
<?}?>
	
<?if( $sCurPage == '/' ){?>
	<?\Ycaweb\Tools::showH1Title()?>
	<div class="page__main-wrapper">
		<div class="main">
			<div class="main__wrapper">
				<div class="main__title">
					<?$APPLICATION->IncludeFile(SITE_DIR."include/cars_title.php", Array(), Array("MODE" => "html", "NAME"  => GetMessage("COPYRIGHT"),));?>
				</div>
<?}?>
				
<?if( $sCurPage == '/creditovanie/' 
		|| $sCurPage == '/creditovanie/autocreditovanie/' 
		|| $sCurPage == '/kompany/' 
		|| $sCurPage == '/contacts/' 
		&& $sCurPage != '/kompany/history/'
){?>
	<div class="page__main-wrapper page__wrapper-full <?$APPLICATION->ShowProperty("wrapper_class");?>">
		<div class="offers-list-wrapper">
			<div class="offers-list">
			<script>
				$(document).ready(function(){
		
					$("#menu a, .anim").hover( function() {
						$(this).animate({"paddingLeft" : "10px"}, 300);
					},
					function() {
						$(this).animate({"paddingLeft" : "0"}, 300);
					});
				});
			</script>
			<div class="top-banner__info-title"><?// \Ycaweb\Tools::showH1Title()?></div>
<?}else{?>

<?}?>