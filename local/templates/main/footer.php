<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

				<div class="menu-footer-wrap links-ff">
					<div class="page__main-wrapper _links">
						<div class="menu-footer">
							<?// списки - для прижатия имеющегося меню вправо?>
							<div class="menu-footer__list">
							<?$APPLICATION->IncludeComponent(
								"bitrix:menu",
								"footer",
								array(
									"ALLOW_MULTI_SELECT" => "N",
									"CHILD_MENU_TYPE" => "left",
									"COMPONENT_TEMPLATE" => "footer",
									"DELAY" => "N",
									"MAX_LEVEL" => "1",
									"MENU_CACHE_GET_VARS" => array(
									),
									"MENU_CACHE_TIME" => "3600",
									"MENU_CACHE_TYPE" => "A",
									"MENU_CACHE_USE_GROUPS" => "Y",
									"ROOT_MENU_TYPE" => "sell",
									"USE_EXT" => "N"
								),
								false
							);?>
							</div>
							<div class="menu-footer__list">

							<?$APPLICATION->IncludeComponent(
								"bitrix:menu",
								"footer",
								array(
									"ALLOW_MULTI_SELECT" => "N",
									"CHILD_MENU_TYPE" => "left",
									"COMPONENT_TEMPLATE" => "footer",
									"DELAY" => "N",
									"MAX_LEVEL" => "1",
									"MENU_CACHE_GET_VARS" => array(
									),
									"MENU_CACHE_TIME" => "3600",
									"MENU_CACHE_TYPE" => "A",
									"MENU_CACHE_USE_GROUPS" => "Y",
									"ROOT_MENU_TYPE" => "services",
									"USE_EXT" => "N"
								),
								false
							);?>
							</div>
							<div class="menu-footer__list">
							<?$APPLICATION->IncludeComponent(
								"bitrix:menu",
								"footer",
								array(
									"ALLOW_MULTI_SELECT" => "N",
									"CHILD_MENU_TYPE" => "left",
									"COMPONENT_TEMPLATE" => "footer",
									"DELAY" => "N",
									"MAX_LEVEL" => "1",
									"MENU_CACHE_GET_VARS" => array(
									),
									"MENU_CACHE_TIME" => "3600",
									"MENU_CACHE_TYPE" => "A",
									"MENU_CACHE_USE_GROUPS" => "Y",
									"ROOT_MENU_TYPE" => "company",
									"USE_EXT" => "N"
								),
								false
							);?></div>
							<div class="menu-footer__list _contacts">
								<div class="links__list-title">
									Контакты
								</div>
								<ul class="links__list">
								<li class="links__list-item">
									<span class="links__list-item-text _phone">
										<?
										$APPLICATION->IncludeFile(
											SITE_DIR."include/footer_phone.php",
											Array(),
											Array("MODE"=>"html")
										);
										?>
									</span>
								</li>
								<li class="links__list-item">
									<span class="links__list-item-text">
										<?
										$APPLICATION->IncludeFile(
											SITE_DIR."include/footer_address.php",
											Array(),
											Array("MODE"=>"html")
										);
										?>
									</span>
								</li>
							</ul>
							</div>
							<div class="menu-footer__list">
							<?$APPLICATION->IncludeComponent(
								"bitrix:menu", 
								"footer", 
								array(
									"ALLOW_MULTI_SELECT" => "N",
									"CHILD_MENU_TYPE" => "left",
									"COMPONENT_TEMPLATE" => "footer",
									"DELAY" => "N",
									"MAX_LEVEL" => "1",
									"MENU_CACHE_GET_VARS" => array(
									),
									"MENU_CACHE_TIME" => "3600",
									"MENU_CACHE_TYPE" => "A",
									"MENU_CACHE_USE_GROUPS" => "Y",
									"ROOT_MENU_TYPE" => "links",
									"USE_EXT" => "N",
									"MENU_PREFIX" => "_internet"
								),
								false
							);?>
							</div>
							<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"social-links", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "content",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "10",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "URL",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"COMPONENT_TEMPLATE" => "social-links",
		"STRICT_SECTION_CHECK" => "N"
	),
	false
);?>
							</ul>
						</div>
					</div>
				</div>
<?if( $sCurPage == '/' ){?>
			</div>
		</div>
	</div>
<?}?>
<?if( $sCurPage == '/creditovanie/' || $sCurPage == '/kompany/' || $sCurPage == '/contacts/' && $sCurPage != '/kompany/history/'){?>
			</div>
		</div>
	</div>
<?}else{?>

<?}?>

	
<?if( $sCurPage == '/kompany/history/'){?>
	<?\Ycaweb\Tools::getTpl('footer-history')?>
<?}else{?>
	<?\Ycaweb\Tools::getTpl('footer')?>
<?}?>
	<div class="page__buffer"></div>
<?if( $sCurPage != '/kompany/history/'){?>	
</section>
<?}?>
<div class="modalbox" id="modalbox">
<div class="onModalCancel" id="onModalCancel"></div>
<div class="modalhtml" id="modalhtml"></div>
</div>
<div class="modallayer" id="modallayer"></div>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter4175221 = new Ya.Metrika2({
                    id:4175221,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/4175221" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<!-- Google.Analytics -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-42232324-1']);
  _gaq.push(['_addOrganic', 'images.yandex.ru', 'text', true]);
  _gaq.push(['_addOrganic', 'go.mail.ru', 'q']);
  _gaq.push(['_addOrganic', 'nova.rambler.ru', 'query']);
  _gaq.push(['_addOrganic', 'nigma.ru', 's']);
  _gaq.push(['_addOrganic', 'webalta.ru', 'q']);
  _gaq.push(['_addOrganic', 'aport.ru', 'r']);
  _gaq.push(['_addOrganic', 'poisk.ru', 'text']);
  _gaq.push(['_addOrganic', 'km.ru', 'sq']);
  _gaq.push(['_addOrganic', 'liveinternet.ru', 'q']);
  _gaq.push(['_addOrganic', 'quintura.ru', 'request']);
  _gaq.push(['_addOrganic', 'search.qip.ru', 'query']);
  _gaq.push(['_addOrganic', 'gde.ru', 'keywords']);
  _gaq.push(['_addOrganic', 'ru.yahoo.com', 'p']);
  _gaq.push(['_trackPageview']);
  setTimeout('_gaq.push([\'_trackEvent\', \'NoBounce\', \'Over 15 seconds\'])',15000);

  (function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
  function onlyNumbers(event){
	  return ((event.charCode >= 48 && event.charCode <= 57 )||event.keyCode == 8 ||event.keyCode == 9 || event.keyCode == 46||event.keyCode == 37|| event.keyCode == 39 );
  }
</script>
<!-- /Google.Analytics -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110238090-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-110238090-1');
</script>
<!-- /Google.Analytics -->

<!-- roistat -->
<script>
(function(w, d, s, h, id) {
    w.roistatProjectId = id; w.roistatHost = h;
    var p = d.location.protocol == "https:" ? "https://" : "http://";
    var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
    var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
})(window, document, 'script', 'cloud.roistat.com', '8bcf2f2f8aa6b2dfeba0c253d5e12496');
</script>
</body>
</html>
