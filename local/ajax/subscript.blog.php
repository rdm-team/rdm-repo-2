<?
if(!empty($_REQUEST['fields'])){
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");	
    //CModule::IncludeModule('subscribe');
    CModule::IncludeModule("form");			
	$get = htmlspecialchars($_REQUEST['get'], ENT_QUOTES);	
	$temp = $_REQUEST['fields'];
	
	$arUser = array();	
	$arAnswer = array();
	$msg = "";
	/* ------------------------------ */
	// Фильтруем полученные данные
	foreach($temp as $k=>$v){
		
		if(strstr($v["name"],'your_')){
			
			$arUser[htmlspecialchars($v["name"], ENT_QUOTES)] = htmlspecialchars($v["value"], ENT_QUOTES);
			
		}else{
			
			$arData[htmlspecialchars($v["name"], ENT_QUOTES)] = htmlspecialchars($v["value"], ENT_QUOTES);
		}
	
	}
	
	/* ------------------------------ */
	// Определяем форму, для почтовых уведомлений
	$form_id_md5 = $arData['data'];
	$rsForms = CForm::GetList($by="s_id", $order="desc", array(), $is_filtered);
	$arT = array();
	
	while ($arForm = $rsForms->Fetch())
	{	
		if($arData['data'] == md5('id_'.$arForm['ID'])){
			break;
		}

	} 

	
	if($arForm){
				
		$rsFieldList = CFormField::GetList($arForm["ID"], "ALL", $by="s_sort", $order="asc", array(), $is_filtered);
	   
	    $arFormFields = array();
	    $arError = array();
		while ($arFieldList = $rsFieldList->GetNext())
		{
			
			$rsAnswer = CFormAnswer::GetByID($arFieldList["ID"]);
			$arAnswer[$arFieldList["ID"]] = $rsAnswer->Fetch();
			$arFieldList["ANSWER"] = $arAnswer;
			
			$arFormFields[] = $arFieldList;
			
			if($arFieldList["REQUIRED"]=="Y"){				
				
				if(empty($arUser[$arFieldList["SID"]])){					
					$arError[] = $arFieldList["SID"];
				}
				
			}
		}

		// Проверяем массив с ошибками
		if(count($arError)==0){
						
	/* ------------------------------ */
	// Получаем список существующих результатов для сверки эл. почты на уникальность
		    $rsResults = CFormResult::GetList($arForm["ID"], 
			($by="s_timestamp"),($order="desc"), 
			array(),$is_filtered, 
			"Y");
			
			$arWebForm = array();
			$boolEmail = true;
			while ($arRes = $rsResults->Fetch())
			{
				$temp  = CFormResult::GetDataByID( $arRes["ID"], array("your_email"));			
				if($temp["your_email"][0]["USER_TEXT"] == $arUser["your_email"]){					
					$boolEmail = false;
					break;
				}
			}

	/* ------------------------------ */
	// Проверка почтового ящика на уникальность
			if($boolEmail){
					/* ------------------------------ */
				$arWebFormListField = array();
				foreach($arFormFields as $arVal){
					$arElem = $arAnswer[$arVal["ID"]];
					$arWebFormListField['form_'.$arElem['FIELD_TYPE'].'_'.$arVal['ID']] =$arUser[$arVal["SID"]];		
				}
					
					
				if ($RESULT_ID = CFormResult::Add($arForm["ID"], $arWebFormListField))
				{
				   $send = "Y";
				   $arUser["unscribe"] = 'https://'.$_SERVER['HTTP_HOST'].'/unsublog/'.md5('u'.$RESULT_ID.$arUser["your_email"]);
				   $arUser["ID"] = $RESULT_ID;
				   //CEvent::Send("SUBSCRIBE_BLOG", 's1', $arUser,"N");
				   CEvent::Send("SUBSCRIBE_BLOG", 's1', $arUser,"N",57);
				   CEvent::Send("SUBSCRIBE_BLOG", 's1', $arUser,"N",58);
				}else{
				   $send = "N";
				} 
				
				$html = '<div class="message"><p>Вы подписаны на рассылку новых новостей</p></div>';				
				$arResult = array("msg"=>"good",'texthtml'=>$html,"arUser"=>$arUser, "status"=>true);


			}else{				
				$arResult = array("status"=>false,"msg"=>"errorEmail",'texthtml'=>'<div class="message"><p>Такая эл. почта уже используется</p></div>');				
			}
			
		}else{ // Если поля не заполнены					
			$arResult = array("status"=>false,"msg"=>"errorForm","error"=>$arError);			
		}
	
	}else{ // Если нет вебформы
		
		$arResult = array("status"=>false,"arForm"=>$arForm,"arT"=>$arT, "msg"=>"nowebform");
	}
	/* ------------------------------ */
	
	
}else{ // Если не пришли нужные данные
	
	$arResult = array("status"=>false,"msg"=>"nodata");
}
echo json_encode($arResult);
?>