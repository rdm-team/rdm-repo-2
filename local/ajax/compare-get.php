<?  session_start();

	if(!empty($_REQUEST["id"]))
	{
		require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
		
		$ID = (int)$_REQUEST["id"];
		$isCompare = 'compare-active';
		$isCompareText = '';
		
		if(isset($_COOKIE["BITRIX_SM_compare"])){

			if(in_array($ID,$_COOKIE["BITRIX_SM_compare"])){
				$arResult = array(
				"id"=>$ID,		
				"status"=>true,		
				"isStatus"=>true,
				"isCompare"=>'compare-active',
				"isCompareText"=>'',
				"COOKIE"=>$_COOKIE["BITRIX_SM_compare"],
				"html"=>'<a class="detail__compare compare-active" data-id="'.$ID.'" href="/auto/favorite/"></a>'
				);	
			}else{
				$arResult = array(
				"id"=>$ID,	
				"status"=>true,			
				"isStatus"=>false,
				"isCompare"=>'compare-inactive',
				"isCompareText"=>'',
				"COOKIE"=>$_COOKIE["BITRIX_SM_compare"],
				"html"=>'<button class="detail__compare onEventCompare compare-inactive" data-id="'.$ID.'"></button>'
				);

				
			}
			
		}else{
			$arResult = array(
			"id"=>$ID,	
			"status"=>true,			
			"isStatus"=>false,
			"isCompare"=>'compare-inactive',
			"isCompareText"=>'',
			"COOKIE"=>$_COOKIE["BITRIX_SM_compare"],
			"html"=>'<button class="detail__compare onEventCompare compare-inactive" data-id="'.$ID.'"></button>'
			);	
		}
			
	}else{
		
		$arResult = array("status"=>false);
	}

	echo json_encode($arResult);

?>