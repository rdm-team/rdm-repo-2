<?  session_start();

	if(!empty($_REQUEST["id"]))
	{
		require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
		
		$ID = (int)$_REQUEST["id"];
		$isCompare = 'compare-active';
		$isCompareText = '';
		
		if(!isset($_COOKIE["BITRIX_SM_compare"])){
			$APPLICATION->set_cookie('compare[0]', $ID,time()+(86400* 5)); 				

				$arResult = array(
				"id"=>$ID,		
				"status"=>true,
				"style"=>"fave-active",	
				"COOKIE"=>$_COOKIE["BITRIX_SM_compare"],
				"html"=>'<a class="detail__compare compare-active" data-id="'.$ID.'" href="/auto/favorite/"></a>',
				"link"=>'<a href="/auto/favorite/" class="favorite__btn favorite-active">Перейти к сравнению</a>'			
				);
		}else{
			
			if (in_array($ID, $_COOKIE["BITRIX_SM_compare"])) {
				$arCookie = $APPLICATION->get_cookie('compare');
				foreach($arCookie as $k=>$arCook){
					if($arCook == $ID){
					$APPLICATION->set_cookie('compare['.$k.']', '',time()+(86400* 5));
					}
				}
				//unset($_COOKIE["BITRIX_SM_compare"][$ID]);
				$arResult = array("status"=>true,'style'=>'fave-inactive',"msg"=>"Элемент уже в сравнении");
				
			}else{			
			
			$ind = count($_COOKIE["BITRIX_SM_compare"]);
			$ind++;
			$APPLICATION->set_cookie('compare['.$ind.']', $ID,time()+(86400* 5));
	
				$arResult = array(
				"id"=>$ID,	
				"status"=>true,			
				"COOKIE"=>$ind,
				"html"=>'<a class="detail__compare compare-active" data-id="'.$ID.'" href="/auto/favorite/"></a>',
				'style'=>'fave-active'
				);
			}
			
		}

	}else{
		
		$arResult = array("status"=>false);
	}

	echo json_encode($arResult);
?>