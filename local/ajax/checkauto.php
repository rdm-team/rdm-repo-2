<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");	
	$arParams = array();

	if($_REQUEST["param"]) {
        foreach ($_REQUEST["param"] as $k => $v) {
            $arParams[$v['name']] = htmlspecialchars($v['value'], ENT_QUOTES);
        }
		
		if(!empty($arParams['autocheck__win'])){
			
			$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_VIN","PROPERTY_GARANTIYA_VALIDITY","PROPERTY_GARANTIYA_CERTIFICATE",);
			$arFilter = Array("IBLOCK_ID"=>5, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y","PROPERTY_VIN"=>$arParams['autocheck__win']);
			$arVin = array();
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			if($ob = $res->GetNextElement())
			{
				$arFields = $ob->GetFields();		
				
				$bValid = false;
				$bGarant = false;
				
				
				// Проверка срока действия гарантии
				if(!empty($arFields["PROPERTY_GARANTIYA_VALIDITY_VALUE"])){
					
					$time = date("Y-m-d H:i:s");
					$nowTime = strtotime($time);
					$newTime = strtotime($arFields["PROPERTY_GARANTIYA_VALIDITY_VALUE"]);
					
					if($nowTime < $newTime){						
						$bValid = true;
						$validity = "<div class='autocheck__item'>Автомобиль имеет железную гарантию <span class='font-bold'>«РДМ-Импорт»</span>, действующую до ".date('d.m.Y',$newTime)."</div>";
					}else{
						$validity = "<span class='checkauto__text'>Автомобиль не имеет железную гарантию <span class='font-bold'>«РДМ-Импорт»</span></span>";
					}
					
				}else{
					
					$validity = "<span class='checkauto__text'>Автомобиль не имеет железную гарантию <span class='font-bold'>«РДМ-Импорт»</span></span>";
					
				}
				
				// Проверка наличия сертификата
				if(!empty($arFields["PROPERTY_GARANTIYA_CERTIFICATE_VALUE"])){
					
					if($arFields["PROPERTY_GARANTIYA_CERTIFICATE_VALUE"]=='Y'){
						$bGarant = true;
						$garant = "<div class='autocheck__item'>Автомобиль имеет сертификат проверки юридической чистоты, действующий бессрочно.</div>";
					}else{
						$garant = "<span class='checkauto__text'>Автомобиль не имеет сертификата проверки юридической чистоты<br><br>Если вы покупали автомобиль и имеете гарантию, но не обнаружили его в базе, напишите, пожалуйста, об этом нам <a href='mailto:admin@rdm-import.ru'>admin@rdm-import.ru</a> - система находится в тестовом режиме.</span>";
					}
					
				}else{
					
					$garant = "<span class='checkauto__text'>Автомобиль не имеет сертификата проверки юридической чистоты.</span>";
					
				}
				
				
				if(!$bValid && !$bGarant){
					$adv = "<span class='checkauto__text'>Если вы покупали автомобиль и имеете гарантию, но не обнаружили его в базе, напишите, пожалуйста, об этом нам <a href='mailto:admin@rdm-import.ru'>admin@rdm-import.ru</a> - система находится в тестовом режиме.</span>";
				}else{
					$adv = '';
				}
				//
				
				//
				
				//$result  = "<div class='autocheck__item'>Автомобиль имеет железную гарантию <span class='font-bold'>«РДМ&nbsp;-&nbsp;Импорт»</span></div>";
				//$result .= "<div class='autocheck__item'>Автомобиль имеет сертификат проверки</div>";
				
				$result =$validity;
				$result .= $garant;
				$result .= $adv;
		
				
				$arResult = array(
				"status"=>true,
				"result"=>$result,
				"ob"=>$ob,
				"arFields"=>$arFields,
				//"time"=>$time,
				//"nowTime"=>$nowTime,
				//"newTime"=>$newTime,
				//"validity"=>$validity
				);
			}else{
				/*
				$result = "<span class='checkauto__text'>Ваш автомобиль не проходил проверку юридической чистоты и не имеет программы гарантии от компании РДМ-Импорт.</span>";
				$result .= "<span class='checkauto__text'>Если вы покупали автомобиль и имеете гарантию, но не обнаружили его в базе, напишите пожалуйста об этом нам, 
				система находиться в тестовом режиме <a class='autocheck__alink' href='mailto:admin@rdm-import.ru'>admin@rdm-import.ru</a></span>";
				*/
				/*
				$result = "Автомобиль не имеет железную гарантию <span class='font-bold'>«РДМ - Импорт»</span>";
				$result .= "Автомобиль не имеет сертификата проверки юридической чистоты";
				$result .= "<span class='checkauto__text'>Если вы покупали автомобиль и имеете гарантию, но не обнаружили его в базе, напишите пожалуйста об этом нам, 
				система находиться в тестовом режиме <a class='autocheck__alink' href='mailto:admin@rdm-import.ru'>admin@rdm-import.ru</a></span>";				
				*/
				$arResult = array("status"=>true,"result"=>$result);
			}

		
		}else{
			
			$arResult = array("status"=>false,"result"=>"<span class='checkauto__text'>Введите VIN</span>");
		}
		
		
		
		
		
    }else{
		
		$arResult = array("status"=>false,"result"=>"<span class='checkauto__text'>VIN или номер кузова автомобиля</span>");
		
	}

	echo json_encode($arResult);
?>