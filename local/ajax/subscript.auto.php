<?

if(!empty($_REQUEST['get']) && !empty($_REQUEST['fields'])){
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");	
    CModule::IncludeModule('subscribe');
    CModule::IncludeModule("form");			
	$get = htmlspecialchars($_REQUEST['get'], ENT_QUOTES);	
	$temp = $_REQUEST['fields'];
	
	$arUser = array();	
	$arAnswer = array();
	$arKuzov = array();
	$arKuzovID = "";
	$msg = "";
	/* ------------------------------ */
	// Фильтруем полученные данные
	foreach($temp as $k=>$v){
		
		if(strstr($v["name"],'kuzov_')){
			
			$arKuzov[] = htmlspecialchars($v["value"], ENT_QUOTES);
		
		}elseif(strstr($v["name"],'your_')){
			
			$arUser[htmlspecialchars($v["name"], ENT_QUOTES)] = htmlspecialchars($v["value"], ENT_QUOTES);
			
		}else{
			
			$arData[htmlspecialchars($v["name"], ENT_QUOTES)] = htmlspecialchars($v["value"], ENT_QUOTES);
		}
	
	}
	
	$arKuzovID = implode(',',$arKuzov);
	/* ------------------------------ */
	// Определяем форму, для почтовых уведомлений
	$form_id_md5 = $arData['data'];
	$rsForms = CForm::GetList($by="s_id", $order="desc", array(), $is_filtered);
	$arT = array();
	
	while ($arForm = $rsForms->Fetch())
	{	
		if($arData['data'] == md5('id_'.$arForm['ID'])){

			break;
		}

	} 

	if($arForm){
		
		
		$rsFieldList = CFormField::GetList($arForm["ID"], "ALL", $by="s_sort", $order="asc", array(), $is_filtered);
	   
	    $arFormFields = array();
	    $arError = array();
		while ($arFieldList = $rsFieldList->GetNext())
		{
			
			$rsAnswer = CFormAnswer::GetByID($arFieldList["ID"]);
			$arAnswer[$arFieldList["ID"]] = $rsAnswer->Fetch();
			$arFieldList["ANSWER"] = $arAnswer;
			
			$arFormFields[] = $arFieldList;
			
			if($arFieldList["REQUIRED"]=="Y"){
				
				if(empty($arUser[$arFieldList["SID"]])){
					
					$arError[] = $arFieldList["SID"];
				}
				
			}
		}
	/* ------------------------------ */
	if(!preg_match("/^([A-Za-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})$/", $arUser["your_email"])){
		
		$arError[] = 'your_email';
	}
	/* ------------------------------ */

		$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
		$arFilter = Array("IBLOCK_ID"=>6,"ID"=>$arKuzov, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
		$res = CIBlockElement::GetList(Array(), $arFilter, false,true, $arSelect);
		//$this->arResult["kuzov"] = array();
		$arKuzovFull = array();
		$arKuzovHtml = "";
		while($ob = $res->GetNextElement())
		{
			$arFields = $ob->GetFields();
			if (in_array($arFields["ID"], $arKuzov)){
			$arKuzovFull[] = $arFields["NAME"];
			$arKuzovHtml.="<li>Обновления кузова ".$arFields["NAME"]."</li>";
			}
		}
		$arKuzovFull = implode(',',$arKuzovFull);
	/* ------------------------------ */
	
	
	
	
		// Проверяем массив с ошибками
		if(count($arError)==0){
			
	/* ------------------------------ */
	// Получаем список существующих результатов для сверки эл. почты на уникальность
		    $rsResults = CFormResult::GetList($arForm["ID"], 
			($by="s_timestamp"),($order="desc"), 
			array(),$is_filtered, 
			"Y");
			
			$arWebForm = array();
			$boolEmail = true;
			while ($arRes = $rsResults->Fetch())
			{
				$temp  = CFormResult::GetDataByID( $arRes["ID"], array("your_email"));
				
				if($temp["your_email"][0]["USER_TEXT"] == $arUser["your_email"]){					
					$boolEmail = false;
					break;
				}
			}
			
			/* ------------------------------ */
					ob_start();	
					?>
					<div class="message">
					<strong>Форма подписки обновлена</strong>
					<?if(!empty($arKuzovFull)){?>
					<ul>
					<?=$arKuzovHtml?>
					</ul>
					<?}else{?>
						<p>У вас нет выбранных кузовов.</p>						
					<?}?>
					</div>
					<?
					$htmlUpdate = ob_get_contents();  	
					ob_end_clean();
				
				/* ------------------------------ */
				$arWebFormListField = array();
				foreach($arFormFields as $arVal){
					$arElem = $arAnswer[$arVal["ID"]];
					$arWebFormListField['form_'.$arElem['FIELD_TYPE'].'_'.$arVal['ID']] =$arUser[$arVal["SID"]];
					
					if($arVal["SID"] == "your_kuzov"){
					$arWebFormListField['form_'.$arElem['FIELD_TYPE'].'_'.$arVal['ID']] = $arKuzovFull;	
						
					}
					
					if($arVal["SID"] == "your_kuzov_type"){
					$arWebFormListField['form_'.$arElem['FIELD_TYPE'].'_'.$arVal['ID']] = $arKuzovID;	
						
					}
					
					
				}
				
	
			// Проверка почтового ящика на уникальность
			if($boolEmail)
			{
				if(empty($arKuzovFull)){
					
					$arResult = array("status"=>false,"msg"=>"errorEmail",'texthtml'=>$htmlUpdate);
					
				}else{
						if ($RESULT_ID = CFormResult::Add($arForm["ID"], $arWebFormListField))
						{
							$send = "Y";
							$arUser["unscribe"] = 'https://'.$_SERVER['HTTP_HOST'].'/unsubauto/'.md5('u'.$RESULT_ID.$arUser["your_email"]);
							$arUser["ID"] = $RESULT_ID;			   
							$arUser["html_list"] = $arKuzovHtml;			   
							CEvent::Send("SUBSCRIBE_AUTO", 's1', $arUser,"N",60);
							CEvent::Send("SUBSCRIBE_AUTO", 's1', $arUser,"N",61);
						   
						}else{
						   $send = "N";
						} 	
					
						$html = '<div class="message"><p>Вы подписаны на рассылку новых поступлений автомобилей</p></div>';
						$arResult = array("msg"=>"good",'texthtml'=>$html,"arUser"=>$arUser, "status"=>true);
						
				}
			}else{
			
				if (CFormResult::Update($arRes["ID"], $arWebFormListField))
				{
					$send = "Y";
					$arUser["unscribe"] = 'https://'.$_SERVER['HTTP_HOST'].'/unsubauto/'.md5('u'.$RESULT_ID.$arUser["your_email"]);
					$arUser["ID"] = $arRes["ID"];			   
					$arUser["html_list"] = $arKuzovHtml;			   
					CEvent::Send("SUBSCRIBE_AUTO", 's1', $arUser,"N",60);
					CEvent::Send("SUBSCRIBE_AUTO", 's1', $arUser,"N",61);
				   
				}else{
				   $send = "N";
				}
				

				
				if(empty($arKuzovFull)){
					
					$msg = "errorEmail";
					$status = false;
				}else{
					$msg = "";
					$status = true;
				}
				
			$arResult = array("status"=>$status,"msg"=>$msg,'texthtml'=>$htmlUpdate);			
		}
			
		}else{ // Если поля не заполнены
			
			
			$arResult = array("status"=>false,"msg"=>"errorForm","error"=>$arError);
			
			
		}
	
	
	
	}else{ // Если нет вебформы
		
		$arResult = array("status"=>false,"arForm"=>$arForm,"arT"=>$arT, "msg"=>"nowebform");
	}
	/* ------------------------------ */

	
}else{ // Если не пришли нужные данные
	
	$arResult = array("status"=>false,"msg"=>"nodata");
}


echo json_encode($arResult);
?>