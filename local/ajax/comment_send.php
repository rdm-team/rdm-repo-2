<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

//echo '<pre>_REQUEST';print_r($_REQUEST);echo '</pre>';

if ($_REQUEST['text'] && /*$_REQUEST['phone'] &&*/ $_REQUEST['id_el'] && $_REQUEST['name'] && !$_REQUEST['name2']) {
    if (Bitrix\Main\Loader::includeModule('iblock')) {
    
        $el = new CIBlockElement;
			
        $PROP = array();
        //$PROP['EMAIL'] = strip_tags(trim($_REQUEST['phone'])); 
        $PROP['NAME'] = $_REQUEST['name']; 		
        $PROP['ID_ELEMENT'] = $_REQUEST['id_el']; 		
        $PROP['AUTHOR_IP'] = $_SERVER['REMOTE_ADDR']; 		
        		
		$res = CIBlockElement::GetByID($_REQUEST['id_el']);
        if($arEl = $res->GetNext())
		{
			$PROP['NAME_TOVAR'] = $arEl['NAME']; 
			$PROP['DETAIL_URL'] =  'https://'.SITE_SERVER_NAME.$arEl['DETAIL_PAGE_URL']; 
		}	
        
        $arSect = CIBlockSection::GetList(array(), array('CODE'=>'kommentarii', 'IBLOCK_ID'=>REVIEWS_IBLOCK_ID), false, array('IBLOCK_ID', 'ID') )->fetch();
        // файлы
        if(isset($_FILES['file']))
        {
            $cntFile = count($_FILES['file']['name']);
            for($i=0; $i < $cntFile; $i++){
                if($_FILES['file']['error'][$i]== 0){
                    $arFile = array(
                        'name' => $_FILES['file']['name'][$i],
                        'size' => $_FILES['file']['size'][$i],
                        'tmp_name' => $_FILES['file']['tmp_name'][$i],
                        'type' => $_FILES['file']['type'][$i]
                    );
                    $fileToSave = array_merge($arFile , array('del' => null, 'MODULE_ID' => 'main'));
                    $filesId[] = CFile::SaveFile($fileToSave, 'tmp_file');
                }
            }
            $PROP['PHOTO'] = $filesId;
        }

        // $text = trim($_REQUEST['text']);
        // if(strpos($text, '>>') !== false){
            // $text = preg_replace('/>>(.*?)<</i', '<div class="comment_citate_text">----$1---</div>',  $text);
        // }
        // //$text = strip_tags($text);
         
        $arLoadReviewArray = Array(
            "IBLOCK_SECTION_ID" => $arSect['ID'],   
            "IBLOCK_ID"      => REVIEWS_IBLOCK_ID,
            "PROPERTY_VALUES"=> $PROP,
            //"NAME"           => 'Комментарий от '.strip_tags(trim($_REQUEST['name'])),
            "NAME"           => 'Комментарий от '.strip_tags(trim($_REQUEST['name'])),
            "ACTIVE"         => "N",         
            "BLOG"         => "Y",         
            "PREVIEW_TEXT"   => trim($_REQUEST['text']),
        );
//p($arLoadReviewArray, 'arLoadReviewArray', 1);
        if($el->Add($arLoadReviewArray))
            echo json_encode(Array('STATUS' => 'OK'));
        else
            echo json_encode(Array('STATUS' => 'ERROR','MESSAGE' => 'Не удалось добавить комментарий, попробуйте ещё раз. '."Error: ".$el->LAST_ERROR));
    }
}
elseif($_REQUEST['name2']){
    echo json_encode(Array('STATUS' => 'BOAT'));
}
else{
    $error_text = '';
    if( !$_REQUEST['text'])
        $arr_error[] = '"Ваш комментарий"';
    if( !$_REQUEST['name'])
        $arr_error[] = '"Имя"';
    //if( !$_REQUEST['phone'])
       // $arr_error[] = '"Контактные данные (телефон или email)"'; 
    $cnt_ar = count($arr_error);
    for($i=0; $i < $cnt_ar; $i++){
        if($i>0) $error_text .= ', ';
        $error_text .= $arr_error[$i];
    }
    if($error_text != '')
        echo json_encode(Array('STATUS' => 'ERROR', 'MESSAGE' => 'Необходимо заполнить следующие поля: '.$error_text));
}