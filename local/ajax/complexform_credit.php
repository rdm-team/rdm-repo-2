<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
    CModule::IncludeModule("form");
	$arUser = array();
	$arParams = array();

	$msg = "";

	/* ------------------------------ */
if(count($_REQUEST)>0)
{
	$url = $_SERVER['HTTP_HOST'];
	// Ключи для ReCaptcha API
	if($url == 'rdm-import.ru'){
		$sitekey = '6LfI7TwUAAAAAGn0uGdlu0PMYWQG3ZomnYFN0IM-';
	}else if($url == 'marketica-dev.ru'){
		$sitekey = '6LeT1z8UAAAAAF9jN_e-hemvdCoRIArgz6R4d4kY';
	}else{
		$sitekey = '6LeT1z8UAAAAAF9jN_e-hemvdCoRIArgz6R4d4kY';
	}


	if($_REQUEST["params"]) {
        foreach ($_REQUEST["params"] as $k => $v) {
            $arParams[$k] = htmlspecialchars((int)$v, ENT_QUOTES);
        }
    }

    foreach($_REQUEST as $k=>$v){
        $req[$k] = htmlspecialchars($v, ENT_QUOTES);
    }

	if(!empty($req["type"])){

		$arSelect = Array(
		"ID",
		"NAME",
		"CODE",
		"DATE_ACTIVE_FROM",
		"DETAIL_PAGE_URL",
		"PREVIEW_TEXT",
		"PREVIEW_PICTURE",
		"PROPERTY_MEDIA_SHOW",
		"PROPERTY_MEDIA_YOUTUBE",
		"PROPERTY_MEDIA_NAME",
		"PROPERTY_MEDIA_BLOCK",
		"PROPERTY_MEDIA_URL",
		);

		$arLeads = array();

		$arFilter = Array("IBLOCK_ID"=>24, "CODE"=>$req["type"], "ACTIVE"=>"Y");
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		if($ob = $res->GetNext())
		{
			//$item = $ob->GetFields();
			//$this->arResult["lead"][] = $ob;

			$arMedia = false;
			$strText = false;
			$blockStyle='two';
			//$mediaStyle='media-half';
			if($ob['PROPERTY_MEDIA_SHOW_ENUM_ID']==114){
				//Показать видео

				if(!empty($ob['PROPERTY_MEDIA_YOUTUBE_VALUE'])){

					$arMedia = array(
					'value'=>$ob['PROPERTY_MEDIA_YOUTUBE_VALUE'],
					'type'=>'video',
					'thumb'=>'https://img.youtube.com/vi/'.$ob['PROPERTY_MEDIA_YOUTUBE_VALUE'].'/0.jpg'
					);

				}

			}elseif($ob['PROPERTY_MEDIA_SHOW_ENUM_ID']==115){
				//Показать фото
				if(!empty($ob['PREVIEW_PICTURE'])){
					$arMedia = array(
					'value'=>CFile::GetFileArray($ob['PREVIEW_PICTURE']),
					'type'=>'photo'
					);
				}
			}else{
				//Ничего не показывать

			$blockStyle='one';

			}

			if(empty($ob['~PREVIEW_TEXT'])){
			$textStyle='';
			$mediaStyle='';
			}else{

				$strText = $ob['~PREVIEW_TEXT'];
			}

			$arLeads[$ob['CODE']] = array(
			'TITLE'=>!empty($ob['~PROPERTY_MEDIA_NAME_VALUE']['TEXT'])?$ob['~PROPERTY_MEDIA_NAME_VALUE']['TEXT']:false,
			'TEXTBLOCK'=>!empty($ob['PROPERTY_MEDIA_BLOCK_VALUE'])?$ob['PROPERTY_MEDIA_BLOCK_VALUE']:false,
			'NAME'=>$ob['NAME'],
			'CODE'=>$ob['CODE'],
			'SHOW'=>$ob['PROPERTY_MEDIA_SHOW_ENUM_ID'],
			'MEDIA' => $arMedia,
			'TEXT' => $strText,
			'STYLE' => $blockStyle,
			);

		}


	$true = true;
	$rsForms = CForm::GetList($by="s_id", $order="desc", array("SID"=>$req["type"]), $true);

	if ($arForm = $rsForms->Fetch())
	{
		$rsFieldList = CFormField::GetList($arForm["ID"], "ALL", $by="s_sort", $order="asc", array("ACTIVE"=>"Y"), $is_filtered);

	    $arFormFields = array();
	    $arError = array();
	    $isRequired = '';
		while ($arFieldList = $rsFieldList->GetNext())
		{

			$rsAnswer = CFormAnswer::GetByID($arFieldList["ID"]);

			$arFieldList["ANSWER"] = $rsAnswer->Fetch();

			$arFormFields[] = $arFieldList;

			if($arFieldList["REQUIRED"]=="Y"){

				if(empty($arUser[$arFieldList["SID"]])){
					$arError[] = $arFieldList["SID"];
                    $isRequired = '';
				}
			}
		}


		$description = "";
		$text = "";
		$captcha = "";
	    if($req["type"]=="credit"){

        $description .= '<div class="complexForm__title">Оставить заявку на кредит</div>';

		$text = '<div class="complexForm__title3">Заказать консультацию кредитного специалиста</div>';

		if(!strstr($_REQUEST['support'],'fix-iphone') && !strstr($_REQUEST['support'],'fix-safari')){
			$captcha =
			'<div class="captcha__wrap formfield" id="captcha">
			<div id="recapchaWidget" class="g-recaptcha" data-sitekey="'.$sitekey.'"></div>
			<div class="error-block trade__error">Вы пропустили капчу</div>
			</div>';
		}

        }elseif($req["type"]=="creditadv"){



        $text =
		'<div class="complexForm__title2">Рассчитать <span class="text__color-red">выгодный</span> автокредит:</div>
			<div class="credit__calc-row">
				<label class="credit__calc-label1">Стоимость автомобиля:</label>
				<input class="credit__calc-input1" type="number" name="result__stoim" id="result__stoim" pattern="\d [0-9]" value="" />
			</div>
			<div class="credit__calc-row">
				<label class="credit__calc-label1">Первоначальный платёж, руб:</label>
				<input class="credit__calc-input1" type="number" name="result__vznos" id="result__vznos" pattern="\d [0-9]" value="0" />
			</div>
			<div class="credit__calc-row">
				<label class="credit__calc-label2">Желаемый срок кредита (лет):</label>
				<input class="credit__calc-input2" type="number" name="result__srok" id="result__srok" pattern="^[ 0-9]+$" value="" />
			</div>
			<div class="credit__name">Ежемесячный платеж по кредиту, руб.</div>
			<div class="credit__desc" id="result_credit">
				<span class="credit__desc-msg">Чтобы увидеть сумму платежа в месяц заполните первоначальный взнос и срок кредита</span>
				<span class="credit__desc-min" id="result__min"></span>
				<span class="credit__desc-max" id="result__max"></span>
			</div>
		<div class="complexForm__title3">Закажите <span class="text__color-red">бесплатную</span> консультацию кредитного специалиста</div>
		';

			if(!strstr($_REQUEST['support'],'fix-iphone') && !strstr($_REQUEST['support'],'fix-safari')){

			$captcha =
			'<div class="captcha__wrap formfield" id="captcha">
			<div id="recapchaWidget" class="g-recaptcha" data-sitekey="'.$sitekey.'"></div>
			<div class="error-block trade__error">Вы пропустили капчу</div>
			</div>';
			}

        }elseif($req["type"]=="reserve"){
            //$description = "Оставьте ваши контакты и мы зарезервируем данный автомобиль для вас.";
            $description = "";


        }elseif($req["type"]=="testdrive"){
            //$description = "Оставьте ваши контакты и мы запишем вас на тест-драйв данного автомобиля.";
            $description = "";
		}

		ob_start();?>
		<div class="complex__header">
			<img class="complex__headerLogo" src="/local/assets/img/assets/header/logo.svg" alt="">
		</div>
		<?foreach($arLeads as $arItem){?>
		<div class="tbox" id="lead-<?=$arItem['CODE']?>">
		<?if($arItem['TITLE']!=false){?><div class="tbox__title"><?=$arItem['TITLE']?></div><?}?>
			<?if($arItem['TEXT']!=false){?><div class="tbox-col-<?=$arItem['STYLE']?> tbox__text"><?=$arItem['TEXT']?></div><?}?>
			<?if($arItem['MEDIA']!=false){?>
			<div class="tbox-col-<?=$arItem['STYLE']?> tbox__media">
			<?if($arItem['MEDIA']['type'] == 'video'){?>
			<div class="tbox__video onLeadMedia" data-src="https://www.youtube.com/embed/<?=$arItem['MEDIA']['value']?>" data-media="video">
			<img class="tbox__thumb" src="<?=$arItem['MEDIA']['thumb']?>" alt="" />
			</div>
			<?}elseif($arItem['MEDIA']['type'] == 'photo'){?>
			<div class="tbox__img onLeadMedia" data-src="<?=$arItem['MEDIA']['value']['SRC']?>" data-media="img">
				<img class="tbox__thumb" src="<?=$arItem['MEDIA']['value']['SRC']?>" alt="" />
				</div>
			<?}?>
			</div>
			<?}?>
			<?if($arItem['TEXTBLOCK']!=false){?><span class="tbox__addtext"><?=$arItem['TEXTBLOCK']?></span><?}?>
		</div>
		<?}?>
		<form class="complexForm-form" id="complex-form" method="post">
			<div class="complexForm-group<?=$isRequired?>">

			<?if(!empty($description)){?><div class="complexForm__description" id="complexForm__description"><?=$description?></div><?}?>
			<div class="complexForm__wrp">
			<?=$text?>
			<?foreach($arFormFields as $arFields){

				$placeholder=!empty($arFields["ANSWER"]["FIELD_PARAM"])?$arFields["ANSWER"]["FIELD_PARAM"]:$arFields["TITLE"];

				if($arFields["ANSWER"]["FIELD_TYPE"]=="text"){
			?>
			<div class="complexForm-input-fullrow field <?if($arFields["REQUIRED"]=="Y"){?>required<?}?>" id="<?=$arFields["SID"]?>">
				<input type="<?=$arFields["ANSWER"]["FIELD_TYPE"]?>" name="<?=$arFields["SID"]?>" class="complexForm-in-<?=$arFields["ANSWER"]["FIELD_TYPE"]?>" <?=$placeholder?> value="" />
			<?if($arFields["REQUIRED"]=="Y"){?>
				<div class="error-block">Заполните это поле</div>
			<?}?>
			</div>

				<?}elseif($arFields["ANSWER"]["FIELD_TYPE"]=="textarea"){?>
					<div class="complexForm-input-fullrow field <?if($arFields["REQUIRED"]=="Y"){?>required<?}?>" id="<?=$arFields["SID"]?>">

					<textarea name="<?=$arFields["SID"]?>" class="complexForm-in-<?=$arFields["ANSWER"]["FIELD_TYPE"]?>" <?=$placeholder?>></textarea>
				<?if($arFields["REQUIRED"]=="Y"){?>
					<div class="error-block">Заполните это поле</div>
				<?}?>
			</div>

			<?}else{?>
				<input type="<?=$arFields["ANSWER"]["FIELD_TYPE"]?>" name="<?=$arFields["SID"]?>" value="" />
			  <?}
			}

			foreach($arParams as $k=>$v){?>
				<input type="hidden" name="<?=$k?>" value="<?=$v?>" />
			<?}?>

			<div class="complexForm-input-fullrow edata">
				<input type="checkbox" class="in-text-data data-style onData" name="order_edata" checked="checked">
				<span class="data-text">Подтверждаю согласие на обработку <a href="/personal-data/" target="_blank">персональных&nbsp;данных</a></span>
			</div>
			<?if(!strstr($_REQUEST['support'],'fix-iphone') && !strstr($_REQUEST['support'],'fix-safari')){?>
			<?=$captcha?>
			<?}?>
			<div class="complexForm-input-fullrow complexForm-input-row-center">
				<button class="btn-submit onSubmit">Отправить</button>
			</div>
			</div>
			</div>
		</form>
        <?
		$html = ob_get_contents();ob_end_clean();

		//$arResult = array("status"=>true,"req"=>$req,"request"=>$_REQUEST,"arParams"=>$arParams,"arForm"=>$arForm,"arFormFields"=>$arFormFields,"html"=>$html);
		$arResult = array("status"=>true,'req'=>$captcha,"html"=>$html);

	}else{

		$arResult = array("status"=>false,"msg"=>"not webform type");
	}



	}else{

	$arResult = array("status"=>false,"msg"=>"not type");
	}

}else{

	$arResult = array("status"=>false,"msg"=>"not data");
}


echo json_encode($arResult);