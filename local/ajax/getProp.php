<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

use Bitrix\Main\Loader,
    \Ycaweb\Cars;
Loader::includeModule("ycaweb.cars");

$prop = $_REQUEST["code"];
$propValue = $_REQUEST["value"];


$arResult = array();

switch($prop)
{
    case 'MANUFACTURER':
        $arResult = \Ycaweb\Cars\Cars::getModels($propValue);
        break;
    case 'MODEL':
        $arResult = \Ycaweb\Cars\Cars::getGenerations($propValue);
        break;
    case 'GENERATION':
        $arResult = \Ycaweb\Cars\Cars::getSeries($propValue);
        break;
    case 'SERIE':
        $arResult = \Ycaweb\Cars\Cars::getModifications($propValue);
        break;
    case 'MODIFICATION':
        $arResult = \Ycaweb\Cars\Cars::getCharacteristics($propValue);
        break;
}
echo json_encode($arResult);