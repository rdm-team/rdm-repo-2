<?    define("NO_KEEP_STATISTIC", true);
    define("NO_AGENT_CHECK", true);
    define('PUBLIC_AJAX_MODE', true);
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
    $_SESSION["SESS_SHOW_INCLUDE_TIME_EXEC"]="N";
    $APPLICATION->ShowIncludeStat = false;

    if(isset($_REQUEST["data"])){
		include $_SERVER['DOCUMENT_ROOT'].'/local/configpage.php';
       
        $REQUEST = array();
        $data = $_REQUEST["data"];

        foreach($data as $field){
            
            if(!empty($field["name"]) && strstr($field["name"], 'order_')){
                
                $REQUEST[$field["name"]] = htmlspecialchars($field["value"],ENT_QUOTES);
                
            }
            
        }
		
        CModule::IncludeModule('iblock');
        CModule::IncludeModule("form");
		
     /* ~~~~ */ 
	
		$type = !empty($_REQUEST['dataform'])?htmlspecialchars($_REQUEST["dataform"],ENT_QUOTES):null;
		$arLead = array();
		$strUrl = '<a href="http://'.$_SERVER['HTTP_HOST'].'">РДМ-ИМПОРТ</a>';
		$arSelect = Array(
		"ID", 
		"NAME",
		"CODE", 
		"PROPERTY_MEDIA_URL",
		);
		$arFilter = Array("IBLOCK_ID"=>24, "CODE"=>$type, "ACTIVE"=>"Y");
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		if($ob = $res->GetNext())
		{		
			$arLead = array(		
			'NAME'=>$ob['NAME'],
			'CODE'=>$ob['CODE'],		
			'URL'=>$ob['PROPERTY_MEDIA_URL_VALUE'],
			'ob' => $ob,
			);
			
			$strNamePage = isset($arPages[$ob['PROPERTY_MEDIA_URL_VALUE']])?$arPages[$ob['PROPERTY_MEDIA_URL_VALUE']]:'Страница не определена';			
			$strUrl = isset($arPages[$ob['PROPERTY_MEDIA_URL_VALUE']])?$arPages[$ob['PROPERTY_MEDIA_URL_VALUE']].'<br>':'Страница не определена';			
			$strUrl .= '<a href="http://'.$_SERVER['HTTP_HOST'].$ob['PROPERTY_MEDIA_URL_VALUE'].'">'.$ob['NAME'].'</a>';
		}	
    /* ~~~~ */
	

    
    
        $rsFieldList = CFormField::GetList(17, "ALL", $by="s_sort", $order="asc", array(), $is_filtered);
        $arFieldList = array();
        $arEventFields = array();
        $arFields = array();
        $arErrors = array();
       
        while ($arField = $rsFieldList->GetNext())
        {
            if(!empty($REQUEST[$arField["SID"]])){
                $val = htmlspecialchars($REQUEST[$arField["SID"]], ENT_QUOTES);
                $arFieldList[$arField["SID"]] = $val;
                $arEventFields["form_".$arField["TITLE_TYPE"]."_".$arField["ID"]] = $val;        
            }else{
                $arErrors[$arField["SID"]] = $arField["SID"];                
            }
			
			if($arField["SID"] == 'linkpage'){				
                $arFieldList[$arField["SID"]] = $strUrl;
                $arFieldList["pagename"] = $strNamePage;
                $arFieldList["linkname"] = $arLead["NAME"];
                $arEventFields["form_hidden_".$arField["ID"]] = $strUrl;
			}
        }
        
        
        /* Cогласие обработки персональных данных */ 
				
        CEvent::Send("ORDER_FORM", 's1', $arFieldList,"N",76);

        if ($RESULT_ID = CFormResult::Add(17, $arEventFields))
        {
           $arResult["send"] = "Y";
        }else{
           $arResult["send"] = "N";
        }
	              
    //$arResult = array("status"=>true);
	
    $arResult = array("status"=>true,
	'url'=>$strUrl,
	'type'=>$type,	
	'arLead'=>$arLead,	
	'strNamePage'=>$strNamePage,	
	'arEventFields'=>$arEventFields,
	'arFieldList'=>$arFieldList);
    
    }else{
        
        $arResult = array("status"=>false);
        
    }
    
    echo json_encode($arResult);
?>