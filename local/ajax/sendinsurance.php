<?    define("NO_KEEP_STATISTIC", true);
    define("NO_AGENT_CHECK", true);
    define('PUBLIC_AJAX_MODE', true);
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
    $_SESSION["SESS_SHOW_INCLUDE_TIME_EXEC"]="N";
    $APPLICATION->ShowIncludeStat = false;
 
    
    if(isset($_REQUEST["data"])){
        include $_SERVER['DOCUMENT_ROOT'].'/local/configpage.php';
        $REQUEST = array();
        $data = $_REQUEST["data"];
      
        foreach($data as $field){
            
            if(!empty($field["name"]) && strstr($field["name"], 'zayavka_')){
                
                $REQUEST[$field["name"]] = htmlspecialchars($field["value"],ENT_QUOTES);
                
            }
            
        }
               
        
        CModule::IncludeModule('iblock');
        CModule::IncludeModule("form");
		/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
			$strUrl = '<a href="http://'.$_SERVER['HTTP_HOST'].'">РДМ-ИМПОРТ</a>';
		    //$type = !empty($_REQUEST["type"])?htmlspecialchars($_REQUEST["type"],ENT_QUOTES):"osago";
		    $dataform = !empty($_REQUEST["dataform"])?htmlspecialchars($_REQUEST["dataform"],ENT_QUOTES):"osago";
			$arSelect = Array("ID","NAME","CODE","PROPERTY_MEDIA_URL");
			$arFilter = Array("IBLOCK_ID"=>24, "CODE"=>$dataform, "ACTIVE"=>"Y");
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			if($ob = $res->GetNext())
			{
				
			$strNamePage = isset($arPages[$ob['PROPERTY_MEDIA_URL_VALUE']])?$arPages[$ob['PROPERTY_MEDIA_URL_VALUE']]:'Страница не определена';
			$strUrl = isset($arPages[$ob['PROPERTY_MEDIA_URL_VALUE']])?$arPages[$ob['PROPERTY_MEDIA_URL_VALUE']].'<br>':'Страница не определена';			
			$strUrl .= '<a href="http://'.$_SERVER['HTTP_HOST'].$ob['PROPERTY_MEDIA_URL_VALUE'].'">'.$ob['NAME'].'</a>';
	
			}
		
		/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
	
	
    
        $rsFieldList = CFormField::GetList(10, "ALL", $by="s_sort", $order="asc", array(), $is_filtered);
        $arFieldList = array();
        $arEventFields = array();
        $arFields = array();
        $arErrors = array();
       
        while ($arField = $rsFieldList->GetNext())
        {

            if(!empty($REQUEST[$arField["SID"]])){
                $val = htmlspecialchars($REQUEST[$arField["SID"]], ENT_QUOTES);
                $arFieldList[$arField["SID"]] = $val;
                $arEventFields["form_".$arField["TITLE_TYPE"]."_".$arField["ID"]] = $val;
        
            }else{
                $arErrors[$arField["SID"]] = $arField["SID"];
                
            }
			if($arField["SID"] == 'linkpage'){				
				
                $arFieldList[$arField["SID"]] = $strUrl;
                $arFieldList["pagename"] = $strNamePage;
                $arFieldList["linkname"] = $arLead["NAME"]; 
                $arEventFields["form_hidden_".$arField["ID"]] = $strUrl;
			}   
        }
        //$title = "Заказать расчет КАСКО";
        if($dataform=="osago"){
			
			$title = "Хочу заказать расчет ОСАГО";
			
		}elseif($dataform=="kasko"){
			
			$title = "Хочу заказать расчет КАСКО";
		}
		
        /* Cогласие обработки персональных данных */        
        if(isset($arFieldList["zayavka_edata"])){$edata = "Да";}else{$edata = "Нет";}
        $arFieldList["zayavka_edata"] = $edata;
        $arFieldList["title"] = $title;
        $arFieldList["url"] = $strUrl;
        $arEventFields["form_text_36"] = $edata;
        $arEventFields["form_hidden_67"] = $title;
        //$arEventFields["form_hidden_67"] = $title;
        
        /* Тип формы */
		
        CEvent::Send("SIMPLE_FORM_4", 's1', $arFieldList,"N",63);

        if ($RESULT_ID = CFormResult::Add(10, $arEventFields))
        {
           $arResult["send"] = "Y";
        }else{
           $arResult["send"] = "N";
        }
		
		$arResult = array("status"=>true);
        /*          
		$arResult = array(
		"status"=>true,
		"server"=>$_SERVER,
		"url"=>$URL,
		'type'=>$dataform,	
		'arEventFields'=>$arEventFields,
		'arFieldList'=>$arFieldList,
		'arHidden'=>$arHidden
		);
    */
    
    }else{
        
        $arResult = array("status"=>false);
        
    }
    
    echo json_encode($arResult);
?>