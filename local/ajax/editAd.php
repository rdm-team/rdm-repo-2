<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Loader,
    Bitrix\Main\Config\Option,
    Bitrix\Main\Application,
    \Ycaweb\Cars;

Loader::IncludeModule("iblock");
Loader::includeModule("ycaweb.cars");

$arResult = $arPropsToAdd = array();
$request = Application::getInstance()->getContext()->getRequest();

$arAdProps = $request->getPost("AD_PROPERTY");
$arAdCarProps = $request->getPost("PROPERTY");
$elementID = $request->getPost('ELEMENT_ID');

$arChosenProps =  Cars\Cars::getChosenOptions();
$adsIblockID = Option::get("ycaweb.cars", 'carIblock');

if(!empty($adsIblockID))
{
    $dbIblock = CIBlock::GetList(
        Array(),
        Array(
            'ID' => $adsIblockID,
        ), false
    );

    while($arIblock = $dbIblock->fetch())
    {
        $adsIblockType = $arIblock["IBLOCK_TYPE_ID"];
    }

    foreach($arAdProps as $propCode=>$propValue)
    {
        if(!empty($propValue))
        {
            $arPropsToAdd[$propCode] = $propValue;
        }

    }

    foreach($arAdCarProps as $key=>$value)
    {
        $propVal = trim($value);

        if(!empty($propVal))
        {
            $propertyType = $arChosenProps[$key]['TYPE'];
            $propertyCode = $arChosenProps[$key]['IBLOCK_CODE'];

            switch($propertyType)
            {
                case 'S':
                    $arPropsToAdd[$propertyCode] = $propVal;
                    break;
                case 'N':
                    $arPropsToAdd[$propertyCode] = $propVal;
                    break;
                case 'L':
                    $dbListProps = CIBlockPropertyEnum::GetList(array(), array('IBLOCK_ID' => $adsIblockID, "PROPERTY_ID" => $propertyCode, 'VALUE' => $propVal));
                    if($arListProp = $dbListProps->fetch())
                    {
                        $arPropsToAdd[$propertyCode] = $arListProp['ID'];
                    }
                    else
                    {
                        $ibpenum = new CIBlockPropertyEnum;
                        if($newValID = $ibpenum->Add(Array('PROPERTY_ID' => $arChosenProps[$key]['IBLOCK_ID'], 'VALUE' => $propVal)))
                        {
                            $arPropsToAdd[$propertyCode] = $newValID;
                        }
                    }
                    break;
                case 'E':
                    if(!empty($arChosenProps[$key]['LINK_IBLOCK_ID']))
                    {
                        $arFilter = array(
                            'IBLOCK_ID' => $arChosenProps[$key]['LINK_IBLOCK_ID']
                        );

                        if( $propertyCode == 'DRIVE' || $propertyCode == 'FUEL' )
                        {
                            $arFilter = array(
                                'LOGIC' => 'OR',
                                '%NAME' => $propVal,
                                '%PROPERTY_NAME_VARIETY' => $propVal
                            );
                        }
                        else
                        {
                            $arFilter['NAME'] = $propVal;
                        }

                        $dbEl = CIBlockElement::GetList(array(), $arFilter, false, false, array('IBLOCK_ID', 'ID'));
                        while($arEl = $dbEl->fetch())
                        {
                            $arPropsToAdd[$propertyCode] = $arEl['ID'];
                        }
                    }
                    break;
            }
        }
    }

    $el = new CIBlockElement;

    $elementName = $arPropsToAdd['MANUFACTURER']." ".$arPropsToAdd['MODEL'];

    if($elementID == 0)
    {

        $id = $el->Add(array(
            'ACTIVE' => 'N',
            'NAME' => $elementName,
            'IBLOCK_ID' => $adsIblockID,
            'CODE' => $arFields["CODE"],
            'PROPERTY_VALUES' => $arPropsToAdd,
        ));

        if($id)
        {
            $arResult["ELEMENT_ID"] = $id;
        }
        else
        {
            $arResult["ERROR"] = $el->LAST_ERROR;
        }
    }
    else
    {
        foreach ($arPropsToAdd as $key=>$value)
        {
            CIBlockElement::SetPropertyValues($elementID, $adsIblockID, $value, $key);
        }
        $arResult["ELEMENT_ID"] = $elementID;
    }

    if(isset($arResult["ELEMENT_ID"]))
    {
        $arResult["RESULT_URL"] = SITE_DIR."bitrix/admin/iblock_element_edit.php?&WF=Y?&lang=".LANGUAGE_ID."&IBLOCK_ID=".$adsIblockID."&type=".$adsIblockType."&ID=".$arResult["ELEMENT_ID"];
    }
}
else
{
    $arResult["ERROR"] = "Не выбраны необходимые параметры в настройках модуля!";
}
echo json_encode($arResult);