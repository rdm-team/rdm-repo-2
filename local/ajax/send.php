<?    define("NO_KEEP_STATISTIC", true);
    define("NO_AGENT_CHECK", true);
    define('PUBLIC_AJAX_MODE', true);
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
    $_SESSION["SESS_SHOW_INCLUDE_TIME_EXEC"]="N";
    $APPLICATION->ShowIncludeStat = false;
 
    
    if(isset($_REQUEST["data"])){
        include $_SERVER['DOCUMENT_ROOT'].'/local/configpage.php';
        $REQUEST = array();
        $data = $_REQUEST["data"];
        $arCalc = array(
		"calc-1" => "Калькулятор выкупа",
		"calc-2" => "Калькулятор обмена",
		"calc-3" => "Комиссионный калькулятор",
		"calc-4" => "Заявка на обмен"
		);
        
        foreach($data as $field){
            
            if(!empty($field["name"]) && strstr($field["name"], 'zayavka_')){
                
                $REQUEST[$field["name"]] = htmlspecialchars($field["value"],ENT_QUOTES);
                
            }
            
        }
        
        $type = !empty($_REQUEST["type"])?htmlspecialchars($_REQUEST["type"],ENT_QUOTES):"calc-1";
        

        CModule::IncludeModule('iblock');
        CModule::IncludeModule("form"); 
     /* ~~~~ */ 
		$pageDetail = false;
		if(isset($_REQUEST['href'])){

			$url = htmlspecialchars($_REQUEST['href'],ENT_QUOTES);
			$arUrl = parse_url($url);
			$url = trim($arUrl['path'],'/');

			$arUrl = explode('/',$url);
			$arUrl = array_reverse($arUrl);
			$autoCode = array_shift($arUrl);
			$pageDetail = true;
		}
		/* ~~~~ */ 
		
		$dataform = !empty($_REQUEST['dataform'])?htmlspecialchars($_REQUEST["dataform"],ENT_QUOTES):null;
		$arLead = array();
		$strUrl = '<a href="http://'.$_SERVER['HTTP_HOST'].'">РДМ-ИМПОРТ</a>';
		$arSelect = Array(
		"ID", 
		"NAME",
		"CODE", 
		"PROPERTY_MEDIA_URL",
		);
		$arFilter = Array("IBLOCK_ID"=>24, "CODE"=>$dataform, "ACTIVE"=>"Y");
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		if($ob = $res->GetNext())
		{		
			$arLead = array(		
			'NAME'=>$ob['NAME'],
			'CODE'=>$ob['CODE'],		
			'URL'=>$ob['PROPERTY_MEDIA_URL_VALUE'],
			'ob' => $ob,
			);
			
			if($pageDetail){
				
				$url = 'http://'.$_SERVER['HTTP_HOST'].$ob['PROPERTY_MEDIA_URL_VALUE'].$autoCode.'/';
			}else{
				$url = 'http://'.$_SERVER['HTTP_HOST'].$ob['PROPERTY_MEDIA_URL_VALUE'];
				
			}
			
			$strNamePage = isset($arPages[$ob['PROPERTY_MEDIA_URL_VALUE']])?$arPages[$ob['PROPERTY_MEDIA_URL_VALUE']]:'Страница не определена';
			$strUrl = isset($arPages[$ob['PROPERTY_MEDIA_URL_VALUE']])?$arPages[$ob['PROPERTY_MEDIA_URL_VALUE']].'<br>':'Страница не определена';			
			$strUrl .= '<a href="'.$url.'">'.$ob['NAME'].'</a>';
		}
		



		
    /* ~~~~ */
    
        $rsFieldList = CFormField::GetList(5, "ALL", $by="s_sort", $order="asc", array(), $is_filtered);
        $arFieldList = array();
        $arEventFields = array();
        $arFields = array();
        $arErrors = array();
       
        while ($arField = $rsFieldList->GetNext())
        {

            if(!empty($REQUEST[$arField["SID"]])){
                $val = htmlspecialchars($REQUEST[$arField["SID"]], ENT_QUOTES);
                $arFieldList[$arField["SID"]] = $val;
                $arEventFields["form_".$arField["TITLE_TYPE"]."_".$arField["ID"]] = $val;
        
            }else{
                $arErrors[$arField["SID"]] = $arField["SID"];                
            }
			
			if($arField["SID"] == 'linkpage'){				
				
                $arFieldList[$arField["SID"]] = $strUrl;
                $arFieldList["pagename"] = $strNamePage;
                $arFieldList["linkname"] = $arLead["NAME"]; 
                $arEventFields["form_hidden_".$arField["ID"]] = $strUrl;
			}   
        }
        
        
        /* Cогласие обработки персональных данных */        
        if(isset($arFieldList["zayavka_edata"])){$edata = "Да";}else{$edata = "Нет";}
        $arFieldList["zayavka_edata"] = $edata;
        $arEventFields["form_text_36"] = $edata;
        
        /* Тип формы */
        $arFieldList["zayavka_type"] = $arCalc[$type];
        $arEventFields["form_text_35"] = $arCalc[$type];
        
        CEvent::Send("SIMPLE_FORM_4", 's1', $arFieldList,"N",52);

        if ($RESULT_ID = CFormResult::Add(5, $arEventFields))
        {
           $arResult["send"] = "Y";
        }else{
           $arResult["send"] = "N";
        }
                  
    //$arResult = array("status"=>true);
    $arResult = array("status"=>true,
	'url'=>$strUrl,
	'type'=>$type,	
	'arEventFields'=>$arEventFields,
	'arFieldList'=>$arFieldList,
	'req'=>$_REQUEST
	);
    
    }else{
        
        $arResult = array("status"=>false);
        
    }
    
    echo json_encode($arResult);
?>