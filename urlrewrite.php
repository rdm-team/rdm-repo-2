<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/auto/view/([0-9a-zA-Z_-]+)/?(.*)#",
		"RULE" => "ELEMENT_ID=\$1",
		"ID" => "",
		"PATH" => "/auto/view/index.php",
	),
	array(
		"CONDITION" => "#^/auto/([ 0-9a-zA-Z_-]+)/([ 0-9a-zA-Z_-]+)/([ 0-9a-zA-Z_-]+)/#",
		"RULE" => "filter%5BMANUFACTURER%5D=\$1&filter%5BMODEL%5D=\$2&filter%5BPRICE%5D%5BFROM%5D=&filter%5BPRICE%5D%5BTO%5D=&filter%5BYEAR%5D%5BFROM%5D=\$3&filter%5BYEAR%5D%5BTO%5D=\$3&filter%5BENGINE%5D%5BFROM%5D=&filter%5BENGINE%5D%5BTO%5D=&filter%5BFUEL%5D=&filter%5BDRIVE%5D=&filter%5BTRANSMISSION%5D=&start_filter=filter&filter%5BPOWER%5D=&",
		"ID" => "bitrix:news.section",
		"PATH" => "/auto/index.php",
	),
	array(
		"CONDITION" => "#^/auto/([ 0-9a-zA-Z_-]+)/([ 0-9a-zA-Z_-]+)/#",
		"RULE" => "filter%5BMANUFACTURER%5D=\$1&filter%5BMODEL%5D=\$2&filter%5BPRICE%5D%5BFROM%5D=&filter%5BPRICE%5D%5BTO%5D=&filter%5BYEAR%5D%5BFROM%5D=&filter%5BYEAR%5D%5BTO%5D=&filter%5BENGINE%5D%5BFROM%5D=&filter%5BENGINE%5D%5BTO%5D=&filter%5BFUEL%5D=&filter%5BDRIVE%5D=&filter%5BTRANSMISSION%5D=&start_filter=filter&filter%5BPOWER%5D=&",
		"ID" => "bitrix:news.section",
		"PATH" => "/auto/index.php",
	),
	array(
		"CONDITION" => "#^/autotest/([0-9a-zA-Z_-]+)/?(.*)#",
		"RULE" => "ELEMENT_ID=\$1",
		"ID" => "",
		"PATH" => "/autotest/detail.php",
	),
	array(
		"CONDITION" => "#^/unsublog/([0-9a-zA-Z_-]+)/?(.*)#",
		"RULE" => "SECURE_CODE=\$1",
		"ID" => "",
		"PATH" => "/local/ajax/unsubscript.blog.php",
	),
	array(
		"CONDITION" => "#^/vacancy/([0-9a-zA-Z_-]+)/?(.*)#",
		"RULE" => "ELEMENT_CODE=\$1",
		"ID" => "",
		"PATH" => "/vacancy/detail.php",
	),
	array(
		"CONDITION" => "#^/last_auto/([0-9a-zA-Z]+)/?(.*)#",
		"RULE" => "ELEMENT_ID=\$1",
		"ID" => "",
		"PATH" => "/last_auto/index.php",
	),
	array(
		"CONDITION" => "#^/sell/([0-9a-zA-Z_-]+)/?(.*)#",
		"RULE" => "ELEMENT_CODE=\$1",
		"ID" => "",
		"PATH" => "/sell/index.php",
	),
	array(
		"CONDITION" => "#^/blog/([0-9a-zA-Z_-]+)/?(.*)#",
		"RULE" => "ELEMENT_CODE=\$1",
		"ID" => "",
		"PATH" => "/blog/detail.php",
	),
	array(
		"CONDITION" => "#^/bitrix/services/ymarket/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/bitrix/services/ymarket/index.php",
	),
	array(
		"CONDITION" => "#^/auto/([ 0-9a-zA-Z_-]+)/#",
		"RULE" => "filter%5BMANUFACTURER%5D=\$1&filter%5BMODEL%5D=&filter%5BPRICE%5D%5BFROM%5D=&filter%5BPRICE%5D%5BTO%5D=&filter%5BYEAR%5D%5BFROM%5D=&filter%5BYEAR%5D%5BTO%5D=&filter%5BENGINE%5D%5BFROM%5D=&filter%5BENGINE%5D%5BTO%5D=&filter%5BFUEL%5D=&filter%5BDRIVE%5D=&filter%5BTRANSMISSION%5D=&start_filter=filter&filter%5BPOWER%5D=&",
		"ID" => "news",
		"PATH" => "/auto/index.php",
	),
	array(
		"CONDITION" => "#^/complexform_credit/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/local/ajax/complexform_credit.php",
	),
	array(
		"CONDITION" => "#^/complex-advanced/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/local/ajax/complex-advanced.php",
	),
	array(
		"CONDITION" => "#^/insurenceavarkom/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/local/ajax/insuranceAvarkom.php",
	),
	array(
		"CONDITION" => "#^/compare-remove/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/local/ajax/compare-remove.php",
	),
	array(
		"CONDITION" => "#^/sendinsurence/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/local/ajax/sendinsurance.php",
	),
	array(
		"CONDITION" => "#^/send-trade-in/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/local/ajax/send-trade-in.php",
	),
	array(
		"CONDITION" => "#^/complex-send/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/local/ajax/complex-send.php",
	),
	array(
		"CONDITION" => "#^/compare-add/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/local/ajax/compare-add.php",
	),
	array(
		"CONDITION" => "#^/on-trade-in/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/local/ajax/on-trade-in.php",
	),
	array(
		"CONDITION" => "#^/compare-get/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/local/ajax/compare-get.php",
	),
	array(
		"CONDITION" => "#^/complexform/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/local/ajax/complexform.php",
	),
	array(
		"CONDITION" => "#^/trade-send/# ",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/local/ajax/trade-send.php",
	),
	array(
		"CONDITION" => "#^/auto/view/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/auto/view/index.php",
	),
	array(
		"CONDITION" => "#^/unsubauto/#",
		"RULE" => "SECURE_CODE=\$1",
		"ID" => "",
		"PATH" => "/local/ajax/unsubscript.auto.php",
	),
	array(
		"CONDITION" => "#^/checkauto/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/local/ajax/checkauto.php",
	),
	array(
		"CONDITION" => "#^/products/#",
		"RULE" => "",
		"ID" => "bitrix:catalog",
		"PATH" => "/products/index.php",
	),
	array(
		"CONDITION" => "#^/services/#",
		"RULE" => "",
		"ID" => "bitrix:catalog",
		"PATH" => "/services/index.php",
	),
	array(
		"CONDITION" => "#^/autotest/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/autotest/index.php",
	),
	array(
		"CONDITION" => "#^/sendform/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/local/ajax/sendform.php",
	),
	array(
		"CONDITION" => "#^/archive/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/archive/index.php",
	),
	array(
		"CONDITION" => "#^/subauto/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/local/ajax/subscript.auto.php",
	),
	array(
		"CONDITION" => "#^/sublog/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/local/ajax/subscript.blog.php",
	),
	array(
		"CONDITION" => "#^/order/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/order/index.php",
	),
	array(
		"CONDITION" => "#^/supad/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/local/ajax/supad.php	 ",
	),
	array(
		"CONDITION" => "#^/news/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/news/index.php",
	),
);

?>