<div class="form_back" id="formDiagnostic">
	<img src="/local/assets/img/logosm.png"><br>
	<div class="dg_form">
		<p>
			 Компания РДМ-Импорт сделает вам бесплатную диагностику автомобиля.
		</p>
<?$APPLICATION->IncludeComponent(
	"api:feedbackex", 
	"uikit", 
	array(
		"API_FEX_FORM_ID" => "FORM12",
		"YATARGET" => "yadiagnost",	// Яндекс цель
		"BCC" => "",
		"COLOR" => "default",
		"COMPONENT_TEMPLATE" => "uikit",
		"DATETIME" => "",
		"DIR_URL" => "",
		"DISABLE_SEND_MAIL" => "N",
		"DISPLAY_FIELDS" => array(
			0 => "TITLE",
			1 => "EMAIL",
			2 => "PHONE",
			3 => "MESSAGE",
		),
		"EMAIL_ERROR_MESS" => "Указанный E-mail некорректен",
		"EMAIL_TO" => "rdmimportx@mail.amocrm.ru",
		"FIELD_ERROR_MESS" => "#FIELD_NAME# обязательное",
		"FIELD_NAME_POSITION" => "stacked",
		"FIELD_SIZE" => "default",
		"FORM_AUTOCOMPLETE" => "Y",
		"FORM_CLASS" => "",
		"FORM_FIELD_WIDTH" => "",
		"FORM_LABEL_TEXT_ALIGN" => "left",
		"FORM_LABEL_WIDTH" => "150px",
		"FORM_SUBMIT_CLASS" => "uk-button uk-width-1-1",
		"FORM_SUBMIT_STYLE" => "",
		"FORM_SUBMIT_VALUE" => "Отправить",
		"FORM_TEXTAREA_ROWS" => "5",
		"FORM_TITLE" => "Обратная связь",
		"FORM_TITLE_LEVEL" => "3",
		"FORM_WIDTH" => "",
		"HIDE_ASTERISK" => "N",
		"HIDE_FIELD_NAME" => "Y",
		"MAIL_SEND_USER" => "N",
		"MAIL_SUBJECT_ADMIN" => "#SITE_NAME#: Сообщение из формы обратной связи (диагностика авто)",
		"MAIL_SUBJECT_USER" => "#SITE_NAME#: Копия сообщения из формы обратной связи",
		"MODAL_BTN_CLASS" => "api_button",
		"MODAL_BTN_ID" => "shw",
		"MODAL_BTN_SPAN_CLASS" => "api_icon",
		"MODAL_BTN_TEXT" => "Обратная связь",
		"MODAL_FOOTER_TEXT" => "",
		"MODAL_HEADER_TEXT" => "Обратная связь",
		"MODAL_ID" => "#API_FEX_MODAL_FORM9",
		"OK_TEXT" => "Спасибо!",
		"OK_TEXT_AFTER" => "Спасибо, наш менеджер свяжется с Вами!",
		"PAGE_TITLE" => "",
		"PAGE_URL" => "",
		"REPLACE_FIELD_FROM" => "Y",
		"REQUIRED_FIELDS" => array(
			0 => "PHONE",
		),
		"SCROLL_SPEED" => "1000",
		"THEME" => "gradient",
		"TITLE_DISPLAY" => "N",
		"USE_AUTOSIZE" => "Y",
		"USE_JQUERY" => "Y",
		"USE_MODAL" => "N",
		"USE_PLACEHOLDER" => "N",
		"USE_SCROLL" => "Y",
		"WRITE_MESS_DIV_STYLE" => "padding:10px;border-bottom:1px dashed #dadada;",
		"WRITE_MESS_DIV_STYLE_NAME" => "font-weight:bold;",
		"WRITE_MESS_DIV_STYLE_VALUE" => "",
		"WRITE_MESS_FILDES_TABLE" => "N",
		"WRITE_MESS_TABLE_STYLE" => "border-collapse: collapse; border-spacing: 0;",
		"WRITE_MESS_TABLE_STYLE_NAME" => "max-width: 200px; color: #848484; vertical-align: middle; padding: 5px 30px 5px 0px; border-bottom: 1px solid #e0e0e0; border-top: 1px solid #e0e0e0;",
		"WRITE_MESS_TABLE_STYLE_VALUE" => "vertical-align: middle; padding: 5px 30px 5px 0px; border-bottom: 1px solid #e0e0e0; border-top: 1px solid #e0e0e0;",
		"LEAD_CODE"=>array('diagnostic')
	),
	false
);?>
	</div>
</div>