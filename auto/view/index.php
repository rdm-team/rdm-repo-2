<?
session_start();
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Купить авто");

if (empty($_REQUEST['ELEMENT_ID']))
{
	$APPLICATION->RestartBuffer();
	require $_SERVER["DOCUMENT_ROOT"] . '/bitrix/header.php';
	include $_SERVER["DOCUMENT_ROOT"] . '/404.php';
	require $_SERVER["DOCUMENT_ROOT"] . '/bitrix/footer.php';
	die;
}
?>

<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock") or die("Failed to load module iblock\n");
$arSelect = Array("ID");
$arFilter = Array("EXTERNAL_ID" => $_REQUEST['ELEMENT_ID'], "IBLOCK_ID"=>5, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$list = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
$item = $list->GetNextElement();
if($item){
	$arItem = $item->GetFields();
	$_REQUEST['ELEMENT_ID'] = $arItem['ID'];
}
?>

<?
$APPLICATION->IncludeComponent(
	"bitrix:news", 
	"advertisments", 
	array(
		"ADD_ELEMENT_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_ACTIVE_DATE_FORMAT" => "j F Y",
		"DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
		"DETAIL_DISPLAY_TOP_PAGER" => "N",
		"DETAIL_FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_PICTURE",
			2 => "DETAIL_TEXT",
			3 => "DETAIL_PICTURE",
			4 => "DATE_ACTIVE_FROM",
			5 => "ACTIVE_FROM",
			6 => "DATE_ACTIVE_TO",
			7 => "ACTIVE_TO",
			8 => "SHOW_COUNTER",
			9 => "SHOW_COUNTER_START",
			10 => "DATE_CREATE",
			11 => "",
		),
		"DETAIL_PAGER_SHOW_ALL" => "Y",
		"DETAIL_PAGER_TEMPLATE" => "",
		"DETAIL_PAGER_TITLE" => "",
		"DETAIL_PROPERTY_CODE" => array(
			0 => "YOUTUBE",
			1 => "IS_HYBRID",
			2 => "MODEL_YEAR",
			3 => "CITY",
			4 => "HAS_RF_MILEAGE",
			5 => "IS_FOREIGN",
			6 => "CONTACT_NAME",
			7 => "MANUFACTURER",
			8 => "MODEL",
			9 => "POWER",
			10 => "IS_NEW",
			11 => "ENGINE",
			12 => "DRIVE",
			13 => "MILEAGE",
			14 => "STEERING_WHEEL",
			15 => "PRICE",
			16 => "PHONE",
			17 => "TRANSMISSION",
			18 => "CAR_BODY",
			19 => "FUEL",
			20 => "COLOR",
			21 => "MORE_PHOTO",
			22 => "",
		),
		"DETAIL_SET_CANONICAL_URL" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"FILE_404" => "",
		"FILTER_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "arrAdsFilter",
		"FILTER_PROPERTY_CODE" => array(
			0 => "IS_HYBRID",
			1 => "MODEL_YEAR",
			2 => "HAS_RF_MILEAGE",
			3 => "IS_FOREIGN",
			4 => "MANUFACTURER",
			5 => "MODEL",
			6 => "IS_NEW",
			7 => "ENGINE",
			8 => "DRIVE",
			9 => "MILEAGE",
			10 => "STEERING_WHEEL",
			11 => "PRICE",
			12 => "TRANSMISSION",
			13 => "CAR_BODY",
			14 => "FUEL",
			15 => "COLOR",
			16 => "COST",
			17 => "TRANSMISSON",
			18 => "DIAGNOZ",
			19 => "AUTOCODE",
			20 => "INSPECTIA",
			21 => "",
		),
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "5",
		"IBLOCK_TYPE" => "ads",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"LIST_ACTIVE_DATE_FORMAT" => "j F Y",
		"LIST_FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_PICTURE",
			2 => "DETAIL_TEXT",
			3 => "DETAIL_PICTURE",
			4 => "DATE_ACTIVE_FROM",
			5 => "ACTIVE_FROM",
			6 => "DATE_ACTIVE_TO",
			7 => "ACTIVE_TO",
			8 => "SHOW_COUNTER",
			9 => "SHOW_COUNTER_START",
			10 => "DATE_CREATE",
			11 => "",
		),
		"LIST_PROPERTY_CODE" => array(
			0 => "YOUTUBE",
			1 => "IS_HYBRID",
			2 => "MODEL_YEAR",
			3 => "CITY",
			4 => "HAS_RF_MILEAGE",
			5 => "IS_FOREIGN",
			6 => "CONTACT_NAME",
			7 => "MANUFACTURER",
			8 => "MODEL",
			9 => "POWER",
			10 => "IS_NEW",
			11 => "ENGINE",
			12 => "DRIVE",
			13 => "MILEAGE",
			14 => "STEERING_WHEEL",
			15 => "STATE",
			16 => "PRICE",
			17 => "PHONE",
			18 => "TRANSMISSION",
			19 => "CAR_BODY",
			20 => "FUEL",
			21 => "COLOR",
			22 => "",
		),
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "main",
		"PAGER_TITLE" => "Объявления",
		"PREVIEW_TRUNCATE_LEN" => "",
		"SEF_FOLDER" => "/auto/view/",
		"SEF_MODE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "Y",
		"SET_TITLE" => "Y",
		"SHOW_404" => "Y",
		"SORT_BY1" => "DATE_CREATE",
		"SORT_BY2" => "PROPERTY_PRICE",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"USE_CATEGORIES" => "N",
		"USE_FILTER" => "Y",
		"USE_PERMISSIONS" => "N",
		"USE_RATING" => "N",
		"USE_RSS" => "N",
		"USE_SEARCH" => "N",
		"USE_SHARE" => "N",
		"COMPONENT_TEMPLATE" => "advertisments",
		"RESIZE_PHOTO_WIDTH" => "1200",
		"RESIZE_PHOTO_HEIGHT" => "700",
		"STRICT_SECTION_CHECK" => "N",
		"SEF_URL_TEMPLATES" => array(
			"news" => "",
			"section" => "",
			"detail" => "#EXTERNAL_ID#/",
		)
	),
	false
);?>
 <?$APPLICATION->IncludeComponent(
"vegas:subscription.registration.auto", 
"", 
array(	
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_FILTER" => "N",
	"CACHE_GROUPS" => "Y",
	"SET_STATUS_404" => "N",
	"SHOW_404" => "N",
	"MESSAGE_404" => "",
	"FORM_ID" => "8"
),
false
);?> 
<?$APPLICATION->IncludeComponent("vegas:complex.form","",array(),false);?>
<?$APPLICATION->IncludeComponent("vegas:calcForm","orderToBarter",array('AJAX_MODE' => 'Y',"LEAD_CODE" => array("kartobmen")),false);?>
<?$APPLICATION->IncludeComponent("vegas:winVideo","",array('AJAX_MODE' => 'Y'),false);?>
<?/*$APPLICATION->IncludeComponent("vegas:trade.form","",array("ELEMENT_ID"=>$ELEMENT_ID),false);*/?>
<?

?>

<div id="allgarant" class="modal__allgarant" >
<img class="allgarant__desctop" src="/local/assets/img/allgarant/kvadro.jpg" alt="" />
<img class="allgarant__mobile" src="/local/assets/img/allgarant/vertical-mobile.jpg" alt="" />
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>