<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("list");
?><div style="width:960px;min-height:300px;margin:auto;">
	 <?
$_SESSION["CATALOG_COMPARE_LIST"] = array(5 => array("ITEMS"=>array(3405,3406,3399)));
/*echo"<pre>";
print_r($_SESSION);
echo"</pre>";*/
?>
</div>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.compare.list",
	"",
	Array(
		"ACTION_VARIABLE" => "action",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"COMPARE_URL" => "compare.php",
		"DETAIL_URL" => "",
		"IBLOCK_ID" => "5",
		"IBLOCK_TYPE" => "ads",
		"NAME" => "CATALOG_COMPARE_LIST",
		"POSITION" => "top left",
		"POSITION_FIXED" => "Y",
		"PRODUCT_ID_VARIABLE" => "id"
	)
);?><?$APPLICATION->IncludeComponent(
	"bitrix:catalog.compare.result", 
	".default", 
	array(
		"ACTION_VARIABLE" => "action",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BASKET_URL" => "/personal/basket.php",
		"CONVERT_CURRENCY" => "N",
		"DETAIL_URL" => "",
		"DISPLAY_ELEMENT_SELECT_BOX" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_FIELD_BOX" => "name",
		"ELEMENT_SORT_FIELD_BOX2" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER_BOX" => "asc",
		"ELEMENT_SORT_ORDER_BOX2" => "desc",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "NAME",
			2 => "PREVIEW_TEXT",
			3 => "PREVIEW_PICTURE",
			4 => "DETAIL_TEXT",
			5 => "DETAIL_PICTURE",
			6 => "DATE_ACTIVE_FROM",
			7 => "DATE_ACTIVE_TO",
		),
		"HIDE_NOT_AVAILABLE" => "N",
		"IBLOCK_ID" => "5",
		"IBLOCK_TYPE" => "ads",
		"NAME" => "CATALOG_COMPARE_LIST",
		"PRICE_CODE" => array(
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PROPERTY_CODE" => array(
			0 => "YOUTUBE",
			1 => "IS_HYBRID",
			2 => "MODEL_YEAR",
			3 => "CITY",
			4 => "HAS_RF_MILEAGE",
			5 => "IS_FOREIGN",
			6 => "CONTACT_NAME",
			7 => "MANUFACTURER",
			8 => "MODEL",
			9 => "MODIFICATION",
			10 => "POWER",
			11 => "IS_NEW",
			12 => "OBMEN",
			13 => "OBMEN2",
			14 => "ENGINE",
			15 => "GENERATION",
			16 => "DRIVE",
			17 => "MILEAGE",
			18 => "STEERING_WHEEL",
			19 => "SERIE",
			20 => "STATE",
			21 => "PRICE",
			22 => "PHONE",
			23 => "TRANSMISSION",
			24 => "CAR_BODY",
			25 => "FUEL",
			26 => "COLOR",
			27 => "",
		),
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SHOW_PRICE_COUNT" => "1",
		"TEMPLATE_THEME" => "blue",
		"USE_PRICE_COUNT" => "N",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>