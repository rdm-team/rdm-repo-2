<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О компании");
$APPLICATION->SetPageProperty("not_show_nav_chain", "Y");
$APPLICATION->SetPageProperty("wrapper_class", "about");
?><style>
.page__main-wrapper._search._js-search{display:none;}
.page__header._fix .header{display:none !important}
</style>
<div class="bg_moz" style="background: url(/local/assets/img/banner_company1.jpg);margin-top: -40px; background-size:cover; color:white; min-height:585px;">
	<div class="offers-list__item">
		<div class="top-banner__info-title">
			<div class="b-title">
				<h1>Мы – компания с богатой историей</h1>
			</div>
		</div>
		<div class="offers-list__item-info">
			<div class="offers-list__item-desc">
				<div class="offers-list__item-text t-rgh"> 
					<p>
						 РДМ-Импорт старейшая компания в Новосибирске по продаже автомобилей. Мы начинали с привоза аукционных автомобилей из Японии, Кореи, Сингапура, Америки, Канады, прошли долгий путь становления и получения опыта. <br>
					</p>
					<p>На протяжении 16 лет работы наша цель: улучшать и облегчать жизнь людей помогая им покупать и продавать автомобили безопасно и цивилизовано.</p>
					<p>В 2017 году мы стали лучшим автосалоном Сибири по клиентскому сервису по версии ДубльГис и <a class="text__color-white" href="http://flamp.ru/">FLAMP.RU</a></p>
			
					<p>Мы продаем автомобили в отличном техническом состоянии с 4х кратной гарантией. И возвращаем деньги, если наши автомобили и наш сервис не устраивают клиента.</p>
 <span class="nav-link"> <a class="link-icon text__color-white more_about" href="/kompany/history/">Узнать больше о нас</a> </span>
				</div>
			</div>
		</div>
		 <!-- asdasdasd -->
		<div class="offers-list__item-info">
			<div class="offers-list__item-text">
				<p style="font-size:1.5em;">
 <b>Наши документы </b>
				</p>
 <b> </b>
				<p class="doc">
 <a class="gallery fancy-image" rel="group3" href="../local/assets/img/doc/svidetelstvo_o_reg.jpg">свидетельство о государственной регистрации юридического лица</a>
				</p>
				<p class="doc">
 <a class="gallery fancy-image" rel="group3" href="../local/assets/img/doc/svidetelstvo_post.jpg"> свидетельство о постановке на налоговый учет</a>
				</p>
				<p class="doc">
 <a  class="reckv_block onRecv">реквизиты компании</a><a class="gallery" target="_blank" rel="group3" href="../local/files/details.docx"> <!--реквизиты компании--></a>
				</p>
			</div>
			<div class="offers-list__item-desc">
				<div class="offers-list__item-text">
				</div>
			</div>
		</div>
	</div>
	<div style="max-width:940px; margin:0 auto;margin-top: -50px;">
 <img src="../local/assets/img/line.png" style="width:100%">
	</div>
</div>
<div class="offers-list__item">
	<div class="top-banner__info-title">
		<div class="b-title">
			<h2>У нас работают профессионалы и те, кто стремится к этому</h2>
		</div>
	</div>
	<div class="offers-list__item-text">
		<p>
			 Наше предназначение - обеспечивать жителей Новосибирска и регионов Сибири ТОЛЬКО качественными и проверенными автомобилями, чтобы выбор и покупка были удобными, безопасными, и приятными, а с машинами не было сюрпризов и проблем в будущем. <br>
 <br>
			 Для нас это не просто слова - это закон. Мы создаем цивилизованный рынок продажи автотранспорта: без обмана, без лжи, без унижения и хамства. Где продавец не боится смотреть в глаза, и отношения построены на взаимном уважении и выгоде. Мы приглашаем каждого, кому близки эти цели, в нашу команду.
		</p>
	</div>
	<div class="offers-list__item-info" style="padding-left:0">
		<div class="offers-list__item-desc">
			<div class="offers-list__item-text">
				<p class="car_lg">
 <b>Мы помогли выбрать 12850&nbsp;машин </b>
				</p>
 <b> </b>
				<p class="car_lg">
 <b>
					Мы помогли продать 3031&nbsp;машину</b>
				</p>
			</div>
		</div>
	</div>
	<div class="offers-list__item-info" style="padding-left:0">
		<div class="offers-list__item-desc">
			<div class="offers-list__item-text">
				<div class="_read-more">
					<div id="video_smotr" class="offers-list-links__item-link eventVideo" data-src="ut01hN2xfak">
						 Посмотрите видео
					</div>
				</div>
			</div>
		</div>
	</div>
	 <!-- asdasdasd -->
	<div>
		<div class="offers-list__item-text" style="text-align: center;">
			<p>
			</p>
		</div>
	</div>
</div>
<div class="wrap-full banner">
 <img src="/local/assets/img/banner_company.webp" class="banner__img">
	<div class="banner__nav">
		<div class="comand eventVideo" data-src="ZdsOB105t_k">
			 Приглашаем в команду
		</div>
	</div>
</div>
 <!--div class="offers-list-links__item-link comand eventVideo" data-src="ZdsOB105t_k">Приглашаем в команду</div--> <!-- Nagradi -->
<div class="offers-list__item">
	<div class="top-banner__info-title">
		<div class="b-title">
			<h2>Наши награды</h2>
		</div>
	</div>
	<div class="offers-list__item-text">
		<p>
			 Больше всего мы ценим надежность и стабильность в отношениях с партнерами, клиентами и поставщиками. За 14 лет работы РДМ-Импорт получил десятки подтверждений. Нам доверяют серьезные компании, такие как Альфа-Банк, ВТБ24, Сбербанк, Сургутнефтегаз и многие многие другие. С нами вы можете быть уверены в том, что работа будет сделана до конца и своевременно, а результат соответствует ожиданиям.
		</p>
	</div>
	 <!-- asdasdasd -->
	<div style="max-width:1240px; margin:40px auto 15px;">
		 <?$APPLICATION->IncludeComponent(
	"hmweb:medialibrary.slider",
	"template1",
	Array(
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CAROUSEL_WRAP" => "circular",
		"COLLECTIONS" => array(0=>"1",),
		"COMPONENT_TEMPLATE" => ".default",
		"COUNT_ELEMENT_DEFAULT" => "4",
		"RESIZE_TYPE" => "BX_RESIZE_IMAGE_EXACT",
		"START_ELEMENT_SLIDER" => "1",
		"TEMPLATE_THEME" => "blue",
		"THUMB_IMG_HEIGHT" => "300",
		"THUMB_IMG_WIDTH" => "150",
		"USE_AUTOSCROLL" => "N",
		"USE_JQUERY" => "N",
		"USE_PAGINATION" => "N",
		"USE_THUMB_IMG" => "Y"
	)
);?>
	</div>
</div>
 <!--Выданные авто--> <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"lost-auto1",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "lost-auto",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"NAME",1=>"",),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "15",
		"IBLOCK_TYPE" => "-",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "2",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"",1=>"Марка",2=>"Модель",3=>"",),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>
<div class="offers-list__item">
	<div class="top-banner__info-title">
		<div class="b-title">
			<h2>Отзывы о нашей работе</h2>
			 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"video_otziv_in",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "video_otziv_in",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"",1=>"",),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "16",
		"IBLOCK_TYPE" => "videos_youtube",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"VIDEO_LINK_OTZIV",1=>"Ссылка",2=>"",),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>
			<div class="all_otzivi">
 <a href="https://www.youtube.com/playlist?list=PLRo6ODoWfWaRWlRyn_7Ds6Qjt8e7jnMNO" target="_blank">Смотреть все отзывы</a>
			</div>
		</div>
	</div>
	<div class="rdm-flamp">
 <a class="flamp-widget" href="https://novosibirsk.flamp.ru/firm/rdm_import_ooo_avtosalon-141265769513599" data-flamp-widget-type="responsive-new" data-flamp-widget-count="1" data-flamp-widget-id="141265769513599" data-flamp-widget-width="100%">Отзывы о нас на Флампе</a><script>!function(d,s){var js,fjs=d.getElementsByTagName(s)[0];js=d.createElement(s);js.async=1;js.src="//widget.flamp.ru/loader.js";fjs.parentNode.insertBefore(js,fjs);}(document,"script");</script>
	</div>
</div>
<div class="offers-list__item">
	<div class="top-banner__info-title">
		<div class="b-title">
			<h2>Гарантии</h2>
		</div>
	</div>
	<div class="offers-list__item-text">
		 <!--p>
			 Каждый автомобиль проходит тщательную проверку технического состояния, по результатам которой в электронном виде выдается диагностический лист и сертификат качества, гарантирующий исправность. Каждое транспортное средство мы проверяем на юридическую чистоту, ограничения и залог. При покупки проводим должное оформление полного комплекта документов на фирменном бланке в строгом соответствии с законодательством. Мы гарантируем отсутствие проблем при постановке автомобиля на учет и сдаче налоговой отчетности.
		</p>
		<p>
 <br>
			 Для нас гарантии являются незыблемыми при любых обстоятельствах: это больше, чем обязательства перед покупателями, это лицо компании и основа для движения вперед!
		</p-->
	</div>
	<div class="garantii_list">
		<div class="garant__block garant__icon-1"><a class="garant__link" href="#garant_2gis">Лучший автосалон по версии 2ГИС </a>  </div>
		<div class="garant__block garant__icon-2"><a class="garant__link" href="#garant_yur">Гарантия юридической защищенности </a> </div>
		<div class="garant__block garant__icon-3"><a class="garant__link" href="#garant_secure">Гарантия технической безопасности </a></div>
		<div class="garant__block garant__icon-4"><a class="garant__link" href="#garant_probeg">Гарантия реального пробега </a></div>
		<div class="garant__block garant__icon-5"><a class="garant__link" href="#garant_money">Гарантия возврата денег</a></div> <br>
		<div style="text-align:center;">
			 <iframe style="width:100%; margin:15px auto; height:40vh;" src="https://www.youtube.com/embed/iNL4UQY9Ocs" frameborder="0" allowfullscreen></iframe>
		</div>
	</div>
</div>
<div class="irongarant">
	<div class=" irongarant__item irongarant__variant-1" id="garant_2gis" style="background-image:url('/local/assets/img/_MG_8232.jpg')">
		<div class="irongarant__block irongarant__textblock">
			<div class="irongarant__inner textblock-style">

 <span class="irongarant__title">Лучший автосалон по версии 2ГИС</span> 
 <span class="irongarant__text">
				В 2017 году мы заняли первое место среди всех автосалонов Новосибирска в конкурсе «Премия 2ГИС». Мы получили самые высокие оценки пользователей 2ГИС</span>

			</div>
		</div>
		<div class="irongarant__block irongarant__mediablock">
			<div class="irongarant__inner mediablock-style">
 <img src="/local/assets/img/garant/2gis_nagrada.png" class="irongarant__img" alt="...">
			</div>
		</div>
		<div class="irongarant__layout">
		</div>
	</div>
	<div class=" irongarant__item irongarant__variant-2" id="garant_yur" style="background-image:url('/local/assets/img/_MG_8232.jpg')">
		<div class="irongarant__block irongarant__textblock">
			<div class="irongarant__inner textblock-style">
 <span class="irongarant__title">Сертификат проверки автомобиля</span> <span class="irongarant__text">
				Каждый автомобиль имеет Сертификат проверки автомобиля - это официальная бумага с водяными знаками&nbsp; и уникальным номером, который гарантирует пожизненную гарантию на юридическую чистоту автомобиля&nbsp; и возврат денег&nbsp; в случае возникновения такой ситуации. Каждый сертификат застрахован и его подлинность можно проверить в интернете.&nbsp; </span>
			</div>
		</div>
		<div class="irongarant__block irongarant__mediablock">
			<div class="irongarant__inner mediablock-style">
 <img src="/local/assets/img/garant/sertificat.jpg" class="irongarant__img" alt="...">
			</div>
		</div>
		<div class="irongarant__layout">
		</div>
	</div>
	<div class=" irongarant__item irongarant__variant-1" id="garant_secure" style="background-image:url('/local/assets/img/_MG_8232.jpg')">
		<div class="irongarant__block irongarant__textblock">
			<div class="irongarant__inner textblock-style">
 <span class="irongarant__title">Железная гарантия</span> <span class="irongarant__text">
				Каждый автомобиль имеет&nbsp; "Железную&nbsp;Гарантию"&nbsp;-&nbsp;сертификат на&nbsp;бесплатную программу фирменной гарантии без привязки к&nbsp;конкретным станциям тех обслуживания. </span>
			</div>
		</div>
		<div class="irongarant__block irongarant__mediablock">
			<div class="irongarant__inner mediablock-style">
 <img src="/local/assets/img/garant/garant.jpg" class="irongarant__img" alt="...">
			</div>
		</div>
		<div class="irongarant__layout">
		</div>
	</div>
	<div class=" irongarant__item irongarant__variant-2" id="garant_probeg" style="background-image:url('/local/assets/img/_MG_8232.jpg')">
		<div class="irongarant__block irongarant__textblock">
			<div class="irongarant__inner textblock-style">
 <span class="irongarant__title">Отвечаем деньгами
за реальный пробег!</span> 
 <span class="irongarant__text">
				Наши автомобили имеют гарантированно не скрученный пробег. Мы проверяем пробег автомобилей через сервис «АвтоКод» и по другим базам. 
				Если вы найдете у нас автомобиль со скрученным пробегом и предоставите доказательство, то мы заплатим вам 50 000 рублей за помощь в борьбе 
				с нарушением 
				правил компании.</span>
			</div>
		</div>
		<div class="irongarant__block irongarant__mediablock">
			<div class="irongarant__inner mediablock-style">
 <img src="/local/assets/img/garant/sertificato.jpg" class="irongarant__img" alt="...">
			</div>
		</div>
		<div class="irongarant__layout">
		</div>
	</div>
		<div class=" irongarant__item irongarant__variant-1" id="garant_money" style="background-image:url('/local/assets/img/_MG_8232.jpg')">
		<div class="irongarant__block irongarant__textblock">
			<div class="irongarant__inner textblock-style">
				 <span class="irongarant__title">Гарантия возврата денег</span> 
				 <span class="irongarant__text">
				При наступлении гарантийного случая мы ремонтируем или возмещаем стоимость ремонта. Если вам это не подходит - возвращаем деньги за автомобиль</span>
			</div>
		</div>
		<div class="irongarant__block irongarant__mediablock">
			<div class="irongarant__inner mediablock-style">
 			<img src="/local/assets/img/money_garant.png" class="irongarant__img" alt="...">
			</div>
		</div>
		<div class="irongarant__layout">
		</div>
	</div>
</div>
<div class="autocheck">
	<div class="b-title">
		<h3 class="autocheck__h3">Проверить свой автомобиль</h3>
	</div>
	<form class="autocheck__form" id="autocheck__form">
		<div class="autocheck__row">
			<div class="autocheck__col autocheck__col-input">
				<input class="autocheck__win" type="text" name="autocheck__win" placeholder="Введите VIN или номер кузова автомобиля">
			</div>
			<div class="autocheck__col autocheck__col-button">
				<button class="autocheck__btn" id="onCheckauto">Проверить автомобиль</button>
			</div>
		</div>
	</form>
	<div class="autocheck__result" id="autocheck__result">

	</div>
</div>
<div class="rekv__list" id="bl_list">
	<div class="recveziti" id="recvz">
		<div id="pr">
			<p>
 <b>Управляющий Облецов Роман Алексеевич, на основании Устава</b>
			</p>
			<p>
 <br>
				 ООО "РДМ-Импорт" <br>
				 ИНН 5406352193 / КПП 540101001 <br>
				 ОКПО 94909538 <br>
				 Адрес: 630112 Новосибирск Фрунзе 61/2 А/я 160 <br>
				 р/с 40702810700330001207 Новосибирский филиал ПАО АКБ «Связь-Банк» <br>
				 к/с 30101810100000000740 БИК 045004740 <br>
				 тел: (383) 32-812-32
			</p>
		</div>
		<p>
 <a class="gallery" rel="group3" href="../local/files/details.docx">Скачать</a> <a href="#" class="print">Печать</a>
		</p>
	</div>
	<div class="recveziti video_sm">
	</div>
	<div class="recveziti video2_sm">
		 <!--<iframe id="Youtube" style="width:100%; height:240px;" src="https://www.youtube.com/embed/ZdsOB105t_k" frameborder="0" allowfullscreen></iframe>-->
	</div>
</div>
 <?if(true){?> <script src="//www.youtube.com/player_api"></script> <script>
	
$(document).ready(function(){
	var html_to_print=$('#pr').html();
	var iframe=$('<iframe id="print_frame">');
	$('body').append(iframe); //добавляем эту переменную с iframe в наш body (в самый конец)
	var doc = $('#print_frame')[0].contentDocument || $('#print_frame')[0].contentWindow.document;
	var win = $('#print_frame')[0].contentWindow || $('#print_frame')[0];
	$('#print_frame').hide();
	doc.getElementsByTagName('body')[0].innerHTML=html_to_print;
	$('a.print').click(function(){
		win.print();
	})
	
	function onYouTubePlayerAPIReady() {
	  player = new YT.Player('Youtube', {
		events: {'onReady': onPlayerReady}
	  });
	}

	/*$('.reckv_block').click(function(){
		$('.black_list').show();
		$('.recveziti').show();
		$('.video2_sm').hide();
		$('.video_sm').hide();
	})*/

	//document.getElementById('bl_list').onclick = function() {play.pauseVideo();};
})
<!--  AUTO -->

</script> <br>
 <style>
#googmap{height:340px;overflow:hidden; margin-bottom:30px;}
ymaps:nth-child(3) {
    display: none;
}
</style>
<div id="googmap">
</div>
	<script>

      function initMap() {
        var myLatLng = {lat: 55.040644, lng: 82.952241};

        var map = new google.maps.Map(document.getElementById('googmap'), {
          zoom: 15,
		  navigationControl: false,
		  scrollwheel: false,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'РДМ-Импорт'
        });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCS82TBE8pGffUUwzy3iSh-ZBqWXeT0_pw&callback=initMap">
    </script>
<?}?>
<?$APPLICATION->IncludeComponent(
	"vegas:winVideo",
	".default",
Array()
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>