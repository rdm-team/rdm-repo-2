<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("История");
?>
<?$APPLICATION->IncludeComponent(
	"myhistory:slider", 
	".default", 
	array(
		"IBLOCK_ID" => "17",
		"NEWS_COUNT" => "10",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_TEMPLATE" => "ajax_more",
		"IBLOCK_TYPE" => "History",
		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "ASC",
		"CACHE_TYPE" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"COMPONENT_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"INTERVAL" => "3",
		"SPEED" => "500"
	),
	false
);?>
<?if(true){?>
	 <?$APPLICATION->IncludeComponent("bitrix:main.include", "pagetitle", Array(
	"AREA_FILE_SHOW" => "file",	// Показывать включаемую область
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",	// Шаблон области по умолчанию
		"PATH" => "page_title_inc.php",	// Путь к файлу области
	),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"myhistory:history.list", 
	".default", 
	array(
		"IBLOCK_ID" => "18",
		"NEWS_COUNT" => "10",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_TEMPLATE" => "ajax_more",
		"IBLOCK_TYPE" => "History",
		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "DESC",
		"CACHE_TYPE" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "ALBUM",
			2 => "SHOW_DESCR",
		),
		"COMPONENT_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"PAGER_TITLE" => "Блоки",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => ""
	),
	false
);?>

	 <?$APPLICATION->IncludeComponent("bitrix:main.include", "history-description", Array(
	"AREA_FILE_SHOW" => "file",	// Показывать включаемую область
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",	// Шаблон области по умолчанию
		"PATH" => "history_end_inc.php",	// Путь к файлу области
	),
	false
);?>

	 <?$APPLICATION->IncludeComponent("bitrix:main.include", "evo-description", Array(
	"AREA_FILE_SHOW" => "file",	// Показывать включаемую область
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",	// Шаблон области по умолчанию
		"PATH" => "evo_inc.php",	// Путь к файлу области
	),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"myhistory:evolution.list", 
	"grid", 
	array(
		"IBLOCK_ID" => "19",
		"NEWS_COUNT" => "10",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_TEMPLATE" => "ajax_more",
		"IBLOCK_TYPE" => "History",
		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "DESC",
		"CACHE_TYPE" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PROPERTY_CODE" => array(
			0 => "SHOW_DESCR",
			1 => "ALBUM",
			2 => "",
		),
		"COMPONENT_TEMPLATE" => "grid",
		"DISPLAY_TOP_PAGER" => "N",
		"PAGER_TITLE" => "Блоки",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => ""
	),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"myhistory:chronology.list", 
	".default", 
	array(
		"IBLOCK_ID" => "20",
		"NEWS_COUNT" => "10",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_TEMPLATE" => "ajax_more",
		"IBLOCK_TYPE" => "History",
		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "DESC",
		"CACHE_TYPE" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "ALBUM",
			2 => "SHOW_DESCR",
		),
		"COMPONENT_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"PAGER_TITLE" => "Блоки",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => ""
	),
	false
);?>

	 <?$APPLICATION->IncludeComponent("bitrix:main.include", "textblock", Array(
	"AREA_FILE_SHOW" => "file",	// Показывать включаемую область
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",	// Шаблон области по умолчанию
		"PATH" => "textblock_inc.php",	// Путь к файлу области
	),
	false
);?>

	 <?$APPLICATION->IncludeComponent("bitrix:main.include", "textblock", Array(
	"AREA_FILE_SHOW" => "file",	// Показывать включаемую область
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",	// Шаблон области по умолчанию
		"PATH" => "text_inc.php",	// Путь к файлу области
	),
	false
);?>

<?$APPLICATION->IncludeComponent("myhistory:webForm", "template1", Array(
	"FORM_ID" => "4",
		"AJAX_MODE" => "Y",	// Включить режим AJAX
	),
	false
);?>
<?}?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>