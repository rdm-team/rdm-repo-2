<?php 
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>

<?php

if(!strlen($_REQUEST["captcha_word"])>0){ 
   $err .= "! Не введен защитный код<br/>"; 
   $status = false;
} 
elseif(!$APPLICATION->CaptchaCheckCode($_REQUEST["captcha_word"], $_REQUEST["captcha_sid"])){
   $err .= "! Код с картинки заполнен не правильно<br/>";    
   $status = false;
}else{
	$status = true;
	
} 

echo json_encode(array(
              
                            //"REMOTE_ADDR"=>$_SERVER["REMOTE_ADDR"],
                            //"recaptchaResponse"=>$_REQUEST["g-recaptcha-response"],
                            "_REQUEST"=>$_REQUEST,
                            "err"=>$err,
                            "res"=>$res,
                            "status"=>$status,

));

?>