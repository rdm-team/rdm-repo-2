<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$oAssets = \Bitrix\Main\Page\Asset::getInstance();
$oAssets->addJs(ASSETS_PATH . '/js/plugins/jquery.easing.js');
$oAssets->addJs(ASSETS_PATH . '/js/plugins/jquery.dimensions.js');
$oAssets->addJs(ASSETS_PATH . '/js/plugins/jquery.accordion.js');

$APPLICATION->SetAdditionalCSS(ASSETS_PATH . 'css/sell-auto.css');

$APPLICATION->SetPageProperty("wrapper_class", "autocreditovanie");
$APPLICATION->SetPageProperty("not_show_nav_chain", "Y");
$APPLICATION->SetTitle("Автокредит");
?>

<div class="bg_moz sell__banner avtocredit__pos" style="background-image: url(/local/assets/img/pasha_c_kartoi.jpg);">

	<div class="sell__wrap">

		<div class="sell__half bground-black">

			<h1 class="sell__h1">Автокредитование</h1>

			<div class="complexForm__title2">Рассчитать <span class="text__color-red">выгодный</span> автокредит:</div>
			<div class="credit__calc-row">
				<label class="credit__calc-label1">Стоимость автомобиля:</label>
				<input class="credit__calc-input1" type="number" name="result__stoim" id="result__stoim" pattern="\d [0-9]" value="" />
			</div>
			<div class="credit__calc-row">
				<label class="credit__calc-label1">Первоначальный платёж, руб:</label>
				<input class="credit__calc-input1" type="number" name="result__vznos" id="result__vznos" pattern="\d [0-9]" value="0" />
			</div>
			<div class="credit__calc-row">
				<label class="credit__calc-label2">Желаемый срок кредита (лет):</label>
				<input class="credit__calc-input2" type="number" name="result__srok" id="result__srok" pattern="^[ 0-9]+$" value="" />
			</div>
			<div class="credit__name">Ежемесячный платеж по кредиту, руб.</div>
			<div class="credit__desc" id="result_credit">
				<span class="credit__desc-msg">Чтобы увидеть сумму платежа в месяц заполните первоначальный взнос и срок кредита</span>
				<span class="credit__desc-min" id="result__min"></span>
				<span class="credit__desc-max" id="result__max"></span>
			</div>



			<button class="offers-list__item-action-link onFormOpen" data-type="credit" data-mtype="" data-target="yakartcredit" data-cpth="true" data-layout="layout-1">
				<input type="hidden" name="result__min" value="" />
				<input type="hidden" name="result__max" value="" />
				<input type="hidden" name="credit__stoim" value="">
				<input type="hidden" name="credit__vznos" value="">
				<input type="hidden" name="credit__srok" value="">
				<span class="info__credit-number-text">
					Отправить заявку
				</span>
			</button>

			<?CJSCore::Init(array("jquery"));?>
			<div class="complexForm" id="complex__form" style="display:none">
				<div class="complexForm-html" id="complexForm__html"></div>
			</div>
			<div class="complexForm__callback" id="complexForm__callback" style="display:none">
				<div class="complex__header"><img class="complex__headerLogo" src="/local/assets/img/assets/header/logo.svg" alt="" /></div>
				<div id="complexForm__callback-html" class="complexForm__callback-html"></div>
			</div>
		</div>

	</div>
</div>

<div class="offers-list__item">
	<div class="top-banner__info-title">
		<div class="b-title">
			<h2>Преимущества автокредитования у нас:<br></h2>
		</div>
	</div>
	<div class="offers-list__item-info adventage">
		<div class="offers-list__item-desc">
			<div class="offers-list__item-text">
				<p class="rassmotr-zayav">
					 Рассмотрение заявки 14 банками в течение 1го дня
				</p>
				<p class="poluchenie">
					 9 из 10 заявок получают положительное решение<br>
				</p>
				<p class="zaschita">
					 100% защита ваших данных от мошенничества
				</p>
				<p class="result-oplata">
					 Оплата только за результат<br>
				</p>
			</div>
		</div>
	</div>
	 <!-- asdasdasd -->
	<div class="offers-list__item-info adventage">
		<div class="offers-list__item-text">
			<p class="procent">
				 9,9% - низкий процент за счет партнерских программ<br>
			</p>
			<p class="rinok">
				 15 лет надежной работы компании<br>
			</p>
			<p class="doc">
				 Простой и понятный договор оказания услуг
			</p>
			<p class="doci">
				 Все документы официально оформлены и в соответствии с законом
			</p>
		</div>
	</div>
</div>

 <div class="gray_bg" style="position: relative; overflow:hidden;">
<div class="offers-list__item vopros-block">
	<div class="top-banner__info-title">
		<div class="b-title">

		</div>
	</div>
	<div class="auto-block">
		<div class="offers-list__item-desc ">
			<h2>Самые популярные вопросы об автокредите</h2>
			<div class="offers-list__item-text voprosi accordeon">
				 <?$APPLICATION->IncludeComponent(
					"bitrix:support.faq.element.list",
					"mytpl1",
					Array(
						"AJAX_MODE" => "N",
						"AJAX_OPTION_ADDITIONAL" => "",
						"AJAX_OPTION_HISTORY" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "Y",
						"CACHE_GROUPS" => "Y",
						"CACHE_TIME" => "36000000",
						"CACHE_TYPE" => "A",
						"IBLOCK_ID" => "14",
						"IBLOCK_TYPE" => "services",
						"PATH_TO_USER" => "",
						"RATING_TYPE" => "",
						"SECTION_ID" => "1",
						"SHOW_RATING" => ""
					)
				);?><br>
			</div>
			<div class="offers-list__item-actions btn-lft">
				<div class="offers-list__item-action _calc">
					<div class="offers-list__item-action-link onEventOrder" data-form="zadatvopros" data-target="yazadatvopros">Задать вопрос</div>
				</div>
			</div>
		</div>
	</div>
	<div class="auto-block">
		<div class="offers-list__item-desc" style="margin-top: 15px;">
			 <!--h2>Потребительское кредитование</h2-->
			<div class="offers-list__item-text" style="display:none">
				 <? if ($USER->IsAuthorized()){ ?>
				<div class="calculator">
					 Кредитный калькулятор
				</div>
				<div class="calculator">
					 Подбор банка
				</div>
				 <? }?>
			</div>
		</div>
	</div>
</div>
<div id="trailer" class="is_overlay" style="height:100vh;">
	<video id="video" width="100%" height="auto" autoplay="autoplay" loop="loop" preload="auto">
		<source src="/local/assets/img/credit.webm" type="video/webm"></source>
		<source src="/local/assets/img/credit.mp4" type="video/mp4"></source>
	</video>
	<div class="video-bg-layer"></div>
</div>
</div>
<div class="offers-list__item">
	<h2>Доступно о&nbsp;кредитах</h2>
	<div class="offers-list__item-desc">
		<div class="offers-list__item-text">
			 <!--Lorem.. -->
		</div>
	</div>
	<div class="list_video">
		<div class="video_item">
			 <iframe style="width:100%; height:100%;" src="https://www.youtube.com/embed/qyDVhffuoxg" frameborder="0" allowfullscreen></iframe>
			<div class="author_video">
				 Ставка по кредиту от 9.9%, как на новые авто!
			</div>
		</div>
		<div class="video_item">
			 <iframe style="width:100%; height:100%;" src="https://www.youtube.com/embed/I31TNX2UfXo" frameborder="0" allowfullscreen></iframe>
			<div class="author_video">
				 Что такое "Первый льготный платеж" по кредиту?
			</div>
		</div>
		<div class="video_item">
			 <iframe style="width:100%; height:100%;" src="https://www.youtube.com/embed/bVep-XvelEU" frameborder="0" allowfullscreen></iframe>
			<div class="author_video">
				 Ответы на вопросы по кредитованию от руководителя кредитного отдела компании РДМ-Импорт
			</div>
		</div>
		<div class="offers-list__item-actions btn-under-video">
			<div class="offers-list__item-action _calc">
				<div class="offers-list__item-action-link onEventOrder" data-form="credit" data-target="yacredit2">Оставить заявку на кредит</div>
			</div>
		</div>
	</div>
</div>
 <!--                      BANKS  -->
<div class="offers-list__item">
	<h2>Наши банки&nbsp;-&nbsp;партнеры</h2>
	<div class="offers-list__item-desc">
		<div class="offers-list__item-text">
			 <!-- Lorem.. -->
		</div>
	</div>
	<div class="list_bank">
		<div class="bank_item">
		<img src="/local/assets/img/bank/1.png">
		</div>
		<div class="bank_item">
		<img src="/local/assets/img/bank/2.png">
		</div>
		<div class="bank_item">
		<img src="/local/assets/img/bank/3.png">
		</div>
		<div class="bank_item">
		<img src="/local/assets/img/bank/4.png">
		</div>
		<div class="bank_item">
		<img src="/local/assets/img/bank/5.png">
		</div>
		<div class="bank_item">
		<img src="/local/assets/img/bank/6.png">
		</div>
		<div class="bank_item">
		<img src="/local/assets/img/bank/7.png">
		</div>
		<div class="bank_item">
		<img src="/local/assets/img/bank/8.png">
		</div>
		<div class="bank_item">
		<img src="/local/assets/img/bank/9.png">
		</div>
		<div class="bank_item">
		<img src="/local/assets/img/bank/10.png">
		</div>
		<div class="bank_item">
		<img src="/local/assets/img/bank/11.png">
		</div>
		<div class="bank_item">
		<img src="/local/assets/img/bank/12.png">
		</div>
		<div class="bank_item">
		<img src="/local/assets/img/bank/13.gif">
		</div>
		<div class="bank_item">
		<img src="/local/assets/img/bank/14.png">
		</div>
		<div class="bank_item">
		<img src="/local/assets/img/bank/15.png">
		</div>
		<div class="bank_item">
		<img src="/local/assets/img/bank/16.png">
		</div>
		<div class="bank_item">
		<img src="/local/assets/img/bank/17.gif">
		</div>
		<div class="bank_item">
		<img src="/local/assets/img/bank/18.png">
		</div>
		<div class="offers-list__item-actions">
			<div class="offers-list__item-action _calc">
				<div class="offers-list__item-action-link onEventOrder" data-form="bank" data-target="yabank">Подобрать банк</div>
			</div>
		</div>
	</div>
</div>
 <?$APPLICATION->IncludeComponent(
	"vegas:orderForm",
	"autocreditovanie",
	array(
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"COMPONENT_TEMPLATE" => "autocreditovanie",
		"EMAIL_TEMPLATE" => "54",
		"REQUIRED_FIELDS" => "order_email",
		"WEB_FORM_ID" => "ORDER_FORM",
		"LEAD_CODE" => array("zadatvopros","credit","bank")
	),
	false
);?>
<?if(false){?>
<div class="form_back" id="creditRequest" style="display: none;">
	<img src="/local/assets/img/logosm.png"><br>

 <?$APPLICATION->IncludeComponent(
	"api:feedbackex",
	"uikit",
	array(
		"API_FEX_FORM_ID" => "FORM9",
		"BCC" => "",
		"COLOR" => "default",
		"COMPONENT_TEMPLATE" => "uikit",
		"DATETIME" => "",
		"DIR_URL" => "",
		"DISABLE_SEND_MAIL" => "N",
		"DISPLAY_FIELDS" => array(
			0 => "TITLE",
			1 => "EMAIL",
			2 => "PHONE",
			3 => "MESSAGE",
		),
		"EMAIL_ERROR_MESS" => "Указанный E-mail некорректен",
		"EMAIL_TO" => "eg.grean@gmail.com",
		"FIELD_ERROR_MESS" => "#FIELD_NAME# обязательное",
		"FIELD_NAME_POSITION" => "stacked",
		"FIELD_SIZE" => "default",
		"FORM_AUTOCOMPLETE" => "Y",
		"FORM_CLASS" => "",
		"FORM_FIELD_WIDTH" => "",
		"FORM_LABEL_TEXT_ALIGN" => "left",
		"FORM_LABEL_WIDTH" => "150px",
		"FORM_SUBMIT_CLASS" => "uk-button uk-width-1-1",
		"FORM_SUBMIT_STYLE" => "",
		"FORM_SUBMIT_VALUE" => "Отправить",
		"FORM_TEXTAREA_ROWS" => "5",
		"FORM_TITLE" => "Обратная связь",
		"FORM_TITLE_LEVEL" => "3",
		"FORM_WIDTH" => "",
		"HIDE_ASTERISK" => "N",
		"HIDE_FIELD_NAME" => "Y",
		"MAIL_SEND_USER" => "N",
		"MAIL_SUBJECT_ADMIN" => "#SITE_NAME#: Сообщение из формы обратной связи",
		"MAIL_SUBJECT_USER" => "#SITE_NAME#: Копия сообщения из формы обратной связи",
		"MODAL_BTN_CLASS" => "api_button",
		"MODAL_BTN_ID" => "shw",
		"MODAL_BTN_SPAN_CLASS" => "api_icon",
		"MODAL_BTN_TEXT" => "Обратная связь",
		"MODAL_FOOTER_TEXT" => "",
		"MODAL_HEADER_TEXT" => "Обратная связь",
		"MODAL_ID" => "#API_FEX_MODAL_FORM9",
		"OK_TEXT" => "Спасибо!",
		"OK_TEXT_AFTER" => "Мы рассмотрим сообщение и обязательно свяжемся с Вами.<br>Пожалуйста, дождитесь ответа.",
		"PAGE_TITLE" => "",
		"PAGE_URL" => "",
		"REPLACE_FIELD_FROM" => "Y",
		"REQUIRED_FIELDS" => array(
			0 => "PHONE",
		),
		"SCROLL_SPEED" => "1000",
		"THEME" => "gradient",
		"TITLE_DISPLAY" => "N",
		"USE_AUTOSIZE" => "Y",
		"USE_JQUERY" => "Y",
		"USE_MODAL" => "N",
		"USE_PLACEHOLDER" => "N",
		"USE_SCROLL" => "Y",
		"WRITE_MESS_DIV_STYLE" => "padding:10px;border-bottom:1px dashed #dadada;",
		"WRITE_MESS_DIV_STYLE_NAME" => "font-weight:bold;",
		"WRITE_MESS_DIV_STYLE_VALUE" => "",
		"WRITE_MESS_FILDES_TABLE" => "N",
		"WRITE_MESS_TABLE_STYLE" => "border-collapse: collapse; border-spacing: 0;",
		"WRITE_MESS_TABLE_STYLE_NAME" => "max-width: 200px; color: #848484; vertical-align: middle; padding: 5px 30px 5px 0px; border-bottom: 1px solid #e0e0e0; border-top: 1px solid #e0e0e0;",
		"WRITE_MESS_TABLE_STYLE_VALUE" => "vertical-align: middle; padding: 5px 30px 5px 0px; border-bottom: 1px solid #e0e0e0; border-top: 1px solid #e0e0e0;",
		"LEAD_CODE"=>array("zadatvopros","credit","bank")
	),
	false
);?>
</div>
<?}?>
<link rel="stylesheet" type="text/css" href="style.css">
<script type="text/javascript" src="script.js"></script>
 <script>
$(".accordeon .accordeon_slide").hide().prev().click(function() {
	$(this).parents(".accordeon").find(".accordeon_slide").not(this).slideUp().prev().removeClass("active");
	$(this).next().not(":visible").slideDown().prev().addClass("active");
});
</script> <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>