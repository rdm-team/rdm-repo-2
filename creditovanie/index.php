<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Автомобили с пробегом в кредит, автокредит на подержанные б/у авто");
$APPLICATION->SetAdditionalCSS(ASSETS_PATH . 'css/sell-auto.css');
$APPLICATION->SetTitle("Кредитование");
$APPLICATION->SetPageProperty("not_show_nav_chain", "Y");
$APPLICATION->SetPageProperty("wrapper_class", "creditovanie");
?>
<style>
.page__main-wrapper._search._js-search{display:none;}
.page__header._fix .header{display:none !important}
.offers-list__item-action-link {padding: 11px 20px 8px 54px;}
</style>

<div class="bg_moz sell__banner credit__pos" style="background-image: url('/local/assets/img/_MG_9186-1_(1).jpg');">
	<div class="sell__wrap">
		
		<div class="sell__half bground-black">	
					
			<h1 class="sell__h1">Кредитование - первый шаг к Вашей свободе</h1>

			<p>
				 Предназначение отдела кредитования РДМ-Импорт: обеспечить обратившихся к нам людей деньгами на покупку автомобиля.
			</p>
			<p>
				 Не будем красноречиво говорить, скажем только о трех наших преимуществах:
			</p>
			<ul>
				<li>действительно получаем для вас деньги! </li>
				<li>действительно на самых выгодных условиях, как для себя</li>
				<li>действительно «не впариваем»</li>
			</ul>
			<p>
				 Нужны деньги быстро, под хорошие условия, и без лишних переплат - обращайтесь! Мы знаем свое дело. Точка.
			</p>
		
		</div>
	</div>
</div>

<div class="offers-list__item">
	<div class="top-banner__info-title">
		<div class="b-title">
			<h2>Автокредитование </h2>
		</div>
	</div>
	<div class="g-show-mobile">
		<div class="offers-list__item-text">
 <img src="../local/assets/img/_MG_2457_(1).jpg" style="width:100%">
		</div>
	</div>
	<div class="offers-list__item-info">
		<div class="offers-list__item-desc">
			<div class="offers-list__item-text">
				<p>
					 Обратившись к нам мы можем вам гарантировать:
				</p>
				<ul>
					<li>честный выбор оптимального кредита от 14 гибких банков</li>
					<li>быстрое рассмотрение заявки</li>
					<li>минимально существующий на рынке процент по кредиту (от 7.5%)</li>
					<li>экономию от 40 тысяч сразу при оформлении!</li>
				</ul>
				<p>
					 Знаем, так все говорят, но мы делаем! Согласитесь, разница в этом есть.
				</p>
				<p>
					 Также вы можете рассчитывать на дружелюбную обстановку, разумность сотрудников, понимание и искренность к вам.
				</p>
				<div class="offers-list-links _read-mores">
					<div class="offers-list-links__item _read-more">
						<a href="/creditovanie/autocreditovanie/" class="offers-list-links__item-link  more-big">Узнать больше</a>
					</div>
				</div>
				<div class="offers-list__item-actions">
					<div class="btn-cred-link onEventOrder" data-form="avtokreditovanie" data-target="yacredit1">Оставить заявку</div>
				</div>
			</div>
		</div>
	</div>
	<div class="offers-list__item-info g-show-desctop">
		<div class="offers-list__item-text">
			<img src="/local/assets/img/_MG_2457_(1).png" style="width:100%">
		</div>
	</div>
</div>
<div class="offers-list__item potreb">
	<div class="offers-list__item-info">
		<div class="offers-list__item-desc">
 <img src="../local/assets/img/credit/22.jpg" style="width:100%;max-width:380px;">
		</div>
	</div>
	<div class="offers-list__item-info">
		<div class="offers-list__item-desc">
			<div class="top-banner__info-title">
				<div class="b-title">
					<h2>Потребительское кредитование</h2>
				</div>
			</div>
			<div class="offers-list__item-text">
				<p>
					 Честно сказать, мы не занимаемся получением потребительского кредита потому, что он в конечном итоге в среднем на 27% для вас менее выгоден, чем автокредит. Но, если вы нам позвоните, мы сможем рассказать, откуда там вообще могут быть такие цифры, когда обещают 10-16%. И если не передумаете, то хотя бы будете знать это.
				</p>
				<p>
					 ОЧЕНЬ ДАЖЕ ВОЗМОЖНО, этой консультацией мы поможем вам сэкономить очень приличную сумму и получить гораздо больше плюсов. За спрос денег не берем.
				</p>
				<div class="offers-list__item-actions">
					<div class="btn-cred-link onEventOrder" data-form="potreb" data-target="yacredit1">Оставить заявку</div>
				</div>
			</div>
		</div>
	</div>
</div>

 <?$APPLICATION->IncludeComponent(
	"vegas:orderForm", 
	".default", 
	array(
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"COMPONENT_TEMPLATE" => ".default",
		"EMAIL_TEMPLATE" => "54",
		"REQUIRED_FIELDS" => "order_email",
		"WEB_FORM_ID" => "ORDER_FORM",
		"LEAD_CODE" => array("avtokreditovanie","potreb")
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>