<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Статьи об автомобилях");

\Bitrix\Main\Page\Asset::getInstance()->addCss(ASSETS_PATH . '/css/blog.css');
?><style>
.page__main-wrpSearch._search._js-search {
    display: none;
}
section.page__header._fix._js-header._buy-auto._visible{display:none !important;}
</style>

<?$APPLICATION->IncludeComponent(
	"bitrix:news.detail", 
	"blog-detail", 
	array(
		"COMPONENT_TEMPLATE" => "blog-detail",
		"IBLOCK_TYPE" => "content",
		"IBLOCK_ID" => "22",
		"ELEMENT_ID" => "",
		"ELEMENT_CODE" => $_REQUEST["ELEMENT_CODE"],
		"CHECK_DATES" => "Y",
		"FIELD_CODE" => array(
			0 => "TAGS",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "YOUTUBE",
			1 => "",
		),
		"IBLOCK_URL" => "",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"SET_TITLE" => "Y",
		"SET_CANONICAL_URL" => "N",
		"SET_BROWSER_TITLE" => "Y",
		"BROWSER_TITLE" => "-",
		"SET_META_KEYWORDS" => "Y",
		"META_KEYWORDS" => "-",
		"SET_META_DESCRIPTION" => "Y",
		"META_DESCRIPTION" => "-",
		"SET_LAST_MODIFIED" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_ELEMENT_CHAIN" => "Y",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"USE_PERMISSIONS" => "N",
		"STRICT_SECTION_CHECK" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"USE_SHARE" => "N",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Страница",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => ""
	),
	false
);?>
<?if(!empty($_REQUEST["ELEMENT_CODE"])){?>
    <?
    $APPLICATION->IncludeComponent(
        "my:catalog.review", 
        'blog',
        array(
            'ELEMENT_ID' => $_REQUEST["ELEMENT_CODE"],
            'CACHE_TIME' => 60 * 60 * 24 * 2, // 24*2 часа
            'CNT_ELEM' => 10
        )
    );
    ?>
<?}?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>