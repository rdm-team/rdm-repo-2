<!-- -->
<div style="float:left;width: 100%;margin: 10px 0 20px 0;border-bottom: solid 3px #000;padding-bottom: 20px;">

	<div style="float:left;width:25%;min-width:300px;font-size: 0;">
		<img style="width:100%" src="#PICTURE#" alt=""/>
	</div>
	<div style="float:left;width:50%;min-width:145px;font-family:'Arial';color:#000;padding:0 0 0 10px;">
	<div style="display:inline-block;margin:0 0 20px 0;width:100%;">
		<span style="float:left;width:50%;font-size:18px;font-weight:bold;">#NAME#</span>
		<span style="float:left;width:50%;text-align:right;font-weight:bold;">#PRICE#</span>
	</div>
	
	<div style="">
		<span style="display: inline-block;width: 100%;padding: 5px 0;">#YEAR#</span>
		<span style="display: inline-block;width: 100%;padding: 5px 0;">#ENGINE#</span>
		<span style="display: inline-block;width: 100%;padding: 5px 0;">#FUEL#</span>			
		<span style="display: inline-block;width: 100%;padding: 5px 0;">#TRANSMISSION#</span>
		<span style="display: inline-block;width: 100%;padding: 5px 0;">#DRIVE#</span>
		<span style="display: inline-block;width: 100%;padding: 5px 0;">#CAR_BODY#</span>
	</div>
	<div style=""><span style="">#MILEAGE#</span></div>

	</div>
	<div style="display:inline-block;width:100%;text-align: right;">	
	<a href="#PAGE_URL#" 
	style="display: inline-block;padding: 10px 0;margin-top:30px;font-family: 'Arial';color: #fff;font-size: 16px;background: #4790c6;padding: 10px 15px;text-decoration: none;"
	>#TEXT_BUTTON#</a>
	</div>
</div>
<!-- -->