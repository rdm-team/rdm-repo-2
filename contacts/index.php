<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
$APPLICATION->SetPageProperty("not_show_nav_chain", "Y");
$APPLICATION->SetPageProperty("wrapper_class", "contacts");
?>

<style>
#map{height:340px;}
</style>
<div class="offers-list__item">
	<h1>Контакты</h1>
	<div class="offers-list__item-info">

		<div class="offers-list__item-desc">
			<div class="offers-list__item-text">
				 Адрес: г. Новосибирск, ул. Фрунзе 61/2<br>
				<p>
					<table class="contacts-worktime">
						<tr>
							<td>Режим работы:&nbsp;&nbsp;</td>
							<td>Пн-Пт: 10:00-19:00 <span class="_nomobile">| Сб-Вс: 10:00-18:00</span></td>
						</tr>
						<tr>
							<td></td>
							<td><span class="_mobile">Сб-Вс: 10:00-18:00</span></td>
						</tr>
					</table>
				</p>
				<p>
					 E-mail:&nbsp;<noindex><a href="mailto:rdm@rdm-import.ru" target="_blank">rdm@rdm-import.ru</a></noindex>
				</p>
				<p>
					 Телефон:&nbsp;+7 (383) 359-61-59&nbsp; <br>
				</p>
				<p>
					 <?$APPLICATION->IncludeComponent("api:feedbackex", "uikit", Array(
		"API_FEX_FORM_ID" => "FORM1",	// ID формы
		"YATARGET" => "yacontact",	// Яндекс цель
		"BCC" => "",	// Скрытая копия
		"COLOR" => "default",	// Цвет темы
		"COMPONENT_TEMPLATE" => "uikit",
		"DATETIME" => "",	// Дата/Время
		"DIR_URL" => "",	// URL раздела
		"DISABLE_SEND_MAIL" => "N",	// Отключить отправку писем
		"DISPLAY_FIELDS" => array(	// Встроенные поля
			0 => "NAME",
			1 => "EMAIL",
			2 => "PHONE",
			3 => "MESSAGE",
		),
		"EMAIL_ERROR_MESS" => "Указанный E-mail некорректен",	// Cообщение о некорректном e-mail
		"EMAIL_TO" => "star4@rdm-import.ru",	// E-mail, на который будет отправлено письмо
		"FIELD_ERROR_MESS" => "#FIELD_NAME# обязательное",	// Шаблон сообщения об ошибке в поле
		"FIELD_NAME_POSITION" => "stacked",	// Позиция названия поля
		"FIELD_SIZE" => "default",	// Размер полей и кнопок
		"FORM_AUTOCOMPLETE" => "Y",	// Автокомплит значений полей формы
		"FORM_CLASS" => "",	// CSS-классы обертки формы
		"FORM_FIELD_WIDTH" => "",	// Ширина поля, %/px
		"FORM_LABEL_TEXT_ALIGN" => "left",	// Выравнивание текста в названии поля
		"FORM_LABEL_WIDTH" => "0",	// Ширина названия поля, %/px
		"FORM_SUBMIT_CLASS" => "uk-button uk-width-1-1",	// Класс для кнопки "Отправить"
		"FORM_SUBMIT_STYLE" => "",	// Стили для кнопки "Отправить"
		"FORM_SUBMIT_VALUE" => "Отправить",	// Текст для кнопки "Отправить"
		"FORM_TEXTAREA_ROWS" => "5",	// Высота текстовой области (textarea), число
		"FORM_TITLE" => "Обратная связь",	// Заголовок формы
		"FORM_TITLE_LEVEL" => "3",	// Уровень заголовка
		"FORM_WIDTH" => "90%",	// Ширина обертки формы, %/px
		"HIDE_ASTERISK" => "N",	// Убрать двоеточие и звездочки
		"HIDE_FIELD_NAME" => "Y",	// Скрывать названия полей  формы
		"MAIL_SEND_USER" => "N",	// Отправить копию письма посетителю
		"MAIL_SUBJECT_ADMIN" => "#SITE_NAME#: Сообщение из формы обратной связи",	// Тема сообщения для администратора
		"MAIL_SUBJECT_USER" => "#SITE_NAME#: Копия сообщения из формы обратной связи",	// Тема сообщения для посетителя
		"MODAL_BTN_CLASS" => "api_button",
		"MODAL_BTN_ID" => "",
		"MODAL_BTN_SPAN_CLASS" => "api_icon",
		"MODAL_BTN_TEXT" => "Обратная связь",
		"MODAL_FOOTER_TEXT" => "",
		"MODAL_HEADER_TEXT" => "Обратная связь",
		"MODAL_ID" => "#API_FEX_MODAL_FORM3",
		"OK_TEXT" => "Спасибо!",	// Сообщение, выводимое пользователю после отправки
		"OK_TEXT_AFTER" => "Мы обязательно ответим на Ваше сообщение",	// Текст под сообщением
		"PAGE_TITLE" => "",	// Заголовок страницы
		"PAGE_URL" => "",	// URL страницы
		"REPLACE_FIELD_FROM" => "Y",	// Заменять в письме поле "От" на e-mail посетителя
		"REQUIRED_FIELDS" => array(	// Обязательные поля
			0 => "NAME",
			1 => "PHONE",
		),
		"SCROLL_SPEED" => "1000",	// Скорость прокрутки страницы
		"THEME" => "gradient",	// Тема
		"TITLE_DISPLAY" => "N",	// Показывать заголовок  формы
		"USE_AUTOSIZE" => "Y",	// Включить автовысоту текстовых полей
		"USE_JQUERY" => "N",	// Включить jQuery-1.8.3 если что-то не работает
		"USE_MODAL" => "N",	// Выводить в модальном окне
		"USE_PLACEHOLDER" => "Y",	// Включить placeholder
		"USE_SCROLL" => "Y",	// Прокручивать страницу к форме
		"WRITE_MESS_DIV_STYLE" => "padding:10px;border-bottom:1px dashed #dadada;",	// Стили для <div> поля
		"WRITE_MESS_DIV_STYLE_NAME" => "font-weight:bold;",	// Стили для <div> названия поля
		"WRITE_MESS_DIV_STYLE_VALUE" => "",	// Стили для <div> значения поля
		"WRITE_MESS_FILDES_TABLE" => "N",	// Записывать поля в почтовый шаблон таблицей
		"WRITE_MESS_TABLE_STYLE" => "border-collapse: collapse; border-spacing: 0;",	// Стили для <table> всех полей
		"WRITE_MESS_TABLE_STYLE_NAME" => "max-width: 200px; color: #848484; vertical-align: middle; padding: 5px 30px 5px 0px; border-bottom: 1px solid #e0e0e0; border-top: 1px solid #e0e0e0;",	// Стили для <td> названия поля
		"WRITE_MESS_TABLE_STYLE_VALUE" => "vertical-align: middle; padding: 5px 30px 5px 0px; border-bottom: 1px solid #e0e0e0; border-top: 1px solid #e0e0e0;",	// Стили для <td> значения поля
	),
	false
);?>
				</p>
			</div>
		</div>

	</div>
	 <!-- asdasdasd -->
	<div class="offers-list__item-info">

		<div class="offers-list__item-desc">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4572.2932006539995!2d82.94990835169014!3d55.0406471802726!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x569793839b950fc4!2z0KDQlNCcLdCY0LzQv9C-0YDRgg!5e0!3m2!1sru!2sus!4v1509112405178" width="100%" height="540" frameborder="0" style="border:0" allowfullscreen></iframe>
	
		</div>
	</div>
	
</div>
<div><img src="/local/assets/img/_MG_6343-1_(3).jpg" style="width:100%; margin-top:25px;"></div>
 &nbsp;<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
