<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Поиск файлов по маске</title>
<style>
<!--
body { font-family: Arial; font-size: 12px; }
input, textarea, select { font-size: 11px; margin: 3px;}
h1 { font-size: 22px; }
td h2 { font-size: 16px; margin-left: 30px; }
td { font-size: 12px; }
td li{ margin: 7px; }
-->
</style>
</head>
<body>
<h1>Поиск файлов по маске</h1>
<form name="search_form" method="POST">
<table border="0" width="80%">
<tr>
<td style="width:125px;">Encoding:</td>
<td>
<select name="encoding">   
    <option value="utf-8"  <?if ($_POST['encoding']=='utf-8')  {?>selected<?}?>>utf-8</option>
    <option value="windows-1251" <?if ($_POST['encoding']=='windows-1251') {?>selected<?}?>>Windows-1251</option>
    <option value="koi8-r" <?if ($_POST['encoding']=='koi8-r') {?>selected<?}?>>koi8-r</option>
</select>
<input type="submit" name="go_encoding" value="Change"></td>
</tr>
</table>
</form>
<form name="search_form" method="POST">
<table border="0" width="80%">
<tr><td style="width:125px;">Маска:</td>
  <td><textarea name="text" cols="80" rows="5"><?echo $_POST['text'];?></textarea></td>
  <td rowspan="6" valign="top">
  <h2>Примечания</h2>
  <ul>
  <li><strong>Маска</strong> - искомое выражение. Работает только если вводить его построчно!</li>
  <li><strong>Замена</strong> - заменить или нет найденное выражение на введенное в поле "На что заменять".</li>
  <li><strong>На что заменять</strong> - если оставить пустым, то замена произойдет на пробел. Не работает с русскими символами. Данное поле не имеет смысла, когда переключатель "Замена" установлен как "Нет".</li>
  <li><strong>Расширения файлов</strong> - указывает, в каких типах файлов производить поиск. Расширения указываются через запятую.</li>
  <li><strong>Патч</strong> - указывает, от какой директории производить поиск. Если оставить пустым, то поиск будет производиться от корня сайта.</li>
  <li><strong>Если вы меняли кодировку</strong>, то в той же директории, где находится файл "file_finder.php", автоматически создался еще один файл "file_finder_new.php". Не забудьте позже удалить оба файла.</li>
  </ul>
  </td>
</tr>
<tr>
   <td>Замена:</td>
   <td>
      <select name="replace">
         <option selected value="0">Нет</option>
         <option value="1">Да</option>
      </select>
   </td>
   <td></td>
</tr>
<tr><td>На что заменять:</td>
  <td><textarea name="replace_text" cols="80" rows="5"><?echo $_POST['replace_text'];?></textarea></td>
  <td></td>
</tr>
<tr><td>Расширения файлов:</td>
<td><input name="extension" size="30" type="text" value="<?if($_POST['extension']!="")echo $_POST['extension'];else echo 'php,html,htm,js,shtml,tpl,css'?>"></td>
<td></td>
</tr>
<tr><td>Патч:</td>
<td><input name="path" size="50" type="text" value="<?echo $_POST['path'];?>"></td>
<td></td>
</tr>
<tr>
  <td></td><td><input type="submit" name= "go_search" value="Выполнить"></td>
  <td></td>
</tr>
</table>
</form>
<hr align="left" style="width:80%" />
<h2>Результаты</h2>
<ul>
<?php
/*Находит файлы содержащие текст
На крупных CMS  +  слабый хостинг, искать с корня не получается, так как работает долго  и не укладывается в timeout сервера
*/

function find_files($path, $text, $replace=0, $replace_text, $arr_extension) {
  $handle = opendir($path);
  while ( false !== ($file = readdir($handle)) ) {
    if ( ($file !== "..") ) {
	  if(is_file($path."/".$file) && ($file !== ".") && ($file!=='file_finder.php')){
		$arr = pathinfo($path."/".$file);		
		if(in_array($arr['extension'],$arr_extension))//Список расширений файлов которые будут проверяться
		{	
			$fpath = realpath($path."/".$file);
			$fcont=file_get_contents($fpath);			
			if(stripos($fcont, $text)!==false) { // найден искомый  текст						
                                $result++;
				echo "<li>".$path."/".$file."</li>\n";
				//замена искомого текста на что-то другое
				if($replace==1)
				{
				   if(file_put_contents($fpath, str_ireplace($text, $replace_text, $fcont))) echo "Замена произведена.";
				}
			}
		}
	  }
      if ( !is_file($path."/".$file) && ($file !== ".") )
      find_files($path . "/" . $file, $text, $replace, $replace_text, $arr_extension);
    }
  }
  closedir($handle);
  return $result;
}

function encoding()
{
   $default_encoding = 'utf-8';
   $src = file_get_contents("file_finder.php");
   $encoding_start = mb_detect_encoding($src);
   
   $encoding_end = $_POST['encoding'];
   $meta_encoding = '<meta http-equiv="Content-Type" content="text/html; charset='.$default_encoding.'" />';
   $meta_encoding_replace = '<meta http-equiv="Content-Type" content="text/html; charset='.$encoding_end.'" />';
   
   if ($encoding_start == '') { $encoding_start = 'utf-8'; }
   if ($encoding_end   == '') { $encoding_start = 'utf-8'; }
   
   $dst = iconv($encoding_start, $encoding_end, $src);
   if(stripos($src, $meta_encoding)) file_put_contents("file_finder_new.php", str_ireplace($meta_encoding, $meta_encoding_replace, $dst));
   //file_put_contents("file_finder_new.php", $dst);
   
   header('Refresh: 0; url=./file_finder_new.php');
}

$text = $_POST['text'];
$path = $_POST['path'];
$replace = $_POST['replace'];
$replace_text = $_POST['replace_text'];
$extension = trim($_POST['extension']);
$extension = explode(",", $extension);
$i=0;
$res=1;
$arr_extension = array();

while ($i != count($extension))
{
   array_push($arr_extension, $extension[$i]);
   $i++;
}

//заменять найденный текст или нет ? false - не заменять, true - заменять
//if ($replace == false) $replace=false;
if ($replace_text == '') $replace_text = ' ';

//Откуда искать
if ($path == '') $path = $_SERVER["QUERY_STRING"];

//что ищем ?
$text=<<<EOD
$text
EOD;

//По умолчанию будем  искать от корня сайта
if ( $path{0} != "/" )  $path = $_SERVER["DOCUMENT_ROOT"] .$path;
if ($_POST['go_search']) { $count_files = find_files($path, $text, $replace, $replace_text, $arr_extension); }
if ($_POST['go_encoding']) { encoding(); }
?>
</ul>
<?//if ($count_files == 0) echo "Ничего не найдено.";?>
</body>
</html>